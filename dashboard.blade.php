@extends('musrenbang.mobile.layout.layout2')


@section('content')
			

			<div class="top-header bg-blue">
				<div class="container">
					<a href="#" class="menu-mobile pull-left active"><i class="fa fa-bars"></i></a>
					<p class="pull-left">DASHBOARD</p>
					<a href="#" class="pull-right">
						<span class="badge badge-sm up bg-danger pull-right-xs">2</span>
						<i class="fa  fa-bell-o"></i>
					</a>
				</div>
			</div>

			
			

			<div class="content container bg-grey">
				
				
				

	            <div class="list-card box-shadow">
	            	<div class="heading">
	            		<i class="fa fa-pencil"></i>
	            		<p>RENJA</p>
	            	</div>
	            	<div class="list-content">
	            		
	            		<button type="submit" class="w-full m-b-md btn btn-green input-xxl">INPUT USULAN RENJA</button>
	            	</div>
	            </div>

	               <div class="list-card box-shadow">
	            	<div class="heading">
	            		<i class="fa fa-pencil"></i>
	            		<p>PIPPK</p>
	            	</div>
	            	<div class="list-content">
	            		
	            		<button type="submit" class="w-full m-b-md btn btn-green input-xxl">INPUT USULAN PIPPK</button>
	            	</div>
	            </div>
			    
			</div>
@endsection


@section('plugin')
<script>
      var oldMarker;
      function initMap() {
        var bandung = {lat: -6.914744, lng: 107.609810};
        var map = new google.maps.Map(document.getElementById('maps'), {
          zoom: 17,
          center: bandung,
          navigationControl: false,
	      mapTypeControl: false,
	      scaleControl: false,
	      draggable: false,
	      scrollwheel: false,
        });
                
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzixLw-QwjOpEiL8qmveEmafmgM27mgnk&callback=initMap">
    </script>
@endsection      