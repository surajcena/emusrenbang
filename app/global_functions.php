<?php 
use App\Model\Action_log;

	function isMobile() {
        return app()->make('UserAgent')->isMobile() && !app()->make('UserAgent')->isTablet();
    }

    function action_log($aksi, $entity_id=null, $keterangan=''){
    	$action_log = new Action_log;

    	$action_log->id_user 	= Auth::user()->id;
    	$action_log->aksi 		= $aksi;
    	$action_log->entity_id 	= $entity_id;
    	$action_log->keterangan = $keterangan;
    	$action_log->waktu 		= time();

        $action_log->save();

    }

    