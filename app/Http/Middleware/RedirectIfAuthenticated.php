<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            action_log('login');
            /*if(isMobile()){
               // dd();
                return '/musrenbang/2017/mobile/dashboard';
            }*/
            return response('/musrenbang/2017');
            
        }

        return $next($request);
    }
}
