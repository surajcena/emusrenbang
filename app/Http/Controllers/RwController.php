<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\RW;
use App\Model\RT;
use App\Model\kelurahan;
use App\Model\kecamatan;

class RwController extends Controller
{
	public function update(Request $request){
		$this->validate($request,[
	        'rwid' => 'required',
	        'ketuarw' => 'required',
	        'nik' => 'required',
	        'notelepon' => 'required'
	    ]);

	    $rw = RW::find($request->rwid);
	    $rw->RW_KETUA = $request->ketuarw;
	    $rw->RW_NIK = $request->nik;
	    $rw->RW_TELP = $request->notelepon;
	    $rw->update();

	    return back();
	}

	public function updateLurah(Request $request){
		$this->validate($request,[
	        'lurahid' => 'required',
	        'lurah' => 'required',
	        'nip' => 'required',
	        'notelepon' => 'required'
	    ]);

	    $lurah = kelurahan::find($request->lurahid);
	    $lurah->KEL_LURAH = $request->lurah;
	    $lurah->KEL_NIP = $request->nip;
	    $lurah->KEL_TELP = $request->notelepon;
	    $lurah->update();

	    return back()->with('message_title','Success')->with('message','Sukses Mengedit Profil Kelurahan');  
	}

	public function updateCamat(Request $request){
		$this->validate($request,[
	        'camatid' => 'required',
	        'camat' => 'required',
	        'nip' => 'required',
	        'notelepon' => 'required'
	    ]);

	    $camat = kecamatan::find($request->camatid);
	    $camat->KEC_CAMAT = $request->camat;
	    $camat->KEC_NIP = $request->nip;
	    $camat->KEC_TELP = $request->notelepon;
	    $camat->update();

	    return back()->with('message_title','Success')->with('message','Sukses Mengedit Profil Kecamatan');  
	}

	public function tambahrt(Request $request)
	{
		$this->validate($request,[
	        'rwid' => 'required',
	        'namart'=> 'required',
	        'ketuart' => 'required',
	        'nik' => 'required',
	        'notelepon' => 'required'
	    ]);

		$rw = RW::find($request->rwid);

		$rt = new RT();
		$rt->RT_NAMA = $request->namart;
		$rt->RT_KETUA = $request->ketuart;
		$rt->RT_NIK = $request->nik;
		$rt->RT_TELP = $request->notelepon;
		$rt->RT_TAHUN = date('Y');
		$rt->RW_ID = $request->rwid;

		$rt->save();

		//$rw->rt()->save($rt);

		return back();
	}

	
}
