<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
//use Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Usulan;
use App\Model\Usulan_Rekap_Apbd;
use App\Model\Kecamatan;
use App\Model\Kelurahan;
use App\Model\RW;
use App\Model\Isu;
use App\Model\Kamus;
use App\Model\RT;
use App\Model\Usulan_Rekap;
use App\Model\BeritaAcara;
use App\Model\BeritaAcaraFoto;
use App\Model\SKPD;
use App\Model\Likes;
use Response;
use DB;
use View;
use App;
use Session;
use Excel;
use Illuminate\Support\Facades\Redis;
use Log;

class apiController extends Controller{

    public function pesanerror(){
        return View('musrenbang.error');
    }

    public function apiMusrenbang($tahun, Request $req){
        $tablekamus = "REFERENSI.REF_KAMUS";
        $tableisu = "REFERENSI.REF_ISU";
        $tableusulan = "MUSRENBANG.DAT_USULAN";
        $tableuser = "DATA.users";
        $tableskpd = "REFERENSI.REF_SKPD";
        $tablekecamatan = "REFERENSI.REF_KECAMATAN";
        $tablekelurahan = "REFERENSI.REF_KELURAHAN";

        $usulans = Usulan::where("USULAN_TAHUN",$tahun)->where("USULAN_DELETED",0)
        ->where("USULAN_TUJUAN",1)->where("USULAN_POSISI",3)->where("USULAN_STATUS",0)
        ->join($tablekamus,$tablekamus.".KAMUS_ID",$tableusulan.".KAMUS_ID")
        ->join($tableuser,$tableuser.".id",$tableusulan.".USER_CREATED")
        ->join($tablekelurahan,$tablekelurahan.".KEL_ID",$tableuser.".KEL_ID")
        ->join($tablekecamatan,$tablekecamatan.".KEC_ID",$tableuser.".KEC_ID")
        ->join($tableskpd,$tableskpd.".SKPD_ID",$tablekamus.".KAMUS_SKPD")
        ->orderby("SKPD_KODE","SKPD_NAMA","KAMUS_NAMA");
        $tahun = ($req->tahun == "") ? "" : $req->tahun;
        if(strlen($tahun)>0){
            $usulans = $usulans->whereYear('TIME_CREATED',$tahun);
        }
        $bulan = ($req->bulan == "") ? "" : $req->bulan;
        if(strlen($bulan)>0){
            $usulans = $usulans->whereMonth('TIME_CREATED',$bulan);
        }
        $tanggal = ($req->tanggal == "") ? "" : $req->tanggal;
        if(strlen($tanggal)>0){
            $usulans = $usulans->whereDay('TIME_CREATED',$tanggal);
        }
        $created_at = ($req->created_at == "") ? "" : Carbon::parse($req->created_at)->format('Y-m-d');
        if(strlen($created_at)>0){
            $usulans = $usulans->whereDate('TIME_CREATED',$created_at);
        }
        $usulans = $usulans->get();
        $view           = array();
        foreach ($usulans as $data) {
            array_push($view, array( 'SKPD_KODE'        => $data->SKPD_KODE,
                                     'SKPD_NAMA'        => $data->SKPD_NAMA,
                                     'KAMUS_NAMA'        => $data->KAMUS_NAMA,
                                     'USULAN_VOLUME'        => $data->USULAN_VOLUME,
                                     'KAMUS_HARGA'        => $data->KAMUS_HARGA,
                                     'TOTAL'        => $data->USULAN_VOLUME*$data->KAMUS_HARGA,
                                     'KAMUS_SATUAN'        => $data->KAMUS_SATUAN,
                                     'ALAMAT'        => $data->ALAMAT,
                                     'USULAN_URGENSI'        => $data->USULAN_URGENSI,
                                     'PENGUSUL'        => $data->name,
                                     'KELURAHAN'        => $data->KEL_NAMA,
                                     'KECAMATAN'        => $data->KEC_NAMA,
                                     'USULAN_ALASAN'        => $data->USULAN_ALASAN,
                                     'KET_ACC'        => $data->KET_ACC,
                                     'USULAN_CATATANKEL'        => $data->USULAN_CATATANKEL,
                                     'CREATED_AT'               => $data->TIME_CREATED
                                     ));
        }
            $out = array("aaData"=>$view);
            return Response::JSON($out);
    }

    public function apiMus($tahun, Request $req){
        $tablekamus = "REFERENSI.REF_KAMUS";
        $tablekegiatan = "REFERENSI.REF_KEGIATAN";
        $tableisu = "REFERENSI.REF_ISU";
        $tableusulan = "MUSRENBANG.DAT_USULAN";
        $tableuser = "DATA.users";
        $tableskpd = "REFERENSI.REF_SKPD";
        $tablekecamatan = "REFERENSI.REF_KECAMATAN";
        $tablekelurahan = "REFERENSI.REF_KELURAHAN";

        $start = ($req->iDisplayStart == "")? 0 : $req->iDisplayStart;
        $length = ($req->iDisplayLength == "")? 1000 : $req->iDisplayLength;
        $view           = array();
        $data =  Usulan::where("USULAN_TAHUN",$tahun)->where("USULAN_DELETED",0)
        ->where("USULAN_TUJUAN",1)->where("USULAN_POSISI",'>=',3)->where("USULAN_STATUS",2)
        ->join($tablekamus,$tablekamus.".KAMUS_ID",$tableusulan.".KAMUS_ID")
        ->join($tablekegiatan,$tablekegiatan.".KEGIATAN_ID",$tablekamus.".KEGIATAN_ID")
        ->join($tableskpd,$tableskpd.".SKPD_ID",$tablekamus.".KAMUS_SKPD")
        ->selectRaw('"USULAN_TAHUN","KAMUS_NAMA","USULAN_VOLUME","KAMUS_HARGA","USULAN_VOLUME"*"KAMUS_HARGA" as "TOTAL",
        "KAMUS_SATUAN","USULAN_URGENSI","USULAN_LAT","USULAN_LONG","ALAMAT","USULAN_CATATANSKPD","KEGIATAN_NAMA","SKPD_NAMA"')
        ->orderby("SKPD_KODE","SKPD_NAMA","KAMUS_NAMA");
        /*
        ->join($tableuser,$tableuser.".id",$tableusulan.".USER_CREATED")
        ->join($tablekelurahan,$tablekelurahan.".KEL_ID",$tableuser.".KEL_ID")
        ->join($tablekecamatan,$tablekecamatan.".KEC_ID",$tableuser.".KEC_ID")
        */
        if($length>0){
            $data = $data->limit($length)->offset($start)->get();
        } else {
            $data = $data->get();
        }
        foreach ($data as $data) {
            array_push($view, array( 
                'USULAN_TAHUN'                => $data->USULAN_TAHUN,
                'KAMUS_NAMA'               => $data->KAMUS_NAMA,
                'USULAN_VOLUME'            => $data->USULAN_VOLUME,
                'KAMUS_HARGA'              => $data->KAMUS_HARGA,
                'TOTAL'                    => $data->TOTAL,
                'KAMUS_SATUAN'             => $data->KAMUS_SATUAN,
                'USULAN_URGENSI'           => $data->USULAN_URGENSI,
                'USULAN_LAT'           => $data->USULAN_LAT,
                'USULAN_LONG'           => $data->USULAN_LONG,
                'ALAMAT'                   => $data->ALAMAT,
                'USULAN_CATATANSKPD'        => $data->USULAN_CATATANSKPD,
                'KEGIATAN_NAMA'        => $data->KEGIATAN_NAMA,
                'SKPD_NAMA'                 => $data->SKPD_NAMA                        
            ));
        }
        return Response::JSON($view);
    }

    public function apiPIPPK($tahun, Request $req){
        $tablekamus = "REFERENSI.REF_KAMUS";
        $tablekegiatan = "REFERENSI.REF_KEGIATAN";
        $tableisu = "REFERENSI.REF_ISU";
        $tableusulan = "MUSRENBANG.DAT_USULAN";
        $tableuser = "DATA.users";
        $tableskpd = "REFERENSI.REF_SKPD";
        $tablekecamatan = "REFERENSI.REF_KECAMATAN";
        $tablekelurahan = "REFERENSI.REF_KELURAHAN";

        $start = ($req->iDisplayStart == "")? 0 : $req->iDisplayStart;
        $length = ($req->iDisplayLength == "")? 1000 : $req->iDisplayLength;
        $view           = array();
        $data =  Usulan::where("USULAN_TAHUN",$tahun)->where("USULAN_DELETED",0)
        ->where("USULAN_TUJUAN",2)->where("USULAN_POSISI",'>=',2)->where("USULAN_STATUS",2)
        ->join($tablekamus,$tablekamus.".KAMUS_ID",$tableusulan.".KAMUS_ID")
        ->join($tablekegiatan,$tablekegiatan.".KEGIATAN_ID",$tablekamus.".KEGIATAN_ID")
        ->join($tableskpd,$tableskpd.".SKPD_ID",$tablekamus.".KAMUS_SKPD")
        ->selectRaw('"USULAN_TAHUN","KAMUS_NAMA","USULAN_VOLUME","KAMUS_HARGA","USULAN_VOLUME"*"KAMUS_HARGA" as "TOTAL",
        "KAMUS_SATUAN","USULAN_URGENSI","USULAN_LAT","USULAN_LONG","ALAMAT","USULAN_CATATANSKPD","KEGIATAN_NAMA","SKPD_NAMA"')
        ->orderby("SKPD_KODE","SKPD_NAMA","KAMUS_NAMA");
        /*
        ->join($tableuser,$tableuser.".id",$tableusulan.".USER_CREATED")
        ->join($tablekelurahan,$tablekelurahan.".KEL_ID",$tableuser.".KEL_ID")
        ->join($tablekecamatan,$tablekecamatan.".KEC_ID",$tableuser.".KEC_ID")
        */
        if($length>0){
            $data = $data->limit($length)->offset($start)->get();
        } else {
            $data = $data->get();
        }
        foreach ($data as $data) {
            array_push($view, array( 
                'USULAN_TAHUN'                => $data->USULAN_TAHUN,
                'KAMUS_NAMA'               => $data->KAMUS_NAMA,
                'USULAN_VOLUME'            => $data->USULAN_VOLUME,
                'KAMUS_HARGA'              => $data->KAMUS_HARGA,
                'TOTAL'                    => $data->TOTAL,
                'KAMUS_SATUAN'             => $data->KAMUS_SATUAN,
                'USULAN_URGENSI'           => $data->USULAN_URGENSI,
                'USULAN_LAT'           => $data->USULAN_LAT,
                'USULAN_LONG'           => $data->USULAN_LONG,
                'ALAMAT'                   => $data->ALAMAT,
                'USULAN_CATATANSKPD'        => $data->USULAN_CATATANSKPD,
                'KEGIATAN_NAMA'        => $data->KEGIATAN_NAMA,
                'SKPD_NAMA'                 => $data->SKPD_NAMA                        
            ));
        }
        return Response::JSON($view);
    }

    public function apiMusTolak($tahun, Request $req){
        $tablekamus = "REFERENSI.REF_KAMUS";
        $tablekegiatan = "REFERENSI.REF_KEGIATAN";
        $tableisu = "REFERENSI.REF_ISU";
        $tableusulan = "MUSRENBANG.DAT_USULAN";
        $tableuser = "DATA.users";
        $tableskpd = "REFERENSI.REF_SKPD";
        $tablekecamatan = "REFERENSI.REF_KECAMATAN";
        $tablekelurahan = "REFERENSI.REF_KELURAHAN";

        $start = ($req->iDisplayStart == "")? 0 : $req->iDisplayStart;
        $length = ($req->iDisplayLength == "")? 1000 : $req->iDisplayLength;
        $view           = array();
        $data =  Usulan::where("USULAN_TAHUN",$tahun)->where("USULAN_DELETED",0)
        ->where("USULAN_TUJUAN",1)->where("USULAN_POSISI",'>=',3)->where("USULAN_STATUS",0)
        ->join($tablekamus,$tablekamus.".KAMUS_ID",$tableusulan.".KAMUS_ID")
        ->join($tablekegiatan,$tablekegiatan.".KEGIATAN_ID",$tablekamus.".KEGIATAN_ID")
        ->join($tableskpd,$tableskpd.".SKPD_ID",$tablekamus.".KAMUS_SKPD")
        ->selectRaw('"USULAN_TAHUN","KAMUS_NAMA","USULAN_VOLUME","KAMUS_HARGA","USULAN_VOLUME"*"KAMUS_HARGA" as "TOTAL",
        "KAMUS_SATUAN","USULAN_URGENSI","USULAN_LAT","USULAN_LONG","ALAMAT","USULAN_CATATANSKPD","KEGIATAN_NAMA","SKPD_NAMA"')
        ->orderby("SKPD_KODE","SKPD_NAMA","KAMUS_NAMA");
        /*
        ->join($tableuser,$tableuser.".id",$tableusulan.".USER_CREATED")
        ->join($tablekelurahan,$tablekelurahan.".KEL_ID",$tableuser.".KEL_ID")
        ->join($tablekecamatan,$tablekecamatan.".KEC_ID",$tableuser.".KEC_ID")
        */
        if($length>0){
            $data = $data->limit($length)->offset($start)->get();
        } else {
            $data = $data->get();
        }
        foreach ($data as $data) {
            array_push($view, array( 
                'USULAN_TAHUN'                => $data->USULAN_TAHUN,
                'KAMUS_NAMA'               => $data->KAMUS_NAMA,
                'USULAN_VOLUME'            => $data->USULAN_VOLUME,
                'KAMUS_HARGA'              => $data->KAMUS_HARGA,
                'TOTAL'                    => $data->TOTAL,
                'KAMUS_SATUAN'             => $data->KAMUS_SATUAN,
                'USULAN_URGENSI'           => $data->USULAN_URGENSI,
                'USULAN_LAT'           => $data->USULAN_LAT,
                'USULAN_LONG'           => $data->USULAN_LONG,
                'ALAMAT'                   => $data->ALAMAT,
                'USULAN_CATATANSKPD'        => $data->USULAN_CATATANSKPD,
                'KEGIATAN_NAMA'        => $data->KEGIATAN_NAMA,
                'SKPD_NAMA'                 => $data->SKPD_NAMA                        
            ));
        }
        return Response::JSON($view);
    }

    public function apiPIPPKTolak($tahun, Request $req){
        $tablekamus = "REFERENSI.REF_KAMUS";
        $tablekegiatan = "REFERENSI.REF_KEGIATAN";
        $tableisu = "REFERENSI.REF_ISU";
        $tableusulan = "MUSRENBANG.DAT_USULAN";
        $tableuser = "DATA.users";
        $tableskpd = "REFERENSI.REF_SKPD";
        $tablekecamatan = "REFERENSI.REF_KECAMATAN";
        $tablekelurahan = "REFERENSI.REF_KELURAHAN";

        $start = ($req->iDisplayStart == "")? 0 : $req->iDisplayStart;
        $length = ($req->iDisplayLength == "")? 1000 : $req->iDisplayLength;
        $view           = array();
        $data =  Usulan::where("USULAN_TAHUN",$tahun)->where("USULAN_DELETED",0)
        ->where("USULAN_TUJUAN",2)->where("USULAN_POSISI",'>=',2)->where("USULAN_STATUS",0)
        ->join($tablekamus,$tablekamus.".KAMUS_ID",$tableusulan.".KAMUS_ID")
        ->join($tablekegiatan,$tablekegiatan.".KEGIATAN_ID",$tablekamus.".KEGIATAN_ID")
        ->join($tableskpd,$tableskpd.".SKPD_ID",$tablekamus.".KAMUS_SKPD")
        ->selectRaw('"USULAN_TAHUN","KAMUS_NAMA","USULAN_VOLUME","KAMUS_HARGA","USULAN_VOLUME"*"KAMUS_HARGA" as "TOTAL",
        "KAMUS_SATUAN","USULAN_URGENSI","USULAN_LAT","USULAN_LONG","ALAMAT","USULAN_CATATANSKPD","KEGIATAN_NAMA","SKPD_NAMA"')
        ->orderby("SKPD_KODE","SKPD_NAMA","KAMUS_NAMA");
        /*
        ->join($tableuser,$tableuser.".id",$tableusulan.".USER_CREATED")
        ->join($tablekelurahan,$tablekelurahan.".KEL_ID",$tableuser.".KEL_ID")
        ->join($tablekecamatan,$tablekecamatan.".KEC_ID",$tableuser.".KEC_ID")
        */
        if($length>0){
            $data = $data->limit($length)->offset($start)->get();
        } else {
            $data = $data->get();
        }
        foreach ($data as $data) {
            array_push($view, array( 
                'USULAN_TAHUN'                => $data->USULAN_TAHUN,
                'KAMUS_NAMA'               => $data->KAMUS_NAMA,
                'USULAN_VOLUME'            => $data->USULAN_VOLUME,
                'KAMUS_HARGA'              => $data->KAMUS_HARGA,
                'TOTAL'                    => $data->TOTAL,
                'KAMUS_SATUAN'             => $data->KAMUS_SATUAN,
                'USULAN_URGENSI'           => $data->USULAN_URGENSI,
                'USULAN_LAT'           => $data->USULAN_LAT,
                'USULAN_LONG'           => $data->USULAN_LONG,
                'ALAMAT'                   => $data->ALAMAT,
                'USULAN_CATATANSKPD'        => $data->USULAN_CATATANSKPD,
                'KEGIATAN_NAMA'        => $data->KEGIATAN_NAMA,
                'SKPD_NAMA'                 => $data->SKPD_NAMA                        
            ));
        }
        return Response::JSON($view);
    }

}