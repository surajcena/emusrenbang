<?php

namespace App\Http\Controllers\Budgeting\Referensi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SKPD;
use View;
use Carbon;
use Response;
use Illuminate\Support\Facades\Input;
class skpdController extends Controller
{
    public function index($tahun,$status){
    	return View('budgeting.referensi.skpd',['tahun'=>$tahun,'status'=>$status]);
    }

    public function getData($tahun){
    	$data 			= SKPD::where('SKPD_TAHUN',$tahun)->orderBy('SKPD_KODE')->get();
    	$no 			= 1;
    	$aksi 			= '';
    	$view 			= array();
    	foreach ($data as $data) {
    		$aksi 		= '<div class="action visible pull-right"><a onclick="return ubah(\''.$data->SKPD_ID.'\')" class="action-edit"><i class="mi-edit"></i></a><a onclick="return hapus(\''.$data->SKPD_ID.'\')" class="action-delete"><i class="mi-trash"></i></a></div>';
    		array_push($view, array( 'no'				=>$no,
                                     'SKPD_KODE'  		=>$data->SKPD_KODE,
                                     'SKPD_NAMA'		=>$data->SKPD_NAMA,
                                     'SKPD_KEPALA'		=>$data->SKPD_KEPALA_NIP."<br><p class='text-orange'>".$data->SKPD_KEPALA."</p>",
                                     'SKPD_BENDAHARA'	=>$data->SKPD_BENDAHARA_NIP."<br><p class='text-orange'>".$data->SKPD_BENDAHARA."</p>",
                                     'aksi'				=>$aksi));
    		$no++;
    	}
		$out = array("aaData"=>$view);    	
    	return Response::JSON($out);
    }

    public function getDetail($tahun,$status,$id){
    	$data 			= SKPD::where('SKPD_ID',$id)->get();
    	return $data;
    }

    public function submitAdd(){
    	$skpd 		= new SKPD;
    	$cekKode 	= SKPD::where('SKPD_KODE',Input::get('kode_skpd'))
    						->where('SKPD_TAHUN',Input::get('tahun'))
    						->value('SKPD_KODE');
    	if(empty($cekKode)){
	    	$skpd->SKPD_KODE			= Input::get('kode_skpd');
	    	$skpd->SKPD_NAMA			= Input::get('nama_skpd');
	    	$skpd->SKPD_KEPALA_NIP		= Input::get('kepala_nip');
	    	$skpd->SKPD_KEPALA 			= Input::get('kepala_skpd');
	    	$skpd->SKPD_BENDAHARA_NIP 	= Input::get('bendahara_nip');
	    	$skpd->SKPD_BENDAHARA 	 	= Input::get('bendahara_skpd');
	    	$skpd->SKPD_TAHUN 		 	= Input::get('tahun');
	    	$skpd->save();
	    	return '1';
    	}else{
	    	return '0';
    	}
    }

    public function submitEdit(){
    	$cekKode 	= SKPD::where('SKPD_KODE',Input::get('kode_skpd'))
    						->where('SKPD_TAHUN',Input::get('tahun'))
    						->value('SKPD_KODE');
    	if(empty($cekKode) || $cekKode == Input::get('kode_skpd')){    						
    	SKPD::where('SKPD_ID',Input::get('id_skpd'))
    			->update(['SKPD_KODE'			=>Input::get('kode_skpd'),
    					  'SKPD_NAMA'  			=>Input::get('nama_skpd'),
    					  'SKPD_KEPALA_NIP' 	=>Input::get('kepala_nip'),
    					  'SKPD_KEPALA' 		=>Input::get('kepala_skpd'),
    					  'SKPD_BENDAHARA_NIP' 	=>Input::get('bendahara_nip'),
    					  'SKPD_BENDAHARA' 		=>Input::get('bendahara_skpd'),
    					  'SKPD_TAHUN'			=>Input::get('tahun')]);
    		return '1';
    	}else{
	    	return '0';
    	}
    }

    public function delete(){
    	SKPD::where('SKPD_ID',Input::get('id_skpd'))->delete();
    	return 'Berhasil dihapus!';
    }
}
