<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use DB;
use Excel;
use App\Model\Isu;
use App\Model\Kamus;
use App\Model\Kecamatan;
use App\Model\Kelurahan;
use App\Model\RT;
use App\Model\RW;
use App\Model\Usulan;



class ExcelController extends Controller
{
    public function getExport(){
    	$usulan = Usulan::where('RW_ID',1);
    	Excel::create('Export Data', function($excel) use($usulan){
    		$excel->sheet('Sheet 1', function($sheet) use($usulan){
    			$sheet->fromArray($usulan);
    		});
    	})->export('xls');
    }
}
