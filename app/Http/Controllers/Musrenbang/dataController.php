<?php

namespace App\Http\Controllers\Musrenbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Usulan;
use App\Model\Kecamatan;
use App\Model\Kelurahan;
use App\Model\User;
use App\Model\Perencanaan;
use App\Model\Kegiatan;
use App\Model\Program;
use App\Model\Isu;
use App\Model\Kamus;
use Auth;
use Response;
use Log;
use View;

class dataController extends Controller
{
	public function __construct(){
        $this->middleware('auth');
    }

    private function toDataTableUsulanFormat($usulans, $tahun)
    {
    	$jum = 0;
    	$response = array();
    	$no=0;
    	$pagu=0;
		foreach ($usulans as $key => $usulan) {
			$responseelement 				= array();

			$pagu       					= $usulan->kamus->KAMUS_HARGA * $usulan->USULAN_VOLUME;

			//KEGIATAN
			if(empty($usulan->kamus->kegiatan->KEGIATAN_ID))
				$kegiatan 				    = '-';
			else
				$kegiatan 					= $usulan->kamus->kegiatan->program->PROGRAM_NAMA."<br><font style='color:orange;'>".$usulan->kamus->kegiatan->KEGIATAN_NAMA."</font>";

			$volume 					= $usulan->USULAN_VOLUME." ".$usulan->kamus->KAMUS_SATUAN."<br><font style='color:orange;'>".number_format($usulan->kamus->KAMUS_HARGA,0,'.',',')."</font>";

			$pengusul 						= $usulan->user->name;


			$responseelement["usulanid"] 	= $usulan->USULAN_ID;
			$responseelement["no"] 			= $no+1;
			$responseelement["skpd"] 		= $usulan->kamus->getSKPDName();
			$responseelement["kegiatan"] 	= $kegiatan;
			$responseelement["isukamus"] 	= $usulan->getHTMLIsuKamus();
			$responseelement["volume"] 		= $volume;
			$responseelement["total"] 		= "Rp. ".number_format($usulan->kamus->KAMUS_HARGA * $usulan->USULAN_VOLUME,0,'.',',');
			$responseelement["namapengusul"]= $pengusul;
			$responseelement["status"] 		= $usulan->getHTMLIconStatus();



			$responseelement["idpengusul"] 	= $usulan->user->email;

			if  ($usulan->isOnProcess(Auth::user()->level - 4) || $usulan->USULAN_STATUS==0)
				$responseelement["prioritas"] = $usulan->USULAN_PRIORITAS;
			else
				$responseelement["prioritas"] = "<a href='#' onclick='changePriority(".$usulan->USULAN_ID.",".$usulan->user->KEL_ID."); return false;'><span id='usulan-".$usulan->USULAN_ID."'>".$usulan->USULAN_PRIORITAS."</span></a>";
			$responseelement["keterangan"] 	= $usulan->getStringKeteranganAcc();
			$responseelement["skpd"] 		= $usulan->kamus->getSKPDName();
			$responseelement["urgensi"] 	= $usulan->USULAN_URGENSI;
			$responseelement["alamat"] 		= $usulan->ALAMAT;

			//dd($responseelement["kegiatan"]);

			$url = url('/musrenbang/'.$usulan->USULAN_TAHUN.'/usulan/editAcc/'.$usulan->USULAN_ID);

            if((($usulan->USULAN_STATUS == 1 && $usulan->USULAN_POSISI==1) || ($usulan->USULAN_STATUS == 2 && $usulan->USULAN_POSISI==1)
            	|| ($usulan->USULAN_STATUS == 2 && $usulan->USULAN_POSISI==2)|| ($usulan->USULAN_STATUS == 2 && $usulan->USULAN_POSISI==3)
            	|| ($usulan->USULAN_STATUS == 2 && $usulan->USULAN_POSISI==4)|| ($usulan->USULAN_STATUS == 2 && $usulan->USULAN_POSISI==5)
            	|| ($usulan->USULAN_STATUS == 2 && $usulan->USULAN_POSISI==6)) && (Auth::user()->active==1))
            {
            	$url = url('/musrenbang/'.$usulan->USULAN_TAHUN.'/usulan/editAcc/'.$usulan->USULAN_ID);
            }

            $perencanaan = Perencanaan::where('PERENCANAAN_TAHUN', $tahun)->first();

            if(Auth::user()->active==1 && $perencanaan->PERENCANAAN_AKTIF==1){

            $responseelement["aksi"]         = '<div class="action visible pull-right">';
            $responseelement["aksi"]		.= '<a href="'.$url.'" class="action-preveiw"><i class="mi-eye"></i></a>';

        	}else{
        		$responseelement["aksi"]    = '';
        	}

        	$responseelement["aksi"] 		.= '<a href="'.url('/musrenbang/usulan/history/'.$usulan->USULAN_ID).'" target="_blank" class="action-history"><i class="fa fa-book"></i></a>';
            $responseelement["aksi"]		.= '</div>';

			if(($usulan->USER_CREATED == Auth::user()->id) && (Auth::user()->active==1))
			{
				$responseelement["aksi"]  = '<div class="action visible pull-right">';
				$responseelement["aksi"] .= '<a href="'.url('/musrenbang/'.$usulan->USULAN_TAHUN.'/usulan/editAcc/'.$usulan->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>';
				$responseelement["aksi"] .= '<a href="'.url('/musrenbang/'.$usulan->USULAN_TAHUN.'/usulan/edit/'.$usulan->USULAN_ID).'" class="action-edit"><i class="mi-edit"></i></a>';
				$responseelement["aksi"] .= '<a onclick="return hapus(\''.$usulan->USULAN_ID.'\')" class="action-delete"><i class="mi-trash"></i></a>';
				$responseelement["aksi"] .= '</div>';
			}

			array_push($response,$responseelement);
			$jum += $pagu;
		}
		$out = array("aaData"=>$response,"jumlah"=>number_format($jum,0,'.',','));

		return $out;
    }

	public function dataUsulan($tahun,$jenis,$status,$pengusul = "x" ,$isu="x",$kamus="x",$kecamatan="x")
	{
		//REDIS HABIBIE

	$level = [1,2,3,4,5,6];
		$level = [1,2,3,4,5,6];
        $ids = User::whereIn('level',$level)->pluck('id')->all();

        if(Auth::user()->level==5)
        {
        	$ids = Auth::user()->kelurahan->user->whereIn('level',$level)->pluck('id')->all();
        }
        else if(Auth::user()->level==6)
        {
            $ids = Auth::user()->kecamatan->user->whereIn('level',$level)->pluck('id')->all();
        }

		$usulans = Usulan::where("USULAN_TAHUN",$tahun)
					->where('USULAN_DELETED',0)
					->where('USULAN_TUJUAN',$jenis)
					->wherein('USER_CREATED',$ids)
					->with([
						"user",
						"kamus",
						"kamus.isu",
						"kamus.skpd",
					]);

		if(Auth::user()->level == 5)
		{

			if($status == 1)
			{
				$usulans = $usulans->where('USULAN_STATUS',1);

			}
			else if($status == 2)
			{
				$usulans = $usulans->where('USULAN_STATUS',2);
			}
			else if($status == 0)
			{
				$usulans = $usulans->where('USULAN_STATUS',0);
			}
		}

		else if(Auth::user()->level == 6)
		{

			if($status == 1)
			{
				$usulans = $usulans->where('USULAN_STATUS',2)->where('USULAN_POSISI',1);
			}
			else if($status == 2)
			{
				$usulans = $usulans->where('USULAN_STATUS',2)->where('USULAN_POSISI',">=",2);
			}
			else if($status == 0)
			{
				$usulans = $usulans->where('USULAN_STATUS',0)->where('USULAN_POSISI',">=",2);
			}
		}

		else if(Auth::user()->level == 7 )
		{
			if($status == 1)
			{
				$usulans = $usulans->where('USULAN_STATUS',2)->where('USULAN_POSISI',2);
			}
			else if($status == 2)
			{
				$usulans = $usulans->where('USULAN_STATUS',2)->where('USULAN_POSISI',">=",3);
			}
			else if($status == 0)
			{
				$usulans = $usulans->where('USULAN_STATUS',0)->where('USULAN_POSISI',">=",3);
			}
		}
		else if(Auth::user()->level == 10 )
		{
			if($status == 1)
			{
				$usulans = $usulans->where('USULAN_STATUS',2)->where('USULAN_POSISI',3);
			}
			else if($status == 2)
			{
				$usulans = $usulans->where('USULAN_STATUS',2)->where('USULAN_POSISI',">=",4);
			}
			else if($status == 0)
			{
				$usulans = $usulans->where('USULAN_STATUS',0)->where('USULAN_POSISI',">=",4);
			}
		}
		if(Auth::user()->level == 7)
		{
			$usulans = $usulans->UsulanSKPD(Auth::user()->SKPD_ID)->NotDummies();
		}

		if ($pengusul != "x")
		{

		    $usulans = $usulans->where('USER_CREATED',$pengusul);
		}

		/*

		if(Auth::user()->level == 8)
		{
 			if(Auth::user()->email == "sosbudpem")
 			{
 				$usulans = $usulans->usulanBidang(1);
 			}
 			else if(Auth::user()->email == "ipw")
 			{
 				$usulans = $usulans->usulanBidang(2);
 			}
 			else if(Auth::user()->email == "ekonomi")
 			{
 				$usulans = $usulans->usulanBidang(3);
 			}
		}

		//Check Status
		//check status kelurahan

		//check status kecamatan

		else if(Auth::user()->level == 8)
		{
			if($status == 1)
			{
				$usulans = $usulans->where('USULAN_STATUS',2)->where('USULAN_POSISI',3);
			}
			else if($status == 2)
			{
				$usulans = $usulans->where('USULAN_STATUS',2)->where('USULAN_POSISI',"=",4);
			}
			else if($status == 0)
			{
				$usulans = $usulans->where('USULAN_STATUS',0)->where('USULAN_POSISI',"=",4);
			}
		}*/

		/*if($kecamatan != "x")
		{
			$usulans = $usulans->wherehas("user",function($user) use($kecamatan){
				$user->where('KEC_ID',$kecamatan);
			});
		}

		if ($kamus != "x")
		{
		    $usulans = $usulans->where('KAMUS_ID',$kamus);
		}
		if ($isu != "x")
		{
		    $usulans = $usulans->whereHas('kamus',function($kamus) use($isu){
		    	return $kamus->where('ISU_ID',$isu);
		    });
		}*/
//$usulans = $usulans->take(50)->get(); dd($usulans);
$usulans = $usulans->get();


		$out = $this->toDataTableUsulanFormat($usulans, $tahun);

		return Response::JSON($out);
	}

	public function dataUsulanSKPD($tahun,$idskpd)
	{
		$usulans = Usulan::query();
		$level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();

        if(Auth::user()->level==5)
        {
        	$ids = Auth::user()->kelurahan->user->whereIn('level',$level)->pluck('id')->all();
        }
        else if(Auth::user()->level==6)
        {
            $ids = Auth::user()->kecamatan->user->whereIn('level',$level)->pluck('id')->all();
            $usulans = $usulans->UsulanKecamatan();
        }
        else if(Auth::user()->level==8)
        {
        	$usulans = $usulans->UsulanBappeda();
        }

        $usulans =$usulans->whereIn("USER_CREATED",$ids)
        			->NotDummies()
        			->whereHas('kamus',function($kamus) use ($idskpd){
        				return $kamus->where('KAMUS_SKPD',$idskpd);
        			})->with([
						"user",
						"kamus",
						"kamus.isu",
						"kamus.skpd",
					])
        			->get();

        $out = $this->toDataTableUsulanFormat($usulans);

		return Response::JSON($out);
	}

	public function dataUsulanKecamatan($tahun,$idkecamatan)
	{
		$usulans = Usulan::query();
		$level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();

        $usulans = $usulans->UsulanBappeda();

        $usulans = $usulans->whereIn("USER_CREATED",$ids)
        			->NotDummies()
					->whereHas('user',function($user) use ($idkecamatan){
						return $user->where('KEC_ID',$idkecamatan);
					})->with([
						"user",
						"kamus",
						"kamus.isu",
						"kamus.skpd",
					])
					->get();
		$out = $this->toDataTableUsulanFormat($usulans);

		return Response::JSON($out);
	}

	public function dataRekapKecamatan($tahun)
	{
		$kecamatans = Kecamatan::with(
			[
				"usulan"=>function($usulan){
					$usulan->UsulanBappeda();
				},
				"usulan.kamus",
				'user',
			]
		)
		->whereNotIn('KEC_ID',[31,32,9999])
		->get();
		$response = array();

		foreach ($kecamatans as $key => $kecamatan) {
			$usulan = $kecamatan->usulan;

			$renja = $usulan->where('USULAN_TUJUAN',1);

			$jumlahrenja = $renja->count();

			$jumlahpengusulrenja = $renja->unique('USER_CREATED')->count();

			$pippk = $usulan->where('USULAN_TUJUAN',2);

			$jumlahpippk = $pippk->count();

			$jumlahpengusulpippk = $pippk->unique('USER_CREATED')->count();

			array_push($response, array(
			                         'kecamatanid'               => $kecamatan->KEC_ID,
			                         'no'                       => $key+1,
			                         'kecamatan'                 => $kecamatan->KEC_NAMA,
			                         'jumlahrenja'              => number_format($jumlahrenja,0,'.',','),
			                         'jumlahpengusulrenja'              => number_format($jumlahpengusulrenja,0,'.',','),
			                         'totalnominalrenja'        => number_format($kecamatan->getNominalUsulan(1),0,'.',','),
			                         'jumlahpippk'              => number_format($jumlahpippk,0,'.',','),
			                         'jumlahpengusulpippk'              => number_format($jumlahpengusulpippk,0,'.',','),
			                         'totalnominalpippk'        => number_format($kecamatan->getNominalUsulan(2),0,'.',','),
			                         ));
		}

		$out = array("aaData"=>$response);
		return Response::JSON($out);
	}


	public function rekapkelurahan($tahun){

		$level = [1,2,3,4,5,6];

        $ids = User::whereIn('level',$level)->pluck('id')->all();

        if(Auth::user()->level==5)
        {
        	$ids = Auth::user()->kelurahan->user->whereIn('level',$level)->pluck('id')->all();
        }

		$kegiatan = Kegiatan::join('REFERENSI.REF_KAMUS','REF_KAMUS.KEGIATAN_ID','=','REF_KEGIATAN.KEGIATAN_ID')
					->join('REFERENSI.REF_ISU','REF_ISU.ISU_ID','=','REF_KAMUS.ISU_ID')
			  		->join('MUSRENBANG.DAT_USULAN','DAT_USULAN.KAMUS_ID','=','REF_KAMUS.KAMUS_ID')
			  		->join('REFERENSI.REF_SKPD','REF_SKPD.SKPD_ID','=','REF_KAMUS.KAMUS_SKPD')
			  		->join('REFERENSI.REF_PROGRAM','REF_PROGRAM.PROGRAM_ID','=','REF_KEGIATAN.PROGRAM_ID')
			  		->where('USULAN_TAHUN',$tahun)
			  		->where('USULAN_DELETED',0)
			  		->where('USULAN_TUJUAN',1)
			  		->where('USULAN_PRIORITAS',1)
			  		->wherein('USER_CREATED',$ids)
			  		->groupBy("SKPD_NAMA", "PROGRAM_NAMA", "REF_KEGIATAN.KEGIATAN_ID", "KEGIATAN_NAMA", "ISU_NAMA", "KAMUS_NAMA", "USULAN_STATUS", "USULAN_POSISI", "REF_KAMUS.KAMUS_ID","USULAN_TUJUAN")
			  		->selectRaw(' "SKPD_NAMA", "PROGRAM_NAMA", "REF_KEGIATAN"."KEGIATAN_ID", "KEGIATAN_NAMA", "ISU_NAMA", "REF_KAMUS"."KAMUS_ID","KAMUS_NAMA", "USULAN_STATUS", "USULAN_POSISI", "USULAN_TUJUAN" ')
					->get();

		$response = array();
		$no =1;
		$keg = '';
		foreach ($kegiatan as $kegiatan) {

			if($keg != $kegiatan->PROGRAM_NAMA){

			$progkeg = $kegiatan->PROGRAM_NAMA."<br><font style='color:orange'>".$kegiatan->KEGIATAN_NAMA.'</font>';
			$isukamus = $kegiatan->ISU_NAMA."<br><font style='color:orange'>".$kegiatan->KAMUS_NAMA.'</font>';

			if($kegiatan->USULAN_STATUS == 1){
				$status = '<i class="fa fa-refresh text-info"></i> Kelurahan';
			}elseif($kegiatan->USULAN_STATUS == 2 && $kegiatan->USULAN_POSISI==1){
				$status     = '<i class="fa fa-check text-success"></i> Kelurahan';
			}elseif($kegiatan->USULAN_STATUS == 0 && $kegiatan->USULAN_POSISI==1){
				$status     = '<i class="fa fa-close text-danger"></i> Kelurahan';
			}
			//

			$aksi  = '<a class="action-preview" data-toggle="modal" data-target="#detail-usulan">
			<i class="mi-eye"></i></a><br>';
			$aksi  .= '<a href="'.url('/musrenbang/'.$tahun.'/data/show/detail/usulan/'.$kegiatan->KAMUS_ID).'" class="action-edit"><i class="mi-edit"></i></a>';



			array_push($response, array(
									 'no'		  	=> $no,
			                         'kegiatanid'   => $kegiatan->KEGIATAN_ID,
			                         'skpd'       	=> $kegiatan->SKPD_NAMA,
			                         'progkeg'    	=> $progkeg,
			                         'isukamus'   	=> $isukamus,
			                         'status'     	=> $status,
			                         'aksi'       	=> $aksi,
			                         ));

			}



			$keg = $kegiatan->PROGRAM_NAMA ;
			$no++;
		}

		$out = array("aaData"=>$response);
		return Response::JSON($out);
	}


	public function getUsulan($tahun,$id){
		$level = [1,2,3,4,5,6];

        $ids = User::whereIn('level',$level)->pluck('id')->all();

        if(Auth::user()->level==5)
        {
        	$ids = Auth::user()->kelurahan->user->whereIn('level',$level)->pluck('id')->all();
        }

        $data     = Usulan::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
							->join('REFERENSI.REF_ISU','REF_ISU.ISU_ID','=','REF_KAMUS.ISU_ID')
        					->where('DAT_USULAN.KAMUS_ID',$id)
        					->where('USULAN_DELETED',0)
					  		->where('USULAN_TUJUAN',1)
					  		->where('USULAN_PRIORITAS',1)
					  		->wherein('USER_CREATED',$ids)
        					->where('USULAN_TAHUN',$tahun)->get();

        $view           = array();

        foreach ($data as $data) {
            $kamus     = $data->ISU_NAMA."<br><p class='text-orange'>".$data->KAMUS_NAMA."</p>";
            $vol       = $data->USULAN_VOLUME."<br><p class='text-orange'>".$data->KAMUS_SATUAN."</p>";
            $total     = $data->USULAN_VOLUME*$data->KAMUS_HARGA;

            $img = '/assets/img/01.jpg';
            $timeline = '<div>
                          <a class="pull-left thumb-sm avatar m-l-n-md">
                            <img src="'.$img.'" class="b-2x b-white img-circle" alt="...">
                          </a>
                          <div class="m-l-xxl">
                            <div class="m-b-sm">
                              <a class="text-orange">'.$kamus.'</a> <a class="h4 font-semibold">'.$vol.'</a>
                              <span class="text-muted m-l-sm pull-right">
                                '.$total.'
                              </span>
                            </div>
                            <div class="m-b">
                              <div>'.$kamus.'</div>
                            </div>
                          </div>
                        </div>
                        <hr>';

            array_push($view, array( 'USULAN_ID'      	=>$data->USULAN_ID,
                                     'PENGUSUL'    		=>$data->user->name,
                                     'KAMUS'    		=>$kamus,
                                     'VOLUME'    		=>$vol,
                                     'log'    		=>$timeline,
                                     'HARGA'  			=>number_format($data->KAMUS_HARGA,0,'.',','),
                                     'TOTAL' 		    =>number_format($total,0,'.',','),
						));
        }
        $out = array("header"=>$view);
        return Response::JSON($out);
    }


     public function showDetailUsulan($tahun,$id,$kecid = null){
        $kec   = Kecamatan::all();
        $kel    = Kelurahan::where('KEC_ID',Auth::user()->KEC_ID)->get();
        //dd($kel);
        $isu    = Isu::get();
        $kamus  = Kamus::get();

        $data   = array('tahun' => $tahun,
                        'isu'=>$isu,
                        'kamus'=>$kamus,
                        'id' => $id,
                        'kelurahan' => $kel,
                        'kecamatan' => $kec,
                         );
        $data["kecid"] = $kecid == null ? "x" : $kecid;

        if(Auth::user()->level==5){
            $k = Auth::user()->KEL_ID;
            $users  = USER::whereIn('level',[1,2,3,4,5])->where('KEL_ID',Auth::user()->KEL_ID)->orderBy('email')->get();

            $data["users"] = $users;


            $data["diproses"] =  USULAN::wherehas('user',function($u) use ($k){
                return $u->where('KEL_ID',$k);
            })->where('USULAN_STATUS',1)->where('USULAN_TUJUAN',$id)->where('USULAN_TAHUN',$tahun)->where('USULAN_DELETED',0)->count();

            $data["diterima"] =  USULAN::wherehas('user',function($u) use ($k){
                return $u->where('KEL_ID',$k);
            })->where('USULAN_STATUS',2)->where('USULAN_TUJUAN',$id)->where('USULAN_TAHUN',$tahun)->where('USULAN_DELETED',0)->count();

            $data["ditolak"] =  USULAN::wherehas('user',function($u) use ($k){
                return $u->where('KEL_ID',$k);
            })->where('USULAN_STATUS',0)->where('USULAN_TUJUAN',$id)->where('USULAN_TAHUN',$tahun)->where('USULAN_DELETED',0)->count();

            return View::make('musrenbang.usulan.show-usulan-detail',$data);
        }
    }

}
