<?php

namespace App\Http\Controllers\Musrenbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Usulan;
use App\Model\SKPD;
use App\Model\User;
use App\Model\Kecamatan;
use Auth;
use Excel;
use PDF;
use DB;
use Carbon;

class publicExportController extends Controller
{
    private function doExportUsulan($response,$title = "Export",$skpd = null,$kecamatan = null)
    {
        Excel::create($title, function($excel) use ($response,$skpd,$kecamatan) {

            $excel->sheet('Sheet 1', function($sheet) use ($response,$skpd,$kecamatan) {
                $sheet->setOrientation('landscape');
                $sheet->setHorizontalCentered(true);
                $sheet->getPageSetup()->setPaperSize(14);
                $sheet->setPageMargin([
                    0.1, 0.30, 0.25, 0.5
                ]);
                $sheet->setScale(95);
                
                $header = [
                    "NO",
                    "Lokasi/Nama Pengusul (Kecamatan/Kelurahan/RW)",
                    "Kegiatan/Isu",
                    "Urgensi",
                    "No Prioritas",
                    "Volume",
                    "Satuan",
                    "Harga Satuan (Rp)",
                    "Anggaran (Rp)",
                    "Keterangan",
                ];
                $sheet->setWidth(array(
                    'A'     =>  5,
                    'B'     =>  26,
                    'C'     =>  25,
                    'D'     =>  20,
                    'E'     =>  10,
                    'F'     =>  10,
                    'G'     =>  10,
                    'H'     =>  15,
                    'I'     =>  15,
                    'J'     =>  20,
                ));
                $sheet->setColumnFormat(array(
                    'F' => "#,##0",
                    'H' => "#,##0",
                    'I' => "#,##0",
                ));
                $sheet->row(1,["LAMPIRAN II"]);
                $sheet->row(2,["Daftar Urutan Kegiatan Prioritas Kecamatan"]);
                $sheet->row(3,["PD Penanggung Jawab : ".$skpd]);
                $sheet->row(4,$header);
                $sheet->row(5,[1,2,3,4,5,6,7,8,9,10]);
                $sheet->fromArray($response, null, 'A6', true,false);
                $sheet->mergeCells('A1:J1');
                $sheet->mergeCells('A2:J2');
                $sheet->mergeCells('A3:J3');
                $sheet->setBorder('A4:J'.(count($response)+5),"thin");
                $sheet->cells('A1:A2',function($cells){
                    $cells->setAlignment('center');
                    $cells->setFontWeight("bold");
                    //$cells->setBorder('solid','solid','solid','solid');
                });
                $sheet->cells('A3',function($cells){
                    $cells->setAlignment('center');
                    //$cells->setBorder('solid','solid','solid','solid');
                });
                $sheet->cells('A4:J5',function($cells){
                    $cells->setAlignment('center');
                    $cells->setValignment('top');
                    $cells->setFontWeight("bold");
                    //$cells->setBorder('solid','solid','solid','solid');
                });

                $sheet->cells('A6:J'.(count($response)+5),function($cells){
                    $cells->setValignment('top');
                });
                $sheet->cells('A6:A'.(count($response)+5),function($cells){
                    $cells->setAlignment('center');
                });
                $sheet->cells('F6:G'.(count($response)+5),function($cells){
                    $cells->setAlignment('center');
                });
                $sheet->setRowsToRepeatAtTop([4,5]);
                $sheet->getStyle('A1:J'.(count($response)+5))->getAlignment()->setWrapText(true);
                $sheet->setAutoFilter('A5:J'.(count($response)+5));
            });
        })->export('xls');
    }
    private function doExportUsulan2($response,$title = "Export",$skpd = null,$kecamatan = null)
    {
		Excel::create($title, function($excel) use ($response,$skpd,$kecamatan) {

            $excel->sheet('Sheet 1', function($sheet) use ($response,$skpd,$kecamatan) {
                $sheet->setOrientation('landscape');
                $sheet->setHorizontalCentered(true);
                $sheet->getPageSetup()->setPaperSize(14);
                $sheet->setPageMargin([
                    0.1, 0.30, 0.25, 0.5
                ]);
                $sheet->setScale(95);
                
            	$header = [
            		"No",
            		"Lokasi/Nama Pengusul (Kecamatan/Kelurahan/RW)",
            		"Kegiatan/Isu",
            		"Urgensi",
            		"Volume",
            		"Anggaran (Rp)",
            		"Alasan",
            	];
                $sheet->setWidth(array(
                    'A'     =>  5,
                    'B'     =>  26,
                    'C'     =>  25,
                    'D'     =>  20,
                    'E'     =>  10,
                    'F'     =>  20,
                    'G'     =>  26,
                ));
            	$sheet->setColumnFormat(array(
                    'E' => "#,##0",
                    'F' => "#,##0",
                ));
                $sheet->row(1,["LAMPIRAN III"]);
                $sheet->row(2,["Daftar Kegiatan yang belum Disepakati dalam Forum Gabungan PD"]);
                $sheet->row(3,["PD Penanggung Jawab : ".$skpd]);
            	$sheet->row(4,$header);
                $sheet->row(5,[1,2,3,4,5,6,7]);
            	$sheet->fromArray($response, null, 'A6', true,false);
                $sheet->mergeCells('A1:G1');
                $sheet->mergeCells('A2:G2');
                $sheet->mergeCells('A3:G3');
            	$sheet->setBorder('A4:G'.(count($response)+5),"thin");
                $sheet->cells('A1:A2',function($cells){
                    $cells->setAlignment('center');
                    $cells->setFontWeight("bold");
                    //$cells->setBorder('solid','solid','solid','solid');
                });
                $sheet->cells('A3',function($cells){
                    $cells->setAlignment('center');
                    //$cells->setBorder('solid','solid','solid','solid');
                });
                $sheet->cells('A4:J5',function($cells){
                    $cells->setAlignment('center');
                    $cells->setValignment('top');
                    $cells->setFontWeight("bold");
                    //$cells->setBorder('solid','solid','solid','solid');
                });

                $sheet->cells('A6:G'.(count($response)+5),function($cells){
                    $cells->setValignment('top');
                });
                $sheet->cells('A6:A'.(count($response)+5),function($cells){
                    $cells->setAlignment('center');
                });
            	$sheet->cells('E6:E'.(count($response)+5),function($cells){
                    $cells->setAlignment('center');
                });
                $sheet->setRowsToRepeatAtTop([4,5]);
                $sheet->getStyle('A1:G'.(count($response)+5))->getAlignment()->setWrapText(true);
                $sheet->setAutoFilter('A5:G'.(count($response)+5));
            });
        })->export('xls');
    }
    private function usulanToExcelFormat($usulans)
    {
        $response = array();
        foreach ($usulans as $key => $usulan) {
            $responseelement = array();


            $responseelement["no"] = $key+1;
            $responseelement["lokasi"] = $usulan->getStringPengusul();
            $responseelement["kegiatanisu"] = $usulan->kamus->KAMUS_NAMA." / ".$usulan->kamus->isu->ISU_NAMA;
            $responseelement["urgensi"] = $usulan->USULAN_URGENSI;
            $responseelement["prioritas"] = "";
            $responseelement["volume"] = $usulan->USULAN_VOLUME;
            $responseelement["satuan"] = $usulan->kamus->KAMUS_SATUAN;
            $responseelement["harga"] = $usulan->kamus->KAMUS_HARGA;
            $responseelement["anggaran"] = $usulan->kamus->KAMUS_HARGA * $usulan->USULAN_VOLUME;
            $responseelement["keterangan"] = $usulan->getStringKeterangan();



            /*$responseelement["tipe"] = "RENJA";
            if($usulan->USULAN_TUJUAN == 2)
            {
                $responseelement["tipe"] = "PIPPK";
            }
            $responseelement["prioritas"] = $usulan->USULAN_PRIORITAS;
            $responseelement["idpengusul"] = $usulan->user->email;
            $responseelement["namapengusul"] = $usulan->getStringPengusul();        
            $responseelement["isu"] = $usulan->kamus->isu->ISU_NAMA;        
            $responseelement["kamus"] = $usulan->kamus->KAMUS_NAMA;     
            $responseelement["urgensi"] = $usulan->USULAN_URGENSI;      
            $responseelement["kriteria"] = $usulan->kamus->KAMUS_KRITERIA;      
            $responseelement["volume"] = floatval($usulan->USULAN_VOLUME);      
            $responseelement["satuan"] = $usulan->kamus->KAMUS_SATUAN;      
            $responseelement["harga"] = floatval($usulan->kamus->KAMUS_HARGA);      
            $responseelement["anggaran"] = floatval($usulan->kamus->KAMUS_HARGA * $usulan->USULAN_VOLUME);      
            $responseelement["alamat"] = $usulan->ALAMAT;       
            $responseelement["peta"]   = $usulan->USULAN_LAT." , ".$usulan->USULAN_LONG;            
            $responseelement["penerima"] = "-";
            if($usulan->user->level == 1)
            {       
                $responseelement["penerima"] = $usulan->getStringRTPenerima();      
            }
            $responseelement["statususulan"] = $usulan->getStringStatus();*/        
            


            array_push($response, $responseelement);
        }
        return $response;
    }
    private function usulanToExcelFormatBaru($usulans)
    {
        $data = array();
        $response["jumlah"] = $usulans->count();
        $response["jumlahakomodir"] = 0;
        $response["jumlahtidakakomodir"] = 0;
        $response["jumlahproses"] = 0; 
        foreach ($usulans as $key => $usulan) {
            $responseelement = array();

            $responseelement["no"] = $key+1;
            $responseelement["lokasi"] = $usulan->getStringPengusul();
            $responseelement["kegiatanisu"] = $usulan->kamus->KAMUS_NAMA." / ".$usulan->kamus->isu->ISU_NAMA;
            $responseelement["urgensi"] = $usulan->USULAN_URGENSI;
            $responseelement["prioritas"] = "";
            $responseelement["volume"] = $usulan->USULAN_VOLUME;
            $responseelement["satuan"] = $usulan->kamus->KAMUS_SATUAN;
            $responseelement["harga"] = $usulan->kamus->KAMUS_HARGA;
            $responseelement["anggaran"] = $usulan->kamus->KAMUS_HARGA * $usulan->USULAN_VOLUME;
            $responseelement["keterangan"] = $usulan->getStringKeterangan();



            /*$responseelement["tipe"] = "RENJA";
            if($usulan->USULAN_TUJUAN == 2)
            {
                $responseelement["tipe"] = "PIPPK";
            }
            $responseelement["prioritas"] = $usulan->USULAN_PRIORITAS;
            $responseelement["idpengusul"] = $usulan->user->email;
            $responseelement["namapengusul"] = $usulan->getStringPengusul();        
            $responseelement["isu"] = $usulan->kamus->isu->ISU_NAMA;        
            $responseelement["kamus"] = $usulan->kamus->KAMUS_NAMA;     
            $responseelement["urgensi"] = $usulan->USULAN_URGENSI;      
            $responseelement["kriteria"] = $usulan->kamus->KAMUS_KRITERIA;      
            $responseelement["volume"] = floatval($usulan->USULAN_VOLUME);      
            $responseelement["satuan"] = $usulan->kamus->KAMUS_SATUAN;      
            $responseelement["harga"] = floatval($usulan->kamus->KAMUS_HARGA);      
            $responseelement["anggaran"] = floatval($usulan->kamus->KAMUS_HARGA * $usulan->USULAN_VOLUME);      
            $responseelement["alamat"] = $usulan->ALAMAT;       
            $responseelement["peta"]   = $usulan->USULAN_LAT." , ".$usulan->USULAN_LONG;            
            $responseelement["penerima"] = "-";
            if($usulan->user->level == 1)
            {       
                $responseelement["penerima"] = $usulan->getStringRTPenerima();      
            }
            $responseelement["statususulan"] = $usulan->getStringStatus();*/        
            


            array_push($response, $responseelement);
        }
        return $response;
    }
    private function usulanToExcelFormat2($usulans)
    {
    	$response = array();
		foreach ($usulans as $key => $usulan) {
			$responseelement = array();


            $responseelement["no"] = $key+1;
            $responseelement["lokasi"] = $usulan->getStringPengusul();
            $responseelement["kegiatanisu"] = $usulan->kamus->KAMUS_NAMA." / ".$usulan->kamus->isu->ISU_NAMA;
            $responseelement["urgensi"] = $usulan->USULAN_URGENSI;
            $responseelement["volume"] = $usulan->USULAN_VOLUME;
            $responseelement["anggaran"] = $usulan->kamus->KAMUS_HARGA * $usulan->USULAN_VOLUME;
			$responseelement["keterangan"] = $usulan->getStringKeterangan();

			array_push($response, $responseelement);
		}
		return $response;
    }
	public function exportusulan($tahun,$jenis)
    {
		$usulans = Usulan::query();
		$level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();

        if(Auth::user()->level==5)
        {
            $ids = Auth::user()->kelurahan->user->whereIn('level',$level)->pluck('id')->all();  
        }
        else if(Auth::user()->level==6)
        {
            $ids = Auth::user()->kecamatan->user->whereIn('level',$level)->pluck('id')->all();   
            $usulans = $usulans->UsulanKecamatan();            
        }
        else if(Auth::user()->level==7)
        {
            $usulans = $usulans->UsulanSKPD(Auth::user()->SKPD_ID);
        }
        else if(Auth::user()->level==8)
        {
            $usulans = $usulans->UsulanBappeda();
        }
        
       	//dd($usulans->toSQL());
		$usulans = $usulans->whereIn('USER_CREATED',$ids)
					->where('USULAN_TUJUAN',$jenis)
					->with([
						"user",
						"user.kecamatan",
						"user.rw",
						"user.kelurahan",
						"kamus",
						"kamus.isu",
					])
					->get();
		
		$jenisusulan = "RENJA";
		if($jenis == 2)
		{
			$jenisusulan = "PIPPK";
		}
		$response = $this->usulanToExcelFormat($usulans);

		$this->doExportUsulan($response,"Usulan ".$jenisusulan);
	}
    public function exportUsulanKecamatan($tahun,$idkecamatan)
    {

        $usulans = Usulan::query();
        $level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();

        $usulans = $usulans->UsulanBappeda();

        $usulans = $usulans->whereIn("USER_CREATED",$ids)
                    ->whereHas('user',function($user) use ($idkecamatan){
                        return $user->where('KEC_ID',$idkecamatan);
                    })->with([
                        "user",
                        "kamus",
                        "kamus.isu",
                        "kamus.skpd",
                    ])
                    ->get();

        $response = $this->usulanToExcelFormat($usulans);
        $kecamatan = Kecamatan::find($idkecamatan)->KEC_NAMA;
        
        $this->doExportUsulan($response,"Usulan Kecamatan".$kecamatan);
    }
	public function exportUsulanSKPD($tahun,$idskpd,$state = null)
	{
		$usulans = Usulan::query();
		$level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();
        
        $usulans = $usulans->UsulanSKPD()->whereIn("USER_CREATED",$ids)
                    ->NotDummies()->where('USULAN_TAHUN','2018')
                    ->where('USULAN_DELETED',0)
        			->whereHas('kamus',function($kamus) use ($idskpd){
        				return $kamus->where('KAMUS_SKPD',$idskpd);
        			})
                    ->with([
                         "user",
                         "user.kecamatan",
                         "user.rw",
                         "user.kelurahan",
                         "kamus",
                         "kamus.isu",
                     ])
                    ->orderBy("USER_CREATED");

        if($state==1)
        {
            $usulans = $usulans->where(function($q){
                //usulan diproses Di kecamatan
                $q->where(function($query){
                    $query->where('USULAN_STATUS',2)->where('USULAN_POSISI',2);
                })
                //usulan diterima di kecamatan
                ->orWhere(function($query){
                    $query->where('USULAN_STATUS',2)->where('USULAN_POSISI',">=",3);
                });
            });
        }
        else if($state==2)
        {
            $usulans = $usulans->where('USULAN_STATUS',0)->where('USULAN_POSISI',">=",3);
        }

        $usulans = $usulans->get();

        $namaskpd = SKPD::find($idskpd)->SKPD_NAMA;

        if($state == 2)
        {
            $response = $this->usulanToExcelFormat2($usulans);   
            //$this->doExportUsulan2($response,"Usulan SKPD ".$namaskpd,$namaskpd);
            $this->doExportUsulan2($response,"Usulan SKPD ".$namaskpd,$namaskpd);
        }else
        {
            $response = $this->usulanToExcelFormat($usulans);
            $this->doExportUsulan($response,"Usulan SKPD ".$namaskpd,$tahun,$namaskpd);
        }
	}
    public function exportRekapKecamatan($tahun)
    {
        $kecamatans = Kecamatan::with(
            [
                "usulan"=>function($usulan){
                    $usulan->UsulanBappeda();
                },
                "usulan.kamus",
                'user',         
            ]
        )
        ->whereNotIn('KEC_ID',[31,32,9999])
        ->get();
        $response = array();

        foreach ($kecamatans as $key => $kecamatan) {
            $usulan = $kecamatan->usulan;

            $renja = $usulan->where('USULAN_TUJUAN',1);

            $jumlahrenja = $renja->count();

            $jumlahpengusulrenja = $renja->unique('USER_CREATED')->count();

            $pippk = $usulan->where('USULAN_TUJUAN',2);

            $jumlahpippk = $pippk->count();

            $jumlahpengusulpippk = $pippk->unique('USER_CREATED')->count();

            array_push($response, array( 
                                     'no'                       => $key+1,
                                     'kecamatan'                 => $kecamatan->KEC_NAMA,
                                     'jumlahrenja'              => $jumlahrenja,
                                     'jumlahpengusulrenja'              => $jumlahpengusulrenja,
                                     'totalnominalrenja'        => $kecamatan->getNominalUsulan(1),
                                     'jumlahpen'              => $jumlahpippk,
                                     'jumlahpengusulpippk'              => $jumlahpengusulpippk,
                                     'totalnominalpippk'        => $kecamatan->getNominalUsulan(2),
                                     ));    
        }
        
        Excel::create('Usulan Per Kecamatan', function($excel) use ($response) {

            $excel->sheet('Sheet 1', function($sheet) use ($response) {
                $header = [
                    "",
                    "",
                    "Jumlah",
                    "Jumlah Pengusul",
                    "Nominal Usulan",
                    "Jumlah",
                    "Jumlah Pengusul",
                    "Nominal Usulan",
                ];
                $sheet->row(1,["No","Kecamatan","Renja","","","PIPPK"]);
                $sheet->row(2,$header);
                $sheet->fromArray($response, null, 'A3', true,false);
                $sheet->setBorder('A1:H'.(count($response)+2),"thin");
                $sheet->mergeCells('C1:E1');
                $sheet->mergeCells('F1:H1');
                $sheet->mergeCells('A1:A2');
                $sheet->mergeCells('B1:B2');
                $sheet->cells('A1:H2',function($cells){
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setFontWeight("bold");
                    //$cells->setBorder('solid','solid','solid','solid');
                });
               
                $sheet->setAutoFilter('A2:H'.(count($response)+2));
            });

        })->export('xls');   
    }
    public function exportRekapSKPD($tahun)
    {
        $usulans = Usulan::query();
        $level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();

        $skpds = SKPD::with(
            [
                "usulan"=>function($usulan) use ($ids)
                {
                    $usulan->UsulanSKPD()->whereIn('USER_CREATED',$ids)->NotDummies();
                },
                "usulan.kamus"
            ]
        )
        ->get();

        $response = array();
        $no = 1;
        foreach($skpds as $skpd)
        {
            $usulan = $skpd->usulan;

            $renja = $usulan->where('USULAN_TUJUAN',1);

            $jumlahrenja = $renja->count();

            $jumlahpengusul = $renja->unique('USER_CREATED')->count();

            array_push($response, array( 
                                     'NO'                  => $no,
                                     'KODESKPD'            => $skpd->SKPD_KODE,
                                     'NAMASKPD'            => $skpd->SKPD_NAMA,
                                     'JUMLAHRENJA'         => $jumlahrenja,
                                     'JUMLAHPENGUSUL'      => $jumlahpengusul,
                                     'TOTALNOMINAL'        => $skpd->totalNominal(1),
                                     ));    
            $no++;
        }
        
        Excel::create('Usulan Per SKPD', function($excel) use ($response) {
            $excel->sheet('Sheet 1', function($sheet) use ($response) {
                $header = [
                    "No",
                    "Kode SKPD",
                    "Nama SKPD",
                    "Jumlah RENJA",
                    "Jumlah Pengusul",
                    "Total Nominal Usulan",
                ];
                $sheet->setColumnFormat(array(
                    'C' => "#,##0",
                    'D' => "#,##0",
                    'E' => "#,##0",
                    'F' => "#,##0",
                ));
                $sheet->row(1,$header);
                $sheet->fromArray($response, null, 'A2', true,false);
                $sheet->setBorder('A1:F'.(count($response)+1),"thin");
                $sheet->cells('A1:F1',function($cells){
                    $cells->setAlignment('center');
                    $cells->setFontWeight("bold");
                    //$cells->setBorder('solid','solid','solid','solid');
                });
                
                $sheet->setAutoFilter('A1:F'.(count($response)+1));

            });

        })->export('xls');
    }

    public function exportRekapSKPDGrafik($tahun)
    {
        $usulans = Usulan::query();
        $level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();

        $skpds = SKPD::with(
            [
                "usulan"=>function($usulan) use ($ids)
                {
                    $usulan->UsulanSKPD()->whereIn('USER_CREATED',$ids)->NotDummies();
                },
                "usulan.kamus"
            ]
        )
        ->get();

        $response = array();
        $no = 1;
        foreach($skpds as $skpd)
        {
            $usulan = $skpd->usulan;

            $renja = $usulan->where('USULAN_TUJUAN',1);

            $renjaproses = $renja->filter(function($value){
                    return (($value->USULAN_STATUS == 2) && ($value->USULAN_POSISI==2));
                })->count();
            
            $renjaterima = $renja->filter(function($value){
                    return (($value->USULAN_STATUS == 2) && ($value->USULAN_POSISI>=3));
                })->count();
            
            $renjatolak = $renja->filter(function($value){
                    return (($value->USULAN_STATUS == 0) && ($value->USULAN_POSISI>=3));
                })->count();

            $jumlahrenja = $renja->count();

            $jumlahpengusul = $renja->unique('USER_CREATED')->count();

            array_push($response, array( 
                                     'NO'                  => $no,
                                     'KODESKPD'            => $skpd->SKPD_KODE,
                                     'NAMASKPD'            => $skpd->SKPD_NAMA,
                                     'JUMLAHRENJA'         => $jumlahrenja,
                                     'JUMLAHPENGUSUL'      => $jumlahpengusul,
                                     'TOTALNOMINAL'        => $skpd->totalNominal(1),
                                     'renjaproses'         => $renjaproses,
                                     'renjaterima'         => $renjaterima,
                                     'renjatolak'         => $renjatolak, 
                                     ));    
            $no++;
        }
        
        Excel::create('Usulan Per SKPD', function($excel) use ($response) {
            $excel->sheet('Sheet 1', function($sheet) use ($response) {
                $header = [
                    "No",
                    "Kode SKPD",
                    "Nama SKPD",
                    "Jumlah RENJA",
                    "Jumlah Pengusul",
                    "Total Nominal Usulan",
                    "Di Proses",
                    "Di Terima",
                    "Di Tolak",
                ];
                $sheet->setColumnFormat(array(
                    'C' => "#,##0",
                    'D' => "#,##0",
                    'E' => "#,##0",
                    'F' => "#,##0",
                    'G' => "#,##0",
                    'H' => "#,##0",
                    'I' => "#,##0",
                ));
                $sheet->row(1,$header);
                $sheet->fromArray($response, null, 'A2', true,false);
                $sheet->setBorder('A1:I'.(count($response)+1),"thin");
                $sheet->cells('A1:I1',function($cells){
                    $cells->setAlignment('center');
                    $cells->setFontWeight("bold");
                    //$cells->setBorder('solid','solid','solid','solid');
                });
                
                $sheet->setAutoFilter('A1:I'.(count($response)+1));

            });

        })->export('xls');
    }

    public function exportUsulanSKPDPerKecamatan($tahun,$idskpd,$state=null)
    {
        $usulans = Usulan::query();
        $level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();
        
        $usulans = $usulans->UsulanSKPD()->whereIn("USER_CREATED",$ids)
                   ->where('USULAN_TAHUN',$tahun)
                    ->NotDummies()
                    ->whereHas('kamus',function($kamus) use ($idskpd){
                        return $kamus->where('KAMUS_SKPD',$idskpd);
                    })
                    ->with([
                         "user",
                         "user.kecamatan",
                         "user.rw",
                         "user.kelurahan",
                         "kamus",
                         "kamus.isu",
                     ])
                    ->orderBy("USER_CREATED");
        
        if($state==1)
        {
            $usulans = $usulans->where(function($q){
                //usulan diproses Di kecamatan
                $q->where(function($query){
                    $query->where('USULAN_STATUS',2)->where('USULAN_POSISI',2);
                })
                //usulan diterima di kecamatan
                ->orWhere(function($query){
                    $query->where('USULAN_STATUS',2)->where('USULAN_POSISI',">=",3);
                });
            });
        }
        else if($state==2)
        {
            $usulans = $usulans->where('USULAN_STATUS',0)->where('USULAN_POSISI',">=",3);
        }

        $usulans = $usulans->get();            
        $grouped = $usulans->groupBy(function($item){
            return $item->user->KEC_ID;
        });
        
        //$response = $this->usulanToExcelFormat($usulans);
        
        $namaskpd = SKPD::find($idskpd)->SKPD_NAMA;
        
        //$this->doExportUsulan($response,"Usulan SKPD ".$namaskpd);
        
        Excel::create("Usulan SKPD ".$namaskpd, function($excel) use ($grouped,$namaskpd,$state) {
            $i=1;
            foreach ($grouped as $key => $usulans) {
                $kecamatan = Kecamatan::find($key)->KEC_NAMA;
                if($state == 2)
                {
                    $response = $this->usulanToExcelFormat2($usulans);   
                    $excel->sheet('Kecamatan '.$kecamatan, function($sheet) use ($response,$namaskpd,$kecamatan) {
                        $sheet->setOrientation('landscape');
                        $sheet->setHorizontalCentered(true);
                        $sheet->getPageSetup()->setPaperSize(14);
                        $sheet->setPageMargin([
                            0.1, 0.30, 0.25, 0.5
                        ]);
                        $sheet->setScale(95);
                        
                        $header = [
                            "No",
                            "Lokasi/Nama Pengusul (Kecamatan/Kelurahan/RW)",
                            "Kegiatan/Isu",
                            "Urgensi",
                            "Volume",
                            "Anggaran (Rp)",
                            "Alasan",
                        ];
                        $sheet->setWidth(array(
                            'A'     =>  5,
                            'B'     =>  26,
                            'C'     =>  25,
                            'D'     =>  20,
                            'E'     =>  10,
                            'F'     =>  20,
                            'G'     =>  26,
                        ));
                        $sheet->setColumnFormat(array(
                            'E' => "#,##0",
                            'F' => "#,##0",
                        ));
                        $sheet->row(1,["LAMPIRAN III"]);
                        $sheet->row(2,["Daftar Kegiatan yang belum Disepakati dalam Forum Gabungan PD"]);
                        $sheet->row(3,["Kecamatan ".$kecamatan.",PD Penanggung Jawab : ".$namaskpd]);
                        $sheet->row(4,$header);
                        $sheet->row(5,[1,2,3,4,5,6,7]);
                        $sheet->fromArray($response, null, 'A6', true,false);
                        $sheet->mergeCells('A1:G1');
                        $sheet->mergeCells('A2:G2');
                        $sheet->mergeCells('A3:G3');
                        $sheet->setBorder('A4:G'.(count($response)+5),"thin");
                        $sheet->cells('A1:A2',function($cells){
                            $cells->setAlignment('center');
                            $cells->setFontWeight("bold");
                            //$cells->setBorder('solid','solid','solid','solid');
                        });
                        $sheet->cells('A3',function($cells){
                            $cells->setAlignment('center');
                            //$cells->setBorder('solid','solid','solid','solid');
                        });
                        $sheet->cells('A4:J5',function($cells){
                            $cells->setAlignment('center');
                            $cells->setValignment('top');
                            $cells->setFontWeight("bold");
                            //$cells->setBorder('solid','solid','solid','solid');
                        });

                        $sheet->cells('A6:G'.(count($response)+5),function($cells){
                            $cells->setValignment('top');
                        });
                        $sheet->cells('A6:A'.(count($response)+5),function($cells){
                            $cells->setAlignment('center');
                        });
                        $sheet->cells('E6:E'.(count($response)+5),function($cells){
                            $cells->setAlignment('center');
                        });
                        $sheet->setRowsToRepeatAtTop([4,5]);
                        $sheet->getStyle('A1:G'.(count($response)+5))->getAlignment()->setWrapText(true);
                        $sheet->setAutoFilter('A5:G'.(count($response)+5));
                    });
                }else
                {
                    $response = $this->usulanToExcelFormat($usulans);
                    $excel->sheet($kecamatan, function($sheet) use ($response,$kecamatan,$namaskpd) 
                                    {
                                            $sheet->setOrientation('landscape');
                                            $sheet->setHorizontalCentered(true);
                                            $sheet->getPageSetup()->setPaperSize(14);
                                            $sheet->setPageMargin([
                                                0.1, 0.30, 0.25, 0.5
                                            ]);
                                            $sheet->setScale(95);
                                            
                                            $header = [
                                                "NO",
                                                "Lokasi/Nama Pengusul (Kecamatan/Kelurahan/RW)",
                                                "Kegiatan/Isu",
                                                "Urgensi",
                                                "No Prioritas",
                                                "Volume",
                                                "Satuan",
                                                "Harga Satuan (Rp)",
                                                "Anggaran (Rp)",
                                                "Keterangan",
                                            ];
                                            $sheet->setWidth(array(
                                                'A'     =>  5,
                                                'B'     =>  26,
                                                'C'     =>  25,
                                                'D'     =>  20,
                                                'E'     =>  10,
                                                'F'     =>  10,
                                                'G'     =>  10,
                                                'H'     =>  15,
                                                'I'     =>  15,
                                                'J'     =>  20,
                                            ));
                                            $sheet->setColumnFormat(array(
                                                'F' => "#,##0",
                                                'H' => "#,##0",
                                                'I' => "#,##0",
                                            ));
                                            $sheet->row(1,["LAMPIRAN II"]);
                                            $sheet->row(2,["Daftar Urutan Kegiatan Prioritas Kecamatan"]);
                                            $sheet->row(3,["Kecamatan ".$kecamatan.",PD Penanggung Jawab : ".$namaskpd]);
                                            $sheet->row(4,$header);
                                            $sheet->row(5,[1,2,3,4,5,6,7,8,9,10]);
                                            $sheet->fromArray($response, null, 'A6', true,false);
                                            $sheet->mergeCells('A1:J1');
                                            $sheet->mergeCells('A2:J2');
                                            $sheet->mergeCells('A3:J3');
                                            $sheet->setBorder('A4:J'.(count($response)+5),"thin");
                                            $sheet->cells('A1:A2',function($cells){
                                                $cells->setAlignment('center');
                                                $cells->setFontWeight("bold");
                                                //$cells->setBorder('solid','solid','solid','solid');
                                            });
                                            $sheet->cells('A3',function($cells){
                                                $cells->setAlignment('center');
                                                //$cells->setBorder('solid','solid','solid','solid');
                                            });
                                            $sheet->cells('A4:J5',function($cells){
                                                $cells->setAlignment('center');
                                                $cells->setValignment('top');
                                                $cells->setFontWeight("bold");
                                                //$cells->setBorder('solid','solid','solid','solid');
                                            });

                                            $sheet->cells('A6:J'.(count($response)+5),function($cells){
                                                $cells->setValignment('top');
                                            });
                                            $sheet->cells('A6:A'.(count($response)+5),function($cells){
                                                $cells->setAlignment('center');
                                            });
                                            $sheet->cells('F6:G'.(count($response)+5),function($cells){
                                                $cells->setAlignment('center');
                                            });
                                            $sheet->setRowsToRepeatAtTop([4,5]);
                                            $sheet->getStyle('A1:J'.(count($response)+5))->getAlignment()->setWrapText(true);
                                            $sheet->setAutoFilter('A5:J'.(count($response)+5));
                                    });
                }
                
                $i++;
            }
        })->export('xls');
    }
    private function doExportUsulanFormatBaru($response,$title = "Export",$tahun,$skpd = null,$kecamatan = null)
    {
        Excel::create($title, function($excel) use ($response,$skpd,$kecamatan,$tahun) {

            $excel->sheet('Sheet 1', function($sheet) use ($response,$skpd,$kecamatan,$tahun) {
                $sheet->setOrientation('landscape');
                $sheet->setHorizontalCentered(true);
                $sheet->getPageSetup()->setPaperSize(14);
                $sheet->setPageMargin([
                    0.1, 0.30, 0.25, 0.5
                ]);
                $sheet->setScale(95);
                
                $header = [
                    "NO",
                    "Urusan / Bidang Urusan",
                    "Prioritas Daerah",
                    "Sasaran Daerah",
                    "Program",
                    "Kegiatan Prioritas",
                    "Sasaran Kegiatan",
                    "Lokasi",
                    "Volume",
                    "Usulan Pagu (Rp)",
                    "Kecamatan",
                    "Status",
                    "Alasan Tidak Diakomodir",
                ];
                $sheet->setWidth(array(
                    'A'     =>  10,
                    'B'     =>  26,
                    'C'     =>  25,
                    'D'     =>  25,
                    'E'     =>  10,
                    'F'     =>  10,
                    'G'     =>  10,
                    'H'     =>  15,
                    'I'     =>  15,
                    'J'     =>  20,
                    'K'     =>  20,
                    'L'     =>  20,
                    'M'     =>  20,
                ));
                $sheet->setColumnFormat(array(
                    'J' => "#,##0"
                ));
                

                $sheet->row(1,["Daftar Urutan Kegiatan Prioritas Kecamatan"]);
                $sheet->row(2,["KOTA BANDUNG"]);
                $sheet->row(3,["SKPD"," : ".$skpd,"","Jumlah Total Usulan"," : ",0]);
                $sheet->row(4,["Tahun"," : ".$tahun,"","Jumlah diakomodir"," : ",0]);
                $sheet->row(5,["","","","Jumlah tidak diakomodir"," : ",0]);
                $sheet->row(6,["Status"," :"."Semua","","Jumlah lagi dalam proses"," : ",0,"","","","","Jumlah Dana Total Usulan : ","",0]);
                $sheet->row(7,$header);
                $sheet->row(8,[1,2,3,4,5,6,7,8,9,10,11,12,13]);
                $sheet->fromArray($response, null, 'A9', true,false);
                
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:C2');
                $sheet->mergeCells('A2:C2');
                $sheet->mergeCells('K6:L6');
                
                $sheet->setBorder('A7:M'.(count($response)+8),"thin");
                $sheet->cells('A1:A6',function($cells){
                    $cells->setFontWeight("bold");
                    //$cells->setBorder('solid','solid','solid','solid');
                });
                $sheet->cells('A7:M8',function($cells){
                    $cells->setAlignment('center');
                    $cells->setValignment('top');
                    $cells->setFontWeight("bold");
                    //$cells->setBorder('solid','solid','solid','solid');
                });

                $sheet->cells('A9:M'.(count($response)+8),function($cells){
                    $cells->setValignment('top');
                });
                $sheet->cells('A9:A'.(count($response)+8),function($cells){
                    $cells->setAlignment('center');
                });
                $sheet->cells('I9:I'.(count($response)+8),function($cells){
                    $cells->setAlignment('center');
                });
                $sheet->setRowsToRepeatAtTop([7,8]);
                $sheet->getStyle('A1:M'.(count($response)+8))->getAlignment()->setWrapText(true);
                $sheet->setAutoFilter('A8:M'.(count($response)+8));
            });
        })->export('xls');
    }
    public function doExportUsulanPDF()
    {
        $pdf = PDF::loadView('musrenbang.pdf.usulan');
        $pdf = $pdf->setPaper('folio','landscape');
        return $pdf->stream();
    }



    public function rekapTahapRw($tahun,$id){
        
        $data   = DB::select('select 
            skpd."SKPD_NAMA" as skpd, keg."KEGIATAN_NAMA" as kegiatan, k."KAMUS_NAMA" as usulan,
            u."USULAN_VOLUME" as volume, k."KAMUS_HARGA" as harga, k."KAMUS_SATUAN" as satuan,
            u."USULAN_VOLUME" * k."KAMUS_HARGA" as nominal, u."USULAN_URGENSI" as urgensi, u."ALAMAT" as alamat, u."USULAN_STATUS" as status, u."USULAN_POSISI" as posisi, u."USULAN_ALASAN" as alasan, users."name" as pengusul, kel."KEL_NAMA" as kelurahan, kec."KEC_NAMA" as kecamatan, u."USULAN_ID" as ids
                from "MUSRENBANG"."DAT_USULAN" u
                join "REFERENSI"."REF_KAMUS" k on k."KAMUS_ID" = u."KAMUS_ID"
                LEFT join "REFERENSI"."REF_SKPD" skpd on skpd."SKPD_ID" = k."KAMUS_SKPD"
                LEFT join "REFERENSI"."REF_KEGIATAN" keg on keg."KEGIATAN_ID" = k."KEGIATAN_ID"
                join "DATA".users on users."id" = u."USER_CREATED"
                join "REFERENSI"."REF_KELURAHAN" kel on kel."KEL_ID" = users."KEL_ID"
                join "REFERENSI"."REF_KECAMATAN" kec on kec."KEC_ID" = users."KEC_ID"
                WHERE u."USULAN_TAHUN"='.$tahun.' and u."USULAN_DELETED"=0 
                and u."USULAN_TUJUAN"='.$id.'order By skpd."SKPD_NAMA", keg."KEGIATAN_NAMA" '
                ); 
               //dd($data);
            $data = array_map(function ($value) {
                    return (array)$value;
                }, $data);
            Excel::create('Rekap Usulan '.Carbon\Carbon::now()->format('d M Y - H'), function($excel) use($data){
                    $excel->sheet('Rekap Usulan', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
            })->download('xls');
            
    }

    

}
