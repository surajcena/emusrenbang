<?php

namespace App\Http\Controllers\musrenbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Usulan;
use App\Model\User;
use Datatables;

class dataTableController extends Controller
{
	public function getUsulan($tahun)
	{
		$level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();

        $usulans = Usulan::where("USULAN_TAHUN",$tahun)
        			->select(
        				[
        					"DATA.users"				
        				]
        			)
        			->join("DATA.users","DATA.users.id","=","MUSRENBANG.DAT_USULAN.USER_CREATED")
        			->join("REFERENSI.REF_KAMUS","REFERENSI.REF_KAMUS.KAMUS_ID","=","MUSRENBANG.DAT_USULAN.KAMUS_ID")
        			->join("REFERENSI.REF_ISU","REFERENSI.REF_ISU.ISU_ID","=","REFERENSI.REF_KAMUS.ISU_ID")
        			->join("REFERENSI.REF_SKPD","REFERENSI.REF_SKPD.SKPD_ID","=","REFERENSI.REF_KAMUS.KAMUS_SKPD")
					->wherein('USER_CREATED',$ids);
					/*->with([
						"user",
						"kamus",
						"kamus.isu",
						"kamus.skpd",
					]);*/
		//dd($usulans->first());
		return Datatables::of($usulans)
            ->setTransformer(new \App\DataTableTransformers\UsulanTransformer)
            ->make(true);
	}
}
