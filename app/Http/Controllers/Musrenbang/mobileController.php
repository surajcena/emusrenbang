<?php

namespace App\Http\Controllers\Musrenbang;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Session;
use Carbon;
use Response;
use Auth;
use App\Model\Isu;
use App\Model\Kamus;
use App\Model\Kecamatan;
use App\Model\Kelurahan;
use App\Model\RT;
use App\Model\RW;
use App\Model\Usulan;
use App\Model\Usulan_Rekap;
use App\Model\UserMusren;
use App\Model\Tujuan;
use Validator;
use App\Model\User;
use App\Model\BeritaAcara;

class mobileController extends BaseController
{
  use ValidatesRequests;
    public function __construct(){
       // $this->middleware('auth');
    }

    public function index($tahun){

        return View::make('musrenbang.mobile.index');
    }
    
    public function login($tahun){
        return View::make('musrenbang.mobile.login');
    }

     public function dashboard($tahun){
         // print_r(Auth::user()->id);exit();

        if(empty(Auth::user()->id)){
            return Redirect('musrenbang/2017/mobile/login');
        }else{
         $user = User::where('id',Auth::user()->id)->first();

         $renjadiproses =  USULAN::where('USULAN_STATUS',1)->where('USULAN_TUJUAN',1)
            ->where('USER_CREATED', Auth::user()->id)->count();
         
        $renjaditerima =  USULAN::where(function($q) {
                      $q->where([
                            ['USULAN_POSISI', '=', '2'],
                            ['USULAN_STATUS', '=', '2'],
                        ]);
                      $q->orwhere([
                            ['USULAN_POSISI', '=', '3'],
                            ['USULAN_STATUS', '=', '2'],
                        ]);
                      $q->orwhere([
                            ['USULAN_POSISI', '=', '4'],
                            ['USULAN_STATUS', '=', '2'],
                        ]);
                  })->where('USULAN_TUJUAN',1)->where('USER_CREATED', Auth::user()->id)->count();

        $renjaditolak  =  USULAN::where('USULAN_STATUS',0)->where('USULAN_TUJUAN',1)->where('USER_CREATED', Auth::user()->id)->count();

        $pippkdiproses =  USULAN::where('USULAN_STATUS',1)->where('USULAN_TUJUAN',2)
            ->where('USER_CREATED', Auth::user()->id)->count();

        $pippkditerima =  USULAN::where(function($q) {
                      $q->where([
                            ['USULAN_POSISI', '=', '2'],
                            ['USULAN_STATUS', '=', '2'],
                        ]);
                      $q->orwhere([
                            ['USULAN_POSISI', '=', '3'],
                            ['USULAN_STATUS', '=', '2'],
                        ]);
                      $q->orwhere([
                            ['USULAN_POSISI', '=', '4'],
                            ['USULAN_STATUS', '=', '2'],
                        ]);
                  })->where('USULAN_TUJUAN',2)->where('USER_CREATED', Auth::user()->id)->count();

        $pippkditolak  =  USULAN::where('USULAN_STATUS',0)->where('USULAN_TUJUAN',2)->where('USER_CREATED', Auth::user()->id)->count();
                  

        $data = array(
                'tahun'  => $tahun,
                'renjadiproses' => $renjadiproses,
                'renjaditerima' => $renjaditerima,
                'renjaditolak'  => $renjaditolak,
                'pippkdiproses' => $pippkdiproses,
                'pippkditerima' => $pippkditerima,
                'pippkditolak'  => $pippkditolak, 
                'user'  => $user,
                'title' => 'Home' );
        // dd($data);
        return View::make('musrenbang.mobile.dashboard',$data);
        }
    }


    public function kamus($tahun){
         $user = User::where('id',Auth::user()->id)->first();
         //
         $kamus_renja   = Kamus::whereHas('isu',function($q) {
            $q->whereIn('ISU_TIPE',['0','1']);
         })->get();

         $kamus_pippk   = Kamus::whereHas('isu',function($q) {
            $q->where('ISU_TIPE',2);
         })->get();
         
         $data = array(
                'tahun' => $tahun,
                'user'  => $user,
                'title' => 'Daftar Usulan',
                'kamus_renja' => $kamus_renja,
                'kamus_pippk' => $kamus_pippk,
                'active' => "class='active'"
                );
        return View::make('musrenbang.mobile.kamus',$data);
    }



    public function daftarUsulan($tahun,$id){
        //$cekData_1      = Usulan_Rekap::where('USULAN_ID',$id)->where('USULAN_POSISI','1')->first();
        /*$user = User::where('id',Auth::user()->id)->first();
        $rts = '';
        $cekData_1      = Usulan::where('USULAN_ID',$id)->where('USULAN_POSISI','1')->first();
        if(empty($cekData_1)){
            $rw     = Usulan::where('USULAN_ID',$id)->where('USULAN_POSISI','2')->first();
        }       
        else{
            $rw     = $cekData_1;
            
            $rts = RT::whereIn('RT_ID',$rw->RT_ID)->first(); 
            $daftar_rt = RT::whereIn('RT_ID',$rw->RT_ID)->orderBy('RT_NAMA')->get(); 

            //dd($rts->rw->kelurahan->kecamatan);
            
        }



         
        $cekData_2      = Usulan_Rekap::where('USULAN_ID',$id)->where('USULAN_POSISI','2')->first();
        if(empty($cekData_2)){
            $kel     = Usulan::where('USULAN_ID',$id)->where('USULAN_POSISI','2')->first();
        }else{
            $kel     = $cekData_2;
            echo "<br>rw : ".$rw;
        }

        $cekData_3      = Usulan_Rekap::where('USULAN_ID',$id)->where('USULAN_POSISI','3')->first();
        if(empty($cekData_3)){
            $kec     = Usulan::where('USULAN_ID',$id)->where('USULAN_POSISI','3')->first();
        }else{
            $kec     = $cekData_3;
        }
        $cekData_4      = Usulan_Rekap::where('USULAN_ID',$id)->where('USULAN_POSISI','4')->first();
        if(empty($cekData_4)){
            $kota     = Usulan::where('USULAN_ID',$id)->where('USULAN_POSISI','4')->first();
        }else{
            $kota     = $cekData_4;
        }

        $timeline       = Usulan::where('USULAN_ID',$id)->first();


        $data = array(
                    'tahun'     => $tahun,
                    'rw'        => $rw,
                    'kel'       => $kel,
                    'kec'       => $kec,
                    'kota'      => $kota,
                    'timeline'  => $timeline,
                    'rts' => $rts,
                    'daftar_rt' => $daftar_rt,
                    'user' => $user);*/

        return View::make('musrenbang.mobile.daftar-usulan');

        
    }

   /* public function cariKamus($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                    ->orWhere("email", "LIKE", "%$keyword%")
                    ->orWhere("blood_group", "LIKE", "%$keyword%")
                    ->orWhere("phone", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }*/


    public function dashboardUsulan($tahun){
        return View::make('musrenbang.mobile.dashboard-usulan');
    }

    public function kecamatan($tahun){
        //echo "hello";
        return View::make('musrenbang.mobile.kecamatan');
    }

     public function kelurahan($tahun){
        //echo "hello";
        return View::make('musrenbang.mobile.kelurahan');
    }

    public function daftarProgram($tahun){
        //echo "hello";
        return View::make('musrenbang.mobile.daftar-program');
    }

    public function daftarUsulanKecamatan($tahun){
        //echo "hello";
        return View::make('musrenbang.mobile.daftar-usulan-kecamatan');
    }

    public function daftarUsulanKelurahan($tahun){
        //echo "hello";
        return View::make('musrenbang.mobile.daftar-usulan-kelurahan');
    }

    public function profile($tahun){
        //echo "hello";
        return View::make('musrenbang.mobile.profile');
    }

     public function profileRw($tahun){
        //echo "hello";
        return View::make('musrenbang.mobile.profile-rw');
    }

    public function notifikasi($tahun){
        //echo "hello";
        return View::make('musrenbang.mobile.notifikasi');
    }

     public function asistensiUsulan($tahun){
        //echo "hello";
        return View::make('musrenbang.mobile.asistensi-usulan');
    }


    public function asistensiDetail($tahun){
        //echo "hello";
        return View::make('musrenbang.mobile.asistensi-detail');
    }

     public function forgotPassword($tahun){
        //echo "hello";
        return View::make('musrenbang.mobile.forgot-password');
    }

    public function input($tahun){
        //echo "hello";
        return View::make('musrenbang.mobile.input-usulan');
    }

     public function navigation($tahun){
        //echo "hello";
        return View::make('musrenbang.mobile.navigation');
    }

    public function tambah($tahun,$id){
        $isu = Isu::where('ISU_TIPE', '<>', 2)->get();
        $user = User::where('id',Auth::user()->id)->first();

        if($id==2){
            $isu = Isu::where('ISU_TIPE', 2)->orderBy('ISU_ID')->get();
        }

        $kamus      = Kamus::all();
        $lokasi     = RW::whereHas('UserMusren', function($q){
                        $q->where('USER_ID',Auth::user()->id);
                        })->where('RW_TAHUN',$tahun)->first();
        $usulan     = Usulan::all();
        $rt         = RT::where('RW_ID',$lokasi->RW_ID)->orderBy('RT_NAMA')->get();
        //dd($rt);
        $data = array(
                    'lokasi'    =>$lokasi,
                    'tahun'     =>$tahun,
                    'isu'       => $isu,
                    'rt'        => $rt, 
                    'usulan_tujuan' => $id,
                    'user'      => $user );
        //var_dump($data['usulan_tujuan']);
        return View('musrenbang.mobile.input', $data);
    }
    
    public function getKamus($tahun){
        $id = 1;
        $data   = Kamus::where('KAMUS_TAHUN',$tahun)->where('KAMUS_KUNCI','0')->where('ISU_ID',$id)->get();
        $view   = "";
        foreach ($data as $data) {
            $view         = $view."<option value='".$data->KAMUS_ID."' style='color:black'>".$data->KAMUS_NAMA."</option>";
        }
        $tipe   = Isu::where('ISU_ID',$id)->value('ISU_TIPE');
        $out    = array('opt'=>$view,'tipe'=>$tipe);
        return $out;
    }

    public function getSatuan($tahun,$id){
        $data   = Kamus::where('KAMUS_ID',$id)->value('KAMUS_SATUAN');
        return $data;
    }

    

}    