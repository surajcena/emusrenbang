<?php

namespace App\Http\Controllers\Musrenbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Redirect;
use Session;
use Carbon;
use Response;
use Auth;
use App\Model\Tahapan;
class tahapanController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index($tahun){
    	return View('musrenbang.referensi.tahapan',compact('tahun'));
    }

    public function getData($tahun){
    	$data 		= Tahapan::all();
    	$view 		= array();
		$out = array("aaData"=>$data);    	
    	return Response::JSON($out);
    }

    public function getDetail($tahun,$id){
    	$data 		= Tahapan::where('ID_TAHAPAN',$id)->first();
    	return Response::JSON($data);
    }

    public function submit($tahun, Request $request){
    	Tahapan::where('ID_TAHAPAN',$request->id_tahapan)->update(['TGL_AWAL'=>$request->awal,'TGL_AKHIR'=>$request->akhir]);
    	return 1;
    }
}
