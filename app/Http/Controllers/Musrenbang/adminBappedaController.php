<?php

namespace App\Http\Controllers\musrenbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Usulan;
use App\Model\User;

class adminBappedaController extends Controller
{
	public function kecamatan($tahun){
	    return View('musrenbang.adminbappeda.kecamatan',compact('tahun'));
	}

	public function usulan($tahun){
		return View('musrenbang.adminbappeda.usulan',compact('tahun'));	
	}
}
