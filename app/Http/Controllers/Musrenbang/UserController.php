<?php

namespace App\Http\Controllers\Musrenbang;

use Illuminate\Support\Facades\Redis;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function setRedis($id,$name,$title)
    {
        //Redis::set('redisid', $id);
        $data = array(
            'a' => $id,
            'b' => $name,
            'c' => $title
            );

        Redis::set('redisid', serialize($data));
        dd('success!');
    }

    public function getRedis(){
        //dd(Redis::get('redisid'));
        dd(unserialize(Redis::get('redisid')));
    }
}