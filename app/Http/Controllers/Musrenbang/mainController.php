<?php
namespace App\Http\Controllers\Musrenbang;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Session;
use Config;
use Carbon;
use App\tipeUsulan;
use App\isu;
use App\kebutuhan;
use App\Model\User;
use App\Model\Usulan;
use App\Model\Kelurahan;
use App\Model\Sessions;
use App\Model\Stat;
use App\Model\Tahapan;
use App\Model\RW;
use App\Model\Kecamatan;
use Auth;
class mainController extends BaseController
{
    public function __construct(){
        $this->middleware('auth');
    }
    
	public function index(Request $request,$tahun)
    { 
        $tgl    = Carbon\Carbon::now()->format('Y-m-d');
        $jam    = Carbon\Carbon::now()->format('H');
        $stat   = Stat::orderBy('id','desc')->first();
        if($tgl == $stat->TGL){
            if($jam == $stat->JAM){
                Stat::where('TGL',$tgl)->where('JAM',$jam)->update(['ONLINE'=> $stat->ONLINE+1]);
            }else{
                $newjam     = new Stat;
                $newjam->TGL        = $tgl;
                $newjam->JAM        = $jam;
                $newjam->ONLINE     = 1;
                $newjam->save();
            }
        }else{
            $newtgl     = new Stat;
            $newtgl->TGL    = $tgl;
            $newtgl->JAM    = $jam;
            $newtgl->ONLINE = 1;
            $newtgl->save();
        }

        User::where('id',Auth::user()->id)->update(['login'=>1]);        
        $user = User::where('id',Auth::user()->id)->first();

        //total usulan RENJA
          $jumRenProses = USULAN::where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',1)
                        ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',1)->count();
          $jumRenTerima = USULAN::where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',2)
                        ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',1)->count();       
          $jumRenTolak = USULAN::where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',0)
                        ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',1)->count(); 
          $jumRenProses2018 = USULAN::where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',1)
                        ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',1)->count();
          $jumRenTerima2018 = USULAN::where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',2)
                        ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',1)->count();       
          $jumRenTolak2018 = USULAN::where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',0)
                        ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',1)->count();                           

        //total usulan PIPPK                
          $jumPIPPKProses = USULAN::where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',1)
                        ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',2)->count();
          $jumPIPPKTerima = USULAN::where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',2)
                        ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',2)->count(); 
          $jumPIPPKTolak = USULAN::where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',0)
                        ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',2)->count(); 
          $jumPIPPKProses2018 = USULAN::where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',1)
                        ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',2)->count();
          $jumPIPPKTerima2018 = USULAN::where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',2)
                        ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',2)->count(); 
          $jumPIPPKTolak2018 = USULAN::where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',0)
                        ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',2)->count();               

          //jumlah nominal renja RENJA
        $totRenProses = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                        ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',1)
                        ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA'); 
        $totRenTerima = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                        ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',2)
                        ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA');
        $totRenTolak = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                        ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',0)
                        ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA');
        $totRenProses2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                        ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',1)
                        ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA'); 
        $totRenTerima2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                        ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',2)
                        ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA');
        $totRenTolak2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                        ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',0)
                        ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA');                

          //jumlah nominal renja PIPPK
        $totPIPPKProses = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                        ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',1)
                        ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA'); 
        $totPIPPKTerima = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                        ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',2)
                        ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA');
        $totPIPPKTolak = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                        ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',0)
                        ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA');
        $totPIPPKProses2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                        ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',1)
                        ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA'); 
        $totPIPPKTerima2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                        ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',2)
                        ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA');
        $totPIPPKTolak2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                        ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',0)
                        ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA');                

        //$info = 'Selamat Datang Dalam web emusrenbang Kota Bandung '  

        $now     = Carbon\Carbon::now()->format('Y-m-d');
        $tahapan1 = Tahapan::where('MENU',1)->first();
        if($now >= $tahapan1->TGL_AWAL && $now <= $tahapan1->TGL_AKHIR){
            $t_rembuk = 0;
        }elseif($now > $tahapan1->TGL_AKHIR){
            $t_rembuk = 1;  
        }else{
            $t_rembuk = 2;
        }   
        $tahapan2 = Tahapan::where('MENU',2)->first();
        if($now >= $tahapan2->TGL_AWAL && $now <= $tahapan2->TGL_AKHIR){
            $t_kel = 0;
        }elseif($now > $tahapan2->TGL_AKHIR){
            $t_kel = 1;  
        }else{
            $t_kel = 2;
        }   
        $tahapan3 = Tahapan::where('MENU',3)->first();
        if($now >= $tahapan3->TGL_AWAL && $now <= $tahapan3->TGL_AKHIR){
            $t_kec = 0;
        }elseif($now > $tahapan3->TGL_AKHIR){
            $t_kec = 1;  
        }else{
            $t_kec = 2;
        }   
        $tahapan4 = Tahapan::where('MENU',4)->first();
        if($now >= $tahapan4->TGL_AWAL && $now <= $tahapan4->TGL_AKHIR){
            $t_skpd = 0;
        }elseif($now > $tahapan4->TGL_AKHIR){
            $t_skpd = 1;  
        }else{
            $t_skpd = 2;
        }   
        $tahapan5 = Tahapan::where('MENU',5)->first();
        if($now >= $tahapan5->TGL_AWAL && $now <= $tahapan5->TGL_AKHIR){
            $t_pippk = 0;
        }elseif($now > $tahapan5->TGL_AKHIR){
            $t_pippk = 1;  
        }else{
            $t_pippk = 2;
        }         

        $keluargaRw = RW::where('KEL_ID',Auth::user()->KEL_ID)->orderBy('RW_NAMA')->get(); 
        $keluargaKel = Kelurahan::where('KEC_ID',Auth::user()->KEC_ID)->orderBy('KEL_NAMA')->get(); 

        $nominal = Usulan::getNominalUsulan();


        $data = array(
                'tahun'             => $tahun,
                'user'              => $user,
                'totRenProses'      => $totRenProses,
                'totRenTerima' => $totRenTerima,
                'totRenTolak' => $totRenTolak,
                'totPIPPKProses' => $totPIPPKProses,
                'totPIPPKTerima' => $totPIPPKTerima,
                'totPIPPKTolak' => $totPIPPKTolak,
                'totRenProses2018' => $totRenProses2018,
                'totRenTerima2018' => $totRenTerima2018,
                'totRenTolak2018' => $totRenTolak2018,
                'totPIPPKProses2018' => $totPIPPKProses2018,
                'totPIPPKTerima2018' => $totPIPPKTerima2018,
                'totPIPPKTolak2018' => $totPIPPKTolak2018,
                't_rembuk' => $t_rembuk,
                't_pippk' => $t_pippk,
                't_kel' => $t_kel,
                't_kec' => $t_kec,
                't_skpd' => $t_skpd,
                'tahapan1' => $tahapan1,
                'tahapan2' => $tahapan2,
                'tahapan3' => $tahapan3,
                'tahapan4' => $tahapan4,
                'tahapan5' => $tahapan5,
                'keluargaRw' => $keluargaRw,
                'keluargaKel' => $keluargaKel,
                'jumRenProses' => $jumRenProses,
                'jumRenTerima' => $jumRenTerima,
                'jumRenTolak' => $jumRenTolak,
                'jumPIPPKProses' => $jumPIPPKProses,
                'jumPIPPKTerima' => $jumPIPPKTerima,
                'jumPIPPKTolak' => $jumPIPPKTolak,
                'jumRenProses2018' => $jumRenProses2018,
                'jumRenTerima2018' => $jumRenTerima2018,
                'jumRenTolak2018' => $jumRenTolak2018,
                'jumPIPPKProses2018' => $jumPIPPKProses2018,
                'jumPIPPKTerima2018' => $jumPIPPKTerima2018,
                'jumPIPPKTolak2018' => $jumPIPPKTolak2018,
                'nominal'=>$nominal
                );

        //dd($data);
        if (Auth::user()->level != 1) {
            # code...
            return Redirect('/musrenbang/2017/usulan/dashboard');
        } else
            return view('musrenbang.index',$data);
    }

    public function logout(){
        User::where('id',Auth::user()->id)->update(['login'=>0]);
        return Redirect('/logout');
    }
}
