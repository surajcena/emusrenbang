<?php
namespace App\Http\Controllers\Musrenbang;

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Session;
use Carbon;
use Response;
use Auth;
use DB;
use App\Model\Isu;
use App\Model\Kamus;
use App\Model\Kecamatan;
use App\Model\Kelurahan;
use App\Model\RT;
use App\Model\RW;
use App\Model\Usulan;
use App\Model\Usulan_Rekap;
use App\Model\Usulan_Rekap_Kec;
use App\Model\Usulan_Rekap_SKPD;
use App\Model\Usulan_Rekap_Kab;
use App\Model\UserMusren;
use App\Model\Tujuan;
use App\Model\Notification_log;
use Validator;
use App\Model\User;
use App\Model\BeritaAcara;
use App\Model\Tahapan;
use Image;
use App\Model\BeritaAcaraFoto;
use App\Model\SKPD;
use Excel;
use Pheanstalk\Pheanstalk;
use Log;
use App\Model\Kriteria;
use App\Model\Usulan_kriteria;
use App\Model\Detail_kriteria;
use App\Model\Usulan2018;
use App\Model\Perencanaan;
use App\Model\Satuan;

class usulanController extends BaseController
{
    use ValidatesRequests;
    public function __construct(){
        $this->middleware('auth');
    }

    private function _parseUserEmail(){
        //diparsing dan dikurangi
        $name = explode(".", Auth::user()->email);

        $ret = $name[0];
        for ($i=1; $i < (count($name)-1); $i++) {
            $ret = $ret.'.'.$name[$i];
        }

        return $ret;
    }

    public function setPriority($usulan_id, $kel_id, $set_usulan_prioritas){
        $pheanstalk = new Pheanstalk(env('BEANSTALK_HOST'));
        var_dump($pheanstalk->useTube('musrenbang-priority')->put("updateprioritykelurahan-".$usulan_id."-".$kel_id."-".$set_usulan_prioritas));
    }

    public function usulanperskpd($tahun)
    {
        return View('musrenbang.usulan.skpd',compact('tahun'));
    }
    public function usulanperkecamatan($tahun)
    {
        return View('musrenbang.usulan.kecamatan',compact('tahun'));
    }

    public function detailusulanperskpd($tahun,$id)
    {
        $skpd = SKPD::find($id);
        return View('musrenbang.usulan.detailskpd',compact('tahun','id','skpd'));
    }
    public function detailusulankecamatan($tahun,$id)
    {
        $kecamatan = Kecamatan::find($id);
        return View('musrenbang.usulan.detailkecamatan',compact('tahun','id','kecamatan'));
    }

    public function usulanperpengusul($tahun)
    {
        return View('musrenbang.usulan.pengusul',compact('tahun'));
    }

    public function getdetailusulanperskpd($tahun,$id)
    {
        $level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();


        if(Auth::user()->level==5)
        {
            $ids = Auth::user()->kelurahan->user->whereIn('level',$level)->pluck('id')->all();
        }
        else if(Auth::user()->level==6)
        {
            $ids = Auth::user()->kecamatan->user->whereIn('level',$level)->pluck('id')->all();
        }

        $skpd = SKPD::where('SKPD_ID',$id)->with(
            [
                'usulan'=>function($usulan) use ($ids)
                {
                    $usulan->whereIn('USER_CREATED',$ids);
                },
                'usulan.kamus',
                'usulan.user',
                'usulan.user.kelurahan',
                'usulan.user.rw',
                'usulan.user.kecamatan',
                'usulan.kamus',
                'usulan.kamus.isu'
            ]
        )->first();

        $response = array();

        $no = 1;
        foreach($skpd->usulan->sortBy('USER_CREATED') as $usulan)
        {
            if($usulan->USULAN_STATUS == 1) $status = '<i class="fa fa-refresh text-info"></i>';
            elseif($usulan->USULAN_STATUS == 2) $status = '<i class="fa fa-check text-success"></i>';
            else $status = '<i class="fa fa-close text-danger"></i>';

            if($usulan->USULAN_POSISI == 1) $posisi = ' Kelurahan';
            elseif($usulan->USULAN_POSISI == 2) $posisi = ' Kecamatan';
            elseif($usulan->USULAN_POSISI == 3) $posisi = ' Kota';
            else $posisi = ' APBD';

            $pengusul = $usulan->user->email;

            $isukamus = $usulan->kamus->isu->ISU_NAMA."<br>".$usulan->kamus->KAMUS_NAMA;

            $urgensi = $usulan->USULAN_URGENSI;

            $lokasi = "Kecamatan ".$usulan->user->kecamatan->KEC_NAMA;

            if($usulan->user->level != 6)
            {
                $lokasi = $lokasi."<br> Kelurahan ".$usulan->user->kelurahan->KEL_NAMA;
                if($usulan->user->level == 1)
                {
                    $lokasi = $lokasi."<br> RW ".$usulan->user->rw->RW_NAMA;
                }
            }

            $alamat = $usulan->ALAMAT;

            $volume = number_format($usulan->USULAN_VOLUME,0,'.',',')." ".$usulan->kamus->KAMUS_SATUAN;

            $harga = $usulan->kamus->KAMUS_HARGA;

            $nominal = $usulan->USULAN_VOLUME * $usulan->kamus->KAMUS_HARGA;

            $beritaacara = "";



            array_push($response, array(
                                        'NO'                    =>  $no,
                                        'PENGUSUL'              =>  $pengusul,
                                        'ISUKAMUS'              =>  $isukamus,
                                        'LOKASI'                =>  $lokasi,
                                        'URGENSI'               =>  $urgensi,
                                        'LOKASI'                =>  $lokasi,
                                        'ALAMAT'                =>  $alamat,
                                        'VOLUME'                =>  $volume,
                                        'HARGA'                 =>  number_format($harga,0,'.',','),
                                        'NOMINAL'               =>  number_format($nominal,0,'.',','),
                                        'STATUS'                =>  "<p>".$status.$posisi."</p>",
                                        'BERITAACARA'           =>  $beritaacara,
                                     ));
            $no++;
        }
        $response = array("aaData"=>$response);
        return Response::json($response);
    }

    public function exportdetailusulanperskpd($tahun,$id)
    {
        $level = [1,2,3,4,5];
        $ids = Auth::user()->kelurahan->user->whereIn('level',$level)->pluck('id')->all();

        if(Auth::user()->level==6)
        {
            $ids = Auth::user()->kecamatan->user->whereIn('level',$level)->pluck('id')->all();
        }

        $skpd = SKPD::where('SKPD_ID',$id)->with(
            [
                'usulan'=>function($usulan) use ($ids)
                {

                    $usulan->whereIn('USER_CREATED',$ids)->UsulanKecamatan;
                },
                'usulan.kamus',
                'usulan.user',
                'usulan.user.kelurahan',
                'usulan.user.rw',
                'usulan.user.kecamatan',
                'usulan.kamus',
                'usulan.kamus.isu'
            ]
        )->first();

        $response = array();

        $no = 1;
        foreach($skpd->usulan->sortBy('USER_CREATED') as $usulan)
        {
            if($usulan->USULAN_STATUS == 1) $status = '<i class="fa fa-refresh text-info"></i>';
            elseif($usulan->USULAN_STATUS == 2) $status = '<i class="fa fa-check text-success"></i>';
            else $status = '<i class="fa fa-close text-danger"></i>';

            if($usulan->USULAN_POSISI == 1) $posisi = ' Kelurahan';
            elseif($usulan->USULAN_POSISI == 2) $posisi = ' Kecamatan';
            elseif($usulan->USULAN_POSISI == 3) $posisi = ' Kota';
            else $posisi = ' APBD';

            $pengusul = $usulan->user->email;

            $isukamus = $usulan->kamus->isu->ISU_NAMA."<br>".$usulan->kamus->KAMUS_NAMA;

            $urgensi = $usulan->USULAN_URGENSI;

            $lokasi = "Kecamatan ".$usulan->user->kecamatan->KEC_NAMA;

            if($usulan->user->level != 6)
            {
                $lokasi = $lokasi."<br> Kelurahan ".$usulan->user->kelurahan->KEL_NAMA;
                if($usulan->user->level == 1)
                {
                    $lokasi = $lokasi."<br> RW ".$usulan->user->rw->RW_NAMA;
                }
            }

            $alamat = $usulan->ALAMAT;

            $volume = number_format($usulan->USULAN_VOLUME,0,'.',',')." ".$usulan->kamus->KAMUS_SATUAN;

            $harga = $usulan->kamus->KAMUS_HARGA;

            $nominal = $usulan->USULAN_VOLUME * $usulan->kamus->KAMUS_HARGA;

            $beritaacara = "";



            array_push($response, array(
                                        'NO'                    =>  $no,
                                        'PENGUSUL'              =>  $pengusul,
                                        'ISUKAMUS'              =>  $isukamus,
                                        'LOKASI'                =>  $lokasi,
                                        'URGENSI'               =>  $urgensi,
                                        'LOKASI'                =>  $lokasi,
                                        'ALAMAT'                =>  $alamat,
                                        'VOLUME'                =>  $volume,
                                        'HARGA'                 =>  number_format($harga,0,'.',','),
                                        'NOMINAL'               =>  number_format($nominal,0,'.',','),
                                        'STATUS'                =>  "<p>".$status.$posisi."</p>",
                                        'BERITAACARA'           =>  $beritaacara,
                                     ));
            $no++;
        }

        Excel::create('Detail Usulan Per Pengusul', function($excel) use ($response) {

            $excel->sheet('Sheet 1', function($sheet) use ($response) {

                $sheet->fromArray($response, null, 'A1', true);

            });

        })->export('xls');
    }

    public function getusulanperskpd($tahun)
    {
        $level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();

        if(Auth::user()->level==5)
        {
            $ids = Auth::user()->kelurahan->user->whereIn('level',$level)->pluck('id')->all();
        }
        else if(Auth::user()->level==6)
        {
            $ids = Auth::user()->kecamatan->user->whereIn('level',$level)->pluck('id')->all();
        }

        $level = Auth::user()->level;
        $skpds = SKPD::with(
            [
                "usulan"=>function($usulan) use ($ids,$level)
                {
                    $usulan->whereIn('USER_CREATED',$ids)->NotDummies();
                    if($level == 6)
                    {
                        $usulan->UsulanKecamatan();
                    }
                    else if($level == 8)
                    {
                        $usulan->UsulanBappeda();
                    }
                },
                "usulan.kamus",
            ]
        )
        ->get();

        $response = array();
        $no = 1;
        foreach($skpds as $skpd)
        {
            $usulan = $skpd->usulan;

            $renja = $usulan->where('USULAN_TUJUAN',1);

            $jumlahrenja = $renja->count();

            $jumlahpengusul = $renja->unique('USER_CREATED')->count();

            array_push($response, array(
                                     'SKPDID'              => $skpd->SKPD_ID,
                                     'NO'                  => $no,
                                     'KODESKPD'            => $skpd->SKPD_KODE,
                                     'NAMASKPD'            => $skpd->SKPD_NAMA,
                                     'JUMLAHRENJA'         => $jumlahrenja,
                                     'JUMLAHPENGUSUL'      => $jumlahpengusul,
                                     'TOTALNOMINAL'        => number_format($skpd->totalNominal(1),0,'.',','),
                                     ));
            $no++;
        }
        $response = array("aaData"=>$response);
        return Response::json($response);
    }
    public function getusulanperpengusul($tahun)
    {
        $level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();

        if(Auth::user()->level==5)
        {
            $ids = Auth::user()->kelurahan->user->whereIn('level',$level)->pluck('id')->all();
        }
        else if(Auth::user()->level==6)
        {
            $ids = Auth::user()->kecamatan->user->whereIn('level',$level)->pluck('id')->all();
        }
        $level = Auth::user()->level;
        $users = User::with(
            [
                "usulan"=>function($usulan) use ($level){
                    if($level == 6)
                    {
                        $usulan->UsulanKecamatan();
                    }
                    else if ($level == 8)
                    {
                        $usulan->UsulanBappeda();
                    }
                },
                "usulan.kamus",
            ]
        )
        ->whereIn('id',$ids)
        ->get();

        $response = array();
        $no = 1;
        foreach($users as $user)
        {
            $usulan = $user->usulan;

            $renja = $usulan->where('USULAN_TUJUAN',1);

            $jumlahrenja = $renja->count();

            $jumlahpengusulrenja = $renja->unique('USER_CREATED')->count();

            $pippk = $usulan->where('USULAN_TUJUAN',2);

            $jumlahpippk = $pippk->count();

            $jumlahpengusulpippk = $pippk->unique('USER_CREATED')->count();

            array_push($response, array(
                                     'USERID'                   => $user->id,
                                     'NO'                       => $no,
                                     'PENGUSUL'                 => $user->email,
                                     'JUMLAHRENJA'              => $jumlahrenja,
                                     'JUMLAHPENGUSULRENJA'      => $jumlahpengusulrenja,
                                     'TOTALNOMINALRENJA'        => number_format($user->totalNominal(1),0,'.',','),
                                     'JUMLAHPIPPK'              => $jumlahpippk,
                                     'JUMLAHPENGUSULPIPPK'      => $jumlahpengusulpippk,
                                     'TOTALNOMINALPIPPK'        => number_format($user->totalNominal(2),0,'.',','),
                                     ));
            $no++;
        }
        $response = array("aaData"=>$response);
        return Response::json($response);
    }

    public function exportusulanperpengusul($tahun)
    {
        $level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();

        if(Auth::user()->level==5)
        {
            $ids = Auth::user()->kelurahan->user->whereIn('level',$level)->pluck('id')->all();
        }
        else if(Auth::user()->level==6)
        {
            $ids = Auth::user()->kecamatan->user->whereIn('level',$level)->pluck('id')->all();
        }

        $level = Auth::user()->level;
        $users = User::with(
            [
                "usulan"=>function($usulan) use ($level){
                    if($level == 6)
                    {
                        $usulan->UsulanKecamatan();
                    }
                    else if($level == 8)
                    {
                        $usulan->UsulanBappeda();
                    }
                },
                "usulan.kamus"
            ]
        )
        ->whereIn('id',$ids)
        ->get();



        $response = array();
        $no = 1;
        foreach($users as $user)
        {
            $usulan = $user->usulan;

            $renja = $usulan->where('USULAN_TUJUAN',1);

            $jumlahrenja = $renja->count();

            $jumlahpengusulrenja = $renja->unique('USER_CREATED')->count();

            $pippk = $usulan->where('USULAN_TUJUAN',2);

            $jumlahpippk = $pippk->count();

            $jumlahpengusulpippk = $pippk->unique('USER_CREATED')->count();

            array_push($response, array(
                                     'NO'                       => $no,
                                     'PENGUSUL'                 => $user->email,
                                     'JUMLAHRENJA'              => $jumlahrenja,
                                     'JUMLAHPENGUSULRENJA'      => $jumlahpengusulrenja,
                                     'TOTALNOMINALRENJA'        => $user->totalNominal(1),
                                     'JUMLAHPIPPK'              => $jumlahrenja,
                                     'JUMLAHPENGUSULPIPPK'      => $jumlahpengusulpippk,
                                     'TOTALNOMINALPIPPK'        => $user->totalNominal(2),
                                     ));
            $no++;
        }

        Excel::create('Usulan Per Pengusul', function($excel) use ($response) {

            $excel->sheet('Sheet 1', function($sheet) use ($response) {
                $header = [
                    "",
                    "",
                    "Jumlah",
                    "Jumlah Pengusul",
                    "Nominal Usulan",
                    "Jumlah",
                    "Jumlah Pengusul",
                    "Nominal Usulan",
                ];
                $sheet->setColumnFormat(array(
                    'C' => "#,##0",
                    'D' => "#,##0",
                    'E' => "#,##0",
                    'F' => "#,##0",
                    'G' => "#,##0",
                    'H' => "#,##0",
                ));
                $sheet->row(1,["No","Pengusul","Renja","","","PIPPK"]);
                $sheet->row(2,$header);
                $sheet->fromArray($response, null, 'A3', true,false);
                $sheet->setBorder('A1:H'.(count($response)+2),"thin");
                $sheet->mergeCells('C1:E1');
                $sheet->mergeCells('F1:H1');
                $sheet->mergeCells('A1:A2');
                $sheet->mergeCells('B1:B2');
                $sheet->cells('A1:H2',function($cells){
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setFontWeight("bold");
                    //$cells->setBorder('solid','solid','solid','solid');
                });

                $sheet->setAutoFilter('A2:H'.(count($response)+2));
            });

        })->export('xls');

    }

    public function exportusulanperskpd($tahun)
    {
        $level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();

        if(Auth::user()->level==6)
        {
            $ids = Auth::user()->kecamatan->user->whereIn('level',$level)->pluck('id')->all();
        }
        $level = Auth::user()->level;
        $skpds = SKPD::with(
            [
                "usulan"=>function($usulan) use ($ids,$level)
                {
                    $usulan->whereIn('USER_CREATED',$ids);
                    if($level == 6)
                    {
                        $usulan->UsulanKecamatan();
                    }
                    else if($level == 8)
                    {
                        $usulan->UsulanBappeda();
                    }
                },
                "usulan.kamus"
            ]
        )
        ->get();

        $response = array();
        $no = 1;
        foreach($skpds as $skpd)
        {
            $usulan = $skpd->usulan;

            $renja = $usulan->where('USULAN_TUJUAN',1);

            $jumlahrenja = $renja->count();

            $jumlahpengusul = $renja->unique('USER_CREATED')->count();

            array_push($response, array(
                                     'NO'                  => $no,
                                     'KODESKPD'            => $skpd->SKPD_KODE,
                                     'NAMASKPD'            => $skpd->SKPD_NAMA,
                                     'JUMLAHRENJA'         => $jumlahrenja,
                                     'JUMLAHPENGUSUL'      => $jumlahpengusul,
                                     'TOTALNOMINAL'        => $skpd->totalNominal(1),
                                     ));
            $no++;
        }

        Excel::create('Usulan Per SKPD', function($excel) use ($response) {
            $excel->sheet('Sheet 1', function($sheet) use ($response) {
                $header = [
                    "No",
                    "Kode SKPD",
                    "Nama SKPD",
                    "Jumlah RENJA",
                    "Jumlah Pengusul",
                    "Total Nominal Usulan",
                ];
                $sheet->setColumnFormat(array(
                    'C' => "#,##0",
                    'D' => "#,##0",
                    'E' => "#,##0",
                    'F' => "#,##0",
                ));
                $sheet->row(1,$header);
                $sheet->fromArray($response, null, 'A2', true,false);
                $sheet->setBorder('A1:F'.(count($response)+1),"thin");
                $sheet->cells('A1:F1',function($cells){
                    $cells->setAlignment('center');
                    $cells->setFontWeight("bold");
                    //$cells->setBorder('solid','solid','solid','solid');
                });

                $sheet->setAutoFilter('A1:F'.(count($response)+1));

            });

        })->export('xls');
    }

    public function index($tahun){
        $user = User::where('id',Auth::user()->id)->first();
        //dd($user);
        return View::make('musrenbang.usulan.index',['user'=>$user])->with('tahun', $tahun);
    }
    public function indextujuan($tahun,$tujuan){
        $user = User::where('id',Auth::user()->id)->first();
        $usulan = Usulan::
                    join('REFERENSI.REF_KAMUS', 'REF_KAMUS.KAMUS_ID', '=', 'DAT_USULAN.KAMUS_ID')
                  ->where('USER_CREATED',Auth::user()->id)->where('USULAN_POSISI',1)
                  ->where('USULAN_TAHUN',$tahun)
                  ->get();
        $sum=0;
        foreach ($usulan as $usul) {
                    $sum+= $usul->KAMUS_HARGA;
                  }

        $diproses =  USULAN::where('USULAN_STATUS','1')
                            ->where('USULAN_TUJUAN',$tujuan)
                            ->where('USER_CREATED', Auth::user()->id)
                            ->where('USULAN_TAHUN',$tahun)
                            ->where('USULAN_DELETED',0)->count();


        $diterima =  USULAN::where(function($q) {
                      $q->where([
                            ['USULAN_POSISI', '=', '1'],
                            ['USULAN_STATUS', '=', '2'],
                        ]);
                      $q->orwhere([
                            ['USULAN_POSISI', '=', '2'],
                            ['USULAN_STATUS', '=', '2'],
                        ]);
                      $q->orwhere([
                            ['USULAN_POSISI', '=', '3'],
                            ['USULAN_STATUS', '=', '2'],
                        ]);
                      $q->orwhere([
                            ['USULAN_POSISI', '=', '4'],
                            ['USULAN_STATUS', '=', '2'],
                        ]);
                      $q->orwhere([
                            ['USULAN_POSISI', '=', '5'],
                            ['USULAN_STATUS', '=', '2'],
                        ]);
                  })->where('USULAN_TUJUAN',$tujuan)->where('USER_CREATED', Auth::user()->id)
        ->where('USULAN_DELETED',0)->where('USULAN_TAHUN',$tahun)->count();
        //dd($diterima);


        $ditolak  =  USULAN::where('USULAN_STATUS',0)
                          ->where('USULAN_TUJUAN',$tujuan)
                          ->where('USULAN_TAHUN',$tahun)
                          ->where('USER_CREATED', Auth::user()->id)
                          ->where('USULAN_DELETED',0)->count();

        $data = array(
                'tahun'  => $tahun,
                'user'   => $user,
                'nama'   => $tujuan,
                'sum'    => number_format($sum,0,'.',','),
                'diproses' => $diproses,
                'diterima' => $diterima,
                'ditolak'  => $ditolak,  );

        return View::make('musrenbang.usulan.index',$data);
    }


    public function indexTujuanPrioritas($tahun,$tujuan){
        $user = User::where('id',Auth::user()->id)->first();
        $usulan = Usulan::
                    join('REFERENSI.REF_KAMUS', 'REF_KAMUS.KAMUS_ID', '=', 'DAT_USULAN.KAMUS_ID')
                  ->where('USER_CREATED',Auth::user()->id)->where('USULAN_POSISI',1)
                  ->where('USULAN_TAHUN',$tahun)
                  ->get();
        $sum=0;
        foreach ($usulan as $usul) {
                    $sum+= $usul->KAMUS_HARGA;
                  }

        $diproses =  USULAN::where('USULAN_STATUS','1')
                            ->where('USULAN_TUJUAN',$tujuan)
                            ->where('USER_CREATED', Auth::user()->id)
                            ->where('USULAN_TAHUN',$tahun)
                            ->where('USULAN_DELETED',0)
                            ->where('USULAN_PRIORITAS',1)->count();

        $diterima =  USULAN::where(function($q) {
                      $q->where([
                            ['USULAN_POSISI', '=', '1'],
                            ['USULAN_STATUS', '=', '2'],
                        ]);
                      $q->orwhere([
                            ['USULAN_POSISI', '=', '2'],
                            ['USULAN_STATUS', '=', '2'],
                        ]);
                      $q->orwhere([
                            ['USULAN_POSISI', '=', '3'],
                            ['USULAN_STATUS', '=', '2'],
                        ]);
                      $q->orwhere([
                            ['USULAN_POSISI', '=', '4'],
                            ['USULAN_STATUS', '=', '2'],
                        ]);
                  })->where('USULAN_TUJUAN',$tujuan)->where('USER_CREATED', Auth::user()->id)
        ->where('USULAN_DELETED',0)->where('USULAN_TAHUN',$tahun)->count();

        $ditolak  =  USULAN::where('USULAN_STATUS',0)
                          ->where('USULAN_TUJUAN',$tujuan)
                          ->where('USULAN_TAHUN',$tahun)
                          ->where('USER_CREATED', Auth::user()->id)
                          ->where('USULAN_DELETED',0)->count();

        $data = array(
                'tahun'  => $tahun,
                'user'   => $user,
                'nama'   => $tujuan,
                'sum'    => number_format($sum,0,'.',','),
                'diproses' => $diproses,
                'diterima' => $diterima,
                'ditolak'  => $ditolak,  );

        return View::make('musrenbang.usulan.index_prioritas',$data);
    }


    public function daftarUsulan($tahun){

        return View::make('musrenbang.usulan.daftar_usulan',['tahun'=>$tahun]);
    }

    public function daftarUsulan1($tahun){
        $pippk     = Usulan::wherehas('kamus',function($q){
                    $q->whereHas('isu',function($x){
                        $x->where('ISU_TIPE','2');
                    });
                })->count();
        $renja  = Usulan::wherehas('kamus',function($q){
                    $q->whereHas('isu',function($x){
                        $x->whereIn('ISU_TIPE',['0','1']);
                    });
                })->count();
        $o  = User::where('login',1)->count();
        $rw     = User::where('app',1)->where('level',1)->count();
        $rwv    = User::where('app',1)->where('level',1)->where('validasi',1)->count();
        $lpm    = User::where('app',1)->where('level',2)->count();
        $lpmv   = User::where('app',1)->where('level',2)->where('validasi',1)->count();
        $pkk    = User::where('app',1)->where('level',3)->count();
        $pkkv   = User::where('app',1)->where('level',3)->where('validasi',1)->count();
        $karta  = User::where('app',1)->where('level',4)->count();
        $kartav = User::where('app',1)->where('level',4)->where('validasi',1)->count();

        $rwpernahusulan = User::where('app',1)->where('level',1)->wherehas('usulan')->count();



        $data   = array('tahun' => $tahun,
                        'rw'=>$rw,
                        'rwv'=>$rwv,
                        'pippk'=>$pippk,
                        'renja'=>$renja,
                        'pkk'=>$pkk,
                        'pkkv'=>$pkkv,
                        'lpm'=>$lpm,
                        'lpmv'=>$lpmv,
                        'karta'=>$karta,
                        'kartav'=>$kartav,
                        'o'=>$o,
                        'rwpernahusulan'=>$rwpernahusulan);
        return View('musrenbang.usulan.daftarUsulan',$data);
    }


    public function tambah($tahun,$id){

        if($tahun!='2018'){
            return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','bukan tahun perencanaan pembangunan');
        }
        $user = User::where('id',Auth::user()->id)->first();
        $isu='';

        if($id==1 && Auth::user()->level==1 || Auth::user()->level==5 || Auth::user()->level==6){
            $isu = Isu::whereIn('ISU_TIPE', [0,1])->where('ISU_TAHUN',$tahun)->get();
        }
        elseif($id==2){

            if(Auth::user()->level==1 || Auth::user()->level==5){
                $isu = Isu::where('ISU_TIPE', 2)->where('ISU_TAHUN',$tahun)->orderBy('ISU_ID')->get();
            }
            elseif(Auth::user()->level==2){
                $isu = Isu::where('ISU_TIPE', 3)->where('ISU_TAHUN',$tahun)->orderBy('ISU_ID')->get();
            }
            elseif(Auth::user()->level==3){
                $isu = Isu::where('ISU_TIPE', 5)->where('ISU_TAHUN',$tahun)->orderBy('ISU_ID')->get();
            }
            elseif(Auth::user()->level==4){
                $isu = Isu::where('ISU_TIPE', 4)->where('ISU_TAHUN',$tahun)->orderBy('ISU_ID')->get();
            }

        }
        else{
            return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','tidak di perkenankan mengusulkan');
        }

       /* $lokasi     = RW::whereHas('usermusren', function($q){
                        $q->where('USER_ID',Auth::user()->id);
                        })->where('RW_TAHUN',$tahun)->first();*/
//        dd($lokasi);

        if (Auth::user()->level==1){
            $rt         = RT::where('RW_ID',Auth::user()->RW_ID)->orderBy('RT_NAMA')->get();
        }elseif (Auth::user()->level==5) {
            $rt         = RW::where('KEL_ID',Auth::user()->KEL_ID)->orderBy('RW_NAMA')->get();
        }elseif (Auth::user()->level==6) {
            $rt         = Kelurahan::where('KEC_ID',Auth::user()->KEC_ID)->orderBy('KEL_NAMA')->get();
        }else{
           $rt = '';
        }

        $satuan = Satuan::take(10)->get();


        $data = array(
                    //'lokasi'    =>$lokasi,
                    'tahun'     =>$tahun,
                    'isu'       => $isu,
                    'rt'        => $rt,
                    'usulan_tujuan' => $id,
                    'user'  => $user,
                    'satuan'  => $satuan
                    );

        if($id==1){
            //batas pengelolaan tahapan renja
            $now    = Carbon\Carbon::now()->format('Y-m-d');
            $tahapan    = Tahapan::where('MENU',1)->first();
            if($now >= $tahapan->TGL_AWAL && $now <= $tahapan->TGL_AKHIR){
                //if(Auth::user()->level==1)
                    return View('musrenbang.usulan.add', $data);
                /*elseif(Auth::user()->level==5 || Auth::user()->level==6)
                    return View('musrenbang.usulan.add-ori', $data);*/
            }else{
                return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','Tahapan input RENJA telah berakhir');
            }

        }elseif($id==2){
            //batas pengelolaan pippk
            $now    = Carbon\Carbon::now()->format('Y-m-d');
            $tahapan    = Tahapan::where('MENU',5)->first();
            if($now >= $tahapan->TGL_AWAL && $now <= $tahapan->TGL_AKHIR){
                if(Auth::user()->level==1 || Auth::user()->level==2 || Auth::user()->level==3 || Auth::user()->level==4)
                    return View('musrenbang.usulan.add', $data);
                elseif(Auth::user()->level==5 || Auth::user()->level==6)
                    return View('musrenbang.usulan.add-ori', $data);
            }else{
                return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','Tahapan input PIPPK telah berakhir');
            }
            //verifikasi jumlah pippk
            if(Auth::user()->level==1){
                $jumlah_pagu = Usulan::leftJoin('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')->where('USULAN_TAHUN',$tahun)->where('USER_CREATED', Auth::user()->id)->where('USULAN_TUJUAN', $id)->where('USULAN_DELETED',0)->sum('KAMUS_HARGA');

                if($jumlah_pagu>=120000000){
                    return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','usulan anda di tolak karena usulan RW untuk PIPPK maksimal dana 100 jt');
                }
            }

            elseif(Auth::user()->level==2 || Auth::user()->level==3 || Auth::user()->level==4){
                    $jumlah_pagu = Usulan::leftJoin('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')->where('USULAN_TAHUN',$tahun)->where('USER_CREATED', Auth::user()->id)->where('USULAN_TUJUAN', $id)->where('USULAN_DELETED',0)->sum('KAMUS_HARGA');

                    if($jumlah_pagu>=120000000){
                        return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','usulan anda di tolak karena usulan RW untuk PIPPK maksimal dana 100 jt');
                    }

                }


        }

    }


    public function detail($tahun,$id){
        $usulan = Usulan::where('USULAN_ID',$id)->where('USULAN_TAHUN',$tahun)->first();

        if(empty($usulan->RT_ID)){
            $daftar_rt = "-";
        }else
            $daftar_rt = RT::whereIn('RT_ID',$usulan->RT_ID)->orderBy('RT_NAMA')->get();

        $data = array(
                    'tahun'      => $tahun,
                    'usulan'     => $usulan,
                    'daftar_rt'  => $daftar_rt);

        return View::make('musrenbang.usulan.detail',$data);
    }

    public function edit($tahun,$id){
        $user = User::where('id',Auth::user()->id)->first();
      $isu            = Isu::where('ISU_TAHUN',$tahun)->get();
        $kamus          = Kamus::where('KAMUS_TAHUN',$tahun)->get();
        $usulan         = Usulan::find($id);
        $lokasi         = RW::whereHas('UserMusren', function($q){
                        $q->where('USER_ID',Auth::user()->id);
                        })->where('RW_TAHUN',$tahun)->first();
      $rt             =  RT::where('RW_ID',Auth::user()->RW_ID)->get();
      $cekData_1      = Usulan_Rekap::where('USULAN_ID',$id)->where('USULAN_POSISI','1')->first();

        if(empty($cekData_1)){
            $rw     = Usulan::where('USULAN_ID',$id)->where('USULAN_POSISI','1')->first();
        }else{
            $rw     = $cekData_1;
        }

        $data = array(
                    'tahun'     => $tahun,
                    'isu'       => $isu,
                    'kamus'     => $kamus,
                    'rw'        => $rw,
                    'rt'        => $rt,
                    'usulan'      => $usulan,
                    'user'    => $user );

        $data['log_id'] = 0; //unused anymore
        return View::make('musrenbang.usulan.edit')->with($data);
    }


     public function editPrioritas($tahun,$id){
        $user = User::where('id',Auth::user()->id)->first();
        $isu            = Isu::all();
        $kamus          = Kamus::all();
        $usulan         = Usulan::find($id);
        $lokasi         = RW::whereHas('UserMusren', function($q){
                        $q->where('USER_ID',Auth::user()->id);
                        })->where('RW_TAHUN',$tahun)->first();
        $rt             =  RT::where('RW_ID',$lokasi->RW_ID)->get();
        $cekData_1      = Usulan_Rekap::where('USULAN_ID',$id)->where('USULAN_POSISI','1')->first();
        if(empty($cekData_1)){
            $rw     = Usulan::where('USULAN_ID',$id)->where('USULAN_POSISI','1')->first();
        }else{
            $rw     = $cekData_1;
        }

        $data = array(
                    'tahun'     => $tahun,
                    'isu'       => $isu,
                    'kamus'     => $kamus,
                    'rw'        => $rw,
                    'rt'        => $rt,
                    'usulan'      => $usulan,
                    'user'    => $user );

        $data['log_id'] = 0; //unused anymore
        return View::make('musrenbang.usulan.edit-prioritas')->with($data);
    }


    public function acc($tahun,$id){
        $isu            = Isu::all();
        $kamus          = Kamus::all();
        $usulan         = Usulan::find($id);
        $tujuan         = Tujuan::all();
        if(Auth::user()->app == 1 or (Auth::user()->level == 2 or Auth::user()->level == 3 or Auth::user()->level == 4)){
            $usulan     = Usulan::where('USULAN_ID',$id)->first();
            //dd($usulan->rt());
            $rt         = RT::where('RW_ID',$usulan->rt()->rw->RW_ID)->get();
        }


        $data = array(
                    'title'     => 'Terima Usulan',
                    'val'       => 'acc',
                    'usulan'    => $usulan,
                    'tahun'     => $tahun,
                    'isu'       => $isu,
                    'kamus'     => $kamus,
                    'tujuan'    => $tujuan,
                    'rt'        => $rt);

        return View('musrenbang.usulan.edit',$data);
    }

    public function submitAdd($tahun, Request $request){
        //$tahun= '2018';

        if($tahun!='2018'){
            return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','bukan tahun yang perencanaan pembangunan');
        }


        $msg = "RT";
        if(Auth::user()->level == 5)
        {
            $msg = "RW";
        }
        $messages = [
            'usulan_tujuan.required' => 'Mohon Mengisi Tujuan Usulan',
            'kamus.required' => 'Mohon Pilih Kamus Usulan',
            'urgensi.required' => 'Mohon Masukkan Urgensi',
            'vol.required' => 'Mohon Isi Volume',
            'vol.min' => 'Volume Minimal 1',
            'alamat.required' => 'Mohon isi Alamat',
            'urgensi.required' => 'Mohon isi Urgensi'
        ];

        $rules = [
            'usulan_tujuan' => 'required',
            'kamus' => 'required',
            'urgensi' => 'required',
            'vol' => 'required | min:1',
            'alamat'  => 'required',
            'urgensi'  => 'required'
        ];

        Validator::make($request->all(),$rules,$messages)->validate();

        if($request->usulan_tujuan==1){

             $jumlah_usulan = Usulan::where('USULAN_TAHUN',$tahun)->where('USULAN_DELETED',0)->where('USER_CREATED', Auth::user()->id)->where('USULAN_TUJUAN', 1)->count();

            if($jumlah_usulan>=4){
                //return "jumlah usulan melebihi maximal";
                return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','usulan anda di tolak karena usulan untuk renja maksimal 4');
            }

        }if($request->usulan_tujuan==2){
            $kamus_usulan = Kamus::find($request->kamus);
            $pagu_usulan = $kamus_usulan ? $kamus_usulan->KAMUS_HARGA : 0;
            $jumlah_pagu = Usulan::where('USULAN_TAHUN',$tahun)->where('USER_CREATED', Auth::user()->id)->where('USULAN_TUJUAN', $request->usulan_tujuan)->where('USULAN_DELETED',0)->get();
            $total =    0;
            foreach($jumlah_pagu as $jp){
                $total += $jp->USULAN_VOLUME * $jp->kamus->KAMUS_HARGA;
            }
            if($total + ($pagu_usulan*$request->vol) >=120000000){
                //return "b"
                return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','usulan anda di tolak karena usulan untuk PIPPK maksimal dana 100 jt');
            }
        }

        $image_arr = [];


        if($request->image != null)
        {
            foreach ($request->image as $image) {

                // $img1               = Input::file('image1');
                $extension = $image->getClientOriginalExtension();
               // Image::make($image->getRealPath())->resize(200, 200)->save($path);
                if(strtoupper($extension) == 'PNG' or strtoupper($extension) == 'JPG' or strtoupper($extension) == 'JPEG'){

                    $img = Image::make($image->getPathName())->resize(1024, null,function($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                    $save_path=public_path("uploads/");
                    if (!file_exists($save_path)) {
                        mkdir($save_path, 666, true);
                    }
                    $name  = $this->generateRandomString(20).'.' . $extension;
                    $img->save(public_path("uploads/$name"));

                    $image_arr[] = $name;

                }else{
                    //Session::flash('error','Dokumen tidak sesuai');
                    return back()->withErrors(["ErrorFile"=>"Format File Tidak Sesuai. Sistem Hanya Menerima File Gambar dengan format JPG atau PNG"]);
                }
            }
        }


        if(Auth::user()->level==1 || Auth::user()->level==2 || Auth::user()->level==3 || Auth::user()->level==4){
            $status     = '1';
            $posisi     = '1';
            $prioritas  = '0';
        }elseif(Auth::user()->level==5){
            $status     = '2';
            $posisi     = '1';
            $prioritas  = '1';
        }elseif(Auth::user()->level==6){
            $status     = '2';
            $posisi     = '2';
            $prioritas  = '1';
        }

        if(empty($request->sat) || empty($request->sat1) || empty($request->vol1) || empty($request->vol2))
        {
            $sat = 0;
            $sat1 = 0;
            $vol1 = 0;
            $vol2 = 0;
        }else{
            $sat = $request->sat;
            $sat1 = $request->sat1;
            $vol1 = $request->vol1;
            $vol2 = $request->vol2;
        }


        $usulan = new Usulan;

        $usulan->USULAN_TAHUN       = $tahun;
        $usulan->KAMUS_ID           = $request->kamus;
        $usulan->USULAN_URGENSI     = $request->urgensi;
        $usulan->USULAN_VOLUME      = $request->vol;
        $usulan->USULAN_VOLUME1      = $vol1;
        $usulan->USULAN_VOLUME2      = $vol2;
        $usulan->USULAN_GAMBAR      = $image_arr;
        $usulan->RT_ID              = $request->rt;
        $usulan->USULAN_LAT         = $request->latitude;
        $usulan->USULAN_LONG        = $request->longitude;
        $usulan->USULAN_STATUS      = $status;
        $usulan->USULAN_POSISI      = $posisi;
        $usulan->USULAN_PRIORITAS   = $prioritas;
        $usulan->USER_CREATED       = Auth::user()->id;
        $usulan->TIME_CREATED       = Carbon\Carbon::now();
        $usulan->IP_CREATED         = $_SERVER['REMOTE_ADDR'];
        $usulan->USER_UPDATED       = Auth::user()->id;
        $usulan->RW_ID              = Auth::user()->RW_ID;
        $usulan->TIME_UPDATED       = Carbon\Carbon::now();
        $usulan->IP_UPDATED         = $_SERVER['REMOTE_ADDR'];
        $usulan->USULAN_TUJUAN      = $request->usulan_tujuan;
        $usulan->ALAMAT             = $request->alamat;
        $usulan->USULAN_DELETED     = 0;
        $usulan->SATUAN_ID          = $sat;
        $usulan->SATUAN_ID1         = $sat1;

        $usulan->save();

        //$usulan = Usulan::find(291);
        //dd($usulan->USULAN_ID);
        /*$id = Usulan::orderBy('USULAN_ID', 'desc')->take(1)->first();

        foreach ($request->kriteria as $kriteria) {
            $usulan_kriteria = new Usulan_kriteria;

            $usulan_kriteria->DETAIL_KRITERIA_ID    = $kriteria;
            $usulan_kriteria->USULAN_ID             = $id->USULAN_ID;

            $usulan_kriteria->save();


        }*/


        Log::debug("Usulan ".$usulan->USULAN_ID." sudah ditambahkan");
        //setiap proses tambah, notification log jalan
        $n = new Notification_log;
        $n->ID_USER_SUBJECT = Auth::user()->email;
        $n->ID_USER_TARGET  = $this->_parseUserEmail();
        $n->PREDIKAT_AKSI   = "menambah";
        $n->OBJECT_AKSI     = "usulan";
        $n->AFFECTED_ROUTES = Route::getCurrentRoute()->getActionName();
        $n->OBJECT_SEBELUM  = null;
        $n->OBJECT_SESUDAH  = base64_encode(serialize($usulan));
        $n->IP_ADDRESS      = $_SERVER['REMOTE_ADDR'];
        $n->IS_READ         = false;
        $n->USULAN_ID       = $usulan->USULAN_ID;
        $n->save();

        /*if(isMobile()){
            return "c";
            return Redirect('musrenbang/'.$tahun.'/mobile/daftarUsulan/'.$request->usulan_tujuan);
        }*/

        if(Auth::user()->level==5 || Auth::user()->level==6){
             return Redirect('musrenbang/'.$tahun.'/usulan/show/'.$request->usulan_tujuan)->with('message_title','Success')->with('message','Sukses menambahkan usulan');
        }


        //return response()->json([]);
        return Redirect('musrenbang/'.$tahun.'/usulan-'.$request->usulan_tujuan)->with('message_title','Success')->with('message','Sukses menambahkan usulan');



    }

    public function submitEdit($tahun){
        $status='';

        if (Input::get('val') == 'acc') {

            $posisi = 1;
            if(Auth::user()->app == 1 && Auth::user()->level == 5){  //akomodir kelurahan
                $posisi = 1;
                $status = 2;
            }
            elseif(Auth::user()->app == 1 && Auth::user()->level == 6){   //akomodir kecamatan
                $posisi = 2;
                $status = 2;
            }
            elseif(Auth::user()->app == 1 && Auth::user()->level == 7) {  //akomodir kota
                $posisi = 3;
                $status = 2;
            }
            elseif(Auth::user()->app == 1 && Auth::user()->level == 8) {   //akomodir apbd
                $posisi = 4;
                $status = 2;
            }
            elseif(Auth::user()->app == 1 && Auth::user()->level == 10) {   //akomodir kota ke ??
                $posisi = 4;
                $status = 2;
            }

            $pheanstalk = new Pheanstalk(env('BEANSTALK_HOST'));

            if(Auth::user()->level == 5){

                //batas pengelolaan tahapan verifikasi kelurahan
                $now    = Carbon\Carbon::now()->format('Y-m-d');
                $tahapan    = Tahapan::where('MENU',2)->first();
                if($now >= $tahapan->TGL_AWAL && $now <= $tahapan->TGL_AKHIR){
                    //return View('musrenbang.usulan.add', $data);

                    Usulan::where('USULAN_ID',Input::get('usulan_id'))->update(['USULAN_STATUS' => '2',"USULAN_CATATANKEL" => Input::get('keterangan'),"USULAN_PRIORITAS" => Input::get('prioritas')]);

                    $data       = Usulan::find(Input::get('usulan_id'));

                    $rekap      = new Usulan_Rekap;

                    $rekap->USULAN_ID       = $data->USULAN_ID;
                    $rekap->USULAN_TAHUN    = $data->USULAN_TAHUN;
                    $rekap->KAMUS_ID        = $data->KAMUS_ID;
                    $rekap->USULAN_URGENSI  = $data->USULAN_URGENSI;
                    $rekap->USULAN_VOLUME   = $data->USULAN_VOLUME;
                    $rekap->USULAN_GAMBAR   = $data->USULAN_GAMBAR != null ? $data->USULAN_GAMBAR[0] : null;
                    $rekap->RT_ID           = $data->RT_ID != null ? $data->RT_ID[0] : null;
                    $rekap->USULAN_LAT      = $data->USULAN_LAT;
                    $rekap->USULAN_LONG     = $data->USULAN_LONG;
                    $rekap->USULAN_PRIORITAS= $data->USULAN_PRIORITAS;
                    $rekap->USULAN_STATUS   = $status;
                    $rekap->USULAN_POSISI   = $posisi;
                    $rekap->USER_CREATED    = $data->USER_CREATED;
                    $rekap->TIME_CREATED    = $data->TIME_CREATED;
                    $rekap->IP_CREATED      = $data->IP_CREATED;
                    $rekap->USER_UPDATED    = $data->USER_UPDATED;
                    $rekap->TIME_UPDATED    = $data->TIME_UPDATED;
                    $rekap->IP_UPDATED      = $data->IP_UPDATED;
                    $rekap->USULAN_TUJUAN    = $data->USULAN_TUJUAN;

                    $rekap->save();

                    $usulan_id = $data->USULAN_ID;
                    $kel_id = Auth::user()->KEL_ID;
                    $set_usulan_prioritas = Input::get('prioritas');

                    //$pheanstalk->useTube('musrenbang-priority')->put("updateprioritykelurahan-".$usulan_id."-".$kel_id."-".$set_usulan_prioritas);


                }else{
                    return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','Verifikasi  di tahap kelurahan telah berakhir');
                }

            }
            elseif(Auth::user()->level == 6){

                //batas pengelolaan tahapan verifikasi kelurahan
                $now    = Carbon\Carbon::now()->format('Y-m-d');
                $tahapan    = Tahapan::where('MENU',3)->first();
                if($now >= $tahapan->TGL_AWAL && $now <= $tahapan->TGL_AKHIR){
                    //return View('musrenbang.usulan.add', $data);

                    Usulan::where('USULAN_ID',Input::get('usulan_id'))->update(['USULAN_POSISI' => '2',"USULAN_CATATANKEC" => Input::get('keterangan'),"USULAN_PRIORITAS" => Input::get('prioritas')]);
                    $data       = Usulan::find(Input::get('usulan_id'));

                    $rekap      = new Usulan_Rekap_Kec;

                    $rekap->USULAN_ID       = $data->USULAN_ID;
                    $rekap->USULAN_TAHUN    = $data->USULAN_TAHUN;
                    $rekap->KAMUS_ID        = $data->KAMUS_ID;
                    $rekap->USULAN_URGENSI  = $data->USULAN_URGENSI;
                    $rekap->USULAN_VOLUME   = $data->USULAN_VOLUME;
                    $rekap->USULAN_GAMBAR   = $data->USULAN_GAMBAR != null ? $data->USULAN_GAMBAR[0] : null;
                    $rekap->RT_ID           = $data->RT_ID != null ? $data->RT_ID[0] : null;
                    $rekap->USULAN_LAT      = $data->USULAN_LAT;
                    $rekap->USULAN_LONG     = $data->USULAN_LONG;
                    $rekap->USULAN_PRIORITAS= $data->USULAN_PRIORITAS;
                    $rekap->USULAN_STATUS   = $status;
                    $rekap->USULAN_POSISI   = $posisi;
                    $rekap->USER_CREATED    = $data->USER_CREATED;
                    $rekap->TIME_CREATED    = $data->TIME_CREATED;
                    $rekap->IP_CREATED      = $data->IP_CREATED;
                    $rekap->USER_UPDATED    = $data->USER_UPDATED;
                    $rekap->TIME_UPDATED    = $data->TIME_UPDATED;
                    $rekap->IP_UPDATED      = $data->IP_UPDATED;
                    $rekap->USULAN_TUJUAN    = $data->USULAN_TUJUAN;

                    $rekap->save();

                }else{
                    return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','Verifikasi  di tahap Kecamatan telah berakhir');
                }

            }
            elseif(Auth::user()->level == 7){

                //batas pengelolaan tahapan verifikasi kelurahan
                $now    = Carbon\Carbon::now()->format('Y-m-d');
                $tahapan    = Tahapan::where('MENU',4)->first();
                if($now >= $tahapan->TGL_AWAL && $now <= $tahapan->TGL_AKHIR){
                    //return View('musrenbang.usulan.add', $data);

                    Usulan::where('USULAN_ID',Input::get('usulan_id'))->update(['USULAN_STATUS' => '2',"USULAN_CATATANSKPD" => Input::get('keterangan')]);
                    $data       = Usulan::find(Input::get('usulan_id'));
                    $rekap      = new Usulan_Rekap_SKPD;

                    $rekap->USULAN_TAHUN    = $data->USULAN_TAHUN;
                    $rekap->KAMUS_ID        = $data->KAMUS_ID;
                    $rekap->USULAN_URGENSI  = $data->USULAN_URGENSI;
                    $rekap->USULAN_VOLUME   = $data->USULAN_VOLUME;
                    $rekap->USULAN_GAMBAR   = $data->USULAN_GAMBAR != null ? $data->USULAN_GAMBAR[0] : null;
                    $rekap->RT_ID           = $data->RT_ID != null ? $data->RT_ID[0] : null;
                    $rekap->USULAN_LAT      = $data->USULAN_LAT;
                    $rekap->USULAN_LONG     = $data->USULAN_LONG;
                    $rekap->USULAN_PRIORITAS= $data->USULAN_PRIORITAS;
                    $rekap->USULAN_STATUS   = $status;
                    $rekap->USULAN_POSISI   = $posisi;
                    $rekap->USER_CREATED    = $data->USER_CREATED;
                    $rekap->TIME_CREATED    = $data->TIME_CREATED;
                    $rekap->IP_CREATED      = $data->IP_CREATED;
                    $rekap->USER_UPDATED    = $data->USER_UPDATED;
                    $rekap->TIME_UPDATED    = $data->TIME_UPDATED;
                    $rekap->IP_UPDATED      = $data->IP_UPDATED;
                    $rekap->USULAN_TUJUAN   = $data->USULAN_TUJUAN;

                    $rekap->save();

                 }else{
                    return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','Verifikasi  di tahap Forum SKPD telah berakhir');
                }
            }            elseif(Auth::user()->level == 10){

                //batas pengelolaan tahapan verifikasi kelurahan
                $now    = Carbon\Carbon::now()->format('Y-m-d');
                $tahapan    = Tahapan::where('MENU',6)->first();
                if($now >= $tahapan->TGL_AWAL && $now <= $tahapan->TGL_AKHIR){
                    //return View('musrenbang.usulan.add', $data);

                    Usulan::where('USULAN_ID',Input::get('usulan_id'))->update(['USULAN_STATUS' => '2',"USULAN_CATATANBAPPEDA" => Input::get('keterangan')]);
                    $data       = Usulan::find(Input::get('usulan_id'));

                    $rekap      = new Usulan_Rekap_Kab;

                    $rekap->USULAN_ID       = $data->USULAN_ID;
                    $rekap->USULAN_URGENSI  = $data->USULAN_URGENSI;
                    $rekap->USULAN_VOLUME   = $data->USULAN_VOLUME;
                    $rekap->USULAN_GAMBAR   = $data->USULAN_GAMBAR != null ? $data->USULAN_GAMBAR[0] : null;
                    $rekap->RT_ID           = $data->RT_ID != null ? $data->RT_ID[0] : null;
                    $rekap->USULAN_LAT      = $data->USULAN_LAT;
                    $rekap->USULAN_LONG     = $data->USULAN_LONG;
                    $rekap->USULAN_PRIORITAS= $data->USULAN_PRIORITAS;
                    $rekap->USULAN_STATUS   = $status;
                    $rekap->USULAN_POSISI   = $posisi;
                    $rekap->USER_CREATED    = $data->USER_CREATED;
                    $rekap->TIME_CREATED    = $data->TIME_CREATED;
                    $rekap->IP_CREATED      = $data->IP_CREATED;
                    $rekap->USER_UPDATED    = $data->USER_UPDATED;
                    $rekap->TIME_UPDATED    = $data->TIME_UPDATED;
                    $rekap->IP_UPDATED      = $data->IP_UPDATED;
                    $rekap->USULAN_TUJUAN   = $data->USULAN_TUJUAN;

                    $rekap->save();

                 }else{
                    return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','Verifikasi  di tahap Kabupaten telah berakhir');
                }
            }

            $id = Input::get('usulan_id');

            $n = new Notification_log;
            Log::debug("Usulan ".Input::get('usulan_id')." sudah disetujui");

            $usulan_data                    = Usulan::find(Input::get('usulan_id'));

            $n->OBJECT_SEBELUM  = base64_encode(serialize($usulan_data));

            $usulan_data->USULAN_URGENSI    = Input::get('urgensi');
            $usulan_data->ALAMAT            = Input::get('alamat');
            $usulan_data->USULAN_VOLUME     = Input::get('vol');
            $usulan_data->KET_ACC           = Input::get('keterangan');
            $usulan_data->USULAN_TUJUAN     = Input::get('tujuan');
            $usulan_data->USULAN_STATUS     = $status;
            $usulan_data->USULAN_POSISI     = $posisi;
            $usulan_data->USULAN_LAT        = Input::get('latitude');
            $usulan_data->USULAN_LONG       = Input::get('longitude');
            $usulan_data->USER_UPDATED      = Auth::user()->id;
            $usulan_data->TIME_UPDATED      = Carbon\Carbon::now();
            $usulan_data->IP_UPDATED        = $_SERVER['REMOTE_ADDR'];
            $usulan_data->save();

            $n->ID_USER_SUBJECT = Auth::user()->email;
            $n->ID_USER_TARGET  = $this->_parseUserEmail();
            $n->PREDIKAT_AKSI   = "menyetujui";
            $n->OBJECT_AKSI     = "usulan";
            $n->AFFECTED_ROUTES = Route::getCurrentRoute()->getActionName();
            $n->OBJECT_SESUDAH  = base64_encode(serialize($usulan_data));
            $n->IP_ADDRESS      = $_SERVER['REMOTE_ADDR'];
            $n->IS_READ         = false;
            $n->USULAN_ID       = $usulan_data->USULAN_ID;
            $n->save();
            $url = Input::get('backurl');
            return Redirect($url)->with('message_title','Success')->with('message','Sukses mengakomodir usulan');

        } else if(Input::get('val') == 'edit'){
                $n = new Notification_log;
                Log::debug("Usulan ".Input::get('usulan_id')." sudah diupdate");

                $data = Usulan::find(Input::get('usulan_id'));
                $n->OBJECT_SEBELUM  = base64_encode(serialize($data));

                //$data->KAMUS_ID         = Input::get('kamus');
                $data->USULAN_URGENSI   = Input::get('urgensi');
                $data->USULAN_VOLUME    = Input::get('vol');
                $data->USULAN_VOLUME1   = Input::get('vol1');
                $data->USULAN_VOLUME2   = Input::get('vol2');
                $data->ALAMAT           = Input::get('alamat');
                $data->USULAN_LAT       = Input::get('latitude');
                $data->USULAN_LONG      = Input::get('longitude');
                $data->USER_UPDATED     = Auth::user()->id;
                $data->TIME_UPDATED     = Carbon\Carbon::now();
                $data->IP_UPDATED       = $_SERVER['REMOTE_ADDR'];
                $data->save(); //update usulan

                $n->ID_USER_SUBJECT = Auth::user()->email;
                $n->ID_USER_TARGET  = $this->_parseUserEmail();
                $n->PREDIKAT_AKSI   = "mengubah";
                $n->OBJECT_AKSI     = "usulan";
                $n->AFFECTED_ROUTES = Route::getCurrentRoute()->getActionName();
                $n->OBJECT_SESUDAH  = base64_encode(serialize($data));
                $n->IP_ADDRESS      = $_SERVER['REMOTE_ADDR'];
                $n->IS_READ         = false;
                $n->USULAN_ID       = $data->USULAN_ID;
                $n->save();

                $id = Input::get('usulan_tujuan');

                if(Auth::user()->level==5 || Auth::user()->level==6 || Auth::user()->level==7){
                    return Redirect('musrenbang/'.$tahun.'/usulan/show/'.$data->USULAN_TUJUAN)->with('message_title','Success')->with('message','Sukses mengedit usulan');
                }
                return Redirect('musrenbang/'.$tahun.'/usulan-'.Input::get('usulan_tujuan'))->with('message_title','Success')->with('message','Sukses mengedit usulan');
        }else{
                Log::debug("Usulan ".Input::get('usulan_id')." sudah ga tau diapain");

                $usulan_data = Usulan::find(Input::get('usulan_id'));

                $n = new Notification_log; //save data logging
                $n->ID_USER_SUBJECT = Auth::user()->email;
                $n->ID_USER_TARGET  = $this->_parseUserEmail();
                $n->PREDIKAT_AKSI   = "mengubah";
                $n->OBJECT_AKSI     = "usulan";
                $n->AFFECTED_ROUTES = Route::getCurrentRoute()->getActionName();
                $n->OBJECT_SEBELUM  = base64_encode(serialize($usulan_data));

                $usulan_data->USULAN_URGENSI    = Input::get('urgensi');
                $usulan_data->USULAN_VOLUME     = Input::get('vol');
                $usulan_data->USULAN_TUJUAN     = Input::get('tujuan');
                $usulan_data->USULAN_STATUS     = '1';
                $usulan_data->USER_UPDATED      = Auth::user()->id;
                $usulan_data->TIME_UPDATED      = Carbon\Carbon::now();
                $usulan_data->IP_UPDATED        = $_SERVER['REMOTE_ADDR'];
                $usulan_data->save(); //update data

                $n->OBJECT_SESUDAH  = base64_encode(serialize($usulan_data));
                $n->IP_ADDRESS      = $_SERVER['REMOTE_ADDR'];
                $n->IS_READ         = false;
                $n->USULAN_ID       = $usulan_data->USULAN_ID;
                $n->save(); //save data logging

                $id = Input::get('USULAN_TUJUAN');

                if(Auth::user()->level==5){
                    return back();
                }
                return Redirect('musrenbang/'.$tahun.'/usulan-2')->with('message_title','Success')->with('message','Sukses mengedit usulan');
      }

    }



    public function deny($tahun){
        if(Auth::user()->level == 5 || Auth::user()->level == 6 || Auth::user()->level==7){
            $posisi = 1;
            if(Auth::user()->level == 6)
            {
                $posisi = 2;
            }
            if(Auth::user()->level == 7)
            {
                $posisi = 3;
            }

            $usulan_data = Usulan::find(Input::get('usulan_id'));

            Log::debug("Usulan ".Input::get('usulan_id')." sudah ditolak");

            $n = new Notification_log;
            $n->ID_USER_SUBJECT = Auth::user()->email;
            $n->ID_USER_TARGET  = $this->_parseUserEmail();
            $n->PREDIKAT_AKSI   = "menolak";
            $n->OBJECT_AKSI     = "usulan";
            $n->AFFECTED_ROUTES = Route::getCurrentRoute()->getActionName();
            $n->OBJECT_SEBELUM  = base64_encode(serialize($usulan_data));

            //update status tolak
            $usulan_data->USULAN_ALASAN = Input::get('alasan');
            $usulan_data->USULAN_STATUS = '0';
            $usulan_data->USULAN_POSISI = $posisi;
            $usulan_data->save();
            //end of update status tolak

            $n->OBJECT_SESUDAH  = base64_encode(serialize($usulan_data));
            $n->IP_ADDRESS      =  $_SERVER['REMOTE_ADDR'];
            $n->IS_READ         = false;
            $n->USULAN_ID       = $usulan_data->USULAN_ID;
            $n->save(); //save data logging
            $url = Input::get('backurl');
            return Redirect($url)->with('message_type','error')->with('message_title','Usulan telah di tolak');
        }

    }

    public function kamus($tahun){
        $kamus    = Kamus::Join('REFERENSI.REF_ISU','REF_ISU.ISU_ID','=','REF_KAMUS.ISU_ID')->get();
        $isu = '';
	//dd(Skpd::get()->toArray());
        //die();
	$skpd = Skpd::where('SKPD_BIDANG','!=', 0)->get();

        if(Auth::user()->level==1 || Auth::user()->level==5 || Auth::user()->level==6){
            $isu = Isu::whereIn('ISU_TIPE', [0,1,2])->get();
        }
        elseif(Auth::user()->level==2){
            $isu = Isu::where('ISU_TIPE', 3)->orderBy('ISU_ID')->get();
        }
        elseif(Auth::user()->level==3){
            $isu = Isu::where('ISU_TIPE', 5)->orderBy('ISU_ID')->get();
        }
        elseif(Auth::user()->level==4){
            $isu = Isu::where('ISU_TIPE', 4)->orderBy('ISU_ID')->get();
        }elseif(Auth::user()->level==7) {
            $isu = Isu::whereHas('kamus',function($x){
                        $x->where('KAMUS_SKPD', Auth::user()->SKPD_ID);
                    })->get();
        }

        $user = User::where('id',Auth::user()->id)->first();
        $data = array(
                'tahun' => $tahun,
                'user'  => $user,
                'kamus' => $kamus,
                'isu'   => $isu,
                'skpd'  => $skpd
                );

        return View::make('musrenbang.usulan.kamus',$data);
    }

    public function delete($tahun){
        Usulan::where('USULAN_ID',Input::get('id'))->update(['USULAN_DELETED' => '1']);

        $n = new Notification_log;
        Log::debug("Usulan ".Input::get('id')." sudah dihapus");

        $n->ID_USER_SUBJECT = Auth::user()->email;
        $n->ID_USER_TARGET  = $this->_parseUserEmail();
        $n->PREDIKAT_AKSI   = "menghapus";
        $n->OBJECT_AKSI     = "usulan";
        $n->AFFECTED_ROUTES = Route::getCurrentRoute()->getActionName();
        //$n->OBJECT_SESUDAH  = base64_encode(serialize($data));
        $n->IP_ADDRESS      = $_SERVER['REMOTE_ADDR'];
        $n->IS_READ         = false;
        $n->USULAN_ID       = Input::get('id');
        $n->save();

        return "Hapus Data Berhasil!";

        // $usulan = Usulan::find(Input::get('id'));
        // $usulan->delete();
        //Usulan_Rekap::where('USULAN_ID',Input::get('id'))->delete();

        /*Log::debug("Usulan ".$usulan->USULAN_ID." sudah dihapus");

        $n = new Notification_log;
        $n->ID_USER_SUBJECT = Auth::user()->email;
        $n->ID_USER_TARGET  = $this->_parseUserEmail();
        $n->PREDIKAT_AKSI   = "menghapus";
        $n->OBJECT_AKSI     = "usulan";
        $n->AFFECTED_ROUTES = Route::getCurrentRoute()->getActionName();
        $n->OBJECT_SESUDAH  = null;
        $n->OBJECT_SEBELUM  = base64_encode(serialize($usulan));
        $n->IP_ADDRESS      =  $_SERVER['REMOTE_ADDR'];
        $n->IS_READ         = false;
        $n->USULAN_ID       = $usulan->USULAN_ID;
        $n->save();*/

    }



    //API
    public function getData1($tahun,$nama,$status = 1){

        $data           = Usulan::where('USULAN_TAHUN',$tahun)
                          ->where('USER_CREATED', Auth::user()->id)
                          ->where('USULAN_TUJUAN', $nama)
                          ->orderBy('USULAN_ID','asc');

        /*if($tahun == '2017')
            $data = $data->where('USULAN_STATUS',$status)->WhereNull('USULAN_DELETED')->get();
        else */
            $data = $data->where('USULAN_STATUS',$status)->where('USULAN_DELETED',0)
                    ->get();

        $no       = 1;
        $aksi     = '';
        $view     = array();
        $rt       = '';
        $rts      = '';
        $jum      = 0;
        $kel      = 'Kelurahan';
        $kec      = 'Kecamatan';
        $kota     = 'Kota';
        $apbd     = 'APBD';
        $info     = '<i class="fa fa-refresh text-info"></i>';
        $sukses   = '<i class="fa fa-check text-success"></i>';
        $danger   = '<i class="fa fa-close text-danger"></i>';
        foreach ($data as $data) {
            $aksi    = '<div class="action visible pull-right">';

            if($data->USULAN_STATUS == 1)     $status = $info;
            elseif($data->USULAN_STATUS == 2) $status = $sukses;
            else                              $status = $danger;

            if($data->USULAN_POSISI == 1 && $data->USULAN_STATUS == 1 )
                $posisi = $kel;
            elseif($data->USULAN_POSISI == 1 && $data->USULAN_STATUS == 2 )
                $posisi = $kel.'<br>'.$info.' '.$kec;
            elseif($data->USULAN_POSISI == 2 && $data->USULAN_STATUS == 2)
                $posisi = $kec.'<br>'.$info.' '.$kota;
            elseif($data->USULAN_POSISI == 3 && $data->USULAN_STATUS == 2)
                $posisi = $kota.'<br>'.$info.' '.$apbd;
            elseif($data->USULAN_POSISI == 1 && $data->USULAN_STATUS == 0)
                $posisi = $kel;
            elseif($data->USULAN_POSISI == 2 && $data->USULAN_STATUS == 0)
                $posisi = $kec;
            elseif($data->USULAN_POSISI == 3 && $data->USULAN_STATUS == 0)
                $posisi = $kota;
            else
                $posisi = $apbd;

            $pengusul   = 'Kecamatan '.$data->rw->kelurahan->kecamatan->KEC_NAMA.'<br>'.
                          'Kelurahan '.$data->rw->kelurahan->KEL_NAMA.'<br>'.
                          '<p class="text-orange">'.$data->user->name.'</p>';

            $pagu       = $data->USULAN_VOLUME * $data->kamus->KAMUS_HARGA;


            if(((( Auth::user()->level == 1 || Auth::user()->level == 2 || Auth::user()->level == 3 || Auth::user()->level == 4) && $data->USULAN_STATUS != 2) && $data->USULAN_STATUS != 0)  && Auth::user()->active == 1 ){

                //usulan tujuan = 1 renja
                //if($data->USULAN_TUJUAN == 1 ) $aksi   .= '<a onclick="return prioritas(\''.$data->USULAN_ID.'\')" class="action-prioritas">P</a>';
                if($tahun == '2018'){
                    $aksi   .= '<a href="'.url('/musrenbang/'.$tahun.'/usulan/detail/'.$data->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>';
                    $aksi   .= '<a href="'.url('/musrenbang/'.$tahun.'/usulan/edit/'.$data->USULAN_ID).'" class="action-edit"><i class="mi-edit"></i></a>';
                    $aksi   .= '<a onclick="return hapus(\''.$data->USULAN_ID.'\')" class="action-delete"><i class="mi-trash"></i></a>';
                 }
            }else{
                    $aksi   .= '<a href="'.url('/musrenbang/'.$tahun.'/usulan/detail/'.$data->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>';
                if($tahun == '2018'){
                    $aksi   .= '<a onclick="return hapus(\''.$data->USULAN_ID.'\')" class="action-delete"><i class="mi-trash"></i></a>';
                }
            }

           // $aksi   .= '<a href="'.url('/musrenbang/usulan/history/'.$data->USULAN_ID).'" target="_blank" class="action-history"><i class="fa fa-book"></i></a>';
           $aksi   .= '<a onclick="return detail(\''.$data->USULAN_ID.'\')" class="action-delete"><i class="fa fa-book"></i></a>';

            $aksi   .= '</div>';

            $volume = number_format($data->kamus->KAMUS_HARGA,0,'.',',').'<p class="text-orange">'.$data->USULAN_VOLUME." ".$data->kamus->KAMUS_SATUAN.'</p>';


            array_push( $view,
                array( 'USULAN_ID'      => $data->USULAN_ID,
                       'NO'             => $no,
                       'LOKASI'         => $pengusul,
                       'ISU'            => $data->kamus->isu->ISU_NAMA.
                                            "<br><p class='text-orange'>".$data->kamus->KAMUS_NAMA."</p>",
                       'STATUS'         => "<p>".$status.' '.$posisi."</p>",
                       'VOLUME'         => $volume,
                       'ANGGARAN'       => number_format($pagu,0,'.',','),
                       'ALAMAT'         => $data->ALAMAT,
                       'URGENSI'        => $data->USULAN_URGENSI,
                       'AKSI'           => $aksi));
            $no++;
            $jum += $pagu;
        }
            $out = array("aaData"=>$view,"jumlah"=>number_format($jum,0,'.',','));
            return Response::JSON($out);

    }


    public function getDataPrioritas($tahun,$nama,$status = 1){

        $data           = Usulan::where('USULAN_TAHUN',$tahun)
                          ->where('USER_CREATED', Auth::user()->id)
                          ->where('USULAN_TUJUAN', $nama)
                          ->orderBy('USULAN_ID','asc');

        if($tahun == '2017')
            $data = $data->where('USULAN_STATUS',$status)->WhereNull('USULAN_DELETED')->get();
        else
            $data = $data->where('USULAN_STATUS',$status)->where('USULAN_DELETED',0)->where('USULAN_PRIORITAS',1)
                    ->get();

        //$berita = BeritaAcara::where('id',Auth)

        $no       = 1;
        $aksi     = '';
        $view     = array();
        $rt       = '';
        $rts      = '';
        $jum      = 0;
        $kel      = 'Kelurahan';
        $kec      = 'Kecamatan';
        $kota     = 'Kota';
        $apbd     = 'APBD';
        $info     = '<i class="fa fa-refresh text-info"></i>';
        $sukses   = '<i class="fa fa-check text-success"></i>';
        $danger   = '<i class="fa fa-close text-danger"></i>';
        foreach ($data as $data) {
            $aksi    = '<div class="action visible pull-right">';

            if($data->USULAN_STATUS == 1)     $status = $info;
            elseif($data->USULAN_STATUS == 2) $status = $sukses;
            else                              $status = $danger;


            if($data->USULAN_POSISI == 1 && $data->USULAN_STATUS == 1)
                $posisi = $kel;
            elseif($data->USULAN_POSISI == 1 && $data->USULAN_STATUS == 2)
                $posisi = $kel.'<br>'.$info.' '.$kec;
            elseif($data->USULAN_POSISI == 2 && $data->USULAN_STATUS == 2)
                $posisi = $kec.'<br>'.$info.' '.$kota;
            elseif($data->USULAN_POSISI == 3 && $data->USULAN_STATUS == 2)
                $posisi = $kota.'<br>'.$info.' '.$apbd;
            elseif($data->USULAN_POSISI == 1 && $data->USULAN_STATUS == 0)
                $posisi = $kel;
            elseif($data->USULAN_POSISI == 2 && $data->USULAN_STATUS == 0)
                $posisi = $kec;
            elseif($data->USULAN_POSISI == 3 && $data->USULAN_STATUS == 0)
                $posisi = $kota;
            else
                $posisi = $apbd;

            $pengusul   = 'Kecamatan '.$data->rw->kelurahan->kecamatan->KEC_NAMA.'<br>'.
                          'Kelurahan '.$data->rw->kelurahan->KEL_NAMA.'<br>'.
                          'RW '.$data->rw->RW_NAMA;

            $pagu       = $data->USULAN_VOLUME * $data->kamus->KAMUS_HARGA;


            if(((( Auth::user()->level == 1 || Auth::user()->level == 2 || Auth::user()->level == 3 || Auth::user()->level == 4) && $data->USULAN_STATUS != 2) && $data->USULAN_STATUS != 0)  && Auth::user()->active == 1 ){

                $aksi   .= '<a href="'.url('/musrenbang/'.$tahun.'/berita/'.$data->USULAN_TUJUAN).'" class="action-edit">Belum Lengkap</a>';
                $aksi   .= '<a href="'.url('/musrenbang/'.$tahun.'/usulan/detail/'.$data->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>';
                $aksi   .= '<a href="'.url('/musrenbang/'.$tahun.'/usulan/edit/'.$data->USULAN_ID).'" class="action-edit"><i class="mi-edit"></i></a>';
                $aksi   .= '<a onclick="return hapus(\''.$data->USULAN_ID.'\')" class="action-delete"><i class="mi-trash"></i></a>';
            }else{
                $aksi   .= '<a href="'.url('/musrenbang/'.$tahun.'/usulan/detail/'.$data->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>';
                $aksi   .= '<a onclick="return hapus(\''.$data->USULAN_ID.'\')" class="action-delete"><i class="mi-trash"></i></a>';
            }

            $aksi   .= '<a href="'.url('/musrenbang/usulan/history/'.$data->USULAN_ID).'" target="_blank" class="action-history"><i class="fa fa-book"></i></a>';
            $aksi   .= '</div>';


            array_push( $view,
                array( 'USULAN_ID'      => $data->USULAN_ID,
                       'NO'             => $no,
                       'LOKASI'         => $pengusul,
                       'ISU'            => $data->kamus->isu->ISU_NAMA.
                                            "<br><p class='text-orange'>".$data->kamus->KAMUS_NAMA."</p>",
                       'STATUS'         => "<p>".$status.' '.$posisi."</p>",
                       'VOLUME'         => $data->USULAN_VOLUME." ".$data->kamus->KAMUS_SATUAN,
                       'HARGA'          => number_format($data->kamus->KAMUS_HARGA,0,'.',','),
                       'ANGGARAN'       => number_format($pagu,0,'.',','),
                       'ALAMAT'         => $data->ALAMAT,
                       'URGENSI'        => $data->USULAN_URGENSI,
                       'AKSI'           => $aksi));
            $no++;
            $jum += $pagu;
        }
            $out = array("aaData"=>$view,"jumlah"=>number_format($jum,0,'.',','));
            return Response::JSON($out);

    }


    /*public function getData($tahun){
        if(Auth::user()->level == 1){
            $data           = Usulan::where('USULAN_TAHUN',$tahun)->where('USER_CREATED', Auth::user()->id)->get();
        }elseif(Auth::user()->level == 2){

            $data           = Usulan::where('USULAN_TAHUN',$tahun)->where('USULAN_POSISI','>=' ,1)->get();
        }elseif(Auth::user()->level == 3){

            $data           = Usulan::where('USULAN_TAHUN',$tahun)->where('USULAN_POSISI','>='  ,2)->get();
        }else{
            $data           = Usulan::where('USULAN_TAHUN',$tahun)->where('USULAN_POSISI','>=' , 3)->get();
        }
        $no             = 1;
        $aksi           = '';
        $view           = array();
        foreach ($data as $data) {
            if($data->USULAN_STATUS == 1) $status = '<i class="fa fa-refresh text-info"></i>';
            elseif($data->USULAN_STATUS == 2) $status = '<i class="fa fa-check text-success"></i>';
            else $status = '<i class="fa fa-close text-danger"></i>';


            if($data->USULAN_POSISI == 1) $posisi = ' Kelurahan';
            elseif($data->USULAN_POSISI == 2) $posisi = ' Kecamatan';
            elseif($data->USULAN_POSISI == 3) $posisi = ' Kota';
            else $posisi = ' APBD';

            $pengusul   = $data->rt->rw->kelurahan->kecamatan->KEC_NAMA."<br>".
                          $data->rt->rw->kelurahan->KEL_NAMA."<br>RW ".
                          $data->rt->rw->RW_NAMA." - RT ".
                          $data->rt->RT_NAMA;
            $pagu       = $data->USULAN_VOLUME * $data->kamus->KAMUS_HARGA;
            if(Auth::user()->app == 1 and Auth::user()->level == 1){
            $aksi       = $aksi         = ' <div class="action visible pull-right">
                                                <a href="/musrenbang/'.$tahun.'/usulan/detail/'.$data->USULAN_ID.'" class="action-preveiw"><i class="mi-eye"></i></a>
                                                <a href="/musrenbang/'.$tahun.'/usulan/edit/'.$data->USULAN_ID.'" class="action-edit"><i class="mi-edit"></i></a>
                                                <a onclick="return hapus(\''.$data->USULAN_ID.'\')" class="action-delete"><i class="mi-trash"></i></a>
                                            </div>';
            }else{
            $aksi       = $aksi         = ' <div class="action visible pull-right">
                                                <a href="/musrenbang/'.$tahun.'/usulan/detail/'.$data->USULAN_ID.'" class="action-preveiw"><i class="mi-eye"></i></a>
                                            </div>';
            }
            array_push($view, array( 'USULAN_ID'        => $data->USULAN_ID,
                                     'NO'               => $no,
                                     'LOKASI'           => $pengusul,
                                     'ISU'              => $data->kamus->isu->ISU_NAMA."<br><p class='text-orange'>".$data->kamus->KAMUS_NAMA."</p>",
                                     'STATUS'           => "<p>".$status.$posisi."</p>",
                                     'VOLUME'           => $data->USULAN_VOLUME." ".$data->kamus->KAMUS_SATUAN,
                                     'AKSI'             => $aksi,
                                     'ANGGARAN'         =>number_format($pagu,0,'.',',')));
            $no++;
        }
            $out = array("aaData"=>$view);
            return Response::JSON($out);

    }*/

    public function getKamus($tahun,$id){
        $data   = Kamus::where('KAMUS_TAHUN',$tahun)->where('KAMUS_KUNCI','0')->where('ISU_ID',$id);

        if(Auth::user()->level == 1)
        {
            $data = $data->whereHas('isu',function($isu){
                return $isu->wherein('ISU_TIPE',[0,1,2]);
            });
        }
        else  if(Auth::user()->level == 2)
        {
            $data = $data->whereHas('isu',function($isu){
                return $isu->where('ISU_TIPE',3);
            });
        }
        else  if(Auth::user()->level == 3)
        {
            $data = $data->whereHas('isu',function($isu){
                return $isu->where('ISU_TIPE',5);
            });
        }
        else  if(Auth::user()->level == 4)
        {
            $data = $data->whereHas('isu',function($isu){
                return $isu->where('ISU_TIPE',4);
            });
        }

        $data = $data->get();

        $view   = "";
        foreach ($data as $data) {
            $view         = $view."<option value='".$data->KAMUS_ID."' style='color:black'>".$data->KAMUS_NAMA."</option>";
        }
        $tipe   = Isu::where('ISU_ID',$id)->value('ISU_TIPE');
        $out    = array('opt'=>$view,'tipe'=>$tipe);
        return $out;
    }


    public function showKamusUsulan($tahun){

        return View::make('musrenbang.usulan.kamus-usulan')->with('tahun', $tahun);
    }

    public function getKamusUsulan($tahun, $status, $skpd = "x", $isu="x", $kamus="x", $tipe="x"){
        $data ="";


        if($status==1){
            $data   = Kamus::join('REFERENSI.REF_ISU','REF_ISU.ISU_ID','=','REF_KAMUS.ISU_ID')
                          ->whereIn('ISU_TIPE',['0','1'])
                          ->where('KAMUS_TAHUN',$tahun);
        }
        elseif($status==2){
             $data   = Kamus::whereHas('isu',function($x){
                        $x->where('ISU_TIPE',2);
                    })->where('KAMUS_TAHUN',$tahun);

        }
         elseif($status==3){
             $data   = Kamus::whereHas('isu',function($x){
                        $x->where('ISU_TIPE',3);
                    })->where('KAMUS_TAHUN',$tahun);
        }
         elseif($status==4){
            $data   = Kamus::whereHas('isu',function($x){
                        $x->where('ISU_TIPE',5);
                    })->where('KAMUS_TAHUN',$tahun);

        }
        elseif($status==5){
             $data   = Kamus::whereHas('isu',function($x){
                        $x->where('ISU_TIPE',4);
                    })->where('KAMUS_TAHUN',$tahun);

        }

        /*if($status==1){
            if(Auth::user()->level == 1 or Auth::user()->level == 9){
                $data   = Kamus::join('REFERENSI.REF_ISU','REF_ISU.ISU_ID','=','REF_KAMUS.ISU_ID')
                          ->whereIn('ISU_TIPE',['0','1'])
                          ->where('KAMUS_TAHUN',$tahun);

            }elseif(Auth::user()->level==7){
                $data   = Kamus::where('KAMUS_TAHUN',$tahun)
                        ->where('KAMUS_SKPD', Auth::user()->SKPD_ID)
                        ->orderBy('KAMUS_ID','ASC');
            }
        }
        elseif($status==2){
             $data   = Kamus::whereHas('isu',function($x){
                        $x->where('ISU_TIPE',2);
                    })->where('KAMUS_TAHUN',$tahun);

        }
         elseif($status==3){
             $data   = Kamus::whereHas('isu',function($x){
                        $x->where('ISU_TIPE',3);
                    })->where('KAMUS_TAHUN',$tahun);
        }
         elseif($status==4){
            $data   = Kamus::whereHas('isu',function($x){
                        $x->where('ISU_TIPE',5);
                    })->where('KAMUS_TAHUN',$tahun);

        }
        elseif($status==5){
             $data   = Kamus::whereHas('isu',function($x){
                        $x->where('ISU_TIPE',4);
                    })->where('KAMUS_TAHUN',$tahun);

        }*/

        $aksi = '';

        $data = $data->get();


        $view = array();

        $no=1;

        foreach ($data as $data) {

            array_push($view, array(
                                     'NO'              => $no,
                                     'KAMUS_ID'        => $data->KAMUS_ID,
                                     'ISU_NAMA'        => $data->isu->ISU_NAMA,
                                     'KAMUS_NAMA'      => $data->KAMUS_NAMA,
                                     'HARGA'           => number_format($data->KAMUS_HARGA,0,',','.'),
                                     'SATUAN'          => $data->KAMUS_NAMA,
                                     'KRITERIA'        => $data->KAMUS_KRITERIA,
                                     'SKPD'            => $data->skpd != null ? $data->skpd->SKPD_NAMA : null,
                                     'AKSI'            => $aksi,
                                     ));
            $no++;
        }
            $out = array("aaData"=>$view);
            return Response::JSON($out);

    }

    public function getKamusUsulanKriteria($tahun, $id){

        $data   = Kriteria::where('KAMUS_ID',$id);

        $data = $data->get();

       // dd($data);

        $view = array();

        $no=1;

        foreach ($data as $data) {

            array_push($view, array(
                                     'NO'              => $no,
                                     'KRITERIA'        => $data->KRITERIA,
                                     'BOBOT'           => $data->STANDAR_BOBOT,
                                     'PERTANYAAN'      => $data->PERTANYAAN,
                                     'AKSI'            => '<a onclick="return setkriteria(\''.$data->KRITERIA_ID.'\')"><i class="fa fa-pencil"></i></a>',
                                    ));
            $no++;
        }
            $out = array("aaData"=>$view);
            return Response::JSON($out);

    }

    public function simpanKamusBaru($tahun, Request $request){

        $kamus                         = new Kamus;
        $kamus->ISU_ID                 = $request->ISU_ID;
        $kamus->KAMUS_NAMA             = $request->KAMUS_NAMA;
        $kamus->KAMUS_KETERANGAN       = $request->KAMUS_KETERANGAN;
        $kamus->save();

        return 'Kamus berhasil di usulkan!';
    }

    public function getKamusUsulanSingleKriteria($tahun,$id){
        return Kriteria::where('KRITERIA_ID',$id)->first();
    }

    public function setKamusUsulanSingleKriteria($tahun){
        Kriteria::where('KRITERIA_ID', Input::get('KRITERIA_ID'))
            ->update([
                'KRITERIA'      => Input::get('KRITERIA'),
                'STANDAR_BOBOT' => Input::get('STANDAR_BOBOT'),
                'PERTANYAAN'    => Input::get('PERTANYAAN')
                ]);

        return 'Kriteria berhasil di ubah!';
    }

    public function simpanKamusUsulanSingleKriteria($tahun){
        $kriteria                   = new Kriteria;
        $kriteria->KRITERIA         = Input::get('KRITERIA');
        $kriteria->STANDAR_BOBOT    = Input::get('STANDAR_BOBOT');
        $kriteria->PERTANYAAN       = Input::get('PERTANYAAN');
        $kriteria->save();

        return 'Kriteria berhasil di simpan!';
    }

    public function simpanKamusUsulanBaru($tahun){
        $kamus                   = new Kamus;
        $kamus->KAMUS_TAHUN      = Input::get('KAMUS_TAHUN');
        $kamus->KAMUS_NAMA       = Input::get('KAMUS_NAMA');
        $kamus->KAMUS_KUNCI      = Input::get('KAMUS_SKPD');
        $kamus->ISU_ID           = Input::get('ISU_ID');
        $kamus->KAMUS_KETERANGAN = Input::get('KAMUS_KETERANGAN');
        $kamus->save();

        return 'Usulan Kamus berhasil di simpan!';
    }

    public function getKamusUsulanSingle($tahun,$id){
        $data       = Kamus::where('KAMUS_ID',$id)->first();
        return Response::JSON($data);
    }


     public function editKamusUsulan($tahun,$id){
        $isu   = Isu::whereHas('kamus',function($x) use($id){
                        $x->where('KAMUS_ID',$id);
                    })->first();

        $kamus  = Kamus::join('REFERENSI.REF_KRITERIA', 'REF_KAMUS.KAMUS_ID', '=', 'REF_KRITERIA.KAMUS_ID')
         ->join('REFERENSI.REF_DETAIL_KRITERIA', 'REF_DETAIL_KRITERIA.KRITERIA_ID', '=', 'REF_KRITERIA.KRITERIA_ID')
                    ->join('REFERENSI.REF_BOBOT_KRITERIA', 'REF_BOBOT_KRITERIA.BOBOT_KRITERIA_ID', '=', 'REF_DETAIL_KRITERIA.BOBOT_KRITERIA_ID')

                    ->where('REF_KAMUS.KAMUS_ID',$id)->first();

        $kriteria =  Kamus::join('REFERENSI.REF_KRITERIA', 'REF_KAMUS.KAMUS_ID', '=', 'REF_KRITERIA.KAMUS_ID')
                        ->join('REFERENSI.REF_DETAIL_KRITERIA', 'REF_DETAIL_KRITERIA.KRITERIA_ID', '=', 'REF_KRITERIA.KRITERIA_ID')
                    ->join('REFERENSI.REF_BOBOT_KRITERIA', 'REF_BOBOT_KRITERIA.BOBOT_KRITERIA_ID', '=', 'REF_DETAIL_KRITERIA.BOBOT_KRITERIA_ID')
                    ->where('REF_KAMUS.KAMUS_ID',$id)->get();

        $data = array(
                    'tahun'     => $tahun,
                    'isu'       => $isu,
                    'kamus'     => $kamus,
                    'kriteria'  => $kriteria);
        //dd($data);

        return View::make('musrenbang.usulan.edit-kamus')->with($data);
    }



    public function getSatuan($tahun,$id){
        $satuan   = Kamus::where('KAMUS_ID',$id)->first();

        $data = array(
                    'satuan'    =>$satuan->KAMUS_SATUAN,
                    'kriteria'  =>$satuan->KAMUS_KRITERIA);

        return $data;
    }

    //other
    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

     public function forgab($tahun){
        $kamus    = Kamus::ALL();
        $usulan    = Usulan::ALL();
        $isu     = Isu::ALL();
        $data = array(
                    'kamus'    =>$kamus,
                    'usulan'     =>$usulan,
                    'isu'     =>$isu);
        return View::make('musrenbang.usulan.forgab',$data)->with('tahun', $tahun);
    }

    public function getForgab($tahun,$id){
        $data   = Kamus::where('KAMUS_TAHUN',$tahun)->where('KAMUS_KUNCI','0')->where('ISU_ID',$id)->get();
        $view   = "";
        foreach ($data as $data) {
            //$view         = $view."<option value='".$data->KAMUS_ID."'>".$data->KAMUS_NAMA."</option>";
            $view         = $view."<tr>
                                        <td>'".$data->KAMUS_ID."</td>
                                        <td>'".$data->KAMUS_NAMA."</td>
                                        <td>'".$data->KAMUS_NAMA."</td>
                                        <td>'".$data->KAMUS_NAMA."</td>
                                        <td>'".$data->KAMUS_NAMA."</td>
                                        <td>'".$data->KAMUS_NAMA."</td>
                                        <td>'".$data->KAMUS_NAMA."</td>
                                  </tr>";
        }
        //$tipe   = Isu::where('ISU_ID',$id)->value('ISU_TIPE');
        $out    = array('opt'=>$view);
        return $out;
    }


    // public function forgab($tahun){
    //     $kamus    = Kamus::ALL();
    //     $usulan    = Usulan::ALL();
    //     $data = array(
    //                 'kamus'    =>$kamus,
    //                 'usulan'     =>$usulan);
    //     return View::make('musrenbang.usulan.forgab',$data)->with('tahun', $tahun);
    // }

     public function berita($tahun){
        $berita    = BeritaAcara::where('id',Auth::user()->id)->get();
        $BeritaAcaraFoto    = BeritaAcaraFoto::where('id',Auth::user()->id)->get();
        $user = User::where('id',Auth::user()->id)->first();

        $data = array(
                    'berita'    =>$berita,
                    'beritaFoto'    =>$BeritaAcaraFoto,
                    'user'     =>$user,
                    //'id'     =>$id,
                    'tahun'    =>$tahun);

       //

        /*dd($data);                   */

        return View::make('musrenbang.usulan.berita',$data);
    }

    public function tambahBerita(Request $request)
    {
        $this->validate($request,[
            'image' => 'required'
        ]);

        $image_arr = [];
         if($request->image != null)
         {
             foreach ($request->image as $image) {

                 // $img1               = Input::file('image1');
                 $extension = $image->getClientOriginalExtension();
                // Image::make($image->getRealPath())->resize(200, 200)->save($path);
                 if($extension == 'png' or $extension == 'jpg' or $extension == 'PNG' or $extension == 'JPG'){

                     $img = Image::make($image->getPathName())->resize(1024, null,function($constraint) {
                         $constraint->aspectRatio();
                         $constraint->upsize();
                     });

                     $name  = $this->generateRandomString(20).'.' . $extension;
                     $img->save(public_path("uploads/$name"));

                     $image_arr[] = $name;

                 }else{
                     Session::flash('fail','Dokumen tidak sesuai');
                     return Redirect('musrenbang/2017/berita');
                 }
             }
         }

         $beritaacara = beritaacara::create(
            [
                //'JUDUL'=>$request->judul,
                'TGL'=>date("Y-m-d"),
                //'USULAN_ID'=>$request->usulan_id,
                //'KETERANGAN'=>$request->deskripsi,
                'lampiran'=>$image_arr,
                'id'=>Auth::user()->id
            ]
        );

        return back();
    }


    public function tambahBeritaGambar(Request $request)
    {

        $this->validate($request,[
            'image1' => 'required'
        ]);

        $image_arr = [];
         if($request->image1 != null)
         {
             foreach ($request->image1 as $image) {

                 // $img1               = Input::file('image1');
                 $extension = $image->getClientOriginalExtension();
                // Image::make($image->getRealPath())->resize(200, 200)->save($path);
                 if($extension == 'png' or $extension == 'jpg' or $extension == 'PNG' or $extension == 'JPG'){

                     $img = Image::make($image->getPathName())->resize(1024, null,function($constraint) {
                         $constraint->aspectRatio();
                         $constraint->upsize();
                     });

                     $name  = $this->generateRandomString(20).'.' . $extension;
                     $img->save(public_path("uploads/$name"));

                     $image_arr[] = $name;

                 }else{
                     Session::flash('fail','Dokumen tidak sesuai');
                     return Redirect('musrenbang/2017/berita');
                 }
             }
         }

        /*$berita = BeritaAcaraFoto::where('id',Auth::user()->id)->value('BERITA_FOTO_ID');

        $berita->IMAGE = $request->image_arr;
        $berita->tgl_update = date("Y-m-d");
        $berita->update();*/

         $beritaacarafoto = BeritaAcaraFoto::create(
            [
                'tgl_update'=>date("Y-m-d"),
                'image'=>$image_arr,
                'id'=>Auth::user()->id
            ]
        );

        return back();
    }


    public function profile($tahun){
        $user    = User::where('USER_UPDATEDCREATED', Auth::user()->id);

        return View::make('musrenbang.usulan.profile')->with('tahun', $tahun);
    }

    public function profileGetData($tahun){

        $user    = RT::where('RW_ID', Auth::user()->RW_ID)->get();
        //dd($user);
        $view = array();
        $no=1;

        foreach ($user as $user) {

            array_push($view, array(
                                     'NO'           => $no,
                                     'RT_ID'        => $user->RT_ID,
                                     'RT_NAMA'      => $user->RT_NAMA,
                                     'RT_KETUA'     => $user->RT_KETUA,
                                     'RT_NIK'       => $user->RT_NIK,
                                     'RT_TELP'      => $user->RT_TELP,
                                     'AKSI'         => '<button class="buttonubah btn btn-default" data-toggle="modal" data-target="#modaleditrt">Ubah</button>',
                                     ));
            $no++;
        }
            $out = array("aaData"=>$view);
            return Response::JSON($out);

    }

    public function validasi($tahun){
        User::where('id',Auth::user()->id)->update(['validasi'=>1]);
        if(Auth::user()->level != 5) RW::where('RW_ID',Auth::user()->RW_ID)->update(['RW_AKTIF'=>1]);

        return Redirect('musrenbang/2017/profile');
    }


    public function resetPassword($tahun){

        if(Auth::user()->level == 5){
            $user = User::where('KEL_ID',Auth::user()->KEL_ID)->whereIn('level',[1,2,3,4])->get();

            $data = array(
                    'user'    =>$user,
                    'tahun'    =>$tahun
                    );
           // dd($user);
            return View::make('musrenbang.usulan.reset-password',$data);

        }elseif(Auth::user()->level == 6){

            $user = User::where('KEC_ID',Auth::user()->KEC_ID)->where('level',5)->get();


            $data = array(
                    'user'    =>$user,
                    'tahun'    =>$tahun
                    );
           // dd($user);
            return View::make('musrenbang.usulan.reset-password',$data);

        }


    }

    public function resetPassUbah(Request $request){

        $user = User::where('email','=',$request->email)->first();

        if($request->level ==1){


            $user->password = "$2y$10$6KRJ6LsEW8oys6DTlCcIeefviIFEQ3GGgYqnVO53vOgd3HFfG508a";

        }elseif($request->level ==2) {


            $user->password = "$2y$10$sjiz2G.RrrVIujvGBu37F.K3tnlcNYbWW89CpTKiTC09E1OOG8pB2";

        }elseif($request->level ==3) {

            $user->password = "$2y$10$.v9XIeayx2.MwtNp.aI62.Sip3qrAMI35NiJt6uHSetqVUyt1tPQG";

        }elseif($request->level ==4) {

            $user->password = "$2y$10$P1P123M.x4D/9oo5uCB.oOZUfe6/G351MQMIQjriQVDtG2Q2FBrUS";

        }elseif($request->level ==5) {
            $user->password = "$2y$10$9KhzVO22hUmOaBQAprF0GuUUSSKoyAkqds3IuJbf/4QX9RaJg39S.";

        }elseif($request->level ==6) {
            $user->password = "$2y$10$a2jSc1QTITxIoXj8QRjOkuFOtM1mn156JTJ7ERFviYMfNz6wtzVXS";

        }

        $user->save();

        return back();

    }

    public function showRW($tahun){
        $rw = RW::where('KEL_ID',Auth::user()->KEL_ID)->get();
        return View::make('musrenbang.referensi.rw',compact('rw'));
    }



    public function getS($tahun,$tipe,$id){
        $view   = "";
        if($tipe == 'kelurahan'){
            $data = Kelurahan::where('KEC_ID',$id)->orderBy('KEL_NAMA')->get();
            foreach ($data as $data) {
                $view         = $view."<option value='".$data->KEL_ID."' style='color:black'>".$data->KEL_NAMA."</option>";
            }
        }
        elseif($tipe == 'rw'){
            $data = RW::where('KEL_ID',$id)->orderBy('RW_NAMA')->get();
            foreach ($data as $data) {
                $view         = $view."<option value='".$data->RW_ID."' style='color:black'>".$data->RW_NAMA."</option>";
            }
        }
        elseif($tipe == 'isu'){
            if($id == 'pippk') $type = ['2'];
            else $type = ['0','1'];
            $data = Isu::whereIn('ISU_TIPE',$type)->orderBy('ISU_NAMA')->get();
            foreach ($data as $data) {
                $view         = $view."<option value='".$data->ISU_ID."' style='color:black'>".$data->ISU_NAMA."</option>";
            }
        }
        elseif($tipe == 'kamus'){
            $data = Kamus::where('ISU_ID',$id)->orderBy('KAMUS_NAMA')->get();
            foreach ($data as $data) {
                $view         = $view."<option value='".$data->KAMUS_ID."' style='color:black'>".$data->KAMUS_NAMA."</option>";
            }
        }
        return $view;
    }


     public function getUsulan($tahun,$rw,$tipe,$isu,$kamus){
        set_time_limit(120);
        $data = Usulan::where('USULAN_TAHUN',$tahun)->where('KEL_ID',Auth::user()->KEL_ID);
        if($tipe != 'x'){
            if($tipe == 'pippk') $type = [2];
            else $type = [0,1];
            $data   = $data->whereHas('kamus',function($q) use($type){
                $q->whereHas('isu',function($x) use($type){
                    $x->whereIn('ISU_TIPE',$type);
                });
            });
        }


        /*if($kecamatan != 'x'){
            $data   = $data->whereHas('rw',function($q) use($kecamatan){
                $q->whereHas('kelurahan',function($x) use($kecamatan){
                    $x->whereHas('kecamatan',function($y) use($kecamatan){
                        $y->where('KEC_ID',$kecamatan);
                    });
                });
            });
        }
        if($kelurahan != 'x'){
            $data   = $data->whereHas('rw',function($q) use($kelurahan){
                $q->whereHas('kelurahan',function($x) use($kelurahan){
                    $x->where('KEL_ID',$kelurahan);
                });
            });
        }
*/

        if($rw != 'x'){
            $data   = $data->where('RW_ID',$rw);
        }
        if($kamus != 'x'){
            $data   = $data->where('KAMUS_ID',$kamus);
        }
        if($isu != 'x'){
            $data   = $data->whereHas('kamus',function($q) use($isu){
                $q->whereHas('isu',function($x) use($isu){
                    $x->where('ISU_ID',$isu);
                });
            });
        }
        $data       = $data->with("rw","rw.kelurahan","rw.kelurahan.kecamatan","kamus","kamus.isu","user")->get();


        $no         = 1;
        $view       = array();
        foreach ($data as $data) {
            if($data->USULAN_STATUS == 1) $status = '<i class="fa fa-refresh text-info"></i>';
            elseif($data->USULAN_STATUS == 2) $status = '<i class="fa fa-check text-success"></i>';
            else $status = '<i class="fa fa-close text-danger"></i>';


            if($data->USULAN_POSISI == 1) $posisi = ' Kelurahan';
            elseif($data->USULAN_POSISI == 2) $posisi = ' Kecamatan';
            elseif($data->USULAN_POSISI == 3) $posisi = ' Kota';
            else $posisi = ' APBD';

            $lokasi     = 'Kecamatan '.$data->rw->kelurahan->kecamatan->KEC_NAMA.'<br>'.
                          'Kelurahan '.$data->rw->kelurahan->KEL_NAMA.'<br>'.
                          'RW '.$data->rw->RW_NAMA;

            if($data->kamus->isu->ISU_TIPE == 2) $tipe = 'PIPPK';
            else $tipe = 'RENJA';

            array_push($view, array( 'USULAN_ID'        => $data->USULAN_ID,
                                     'TIPE'             => $tipe,
                                     'NO'               => $no,
                                     'LOKASI'           => $lokasi,
                                     'ISU'              => $data->kamus->isu->ISU_NAMA."<br><p class='text-orange'>".$data->kamus->KAMUS_NAMA."</p>",
                                     'STATUS'           => "<p>".$status.$posisi."</p>",
                                     'VOLUME'           => $data->USULAN_VOLUME." ".$data->kamus->KAMUS_SATUAN,
                                     'PENGUSUL'         => $data->user->email,
                                     'TOTAL'            => number_format($data->kamus->KAMUS_HARGA * $data->USULAN_VOLUME,0,',','.'),
                                     'URGENSI'          => $data->USULAN_URGENSI,
                                     ));
            $no++;
        }
            $out = array("aaData"=>$view);
            return Response::JSON($out);
    }


    public function dashboard($tahun){
        $pippk     = Usulan::wherehas('kamus',function($q){
                    $q->whereHas('isu',function($x){
                        $x->where('ISU_TIPE','2');
                    });
                })->count();

        $renja  = Usulan::wherehas('kamus',function($q){
                    $q->whereHas('isu',function($x){
                        $x->whereIn('ISU_TIPE',['0','1']);
                    });
                })->count();

        $keluargaRw   = '';
        $keluargaKel  = '';
        $t_rembuk     = '';
        $t_pippk      = '';
        $t_kel        = '';
        $t_kec        = '';
        $t_skpd       = '';
        $tahapan1     ='';
        $tahapan2     ='';
        $tahapan3     ='';
        $tahapan4     ='';
        $tahapan5     ='';

                         $jumRenProses  = 0;
                        $jumRenTerima   = 0;
                        $jumRenTolak    = 0;
                        $jumPIPPKProses = 0;
                        $jumPIPPKTerima = 0;
                        $jumPIPPKTolak  = 0;
                        $jumRenProses2018 = 0;
                        $jumRenTerima2018 = 0;
                        $jumRenTolak2018 = 0;
                        $jumPIPPKProses2018 = 0;
                        $jumPIPPKTerima2018 = 0;
                        $jumPIPPKTolak2018 = 0;
                        $totRenProses = 0;
                        $totRenTerima= 0;
                        $totRenTolak= 0;
                        $totPIPPKProses= 0;
                        $totPIPPKTerima= 0;
                        $totPIPPKTolak= 0;
                        $totRenProses2018= 0;
                        $totRenTerima2018= 0;
                        $totRenTolak2018= 0;
                        $totPIPPKProses2018= 0;
                        $totPIPPKTerima2018= 0;
                        $totPIPPKTolak2018= 0;


        $now     = Carbon\Carbon::now()->format('Y-m-d');
        $tahapan1 = Tahapan::where('MENU',1)->first();
        if($now >= $tahapan1->TGL_AWAL && $now <= $tahapan1->TGL_AKHIR){
            $t_rembuk = 0;
        }elseif($now > $tahapan1->TGL_AKHIR){
            $t_rembuk = 1;
        }else{
            $t_rembuk = 2;
        }
        $tahapan2 = Tahapan::where('MENU',2)->first();
        if($now >= $tahapan2->TGL_AWAL && $now <= $tahapan2->TGL_AKHIR){
            $t_kel = 0;
        }elseif($now > $tahapan2->TGL_AKHIR){
            $t_kel = 1;
        }else{
            $t_kel = 2;
        }
        $tahapan3 = Tahapan::where('MENU',3)->first();
        if($now >= $tahapan3->TGL_AWAL && $now <= $tahapan3->TGL_AKHIR){
            $t_kec = 0;
        }elseif($now > $tahapan3->TGL_AKHIR){
            $t_kec = 1;
        }else{
            $t_kec = 2;
        }
        $tahapan4 = Tahapan::where('MENU',4)->first();
        if($now >= $tahapan4->TGL_AWAL && $now <= $tahapan4->TGL_AKHIR){
            $t_skpd = 0;
        }elseif($now > $tahapan4->TGL_AKHIR){
            $t_skpd = 1;
        }else{
            $t_skpd = 2;
        }
        $tahapan5 = Tahapan::where('MENU',5)->first();
        if($now >= $tahapan5->TGL_AWAL && $now <= $tahapan5->TGL_AKHIR){
            $t_pippk = 0;
        }elseif($now > $tahapan5->TGL_AKHIR){
            $t_pippk = 1;
        }else{
            $t_pippk = 2;
        }


         $renjaProsesKel  = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',1)->where('USULAN_POSISI',1)->count();
         $renjaTerimaKel  = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',2)->where('USULAN_POSISI','>=',1)->count();
         $renjaTolakKel   = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',0)->where('USULAN_POSISI',1)->count();

         $renjaProsesKec  = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',2)->where('USULAN_POSISI',1)->count();
         $renjaTerimaKec  = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',2)->where('USULAN_POSISI','>=',2)->count();
         $renjaTolakKec   = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',0)->where('USULAN_POSISI',2)->count();

         $renjaProsesSkpd  = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',2)->where('USULAN_POSISI',2)->count();
         $renjaTerimaSkpd  = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',2)->where('USULAN_POSISI','>=',3)->count();
         $renjaTolakSkpd   = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',0)->where('USULAN_POSISI',3)->count();

         $pippkProsesRw  = Usulan::wherehas('user',function($q){
                                $q->where('level','1');
                            })->where('USULAN_TUJUAN',2)->NotDummies()->where('USULAN_STATUS',1)->where('USULAN_POSISI',1)->count();
         $pippkTerimaRw  = Usulan::wherehas('user',function($q){
                                $q->where('level','1');
                            })->where('USULAN_TUJUAN',2)->NotDummies()->where('USULAN_STATUS',2)->where('USULAN_POSISI','>=',1)->count();
         $pippkTolakRw  = Usulan::wherehas('user',function($q){
                                $q->where('level','1');
                            })->where('USULAN_TUJUAN',2)->NotDummies()->where('USULAN_STATUS',0)->where('USULAN_POSISI',1)->count();

         $pippkProsesLpm  = Usulan::wherehas('user',function($q){
                                $q->where('level','2');
                            })->where('USULAN_TUJUAN',2)->NotDummies()->where('USULAN_STATUS',1)->count();
         $pippkTerimaLpm  = Usulan::wherehas('user',function($q){
                                $q->where('level','2');
                            })->where('USULAN_TUJUAN',2)->NotDummies()->where('USULAN_STATUS',2)->count();
         $pippkTolakLpm  = Usulan::wherehas('user',function($q){
                                $q->where('level','2');
                            })->where('USULAN_TUJUAN',2)->NotDummies()->where('USULAN_STATUS',0)->count();

         $pippkProsesPkk  = Usulan::wherehas('user',function($q){
                                $q->where('level','3');
                            })->where('USULAN_TUJUAN',2)->NotDummies()->where('USULAN_STATUS',1)->count();
         $pippkTerimaPkk  = Usulan::wherehas('user',function($q){
                                $q->where('level','3');
                            })->where('USULAN_TUJUAN',2)->NotDummies()->where('USULAN_STATUS',2)->count();
         $pippkTolakPkk  = Usulan::wherehas('user',function($q){
                                $q->where('level','3');
                            })->where('USULAN_TUJUAN',2)->NotDummies()->where('USULAN_STATUS',0)->count();

         $pippkProsesKarta  = Usulan::wherehas('user',function($q){
                                $q->where('level','4');
                            })->where('USULAN_TUJUAN',2)->NotDummies()->where('USULAN_STATUS',1)->count();
         $pippkTerimaKarta  = Usulan::wherehas('user',function($q){
                                $q->where('level','4');
                            })->where('USULAN_TUJUAN',2)->NotDummies()->where('USULAN_STATUS',2)->count();
         $pippkTolakKarta  = Usulan::wherehas('user',function($q){
                                $q->where('level','4');
                            })->where('USULAN_TUJUAN',2)->NotDummies()->where('USULAN_STATUS',0)->count();


        $pippkdesc = [];
        $renjadesc = [];
        $dashboard_skpd = null;

        if(Auth::user()->level==5){
            $k = Auth::user()->KEL_ID;

            $keluargaRw = Kelurahan::where('KEC_ID',Auth::user()->KEC_ID)->orderBy('KEL_NAMA')->get();
            $keluargaKel = Kecamatan::where('KEC_ID',Auth::user()->KEC_ID)->orderBy('KEC_NAMA')->get();

            //total usulan RENJA
            //$pippkdesc["diproses"] =
            //TAHUN 2017
              $jumRenProses = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEL_ID',$k);
                    })->where('USULAN_STATUS',1)->where('USULAN_TUJUAN',1)->where('USULAN_TAHUN',$tahun)->count();
              $jumRenTerima = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEL_ID',$k);
                })->where('USULAN_STATUS',2)->where('USULAN_TUJUAN',1)->where('USULAN_TAHUN',$tahun)->count();
              $jumRenTolak = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEL_ID',$k);
                })->where('USULAN_STATUS',0)->where('USULAN_TUJUAN',1)->where('USULAN_TAHUN',$tahun)->count();
             //TAHUN 2018
              $jumRenProses2018 = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEL_ID',$k);
                })->where('USULAN_STATUS',1)->where('USULAN_TUJUAN',1)->where('USULAN_TAHUN','2018')->count();
              $jumRenTerima2018 = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEL_ID',$k);
                })->where('USULAN_STATUS',2)->where('USULAN_TUJUAN',1)->where('USULAN_TAHUN','2018')->count();

              $jumRenTolak2018 = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEL_ID',$k);
                })->where('USULAN_STATUS',0)->where('USULAN_TUJUAN',1)->where('USULAN_TAHUN','2018')->count();


              //PIPPK tahun 2017
              $jumPIPPKProses = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEL_ID',$k);
                    })->where('USULAN_STATUS',1)->where('USULAN_TUJUAN',2)->where('USULAN_TAHUN',$tahun)->count();
              $jumPIPPKTerima = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEL_ID',$k);
                })->where('USULAN_STATUS',2)->where('USULAN_TUJUAN',2)->where('USULAN_TAHUN',$tahun)->count();
              $jumPIPPKTolak = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEL_ID',$k);
                })->where('USULAN_STATUS',0)->where('USULAN_TUJUAN',2)->where('USULAN_TAHUN',$tahun)->count();
             //TAHUN 2018
              $jumPIPPKProses2018 = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEL_ID',$k);
                })->where('USULAN_STATUS',1)->where('USULAN_TUJUAN',2)->where('USULAN_TAHUN','2018')->count();
              $jumPIPPKTerima2018 = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEL_ID',$k);
                })->where('USULAN_STATUS',2)->where('USULAN_TUJUAN',2)->where('USULAN_TAHUN','2018')->count();

              $jumPIPPKTolak2018 = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEL_ID',$k);
                })->where('USULAN_STATUS',0)->where('USULAN_TUJUAN',2)->where('USULAN_TAHUN','2018')->count();


              //jumlah nominal renja RENJA
            $totRenProses = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',1)
                            ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA');
            $totRenTerima = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',2)
                            ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA');
            $totRenTolak = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',0)
                            ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA');
            $totRenProses2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',1)
                            ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA');
            $totRenTerima2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',2)
                            ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA');
            $totRenTolak2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',0)
                            ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA');

              //jumlah nominal renja PIPPK
            $totPIPPKProses = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',1)
                            ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA');
            $totPIPPKTerima = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',2)
                            ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA');
            $totPIPPKTolak = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',0)
                            ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA');
            $totPIPPKProses2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',1)
                            ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA');
            $totPIPPKTerima2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',2)
                            ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA');
            $totPIPPKTolak2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',0)
                            ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA');


        } else if(Auth::user()->level==6){

            $k = Auth::user()->KEC_ID;

            $keluargaRw = Kelurahan::where('KEC_ID',Auth::user()->KEC_ID)->orderBy('KEL_NAMA')->get();
            $keluargaKel = Kecamatan::where('KEC_ID',Auth::user()->KEC_ID)->orderBy('KEC_NAMA')->get();

            //total usulan RENJA
            //$pippkdesc["diproses"] =
            //TAHUN 2017
              $jumRenProses = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEC_ID',$k);
                    })->where('USULAN_STATUS',1)->where('USULAN_TUJUAN',1)->where('USULAN_TAHUN',$tahun)->count();
              $jumRenTerima = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEC_ID',$k);
                })->where('USULAN_STATUS',2)->where('USULAN_TUJUAN',1)->where('USULAN_TAHUN',$tahun)->count();
              $jumRenTolak = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEC_ID',$k);
                })->where('USULAN_STATUS',0)->where('USULAN_TUJUAN',1)->where('USULAN_TAHUN',$tahun)->count();
             //TAHUN 2018
              $jumRenProses2018 = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEC_ID',$k);
                })->where('USULAN_STATUS',1)->where('USULAN_TUJUAN',1)->where('USULAN_TAHUN','2018')->count();
              $jumRenTerima2018 = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEC_ID',$k);
                })->where('USULAN_STATUS',2)->where('USULAN_TUJUAN',1)->where('USULAN_TAHUN','2018')->count();

              $jumRenTolak2018 = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEC_ID',$k);
                })->where('USULAN_STATUS',0)->where('USULAN_TUJUAN',1)->where('USULAN_TAHUN','2018')->count();


              //PIPPK tahun 2017
              $jumPIPPKProses = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEC_ID',$k);
                    })->where('USULAN_STATUS',1)->where('USULAN_TUJUAN',2)->where('USULAN_TAHUN',$tahun)->count();
              $jumPIPPKTerima = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEC_ID',$k);
                })->where('USULAN_STATUS',2)->where('USULAN_TUJUAN',2)->where('USULAN_TAHUN',$tahun)->count();
              $jumPIPPKTolak = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEC_ID',$k);
                })->where('USULAN_STATUS',0)->where('USULAN_TUJUAN',2)->where('USULAN_TAHUN',$tahun)->count();
             //TAHUN 2018
              $jumPIPPKProses2018 = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEC_ID',$k);
                })->where('USULAN_STATUS',1)->where('USULAN_TUJUAN',2)->where('USULAN_TAHUN','2018')->count();
              $jumPIPPKTerima2018 = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEC_ID',$k);
                })->where('USULAN_STATUS',2)->where('USULAN_TUJUAN',2)->where('USULAN_TAHUN','2018')->count();

              $jumPIPPKTolak2018 = USULAN::wherehas('user',function($u) use ($k){
                            return $u->where('KEC_ID',$k);
                })->where('USULAN_STATUS',0)->where('USULAN_TUJUAN',2)->where('USULAN_TAHUN','2018')->count();


              //jumlah nominal renja RENJA
            $totRenProses = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',1)
                            ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA');
            $totRenTerima = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',2)
                            ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA');
            $totRenTolak = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',0)
                            ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA');
            $totRenProses2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',1)
                            ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA');
            $totRenTerima2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',2)
                            ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA');
            $totRenTolak2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',0)
                            ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',1)->sum('KAMUS_HARGA');

              //jumlah nominal renja PIPPK
            $totPIPPKProses = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',1)
                            ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA');
            $totPIPPKTerima = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',2)
                            ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA');
            $totPIPPKTolak = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',0)
                            ->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA');
            $totPIPPKProses2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',1)
                            ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA');
            $totPIPPKTerima2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',2)
                            ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA');
            $totPIPPKTolak2018 = USULAN::join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                            ->where('USER_CREATED', Auth::user()->id)->where('USULAN_STATUS',0)
                            ->where('USULAN_TAHUN','2018')->where('USULAN_TUJUAN',2)->sum('KAMUS_HARGA');

        }else if(Auth::user()->level==7){
            $o      = User::where('login',1)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $rw     = User::where('app',1)->where('level',1)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $rwv    = User::where('app',1)->where('level',1)->where('KEC_ID',Auth::user()->KEC_ID)->where('validasi',1)->count();
            $lpm    = User::where('app',1)->where('level',2)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $lpmv   = User::where('app',1)->where('level',2)->where('validasi',1)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $pkk    = User::where('app',1)->where('level',3)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $pkkv   = User::where('app',1)->where('level',3)->where('validasi',1)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $karta  = User::where('app',1)->where('level',4)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $kartav = User::where('app',1)->where('level',4)->where('validasi',1)->count();

            $rwpernahusulan = User::where('app',1)->where('level',1)->wherehas('usulan')->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $lpmpernahusulan = User::where('app',1)->where('level',2)->wherehas('usulan')->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $pkkpernahusulan = User::where('app',1)->where('level',3)->wherehas('usulan')->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $kartapernahusulan = User::where('app',1)->where('level',4)->wherehas('usulan')->where('KEC_ID',Auth::user()->KEC_ID)->count();

            $usrname = explode('.',Auth::user()->email);
            $skpd = SKPD::with(
                [
                    'usulan'=>function($usulan){
                        $usulan->UsulanSKPD();
                    },
                    'usulan.kamus',
                ]
            )->where('SKPD_KODE',$usrname[1].'.'.$usrname[2].'.'.$usrname[3])->first();

            if ($skpd != null) {
                //validasi bahwa selalu memiliki hasil
                $usulan = $skpd->usulan;
                $renja_lain = $usulan->where('USULAN_TUJUAN',1);
                $jumlahrenja_lain = $renja_lain->count();
                $jumlahpengusul = $renja_lain->unique('USER_CREATED')->count();

                $dashboard_skpd =  array(
                                     'SKPDID'              => $skpd->SKPD_ID,
                                     'KODESKPD'            => $skpd->SKPD_KODE,
                                     'NAMASKPD'            => $skpd->SKPD_NAMA,
                                     'JUMLAHRENJA'         => $jumlahrenja_lain,
                                     'JUMLAHPENGUSUL'      => $jumlahpengusul,
                                     'TOTALNOMINAL'        => number_format($skpd->totalNominal(1),0,'.',','),
                                     );


                $grafik_isu  = Isu::with(
                        [
                            'kamus'=>function($kamus){
                            },
                        ]
                        )
                        ->whereHas("kamus",function($kamus){
                            return $kamus->where('KAMUS_SKPD',Auth::user()->SKPD_ID);
                        })
                        ->get();

                //dd($grafik_isu);
            }

        }elseif(Auth::user()->level==8){
             $o      = User::where('login',1)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $rw     = User::where('app',1)->where('level',1)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $rwv    = User::where('app',1)->where('level',1)->where('KEC_ID',Auth::user()->KEC_ID)->where('validasi',1)->count();
            $lpm    = User::where('app',1)->where('level',2)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $lpmv   = User::where('app',1)->where('level',2)->where('validasi',1)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $pkk    = User::where('app',1)->where('level',3)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $pkkv   = User::where('app',1)->where('level',3)->where('validasi',1)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $karta  = User::where('app',1)->where('level',4)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $kartav = User::where('app',1)->where('level',4)->where('validasi',1)->count();

            $rwpernahusulan = User::where('app',1)->where('level',1)->wherehas('usulan')->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $lpmpernahusulan = User::where('app',1)->where('level',2)->wherehas('usulan')->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $pkkpernahusulan = User::where('app',1)->where('level',3)->wherehas('usulan')->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $kartapernahusulan = User::where('app',1)->where('level',4)->wherehas('usulan')->where('KEC_ID',Auth::user()->KEC_ID)->count();

        }elseif(Auth::user()->level==9){
             $o      = User::where('login',1)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $rw     = User::where('app',1)->where('level',1)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $rwv    = User::where('app',1)->where('level',1)->where('KEC_ID',Auth::user()->KEC_ID)->where('validasi',1)->count();
            $lpm    = User::where('app',1)->where('level',2)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $lpmv   = User::where('app',1)->where('level',2)->where('validasi',1)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $pkk    = User::where('app',1)->where('level',3)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $pkkv   = User::where('app',1)->where('level',3)->where('validasi',1)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $karta  = User::where('app',1)->where('level',4)->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $kartav = User::where('app',1)->where('level',4)->where('validasi',1)->count();

            $rwpernahusulan = User::where('app',1)->where('level',1)->wherehas('usulan')->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $lpmpernahusulan = User::where('app',1)->where('level',2)->wherehas('usulan')->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $pkkpernahusulan = User::where('app',1)->where('level',3)->wherehas('usulan')->where('KEC_ID',Auth::user()->KEC_ID)->count();
            $kartapernahusulan = User::where('app',1)->where('level',4)->wherehas('usulan')->where('KEC_ID',Auth::user()->KEC_ID)->count();

        }elseif(Auth::user()->level==2 || Auth::user()->level==3 || Auth::user()->level==4){
             $o      = User::where('login',1)->where('KEL_ID',Auth::user()->KEL_ID)->count();
            $rw     = User::where('app',1)->where('level',1)->where('KEL_ID',Auth::user()->KEL_ID)->count();
            $rwv    = User::where('app',1)->where('level',1)->where('KEL_ID',Auth::user()->KEL_ID)->where('validasi',1)->count();
            $lpm    = User::where('app',1)->where('level',2)->where('KEL_ID',Auth::user()->KEL_ID)->count();
            $lpmv   = User::where('app',1)->where('level',2)->where('validasi',1)->where('KEL_ID',Auth::user()->KEL_ID)->count();
            $pkk    = User::where('app',1)->where('level',3)->where('KEL_ID',Auth::user()->KEL_ID)->count();
            $pkkv   = User::where('app',1)->where('level',3)->where('validasi',1)->where('KEL_ID',Auth::user()->KEL_ID)->count();
            $karta  = User::where('app',1)->where('level',4)->where('KEL_ID',Auth::user()->KEL_ID)->count();
            $kartav = User::where('app',1)->where('level',4)->where('validasi',1)->count();

            $rwpernahusulan = User::where('app',1)->where('level',1)->wherehas('usulan')->where('KEL_ID',Auth::user()->KEL_ID)->count();
            $lpmpernahusulan = User::where('app',1)->where('level',2)->wherehas('usulan')->where('KEL_ID',Auth::user()->KEL_ID)->count();
            $pkkpernahusulan = User::where('app',1)->where('level',3)->wherehas('usulan')->where('KEL_ID',Auth::user()->KEL_ID)->count();
            $kartapernahusulan = User::where('app',1)->where('level',4)->wherehas('usulan')->where('KEL_ID',Auth::user()->KEL_ID)->count();

        }

        $data   = array('tahun'             => $tahun,
                        'keluargaRw'        => $keluargaRw,
                        'keluargaKel' => $keluargaKel,
                        't_rembuk' => $t_rembuk,
                        't_pippk' => $t_pippk,
                        't_kel' => $t_kel,
                        't_kec' => $t_kec,
                        't_skpd' => $t_skpd,
                        'tahapan1' => $tahapan1,
                        'tahapan2' => $tahapan2,
                        'tahapan3' => $tahapan3,
                        'tahapan4' => $tahapan4,
                        'tahapan5' => $tahapan5,
                        'jumRenProses' => $jumRenProses,
                        'jumRenTerima' => $jumRenTerima,
                        'jumRenTolak' => $jumRenTolak,
                        'jumPIPPKProses' => $jumPIPPKProses,
                        'jumPIPPKTerima' => $jumPIPPKTerima,
                        'jumPIPPKTolak' => $jumPIPPKTolak,
                        'jumRenProses2018' => $jumRenProses2018,
                        'jumRenTerima2018' => $jumRenTerima2018,
                        'jumRenTolak2018' => $jumRenTolak2018,
                        'jumPIPPKProses2018' => $jumPIPPKProses2018,
                        'jumPIPPKTerima2018' => $jumPIPPKTerima2018,
                        'jumPIPPKTolak2018' => $jumPIPPKTolak2018,
                        'totRenProses'      => $totRenProses,
                        'totRenTerima' => $totRenTerima,
                        'totRenTolak' => $totRenTolak,
                        'totPIPPKProses' => $totPIPPKProses,
                        'totPIPPKTerima' => $totPIPPKTerima,
                        'totPIPPKTolak' => $totPIPPKTolak,
                        'totRenProses2018' => $totRenProses2018,
                        'totRenTerima2018' => $totRenTerima2018,
                        'totRenTolak2018' => $totRenTolak2018,
                        'totPIPPKProses2018' => $totPIPPKProses2018,
                        'totPIPPKTerima2018' => $totPIPPKTerima2018,
                        'totPIPPKTolak2018' => $totPIPPKTolak2018,
                        );
        return View('musrenbang.usulan.dashboard',$data);
    }

    public function showUsulan($tahun,$id,$kecid = null){
        $kec   = Kecamatan::all();
        $kel    = Kelurahan::where('KEC_ID',Auth::user()->KEC_ID)->get();
        $isu    = Isu::get();
        $kamus  = Kamus::get();

        $data   = array('tahun' => $tahun,
                        'isu'=>$isu,
                        'kamus'=>$kamus,
                        'id' => $id,
                        'kelurahan' => $kel,
                        'kecamatan' => $kec,
                         );

        $data["kecid"] = $kecid == null ? "x" : $kecid;

        if(Auth::user()->level==5){
            $k = Auth::user()->KEL_ID;
            $users  = USER::whereIn('level',[1,2,3,4,5])->where('KEL_ID',Auth::user()->KEL_ID)->orderBy('email')->get();

            $data["users"] = $users;

            $data["diproses"] =  USULAN::wherehas('user',function($u) use ($k){
                return $u->where('KEL_ID',$k);
            })->where('USULAN_STATUS',1)->where('USULAN_TUJUAN',$id)->where('USULAN_TAHUN',$tahun)->where('USULAN_DELETED',0)->count();

            $data["diterima"] =  USULAN::wherehas('user',function($u) use ($k){
                return $u->where('KEL_ID',$k);
            })->where('USULAN_STATUS',2)->where('USULAN_TUJUAN',$id)->where('USULAN_TAHUN',$tahun)->where('USULAN_DELETED',0)->count();

            $data["ditolak"] =  USULAN::wherehas('user',function($u) use ($k){
                return $u->where('KEL_ID',$k);
            })->where('USULAN_STATUS',0)->where('USULAN_TUJUAN',$id)->where('USULAN_TAHUN',$tahun)->where('USULAN_DELETED',0)->count();

            return View::make('musrenbang.usulan.show-usulan',$data);
        }

        if(Auth::user()->level==6){
            $k = Auth::user()->KEC_ID;
            $users  = USER::whereIn('level',[1,2,3,4,5,6])->where('KEC_ID',Auth::user()->KEC_ID)->orderBy('email')->get();
            $data["users"] = $users;

            $data["diproses"] =  USULAN::wherehas('user',function($u) use ($k){
                return $u->where('KEC_ID',$k);
            })->where('USULAN_STATUS',2)->where('USULAN_POSISI',1)->where('USULAN_TAHUN',$tahun)->where('USULAN_DELETED',0)->where('USULAN_TUJUAN',$id)->count();

            $data["diterima"] =  USULAN::wherehas('user',function($u) use ($k){
                return $u->where('KEC_ID',$k);
            })->where('USULAN_STATUS',2)->where('USULAN_POSISI',">=",2)->where('USULAN_TAHUN',$tahun)->where('USULAN_DELETED',0)->where('USULAN_TUJUAN',$id)->count();
            $data["ditolak"] =  USULAN::wherehas('user',function($u) use ($k){
                return $u->where('KEC_ID',$k);
            })->where('USULAN_STATUS',0)->where('USULAN_POSISI',">=",2)->where('USULAN_TAHUN',$tahun)->where('USULAN_DELETED',0)->where('USULAN_TUJUAN',$id)->count();

            return View::make('musrenbang.usulan.show-usulan',$data);
        }

        if(Auth::user()->level==7){
            $k = Auth::user()->SKPD_ID;

            $users  = USER::whereIn('level',[1,2,3,4,5])->orderBy('email')->get();
            $data["users"] = $users;

            $data["diproses"] =  USULAN::wherehas('kamus',function($u) use ($k){
                return $u->where("KAMUS_SKPD",$k);
            })->where('USULAN_STATUS',2)->where('USULAN_POSISI',2)->where('USULAN_TUJUAN',$id)
            ->where('USULAN_TAHUN',$tahun)->where('USULAN_DELETED',0)->NotDummies()->count();

            $data["diterima"] =  USULAN::wherehas('kamus',function($u) use ($k){
                return $u->where("KAMUS_SKPD",$k);
            })->where('USULAN_STATUS',2)->where('USULAN_POSISI',">=",3)->where('USULAN_TUJUAN',$id)
            ->where('USULAN_TAHUN',$tahun)->where('USULAN_DELETED',0)->NotDummies()->count();
            $data["ditolak"] =  USULAN::wherehas('kamus',function($u) use ($k){
                return $u->where("KAMUS_SKPD",$k);
            })->where('USULAN_STATUS',0)->where('USULAN_POSISI',">=",3)->where('USULAN_TUJUAN',$id)
            ->where('USULAN_TAHUN',$tahun)->where('USULAN_DELETED',0)->NotDummies()->count();

            return View::make('musrenbang.usulan.show-usulan',$data);
        }
        if(Auth::user()->level==8){

            if(Auth::user()->email=='sosbudpem'){
                $users  = USER::whereHas('skpd',function($x){
                                                $x->where('SKPD_BIDANG', 1);
                                            })->where('level',7)->orderBy('email')->get();

                $data["isu"] = Isu::whereHas('kamus',function($x){
                                    $x->whereHas('skpd',function($w){
                                        $w->where('SKPD_BIDANG', 1);
                                    });
                                })->get();

                $data["kamus"] = Kamus::whereHas('skpd',function($x){
                                                $x->where('SKPD_BIDANG', 1);
                                            })->get();

                $data["diproses"] =  USULAN::whereHas('kamus',function($x){
                                        $x->whereHas('skpd',function($w){
                                            $w->where('SKPD_BIDANG', 1);
                                        });
                                    })->where('USULAN_STATUS',2)->where('USULAN_POSISI',3)->count();
                $data["diterima"] =  USULAN::whereHas('kamus',function($x){
                                        $x->whereHas('skpd',function($w){
                                            $w->where('SKPD_BIDANG', 1);
                                        });
                                    })->where('USULAN_STATUS',2)->where('USULAN_POSISI',"=",4)->count();
                $data["ditolak"]  =  USULAN::whereHas('kamus',function($x){
                                        $x->whereHas('skpd',function($w){
                                            $w->where('SKPD_BIDANG', 1);
                                        });
                                    })->where('USULAN_STATUS',0)->where('USULAN_POSISI',"=",4)->count();

            }
            else if(Auth::user()->email=='ipw'){
                $users  = USER::whereHas('skpd',function($x){
                                                $x->where('SKPD_BIDANG', 2);
                                            })->where('level',7)->orderBy('email')->get();

                $data["isu"] = Isu::whereHas('kamus',function($x){
                                    $x->whereHas('skpd',function($w){
                                        $w->where('SKPD_BIDANG', 2);
                                    });
                                })->get();
                $data["kamus"] = Kamus::whereHas('skpd',function($x){
                                                $x->where('SKPD_BIDANG', 2);
                                            })->get();
                $data["diproses"] =  USULAN::whereHas('kamus',function($x){
                                        $x->whereHas('skpd',function($w){
                                            $w->where('SKPD_BIDANG', 2);
                                        });
                                    })->where('USULAN_STATUS',2)->where('USULAN_POSISI',3)->count();
                $data["diterima"] =  USULAN::whereHas('kamus',function($x){
                                        $x->whereHas('skpd',function($w){
                                            $w->where('SKPD_BIDANG', 2);
                                        });
                                    })->where('USULAN_STATUS',2)->where('USULAN_POSISI',"=",4)->count();
                $data["ditolak"]  =  USULAN::whereHas('kamus',function($x){
                                        $x->whereHas('skpd',function($w){
                                            $w->where('SKPD_BIDANG', 2);
                                        });
                                    })->where('USULAN_STATUS',0)->where('USULAN_POSISI',"=",4)->count();

            }
            else if(Auth::user()->email=='ekonomi'){
                $users  = USER::whereHas('skpd',function($x){
                                                $x->where('SKPD_BIDANG', 3);
                                            })->where('level',7)->orderBy('email')->get();

                $data["isu"] = Isu::whereHas('kamus',function($x){
                                    $x->whereHas('skpd',function($w){
                                        $w->where('SKPD_BIDANG', 3);
                                    });
                                })->get();

                $data["kamus"] = Kamus::whereHas('skpd',function($x){
                                                $x->where('SKPD_BIDANG', 3);
                                            })->get();
                $data["diproses"] =  USULAN::whereHas('kamus',function($x){
                                        $x->whereHas('skpd',function($w){
                                            $w->where('SKPD_BIDANG', 3);
                                        });
                                    })->where('USULAN_STATUS',2)->where('USULAN_POSISI',3)->count();
                $data["diterima"] =  USULAN::whereHas('kamus',function($x){
                                        $x->whereHas('skpd',function($w){
                                            $w->where('SKPD_BIDANG', 3);
                                        });
                                    })->where('USULAN_STATUS',2)->where('USULAN_POSISI',"=",4)->count();
                $data["ditolak"]  =  USULAN::whereHas('kamus',function($x){
                                        $x->whereHas('skpd',function($w){
                                            $w->where('SKPD_BIDANG', 3);
                                        });
                                    })->where('USULAN_STATUS',0)->where('USULAN_POSISI',"=",4)->count();
            }

            $data["users"] = $users;

            return View::make('musrenbang.usulan.show-usulan',$data);
        }
        if(Auth::user()->level==10){

            $users  = USER::whereIn('level',[1,2,3,4,5])->orderBy('email')->get();
            $data["users"] = $users;

            $data["diproses"] =  USULAN::where('USULAN_POSISI',3)->where('USULAN_TUJUAN',$id)
            ->where('USULAN_TAHUN',$tahun)->where('USULAN_DELETED',0)->NotDummies()->count();

            $data["diterima"] =  USULAN::where('USULAN_POSISI',">=",4)->where('USULAN_TUJUAN',$id)
            ->where('USULAN_TAHUN',$tahun)->where('USULAN_DELETED',0)->NotDummies()->count();
            $data["ditolak"] =  USULAN::where('USULAN_STATUS',0)->where('USULAN_POSISI',">=",3)->where('USULAN_TUJUAN',$id)
            ->where('USULAN_TAHUN',$tahun)->where('USULAN_DELETED',0)->NotDummies()->count();

            return View::make('musrenbang.usulan.show-usulan',$data);
        }
    }


    public function editAcc($tahun,$id){
        $user = User::where('id',Auth::user()->id)->first();
        $isu            = Isu::all();
        $kamus          = Kamus::all();
        $usulan         = Usulan::find($id);
        $perencanaan    = Perencanaan::where('PERENCANAAN_TAHUN',$tahun)->first();
        /*$usulan_kriteria  = Usulan_kriteria::where('USULAN_ID',$id)->get();
        $kriteria      = Kriteria::whereBetween('KRITERIA_ID', array(1, 7))->get();*/
       // dd($kriteria);
        $lokasi         = RW::whereHas('UserMusren', function($q){
                        $q->where('USER_ID',Auth::user()->id);
                        })->first();

        $rt             = RT::where('RW_ID',$lokasi->RW_ID)->get();

        $cekData_1      = Usulan_Rekap::where('USULAN_ID',$id)->where('USULAN_POSISI','1')->first();
        if(empty($cekData_1)){
            $rw     = Usulan::where('USULAN_ID',$id)->where('USULAN_POSISI','1')->first();
        } else {
            $rw     = $cekData_1;
        }

        $jumlah_usulan = Usulan::where('USULAN_TAHUN',$tahun)->where('USER_CREATED', Auth::user()->id)->where('USULAN_TUJUAN', $usulan->USULAN_TUJUAN)->count();

        $data = array(
                    'tahun'         => $tahun,
                    'isu'           => $isu,
                    'kamus'         => $kamus,
                    'rw'            => $rw,
                    'rt'            => $rt,
                    'usulan'        => $usulan,
                    'w'             => $lokasi,
                    'user'          => $user,
                    'perencanaan'   => $perencanaan,
                    'jumlah_usulan' => $jumlah_usulan,
                    'total_usulan'  => DB::table('MUSRENBANG.DAT_USULAN')
                                        -> select(DB::raw('count(*) as jumlah_usulan'))
                                        -> join('DATA.users','MUSRENBANG.DAT_USULAN.USER_CREATED','=','DATA.users.id')
                                        -> where('DATA.users.KEL_ID','=',$user->KEL_ID)
                                        -> get()
                                        -> first() -> jumlah_usulan,
                    );

        return View::make('musrenbang.usulan.edit-acc')->with($data);
    }

    public function daftarBerita($tahun){
        $berita = BeritaAcara::whereHas('User',function($x){
                                                $x->where('KEL_ID',Auth::user()->KEL_ID);
                                                $x->whereIn('level',[1,2,3,4]);
                                            })->get();


        $beritaFoto = BeritaAcaraFoto::whereHas('User',function($x){
                                                $x->where('KEL_ID',Auth::user()->KEL_ID);
                                                $x->whereIn('level',[1,2,3,4]);
                                            })->get();

        $user = User::whereHas('BeritaAcara')
                    ->whereHas('BeritaAcaraFoto')
                    ->where('KEL_ID',Auth::user()->KEL_ID)->get();

       // dd($user);

         $data = array(
                    'tahun'         => $tahun,
                    'berita'        => $berita,
                    'beritaFoto'    => $beritaFoto,
                    'user'          => $user);


        //dd($data["user"][1]->BeritaAcara[0]->lampiran);

        return View::make('musrenbang.usulan.daftar-berita')->with($data);
    }


    public function addKelurahan($tahun,$id){

      /*if($id==1){
            $jumlah_usulan = Usulan::where('USULAN_TAHUN',$tahun)->where('RW_ID', Auth::user()->KEL_ID)->where('USULAN_TUJUAN', $id)->count();
            if($jumlah_usulan>=MAX_USULAN){
                return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','usulan anda di tolak karena usulan RW untuk renja maksimal 4');
            }

        }*/

        $user = User::where('id',Auth::user()->id)->first();
        $isu = Isu::where('ISU_TIPE', '<>', 2)->get();

        //$usulan = Usulan::where('tahun','2017')->where('USER_CREATED', Auth::user()->id)->first();

       // $kamusid = $usulan->kamus;

        $kamus      = Kamus::all();
        $lokasi     = RW::whereHas('usermusren', function($q){
                        $q->where('USER_ID',Auth::user()->id);
                        })->where('RW_TAHUN',$tahun)->first();
        // print_r($lokasi);exit();
        $usulan     = Usulan::all();
        $rt         = RT::where('RW_ID',$lokasi->RW_ID)->orderBy('RT_NAMA')->get();
        $rw         = RW::where('KEL_ID', Auth::user()->KEL_ID);
        $data = array(
                    'lokasi'    =>$lokasi,
                    'tahun'     =>$tahun,
                    'isu'       => $isu,
                    'rt'        => $rt,
                    'usulan_tujuan' => $id,
                    'user'  => $user,
                    'rw'    => $rw
                    );
        //var_dump($data['usulan_tujuan']);
        /*if (isMobile()){
            return View('musrenbang.mobile.input', $data);
        }*/
        //dd($data);
        $now    = Carbon\Carbon::now()->format('Y-m-d');
        $tahapan    = Tahapan::where('MENU',1)->first();
        if($now >= $tahapan->TGL_AWAL && $now <= $tahapan->TGL_AKHIR){
            return View('musrenbang.usulan.add-kel', $data);
        }/*else{
            return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','Tahapan telah berakhir');
        }*/
    }


    public function getOnline($tahun){
        $pippk  = Usulan::wherehas('kamus',function($q){
                    $q->whereHas('isu',function($x){
                        $x->where('ISU_TIPE','2');
                    });
                })->count();
        $renja  = Usulan::wherehas('kamus',function($q){
                    $q->whereHas('isu',function($x){
                        $x->whereIn('ISU_TIPE',['0','1']);
                    });
                })->count();
        $o  = User::where('login',1)->where('KEL_ID',Auth::user()->KEL_ID)->count();
        return array('pippk'=>$pippk,'renja'=>$renja,'o'=>$o);
    }


    public function showUsulanData($tahun,$nama,$status,$rw = "x" ,$isu="x",$tipe="x",$kamus="x"){

        if(Auth::user()->level == 1){
            $data           = Usulan::where('USULAN_TAHUN',$tahun)->where('RW_ID', Auth::user()->RW_ID)->where('USULAN_TUJUAN', $nama)->orderBy('USULAN_ID','asc')->get();

        }elseif(Auth::user()->level == 5){

                 $data   = Usulan::wherehas('rw',function($q){
                    $q->whereHas('kelurahan',function($x){
                        $x->where('KEL_ID',Auth::user()->KEL_ID);
                    });
                })->where('USULAN_TAHUN',$tahun)->where('USULAN_POSISI','>=' ,1)
                    ->where('USULAN_TUJUAN', $nama)->orderBy('USULAN_ID','asc');

            if ($rw != "x")
            {
                $data = $data->where('REFERENSI.REF_RW.RW_ID',$rw);
            }

            if ($isu != "x")
            {
                $data = $data->where('REFERENSI.REF_KAMUS.ISU_ID',$isu);
            }

            if ($tipe != "x")
            {
                $data = $data->where('DAT_USULAN.USULAN_TUJUAN',$tipe);
            }

            if ($kamus != "x")
            {
                $data = $data->where('DAT_USULAN.KAMUS_ID',$kamus);
            }

            if($status == 1)
            {
                $data = $data->where('USULAN_STATUS',$status);
            }
            if($status == 2)
            {
                $data = $data->where('USULAN_STATUS',$status);
            }
            if($status == 0)
            {
                $data = $data->where('USULAN_STATUS',$status);
            }

            $data = $data->get();

        }else{

            $data           = Usulan::where('USULAN_TAHUN',$tahun)->where('USULAN_POSISI','>=' , 3)->get();

        }

        $no             = 1;
        $aksi           = '';
        $view           = array();
        $rt             = '';
        $rts            = '';

        foreach ($data as $data) {

            if($data->USULAN_STATUS == 1) $status = '<i class="fa fa-refresh text-info"></i>';
            elseif($data->USULAN_STATUS == 2) $status = '<i class="fa fa-check text-success"></i>';
            else $status = '<i class="fa fa-close text-danger"></i>';


            if($data->USULAN_POSISI == 1) $posisi = ' Kelurahan';
            elseif($data->USULAN_POSISI == 2) $posisi = ' Kecamatan';
            elseif($data->USULAN_POSISI == 3) $posisi = ' Kota';
            else $posisi = ' APBD';

            // $rw = RW::whereIn('RW',$data->RT_ID)->first();

            $pengusul   = $data->user->email;




            $pagu       = $data->USULAN_VOLUME * $data->kamus->KAMUS_HARGA;
            if(Auth::user()->app == 1 and Auth::user()->level == $data->user->level){

            $aksi       = $aksi         = ' <div class="action visible pull-right">
                                                <a href="'.url('/musrenbang/'.$tahun.'/usulan/detail/'.$data->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>
                                                <a href="'.url('/musrenbang/'.$tahun.'/usulan/edit/'.$data->USULAN_ID).'" class="action-edit"><i class="mi-edit"></i></a>
                                                <a onclick="return hapus(\''.$data->USULAN_ID.'\')" class="action-delete"><i class="mi-trash"></i></a>
                                            </div>';
            }else{
            $url = url('/musrenbang/'.$tahun.'/usulan/editAcc/'.$data->USULAN_ID);
            if($data->USULAN_STATUS != 1)
            {
                $url = url('/musrenbang/'.$tahun.'/usulan/detail/'.$data->USULAN_ID);
            }
            $aksi       = $aksi         = ' <div class="action visible pull-right">
                                                <a href="'.$url.'" class="action-preveiw"><i class="mi-eye"></i></a>
                                            </div>';
            }

            array_push($view, array( 'USULAN_ID'        => $data->USULAN_ID,
                                     'NO'               => $no,
                                     'LOKASI'             => $pengusul,
                                     'ISU'              => $data->kamus->isu->ISU_NAMA."<br><p class='text-orange'>".$data->kamus->KAMUS_NAMA."</p>",
                                     'STATUS'           => "<p>".$status.$posisi."</p>",
                                     'VOLUME'           => $data->USULAN_VOLUME." ".$data->kamus->KAMUS_SATUAN,
                                     'HARGA'            => $data->kamus->KAMUS_HARGA,
                                    'ANGGARAN'         => number_format($pagu,0,'.',','),
                                     'AKSI'             => $aksi,
                                     'PRIORITAS'        => $data->USULAN_PRIORITAS,
                                     'KETERANGAN'        => $data->KET_ACC,
                                     ));
            $no++;
        }
            $out = array("aaData"=>$view);
            return Response::JSON($out);

    }


    public function showUsulanDataKel($tahun,$nama,$status,$kel="x",$rw = "x" ,$isu="x",$tipe="x",$kamus="x"){

        if(Auth::user()->level == 6){

            $data   = Usulan::wherehas('rw',function($q){
                    $q->whereHas('kelurahan',function($x){
                        $x->where('KEC_ID',Auth::user()->KEC_ID);
                    });
                })->where('USULAN_TAHUN',$tahun)->where('USULAN_POSISI','>=' ,1)
                    ->where('USULAN_TUJUAN', $nama)->orderBy('USULAN_ID','asc');

            if ($kel != "x")
            {
                $data = $data->where('REFERENSI.REF_KELURAHAN.KEL_ID',$kel);
            }
            if ($rw != "x")
            {
                $data = $data->where('REFERENSI.REF_RW.RW_ID',$rw);
            }

            if ($isu != "x")
            {
                $data = $data->where('REFERENSI.REF_KAMUS.ISU_ID',$isu);
            }

            if ($tipe != "x")
            {
                $data = $data->where('DAT_USULAN.USULAN_TUJUAN',$tipe);
            }

            if ($kamus != "x")
            {
                $data = $data->where('DAT_USULAN.KAMUS_ID',$kamus);
            }

            if($status == 1)
            {

                $data = $data->where('USULAN_STATUS',2)->where('USULAN_POSISI','=' ,1);
            }
            if($status == 2)
            {
                $data = $data->where('USULAN_STATUS',2)->has('usulan_rekap')->where('USULAN_POSISI','=' ,2);

            }
            if($status == 0)
            {
                $data = $data->where('USULAN_STATUS',0)->where('USULAN_POSISI','=' ,1);
            }

            $data = $data->get();

        }

        $no             = 1;
        $aksi           = '';
        $view           = array();
        $rt             = '';
        $rts            = '';

        foreach ($data as $data) {

            if($data->USULAN_STATUS == 1) $status = '<i class="fa fa-refresh text-info"></i>';
            elseif($data->USULAN_STATUS == 2) $status = '<i class="fa fa-check text-success"></i>';
            else $status = '<i class="fa fa-close text-danger"></i>';


            if($data->USULAN_POSISI == 1) $posisi = ' Kelurahan';
            elseif($data->USULAN_POSISI == 2) $posisi = ' Kecamatan';
            elseif($data->USULAN_POSISI == 3) $posisi = ' Kota';
            else $posisi = ' APBD';

            if($data->USULAN_STATUS == 2 && $data->USULAN_POSISI == 1)
            {
                $status = '<i class="fa fa-refresh text-info"></i>';
                $posisi = ' Kecamatan';
            }

            // $rw = RW::whereIn('RW',$data->RT_ID)->first();
            $pengusul   = $data->user->email;

            $pagu       = $data->USULAN_VOLUME * $data->kamus->KAMUS_HARGA;
            if(Auth::user()->level == $data->user->level){

            $aksi       = $aksi         = ' <div class="action visible pull-right">
                                                <a href="'.url('/musrenbang/'.$tahun.'/usulan/detail/'.$data->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>
                                                <a href="'.url('/musrenbang/'.$tahun.'/usulan/edit/'.$data->USULAN_ID).'" class="action-edit"><i class="mi-edit"></i></a>
                                                <a onclick="return hapus(\''.$data->USULAN_ID.'\')" class="action-delete"><i class="mi-trash"></i></a>
                                            </div>';
            }else{

                $url = url('/musrenbang/'.$tahun.'/usulan/editAcc/'.$data->USULAN_ID);
                if($data->USULAN_STATUS != 1)
                {
                    // $url = url('/musrenbang/'.$tahun.'/usulan/detail/'.$data->USULAN_ID);
                }
                $aksi       = $aksi         = ' <div class="action visible pull-right">
                                                    <a href="'.$url.'" class="action-preveiw"><i class="mi-eye"></i></a>
                                                </div>';
            }

            array_push($view, array( 'USULAN_ID'        => $data->USULAN_ID,
                                     'NO'               => $no,
                                     'LOKASI'             => $pengusul,
                                     'ISU'              => $data->kamus->isu->ISU_NAMA."<br><p class='text-orange'>".$data->kamus->KAMUS_NAMA."</p>",
                                     'STATUS'           => "<p>".$status.$posisi."</p>",
                                     'VOLUME'           => $data->USULAN_VOLUME." ".$data->kamus->KAMUS_SATUAN,
                                     'HARGA'            => $data->kamus->KAMUS_HARGA,
                                    'ANGGARAN'         => number_format($pagu,0,'.',','),
                                     'AKSI'             => $aksi,
                                     'PRIORITAS'        => $data->USULAN_PRIORITAS,
                                     'KETERANGAN'        => $data->KET_ACC,
                                     ));
            $no++;
        }
            $out = array("aaData"=>$view);
            return Response::JSON($out);

    }


    public function showUsulanDataKec($tahun,$nama,$status,$kec="x", $kel="x",$rw = "x" ,$isu="x",$kamus="x"){

        if(Auth::user()->level == 7){

            $data   = Usulan::wherehas('rw',function($q){
                    $q->whereHas('kelurahan',function($x){
                        $x->where('KEC_ID',Auth::user()->KEC_ID);
                    });
                })->where('USULAN_TAHUN',$tahun)->where('USULAN_POSISI','>=' ,1)
                    ->where('USULAN_TUJUAN', $nama)->orderBy('USULAN_ID','asc');

            if ($kel != "x")
            {
                $data = $data->where('REFERENSI.REF_KELURAHAN.KEL_ID',$kel);
            }
            if ($rw != "x")
            {
                $data = $data->where('REFERENSI.REF_RW.RW_ID',$rw);
            }

            if ($isu != "x")
            {
                $data = $data->where('REFERENSI.REF_KAMUS.ISU_ID',$isu);
            }

            if ($tipe != "x")
            {
                $data = $data->where('DAT_USULAN.USULAN_TUJUAN',$tipe);
            }

            if ($kamus != "x")
            {
                $data = $data->where('DAT_USULAN.KAMUS_ID',$kamus);
            }

            if($status == 1)
            {
                $data = $data->where('USULAN_STATUS',2)->where('USULAN_POSISI','=' ,1);
            }
            if($status == 2)
            {
                $data = $data->where('USULAN_STATUS',2)->has('usulan_rekap')->where('USULAN_POSISI','=' ,2);
            }
            if($status == 0)
            {
                $data = $data->where('USULAN_STATUS',0)->where('USULAN_POSISI','=' ,1);
            }

            $data = $data->get();

        }

        $no             = 1;
        $aksi           = '';
        $view           = array();
        $rt             = '';
        $rts            = '';

        foreach ($data as $data) {

            if($data->USULAN_STATUS == 1) $status = '<i class="fa fa-refresh text-info"></i>';
            elseif($data->USULAN_STATUS == 2) $status = '<i class="fa fa-check text-success"></i>';
            else $status = '<i class="fa fa-close text-danger"></i>';


            if($data->USULAN_POSISI == 1) $posisi = ' Kelurahan';
            elseif($data->USULAN_POSISI == 2) $posisi = ' Kecamatan';
            elseif($data->USULAN_POSISI == 3) $posisi = ' Kota';
            else $posisi = ' APBD';

            if($data->USULAN_STATUS == 2 && $data->USULAN_POSISI == 1)
            {
                $status = '<i class="fa fa-refresh text-info"></i>';
                $posisi = ' Kecamatan';
            }

            // $rw = RW::whereIn('RW',$data->RT_ID)->first();
            $pengusul   = $data->user->email;

            $pagu       = $data->USULAN_VOLUME * $data->kamus->KAMUS_HARGA;
            if(Auth::user()->app == 1 and Auth::user()->level == 1){

            $aksi       = $aksi         = ' <div class="action visible pull-right">
                                                <a href="'.url('/musrenbang/'.$tahun.'/usulan/detail/'.$data->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>
                                                <a href="'.url('/musrenbang/'.$tahun.'/usulan/edit/'.$data->USULAN_ID).'" class="action-edit"><i class="mi-edit"></i></a>
                                                <a onclick="return hapus(\''.$data->USULAN_ID.'\')" class="action-delete"><i class="mi-trash"></i></a>
                                            </div>';
            }else{
            $aksi       = $aksi         = ' <div class="action visible pull-right">
                                                <a href="'.url('/musrenbang/'.$tahun.'/usulan/editAccKel/'.$data->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>
                                            </div>';
            }

            array_push($view, array( 'USULAN_ID'        => $data->USULAN_ID,
                                     'NO'               => $no,
                                     'LOKASI'             => $pengusul,
                                     'ISU'              => $data->kamus->isu->ISU_NAMA."<br><p class='text-orange'>".$data->kamus->KAMUS_NAMA."</p>",
                                     'STATUS'           => "<p>".$status.$posisi."</p>",
                                     'VOLUME'           => $data->USULAN_VOLUME." ".$data->kamus->KAMUS_SATUAN,
                                     'HARGA'            => $data->kamus->KAMUS_HARGA,
                                    'ANGGARAN'         => number_format($pagu,0,'.',','),
                                     'AKSI'             => $aksi,
                                     'PRIORITAS'        => $data->USULAN_PRIORITAS,
                                     'KETERANGAN'        => $data->KET_ACC,
                                     ));
            $no++;
        }
            $out = array("aaData"=>$view);
            return Response::JSON($out);

    }


    public function getExport($tahun,$rw = "x" ,$isu="x",$tipe="x",$kamus="x"){

        if(Auth::user()->level == 1){
            $data           = Usulan::where('USULAN_TAHUN',$tahun)->where('RW_ID', Auth::user()->RW_ID)->orderBy('USULAN_ID','asc')->get();

        }elseif(Auth::user()->level == 5){

            $data           = Usulan::leftJoin('REFERENSI.REF_RW','REF_RW.RW_ID','=','DAT_USULAN.RW_ID')
                                ->leftJoin('REFERENSI.REF_KAMUS','REFERENSI.REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                                ->where('KEL_ID',Auth::user()->KEL_ID)
                ->where('USULAN_TAHUN',$tahun)->where('USULAN_POSISI','>=' ,1)->orderBy('USULAN_ID','asc');

            if ($rw != "x")
            {
                $data = $data->where('REFERENSI.REF_RW.RW_ID',$rw);
            }

            if ($isu != "x")
            {
                $data = $data->where('REFERENSI.REF_KAMUS.ISU_ID',$isu);
            }

            if ($tipe != "x")
            {
                $data = $data->where('DAT_USULAN.USULAN_TUJUAN',$tipe);
            }

            if ($kamus != "x")
            {
                $data = $data->where('DAT_USULAN.KAMUS_ID',$kamus);
            }

            $data = $data->get();

        }elseif(Auth::user()->level == 6){

            $data   = Usulan::wherehas('rw',function($q){
                    $q->whereHas('kelurahan',function($x){
                        $x->where('KEC_ID',Auth::user()->KEC_ID);
                    });
                })->where('USULAN_TAHUN',$tahun)->where('USULAN_POSISI','>=' ,1)
                    ->orderBy('USULAN_ID','asc');

            if ($rw != "x")
            {
                $data = $data->where('REFERENSI.REF_RW.RW_ID',$rw);
            }

            if ($isu != "x")
            {
                $data = $data->where('REFERENSI.REF_KAMUS.ISU_ID',$isu);
            }

            if ($tipe != "x")
            {
                $data = $data->where('DAT_USULAN.USULAN_TUJUAN',$tipe);
            }

            if ($kamus != "x")
            {
                $data = $data->where('DAT_USULAN.KAMUS_ID',$kamus);
            }

            if($status == 1)
            {
                $data = $data->where('USULAN_STATUS',$status)->whereDoesntHave("usulan_rekap");
            }
            if($status == 2)
            {
                $data = $data->where('USULAN_STATUS',$status)->has('usulan_rekap');
            }
            if($status == 0)
            {
                $data = $data->where('USULAN_STATUS',$status);
            }

            $data = $data->get();


        }else{

            $data           = Usulan::where('USULAN_TAHUN',$tahun)->where('USULAN_POSISI','>=' , 3)->get();

        }

        $no             = 1;
        $aksi           = '';
        $view           = array();
        $rt             = '';
        $rts            = '';

        foreach ($data as $data) {

            $rt = "";
            $i = 0;
            /*foreach ($data->RT_ID as $key => $rtid) {
                $objRT = RT::find($rtid);
                if ($objRT != null)
                    $rt = $rt."RT ".$objRT->RT_NAMA;
                if($i < count($data->RT_ID)-1)
                    $rt = $rt.", ";
                $i++;
            }*/
            $rts = RT::whereIn('RT_ID',$data->RT_ID)->orderBy('RT_NAMA')->get()
                    ->map(function($item){
                        return "RT ".$item->RT_NAMA;
                    })->all();
            $rt = implode(",",$rts);

            if($data->USULAN_POSISI == 1) $posisi = ' Kelurahan';
            elseif($data->USULAN_POSISI == 2) $posisi = ' Kecamatan';
            elseif($data->USULAN_POSISI == 3) $posisi = ' Kota';
            else $posisi = ' APBD';

            $pagu       = $data->USULAN_VOLUME * $data->kamus->KAMUS_HARGA;

            array_push($view, array(
                                     'NO'               => $no,
                                     'Pengusul'         => "Kecamatan ".$data->rw->kelurahan->kecamatan->KEC_NAMA." Kelurahan ".$data->rw->kelurahan->KEL_NAMA." RW ".$data->rw->RW_NAMA,
                                     'Urgensi'          => $data->USULAN_URGENSI,
                                     'Lokasi'               => $data->ALAMAT,
                                     'Kamus'            => $data->kamus->KAMUS_NAMA,
                                     'Penerima Manffat' => $rt,
                                     'Isu'              => $data->kamus->isu->ISU_NAMA,
                                     'Status'           => $posisi,
                                     'VOLUME'           => $data->USULAN_VOLUME,
                                     'SATUAN'           => $data->kamus->KAMUS_SATUAN,
                                     'HARGA'            => $data->kamus->KAMUS_HARGA,
                                     'ANGGARAN'          => $pagu
                                     ));
            $no++;
        }

        Excel::create('export', function($excel) use ($view) {

            $excel->sheet('Sheet 1', function($sheet) use ($view) {

                $sheet->fromArray($view, null, 'A1', true);

            });

        })->export('xls');
    }


    function editAccKel(){
        if(Input::get('val') == 'acc') {
            $posisi = 1;
            if(Auth::user()->app == 1 && Auth::user()->level == 5) $posisi = 2;
            elseif(Auth::user()->app == 1 && Auth::user()->level == 6) $posisi = 3;
            elseif(Auth::user()->app == 1 && Auth::user()->level == 7) $posisi = 4;
            Usulan::where('USULAN_ID',Input::get('usulan_id'))->update(['USULAN_STATUS' => '2']);
            $data       = Usulan::find(Input::get('usulan_id'));
            $rekap      = new Usulan_Rekap;

            $rekap->USULAN_ID       = $data->USULAN_ID;
            $rekap->USULAN_TAHUN    = $data->USULAN_TAHUN;
            $rekap->KAMUS_ID        = $data->KAMUS_ID;
            $rekap->USULAN_URGENSI  = $data->USULAN_URGENSI;
            $rekap->USULAN_VOLUME   = $data->USULAN_VOLUME;
            $rekap->USULAN_GAMBAR   = $data->USULAN_GAMBAR[0];
            $rekap->RT_ID           = $data->RT_ID[0];
            $rekap->USULAN_LAT      = $data->USULAN_LAT;
            $rekap->USULAN_LONG     = $data->USULAN_LONG;
            $rekap->USULAN_PRIORITAS= Input::get('prioritas');
            $rekap->KET_ACC         = Input::get('keterangan');
            $rekap->USULAN_STATUS   = $data->USULAN_STATUS;
            $rekap->USULAN_POSISI   = $data->USULAN_POSISI;
            $rekap->USER_CREATED    = $data->USER_CREATED;
            $rekap->TIME_CREATED    = $data->TIME_CREATED;
            $rekap->IP_CREATED      = $data->IP_CREATED;
            $rekap->USER_UPDATED    = $data->USER_UPDATED;
            $rekap->TIME_UPDATED    = $data->TIME_UPDATED;
            $rekap->IP_UPDATED      = $data->IP_UPDATED;
            $rekap->USULAN_TUJUAN    = $data->USULAN_TUJUAN;

            $rekap->save();



            //$idtujuan = Tujuan::where('TUJUAN_ID', $data->USULAN_TUJUAN)->first();
            $id = Input::get('usulan_id');
            //print_r($rekap);
            //echo "string";
            Usulan::where('USULAN_ID',Input::get('usulan_id'))
                    ->update([
                        'USULAN_URGENSI'       => Input::get('urgensi'),
                        'USULAN_VOLUME'        => Input::get('vol'),
                        //'RT_ID'                => Input::get('rt'),
                        'USULAN_TUJUAN'        => Input::get('tujuan'),
                        'USULAN_STATUS'        => '2',
                        'USULAN_POSISI'        => $posisi,
                        'USER_UPDATED'         => Auth::user()->id,
                        'TIME_UPDATED'         => Carbon\Carbon::now(),
                        'IP_UPDATED'           => $_SERVER['REMOTE_ADDR']]);

            return Redirect('musrenbang/'.$tahun.'/usulan/show/'.$data->USULAN_TUJUAN);

        }
    }




    //get usulan per pengusul  tingkat kelurahan
    //RW
    public function getUsulanRw($tahun,$tab){
        //kelurahan
       if(Auth::user()->level==ROLE_KEL){
            $data   = Usulan::whereHas('rw',function($q){
            $q->where('KEL_ID',Auth::user()->KEL_ID);
            })->where('USULAN_TAHUN',$tahun)
            ->orderBy('USULAN_ID','asc')->get();
       }
       //kecamatan
       elseif(Auth::user()->level==ROLE_KEC){
            $data   = Usulan::wherehas('rw',function($q){
                    $q->whereHas('kelurahan',function($x){
                        $x->where('KEC_ID',Auth::user()->KEC_ID);
                    });
                })->where('USULAN_TAHUN',$tahun)->where('USULAN_POSISI','>=' ,1)
                    ->orderBy('USULAN_ID','asc')->get();
       }

        $no             = 1;
        $aksi           = '';
        $view           = array();
        $rt             = '';
        $rts            = '';

        foreach ($data as $data) {

            if($data->USULAN_STATUS == 1) $status = '<i class="fa fa-refresh text-info"></i>';
            elseif($data->USULAN_STATUS == 2) $status = '<i class="fa fa-check text-success"></i>';
            else $status = '<i class="fa fa-close text-danger"></i>';


            if($data->USULAN_POSISI == 1) $posisi = ' Kelurahan';
            elseif($data->USULAN_POSISI == 2) $posisi = ' Kecamatan';
            elseif($data->USULAN_POSISI == 3) $posisi = ' Kota';
            else $posisi = ' APBD';

            $pengusul   = $data->user->email;


            $pagu       = $data->USULAN_VOLUME * $data->kamus->KAMUS_HARGA;
            if(Auth::user()->app == 1 and Auth::user()->level == $data->user->level){

            $aksi       = $aksi         = ' <div class="action visible pull-right">
                                                <a href="'.url('/musrenbang/'.$tahun.'/usulan/detail/'.$data->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>
                                                <a href="'.url('/musrenbang/'.$tahun.'/usulan/edit/'.$data->USULAN_ID).'" class="action-edit"><i class="mi-edit"></i></a>
                                                <a onclick="return hapus(\''.$data->USULAN_ID.'\')" class="action-delete"><i class="mi-trash"></i></a>
                                            </div>';
            }else{
            $aksi       = $aksi         = ' <div class="action visible pull-right">
                                                <a href="'.url('/musrenbang/'.$tahun.'/usulan/editAcc/'.$data->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>
                                            </div>';
            }

            array_push($view, array( 'USULAN_ID'        => $data->USULAN_ID,
                                     'NO'               => $no,
                                     'LOKASI'             => $pengusul,
                                     'ISU'              => $data->kamus->isu->ISU_NAMA."<br><p class='text-orange'>".$data->kamus->KAMUS_NAMA."</p>",
                                     'STATUS'           => "<p>".$status.$posisi."</p>",
                                     'VOLUME'           => $data->USULAN_VOLUME." ".$data->kamus->KAMUS_SATUAN,
                                     'HARGA'            => $data->kamus->KAMUS_HARGA,
                                    'ANGGARAN'         => number_format($pagu,0,'.',','),
                                     'AKSI'             => $aksi));
            $no++;
        }
            $out = array("aaData"=>$view);
            return Response::JSON($out);

    }


    //LPM
     public function getUsulanLpm($tahun){
        //kelurahan
       if(Auth::user()->level==ROLE_KEL){
        $data   = Usulan::whereHas('user',function($q){
                    $q->where('KEL_ID',Auth::user()->KEL_ID)->where('level',ROLE_LPM);
                    })->where('USULAN_TAHUN',$tahun)
                    ->orderBy('USULAN_ID','asc')->get();
        }
        //kecamatan
       elseif(Auth::user()->level==ROLE_KEC){
            $data   = Usulan::whereHas('user',function($q){
                    $q->where('KEC_ID',Auth::user()->KEC_ID)->where('level',ROLE_LPM);
                    })->where('USULAN_TAHUN',$tahun)
                    ->orderBy('USULAN_ID','asc')->get();
       }

       // dd($data);

        $no             = 1;
        $aksi           = '';
        $view           = array();
        $rt             = '';
        $rts            = '';

        foreach ($data as $data) {

            if($data->USULAN_STATUS == 1) $status = '<i class="fa fa-refresh text-info"></i>';
            elseif($data->USULAN_STATUS == 2) $status = '<i class="fa fa-check text-success"></i>';
            else $status = '<i class="fa fa-close text-danger"></i>';


            if($data->USULAN_POSISI == 1) $posisi = ' Kelurahan';
            elseif($data->USULAN_POSISI == 2) $posisi = ' Kecamatan';
            elseif($data->USULAN_POSISI == 3) $posisi = ' Kota';
            else $posisi = ' APBD';

            // $rw = RW::whereIn('RW',$data->RT_ID)->first();

            $pengusul   = $data->user->email;

            /*$pengusul   = 'Kecamatan '.$data->rw->kelurahan->kecamatan->KEC_NAMA.'<br>'.
                          'Kelurahan '.$data->rw->kelurahan->KEL_NAMA.'<br>'.
                          'RW '.$data->rw->RW_NAMA;*/

            /*$pengusul   = 'Kecamatan '.$data->rw->kelurahan->kecamatan->KEC_NAMA.'<br>';

            if($data->user->level != ROLE_KEC)
            {
                $pengusul   = $pengusul.'Kelurahan '.$data->rw->kelurahan->KEL_NAMA.'<br>';

                if($data->user->level == ROLE_RW)
                {
                    $pengusul = $pengusul.'RW '.$data->rw->RW_NAMA;
                }
                else if($data->user->level == ROLE_LPM)
                {
                    $pengusul = $pengusul.'LPM';
                }
                else if($data->user->level == ROLE_PKK)
                {
                    $pengusul = $pengusul.'PKK';
                }
                else if($data->user->level == ROLE_KARTA)
                {
                    $pengusul = $pengusul.'KARTA';
                }
            }*/


            $pagu       = $data->USULAN_VOLUME * $data->kamus->KAMUS_HARGA;
            if(Auth::user()->app == 1 and Auth::user()->level == $data->user->level){
            // $aksi       = $aksi         = ' <div class="action visible pull-right">
            //                                     <a href="'.url('/musrenbang/'.$tahun.'/usulan/detail/'.$data->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>
            //                                     <a href="'.url('/musrenbang/'.$tahun.'/usulan/edit/'.$data->USULAN_ID).'" class="action-edit"><i class="mi-edit"></i></a>
            //                                     <a onclick="return hapus(\''.$data->USULAN_ID.'\')" class="action-delete"><i class="mi-trash"></i></a>
            //                                 </div>';
            $aksi       = $aksi         = ' <div class="action visible pull-right">
                                                <a href="'.url('/musrenbang/'.$tahun.'/usulan/detail/'.$data->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>
                                                <a href="'.url('/musrenbang/'.$tahun.'/usulan/edit/'.$data->USULAN_ID).'" class="action-edit"><i class="mi-edit"></i></a>
                                                <a onclick="return hapus(\''.$data->USULAN_ID.'\')" class="action-delete"><i class="mi-trash"></i></a>
                                            </div>';
            }else{
            $aksi       = $aksi         = ' <div class="action visible pull-right">
                                                <a href="'.url('/musrenbang/'.$tahun.'/usulan/editAcc/'.$data->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>
                                            </div>';
            }

            array_push($view, array( 'USULAN_ID'        => $data->USULAN_ID,
                                     'NO'               => $no,
                                     'LOKASI'             => $pengusul,
                                     'ISU'              => $data->kamus->isu->ISU_NAMA."<br><p class='text-orange'>".$data->kamus->KAMUS_NAMA."</p>",
                                     'STATUS'           => "<p>".$status.$posisi."</p>",
                                     'VOLUME'           => $data->USULAN_VOLUME." ".$data->kamus->KAMUS_SATUAN,
                                     'HARGA'            => $data->kamus->KAMUS_HARGA,
                                    'ANGGARAN'         => number_format($pagu,0,'.',','),
                                     'AKSI'             => $aksi));
            $no++;
        }
            $out = array("aaData"=>$view);
            return Response::JSON($out);
    }

    //PKK
     public function getUsulanPkk($tahun){
        //kelurahan
       if(Auth::user()->level==ROLE_KEL){
            $data   = Usulan::whereHas('user',function($q){
                    $q->where('KEL_ID',Auth::user()->KEL_ID)->where('level',ROLE_PKK);
                    })->where('USULAN_TAHUN',$tahun)
                    ->orderBy('USULAN_ID','asc')->get();
        }
        //kecamatan
        elseif(Auth::user()->level==ROLE_KEC){
             $data   = Usulan::whereHas('user',function($q){
                    $q->where('KEC_ID',Auth::user()->KEC_ID)->where('level',ROLE_PKK);
                    })->where('USULAN_TAHUN',$tahun)
                    ->orderBy('USULAN_ID','asc')->get();
        }
        dd($data);
    }

    //KARTA
     public function getUsulanKarta($tahun){
        //kelurahan
       if(Auth::user()->level==ROLE_KEL){
            $data   = Usulan::whereHas('user',function($q){
                    $q->where('KEL_ID',Auth::user()->KEL_ID)->where('level',ROLE_KARTA);
                    })->where('USULAN_TAHUN',$tahun)
                    ->orderBy('USULAN_ID','asc')->get();
        }
        //kecamatan
        elseif(Auth::user()->level==ROLE_KEC){
            $data   = Usulan::whereHas('user',function($q){
                    $q->where('KEC_ID',Auth::user()->KEC_ID)->where('level',ROLE_KARTA);
                    })->where('USULAN_TAHUN',$tahun)
                    ->orderBy('USULAN_ID','asc')->get();
        }
       // dd($data);
    }

    //kelurahan yang login
     public function getUsulanKelurahan($tahun){
         $data   = Usulan::where('USER_CREATED',Auth::user()->id)
                    ->orderBy('USULAN_ID','asc')->get();
        dd($data);
    }


    //show controller
    public function showUsulanRw($tahun){
        $kel    = Kelurahan::where('KEC_ID',Auth::user()->KEC_ID)->get();
        $rw     = RW::where('KEL_ID',Auth::user()->KEL_ID)->get();
        $isu    = Isu::get();
        $kamus  = Kamus::get();

        $data   = array('tahun' => $tahun,
                        'rw'=>$rw,
                        'isu'=>$isu,
                        'kamus'=>$kamus,
                        'kelurahan' => $kel );

        return View::make('musrenbang.usulan.show-usulan-pengusul-rw',$data);
    }
    public function showUsulanLpm($tahun){

    }
    public function showUsulanPkk($tahun){

    }
    public function showUsulanKarta($tahun){

    }
    public function showUsulanKelurahan($tahun){

    }


    public function filterUsulanRw($tahun,$status,$rw = "x" ,$isu="x",$tipe="x",$kamus="x"){

        //kelurahan
       if(Auth::user()->level==ROLE_KEL){
            $data   = Usulan::whereHas('rw',function($q){
            $q->where('KEL_ID',Auth::user()->KEL_ID);
            })->where('USULAN_TAHUN',$tahun)
            ->orderBy('USULAN_ID','asc');
       }
       //kecamatan
       elseif(Auth::user()->level==ROLE_KEC){
            $data   = Usulan::wherehas('rw',function($q){
                    $q->whereHas('kelurahan',function($x){
                        $x->where('KEC_ID',Auth::user()->KEC_ID);
                    });
                })->where('USULAN_TAHUN',$tahun)->where('USULAN_POSISI','>=' ,1)
                    ->orderBy('USULAN_ID','asc');
       }

       if ($rw != "x")
        {
            $data = $data->where('REFERENSI.REF_RW.RW_ID',$rw);
        }

        if ($isu != "x")
        {
            $data = $data->where('REFERENSI.REF_KAMUS.ISU_ID',$isu);
        }

        if ($tipe != "x")
        {
            $data = $data->where('DAT_USULAN.USULAN_TUJUAN',$tipe);
        }

        if ($kamus != "x")
        {
            $data = $data->where('DAT_USULAN.KAMUS_ID',$kamus);
        }

        $data = $data->get();

        $no             = 1;
        $aksi           = '';
        $view           = array();
        $rt             = '';
        $rts            = '';

        foreach ($data as $data) {

            if($data->USULAN_STATUS == 1) $status = '<i class="fa fa-refresh text-info"></i>';
            elseif($data->USULAN_STATUS == 2) $status = '<i class="fa fa-check text-success"></i>';
            else $status = '<i class="fa fa-close text-danger"></i>';


            if($data->USULAN_POSISI == 1) $posisi = ' Kelurahan';
            elseif($data->USULAN_POSISI == 2) $posisi = ' Kecamatan';
            elseif($data->USULAN_POSISI == 3) $posisi = ' Kota';
            else $posisi = ' APBD';

            $pengusul   = $data->user->email;


            $pagu       = $data->USULAN_VOLUME * $data->kamus->KAMUS_HARGA;

            if(Auth::user()->app == 1 and Auth::user()->level == $data->user->level){

            $aksi       = $aksi         = ' <div class="action visible pull-right">
                                                <a href="'.url('/musrenbang/'.$tahun.'/usulan/detail/'.$data->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>
                                                <a href="'.url('/musrenbang/'.$tahun.'/usulan/edit/'.$data->USULAN_ID).'" class="action-edit"><i class="mi-edit"></i></a>
                                                <a onclick="return hapus(\''.$data->USULAN_ID.'\')" class="action-delete"><i class="mi-trash"></i></a>
                                            </div>';
            }else{
            $aksi       = $aksi         = ' <div class="action visible pull-right">
                                                <a href="'.url('/musrenbang/'.$tahun.'/usulan/editAcc/'.$data->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>
                                            </div>';
            }

            array_push($view, array( 'USULAN_ID'        => $data->USULAN_ID,
                                     'NO'               => $no,
                                     'LOKASI'             => $pengusul,
                                     'ISU'              => $data->kamus->isu->ISU_NAMA."<br><p class='text-orange'>".$data->kamus->KAMUS_NAMA."</p>",
                                     'STATUS'           => "<p>".$status.$posisi."</p>",
                                     'VOLUME'           => $data->USULAN_VOLUME." ".$data->kamus->KAMUS_SATUAN,
                                     'HARGA'            => $data->kamus->KAMUS_HARGA,
                                    'ANGGARAN'         => number_format($pagu,0,'.',','),
                                     'AKSI'             => $aksi));
            $no++;
        }
            $out = array("aaData"=>$view);
            return Response::JSON($out);

    }

    public function showHistory($usulan_id){
        $str  = "<ul>";
        $data = Notification_log::orderBy('created_at','desc') //awalnya desc terhadap created_at
                        ->where('USULAN_ID',$usulan_id)
                        ->groupBy("created_at")
                        ->groupBy("updated_at")
                        ->groupBy("ID_USER_SUBJECT")
                        ->groupBy("ID_USER_TARGET")
                        ->groupBy("PREDIKAT_AKSI")
                        ->groupBy("OBJECT_AKSI")
                        ->groupBy("AFFECTED_ROUTES")
                        ->groupBy("AFFECTED_MODEL")
                        ->groupBy("OBJECT_SEBELUM")
                        ->groupBy("OBJECT_SESUDAH")
                        ->groupBy("IP_ADDRESS")
                        ->groupBy("id")
                        ->groupBy("IS_READ")
                        ->groupBy("USULAN_ID");

        foreach ($data->get() as $d){
            $str .= "<li>";
            $str .= $d->user_pelaku()->first()->name." ".$d->PREDIKAT_AKSI;
            $str .= " usulan";
            $str .= " pada waktu ".$d->created_at->toDateTimeString();
            $str .= " dari alamat IP: ".$d->IP_ADDRESS.'.<br>';

            if ($d->PREDIKAT_AKSI=='mengubah')
                $str .= $d->pengubahan();

            $str .= "</li>";
        }

        $str .= "</ul>";

        if ($str == "<ul></ul>") {
            $str = "Belum ada histori untuk usulan ini";
        }

        $str = "Usulan dengan ID <u>".$usulan_id."</u> ini memiliki histori: <br>".$str;

        return $str;
    }

    public function balikinUsulan($tahun, $id){
        $usulan = Usulan::find($id);
        if($usulan->USULAN_POSISI == 3) {
            $usulan->USULAN_POSISI = 2;
            $usulan->USULAN_STATUS = 2;
            $usulan->save();
        }else {
            dd($usulan);
        }

    }

    public function grafikRenja($tahun){

        $prosesKel  = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',1)->where('USULAN_POSISI',1)->count();
        $terimaKel  = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',2)->where('USULAN_POSISI',1)->count();
        $tolakKel  = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',0)->where('USULAN_POSISI',1)->count();

        $prosesKec  = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',1)->where('USULAN_POSISI',2)->count();
        $terimaKec  = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',2)->where('USULAN_POSISI',2)->count();
        $tolakKec  = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',0)->where('USULAN_POSISI',2)->count();

        $prosesSkpd  = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',1)->where('USULAN_POSISI',3)->count();
        $terimaSkpd  = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',2)->where('USULAN_POSISI',3)->count();
        $tolakSkpd  = Usulan::where('USULAN_TUJUAN',1)->NotDummies()->where('USULAN_STATUS',0)->where('USULAN_POSISI',3)->count();

        $data = array(
            'tahun'         => $tahun,
            'prosesKel'     => $prosesKel,
            'terimaKel'     => $terimaKel,
            'tolakKel'      => $tolakKel,
            'prosesKec'     => $prosesKec,
            'terimaKec'     => $terimaKec,
            'tolakKec'      => $tolakKec,
            'prosesSkpd'    => $prosesSkpd,
            'terimaSkpd'    => $terimaSkpd,
            'tolakSkpd'     => $tolakSkpd,
            );

        return View('musrenbang.usulan.grafikRenja',$data);
    }

    public function grafikPippk($tahun){
        $prosesRw  = Usulan::whereHas('user',function($q){
                    $q->where('level','1'); })->where('USULAN_TUJUAN',2)->where('USULAN_STATUS',1)->NotDummies()->count();
        $terimaRw  = Usulan::whereHas('user',function($q){
                    $q->where('level','1'); })->where('USULAN_TUJUAN',2)->where('USULAN_STATUS',2)->NotDummies()->count();
        $tolakRw  = Usulan::whereHas('user',function($q){
                    $q->where('level','1'); })->where('USULAN_TUJUAN',2)->where('USULAN_STATUS',0)->NotDummies()->count();

        $prosesLpm  = Usulan::whereHas('user',function($q){
                    $q->where('level','2'); })->where('USULAN_TUJUAN',2)->where('USULAN_STATUS',1)->NotDummies()->count();
        $terimaLpm  = Usulan::whereHas('user',function($q){
                    $q->where('level','2'); })->where('USULAN_TUJUAN',2)->where('USULAN_STATUS',2)->NotDummies()->count();
        $tolakLpm  = Usulan::whereHas('user',function($q){
                    $q->where('level','2'); })->where('USULAN_TUJUAN',2)->where('USULAN_STATUS',0)->NotDummies()->count();

        $prosesPkk  = Usulan::whereHas('user',function($q){
                    $q->where('level','3'); })->where('USULAN_TUJUAN',2)->where('USULAN_STATUS',1)->NotDummies()->count();
        $terimaPkk  = Usulan::whereHas('user',function($q){
                    $q->where('level','3'); })->where('USULAN_TUJUAN',2)->where('USULAN_STATUS',2)->NotDummies()->count();
        $tolakPkk  = Usulan::whereHas('user',function($q){
                    $q->where('level','3'); })->where('USULAN_TUJUAN',2)->where('USULAN_STATUS',0)->NotDummies()->count();

        $prosesKarta  = Usulan::whereHas('user',function($q){
                    $q->where('level','4'); })->where('USULAN_TUJUAN',2)->where('USULAN_STATUS',1)->NotDummies()->count();
        $terimaKarta  = Usulan::whereHas('user',function($q){
                    $q->where('level','4'); })->where('USULAN_TUJUAN',2)->where('USULAN_STATUS',2)->NotDummies()->count();
        $tolakKarta  = Usulan::whereHas('user',function($q){
                    $q->where('level','4'); })->where('USULAN_TUJUAN',2)->where('USULAN_STATUS',0)->NotDummies()->count();

        $data = array(
            'tahun'         => $tahun,
            'prosesRw'     => $prosesRw,
            'terimaRw'     => $terimaRw,
            'tolakRw'      => $tolakRw,
            'prosesLpm'     => $prosesLpm,
            'terimaLpm'     => $terimaLpm,
            'tolakLpm'      => $tolakLpm,
            'prosesPkk'    => $prosesPkk,
            'terimaPkk'    => $terimaPkk,
            'tolakPkk'     => $tolakPkk,
            'prosesKarta'    => $prosesKarta,
            'terimaKarta'    => $terimaKarta,
            'tolakKarta'     => $tolakKarta
            );


        return View('musrenbang.usulan.grafikPIPPK',$data);
    }

    public function grafikRenjaDetail($tahun){
         $data = array(
                'tahun' => $tahun
            );
        return View('musrenbang.usulan.grafik-detail-renja',$data);
    }

    public function getRenja($tahun)
    {
        $skpds = SKPD::with(
            [
                'usulan'=>function($usulan){
                    $usulan->UsulanSKPD()->NotDummies();
                },
                'usulan.kamus',
            ]
        )->get();

        $response = array();
        $no = 1;
        foreach($skpds as $skpd)
        {
            $usulan = $skpd->usulan;

            $renja = $usulan->where('USULAN_TUJUAN',1);


            $renjaproses = $renja->filter(function($value){
                    return (($value->USULAN_STATUS == 2) && ($value->USULAN_POSISI==2));
                })->count();

            $renjaterima = $renja->filter(function($value){
                    return (($value->USULAN_STATUS == 2) && ($value->USULAN_POSISI>=3));
                })->count();

            $renjatolak = $renja->filter(function($value){
                    return (($value->USULAN_STATUS == 0) && ($value->USULAN_POSISI>=3));
                })->count();

            $jumlahrenja = $renja->count();

            $jumlahpengusul = $renja->unique('USER_CREATED')->count();

            array_push($response, array(
                                     'SKPDID'              => $skpd->SKPD_ID,
                                     'NO'                  => $no,
                                     'KODESKPD'            => $skpd->SKPD_KODE,
                                     'NAMASKPD'            => $skpd->SKPD_NAMA,
                                     'JUMLAHRENJA'         => $jumlahrenja,
                                     'JUMLAHPENGUSUL'      => $jumlahpengusul,
                                     'TOTALNOMINAL'        => number_format($skpd->totalNominal(1),0,'.',','),
                                     'PROSES'              => $renjaproses,
                                     'TERIMA'              => $renjaterima,
                                     'TOLAK'              => $renjatolak,
                                     'AKSI'              => '<a href="'.url('/musrenbang/'.$tahun.'/grafikRenja/detail/skpd/'.$skpd->SKPD_ID).'"><i class="mi-eye"></i></a>',
                                     ));
            $no++;
        }
        $response = array("aaData"=>$response);
        return Response::json($response);
    }

    public function grafikRenjaSkpdDetail($tahun,$id){

        $diproses =  USULAN::whereHas('kamus',function($q) use ($id) {
                        $q->where('KAMUS_SKPD',$id);
                    })->NotDummies()->
                    where('USULAN_STATUS', 2)->where('USULAN_POSISI',2)->where('USULAN_TUJUAN',1)->count();

        $diterima =  USULAN::whereHas('kamus',function($q) use ($id){
                        $q->where('KAMUS_SKPD',$id);
                    })->NotDummies()->
                    where('USULAN_STATUS', 2)->where('USULAN_POSISI',3)->where('USULAN_TUJUAN',1)->count();

        $ditolak =  USULAN::whereHas('kamus',function($q) use ($id) {
                        $q->where('KAMUS_SKPD',$id);
                    })->NotDummies()->
                    where('USULAN_STATUS', 0)->where('USULAN_POSISI',3)->where('USULAN_TUJUAN',1)->count();

        $skpd = SKPD::where('SKPD_ID',$id)->first();


        $data = array(
                'tahun' => $tahun,
                'id'    => $id,
                'diproses' => $diproses,
                'diterima' => $diterima,
                'ditolak' => $ditolak,
                'skpd'  => $skpd
            );
        return View('musrenbang.usulan.grafik-detail-renja-skpd',$data);
    }

    public function getRenjaSkpd($tahun,$id,$status)
    {
        if($status==1){
            $skpd = SKPD::where('SKPD_ID',$id)->with(
            [
                'usulan'=>function($usulan) use ($id){
                    $usulan->UsulanSKPD()->NotDummies();
                    $usulan->where('USULAN_STATUS',2);
                    $usulan->where('USULAN_POSISI',2);
                },
                'usulan.kamus',
                'usulan.user',
                'usulan.user.kelurahan',
                'usulan.user.rw',
                'usulan.user.kecamatan',
            ]
            )->first();
        }
        elseif($status==2){
            $skpd = SKPD::where('SKPD_ID',$id)->with(
            [
                'usulan'=>function($usulan) use ($id){
                    $usulan->UsulanSKPD()->NotDummies();
                    $usulan->where('USULAN_STATUS',2);
                },
                'usulan.kamus',
                'usulan.user',
                'usulan.user.kelurahan',
                'usulan.user.rw',
                'usulan.user.kecamatan',
            ]
            )->first();
        }
        elseif($status==0){
            $skpd = SKPD::where('SKPD_ID',$id)->with(
            [
                'usulan'=>function($usulan) use ($id){
                    $usulan->UsulanSKPD()->NotDummies();
                    $usulan->where('USULAN_STATUS',0);
                    $usulan->where('USULAN_POSISI',3);
                },
                'usulan.kamus',
                'usulan.user',
                'usulan.user.kelurahan',
                'usulan.user.rw',
                'usulan.user.kecamatan',
            ]
            )->first();
        }


        $response = array();

        $no = 1;
        foreach($skpd->usulan->sortBy('USER_CREATED') as $usulan)
        {
            if($usulan->USULAN_STATUS == 1) $status = '<i class="fa fa-refresh text-info"></i>';
            elseif($usulan->USULAN_STATUS == 2) $status = '<i class="fa fa-check text-success"></i>';
            else $status = '<i class="fa fa-close text-danger"></i>';

            if($usulan->USULAN_POSISI == 1) $posisi = ' Kelurahan';
            elseif($usulan->USULAN_POSISI == 2) $posisi = ' Kecamatan';
            elseif($usulan->USULAN_POSISI == 3) $posisi = ' Kota';
            else $posisi = ' APBD';

            $pengusul = $usulan->user->email;

            $isukamus = $usulan->kamus->isu->ISU_NAMA."<br>".$usulan->kamus->KAMUS_NAMA;

            $urgensi = $usulan->USULAN_URGENSI;

            $lokasi = "Kecamatan ".$usulan->user->kecamatan->KEC_NAMA;

            if($usulan->user->level != 6)
            {
                $lokasi = $lokasi."<br> Kelurahan ".$usulan->user->kelurahan->KEL_NAMA;
                if($usulan->user->level == 1)
                {
                    $lokasi = $lokasi."<br> RW ".$usulan->user->rw->RW_NAMA;
                }
            }

            $alamat = $usulan->ALAMAT;

            $volume = number_format($usulan->USULAN_VOLUME,0,'.',',')." ".$usulan->kamus->KAMUS_SATUAN;

            $harga = $usulan->kamus->KAMUS_HARGA;

            $nominal = $usulan->USULAN_VOLUME * $usulan->kamus->KAMUS_HARGA;

            $beritaacara = "";



            array_push($response, array(
                                        'NO'                    =>  $no,
                                        'PENGUSUL'              =>  $pengusul,
                                        'ISUKAMUS'              =>  $isukamus,
                                        'LOKASI'                =>  $lokasi,
                                        'URGENSI'               =>  $urgensi,
                                        'LOKASI'                =>  $lokasi,
                                        'ALAMAT'                =>  $alamat,
                                        'VOLUME'                =>  $volume,
                                        'HARGA'                 =>  number_format($harga,0,'.',','),
                                        'NOMINAL'               =>  number_format($nominal,0,'.',','),
                                        'STATUS'                =>  "<p>".$status.$posisi."</p>",
                                        'BERITAACARA'           =>  $beritaacara,
                                     ));
            $no++;
        }
        $response = array("aaData"=>$response);
        return Response::json($response);
    }

   public function getKamusIsu($tahun,$id){
        $data   = Isu::whereHas('kamus',function($q) use ($id) {
                        $q->where('KAMUS_SKPD',$id);
                    })->get();
        $view   = "";
        foreach ($data as $data) {
            $view         = $view."<option value='".$data->ISU_ID."' style='color:black'>".$data->ISU_NAMA."</option>";
        }
        $out    = array('opt'=>$view);
        return $out;
    }

    public function getKamusKamus($tahun,$id){
        $data   = Kamus::where('ISU_ID',$id)->get();
        $view   = "";
        foreach ($data as $data) {
            $view         = $view."<option value='".$data->KAMUS_ID."' style='color:black'>".$data->KAMUS_NAMA."</option>";
        }
        $out    = array('opt'=>$view);
        return $out;
    }


    public function usulkanKembali($tahun){

        $data = array(
                'tahun' => $tahun
            );
        return View('musrenbang.usulan.usulkan-kembali',$data);
    }


     public function usulkanKembaliGetData($tahun){

        $data = Usulan::
                join('DATA.users','users.id','=','DAT_USULAN.USER_CREATED')
                ->join('REFERENSI.REF_KAMUS','REF_KAMUS.KAMUS_ID','=','DAT_USULAN.KAMUS_ID')
                ->join('REFERENSI.REF_ISU','REF_ISU.ISU_ID','=','REF_KAMUS.ISU_ID')
                ->where('USULAN_POSISI',3)->where('USULAN_STATUS',2)->WhereNull('USULAN_IDN')
                ->where('USER_CREATED',Auth::user()->id)->where('USULAN_TUJUAN',1)->get();
        //dd($data);
        $response = array();

        $no = 1;
        foreach($data as $usulan)
        {

            $total    = $usulan->KAMUS_HARGA * $usulan->USULAN_VOLUME;
            $isukamus = $usulan->ISU_NAMA."<br><p class='text-orange'>".$usulan->KAMUS_NAMA.'</p>';
            $aksi     = '<a href="'.url('/musrenbang/'.$tahun.'/usulan/usulkankembali/add/'.$usulan->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>';


            array_push($response, array(
                                        'USULAN_ID'             =>  $usulan->USULAN_ID,
                                        'NO'                    =>  $no,
                                        'PENGUSUL'              =>  $usulan->email,
                                        'ISUKAMUS'              =>  $isukamus,
                                        'URGENSI'               =>  $usulan->USULAN_URGENSI,
                                        //'LOKASI'                =>  $lokasi,
                                        'ALAMAT'                =>  $usulan->ALAMAT,
                                        'VOLHAR'                =>  $usulan->USULAN_VOLUME."<br><p class='text-orange'>".number_format($usulan->KAMUS_HARGA,0,'.',',').'</p>',
                                        'TOTAL'                 =>  number_format($total,0,'.',','),
                                        'STATUS'                =>  "<p> verifikasi APBD</p>",
                                        'OPSI'                  =>  $aksi,
                                     ));
            $no++;
        }
        $response = array("aaData"=>$response);
        return Response::json($response);

    }


    public function usulankanKembaliAdd($tahun,$id){
        $user = User::where('id',Auth::user()->id)->first();
        $usulan         = Usulan::find($id);
        $kamus          = Kamus::All();
        $isu            = Isu::All();

        $lokasi         = RW::whereHas('UserMusren', function($q){
                        $q->where('USER_ID',Auth::user()->id);
                        })->where('RW_TAHUN',$tahun)->first();

        $rt             = RT::where('RW_ID',$lokasi->RW_ID)->get();

        $data = array(
                    'tahun'         => $tahun,
                    'rt'            => $rt,
                    'usulan'        => $usulan,
                    'isu'           => $isu,
                    'kamus'         => $kamus,
                    'lokasi'             => $lokasi,
                    'user'          => $user,
                    'total_usulan'  => DB::table('MUSRENBANG.DAT_USULAN')
                                        -> select(DB::raw('count(*) as jumlah_usulan'))
                                        -> join('DATA.users','MUSRENBANG.DAT_USULAN.USER_CREATED','=','DATA.users.id')
                                        -> where('DATA.users.KEL_ID','=',$user->KEL_ID)
                                        -> get()
                                        -> first() -> jumlah_usulan,
                    );

        return View::make('musrenbang.usulan.add-usulkan-kembali')->with($data);
    }


    public function submitAddKembali($tahun, Request $request){
        $tahun1 = '2018';

        $msg = "RT";
        if(Auth::user()->level == 5)
        {
            $msg = "RW";
        }
        $messages = [
            'kamus.required' => 'Mohon Pilih Kamus Usulan',
            'urgensi.required' => 'Mohon Masukkan Urgensi',
        ];

        $rules = [
            'kamus' => 'required',
            'urgensi' => 'required',
        ];

        Validator::make($request->all(),$rules,$messages)->validate();

        if($request->usulan_tujuan==1){

             $jumlah_usulan = Usulan::where('USULAN_TAHUN',$tahun1)->where('USER_CREATED', Auth::user()->id)->where('USULAN_TUJUAN', 1)->count();

            if($jumlah_usulan>=MAX_USULAN){
               return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','usulan anda di tolak karena usulan untuk renja maksimal 4');
            }

        }elseif($request->usulan_tujuan==2){
            $kamus_usulan = Kamus::find($request->kamus);
            $pagu_usulan = $kamus_usulan ? $kamus_usulan->KAMUS_HARGA : 0;
            $jumlah_pagu = Usulan::where('USULAN_TAHUN',$tahun1)->where('USER_CREATED', Auth::user()->id)->where('USULAN_TUJUAN', $request->usulan_tujuan)->get();
            $total =    0;
            foreach($jumlah_pagu as $jp){
                $total += $jp->USULAN_VOLUME * $jp->kamus->KAMUS_HARGA;
            }
            if($total + ($pagu_usulan*$request->vol) >=PAGU_PIPPK){
                return redirect()->back()->with('message_type','error')->with('message_title','usulan di tolak')->with('message','usulan anda di tolak karena usulan untuk PIPPK maksimal dana 100 jt');
            }
        }


        $image_arr = [];


        if($request->image != null)
        {
            foreach ($request->image as $image) {

                // $img1               = Input::file('image1');
                $extension = $image->getClientOriginalExtension();
               // Image::make($image->getRealPath())->resize(200, 200)->save($path);
                if(strtoupper($extension) == 'PNG' or strtoupper($extension) == 'JPG' or strtoupper($extension) == 'JPEG'){

                    $img = Image::make($image->getPathName())->resize(1024, null,function($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });

                    $name  = $this->generateRandomString(20).'.' . $extension;
                    $img->save(public_path("uploads/$name"));

                    $image_arr[] = $name;

                }else{
                    //Session::flash('error','Dokumen tidak sesuai');
                    return back()->withErrors(["ErrorFile"=>"Format File Tidak Sesuai. Sistem Hanya Menerima File Gambar dengan format JPG atau PNG"]);
                }
            }
        }

        Usulan::where('USULAN_ID',Input::get('usulan_id'))->update(['USULAN_IDN' => '1']);

        $usulan = new Usulan;

        $usulan->USULAN_TAHUN       = $tahun1;
        $usulan->KAMUS_ID           = $request->kamus;
        $usulan->USULAN_URGENSI     = $request->urgensi;
        $usulan->USULAN_VOLUME      = $request->vol;
        $usulan->USULAN_GAMBAR      = $image_arr;
        $usulan->RT_ID              = $request->rt;
        $usulan->USULAN_LAT         = $request->latitude;
        $usulan->USULAN_LONG        = $request->longitude;
        $usulan->USULAN_STATUS      = '1';
        $usulan->USULAN_POSISI      = '1';
        $usulan->USULAN_PRIORITAS   = '0';
        $usulan->USER_CREATED       = Auth::user()->id;
        $usulan->TIME_CREATED       = Carbon\Carbon::now();
        $usulan->IP_CREATED         = $_SERVER['REMOTE_ADDR'];
        $usulan->USER_UPDATED       = Auth::user()->id;
        $usulan->RW_ID              = Auth::user()->RW_ID;
        $usulan->TIME_UPDATED       = Carbon\Carbon::now();
        $usulan->IP_UPDATED         = $_SERVER['REMOTE_ADDR'];
        $usulan->USULAN_TUJUAN      = $request->usulan_tujuan;
        $usulan->ALAMAT             = $request->alamat;
        $usulan->USULAN_DELETED     = 0;


        $usulan->save();


        Log::debug("Usulan ".$usulan->USULAN_ID." sudah ditambahkan");
        //setiap proses tambah, notification log jalan
        $n = new Notification_log;
        $n->ID_USER_SUBJECT = Auth::user()->email;
        $n->ID_USER_TARGET  = $this->_parseUserEmail();
        $n->PREDIKAT_AKSI   = "menambah";
        $n->OBJECT_AKSI     = "usulan";
        $n->AFFECTED_ROUTES = Route::getCurrentRoute()->getActionName();
        $n->OBJECT_SEBELUM  = null;
        $n->OBJECT_SESUDAH  = base64_encode(serialize($usulan));
        $n->IP_ADDRESS      = $_SERVER['REMOTE_ADDR'];
        $n->IS_READ         = false;
        $n->USULAN_ID       = $usulan->USULAN_ID;
        $n->save();

        /*if(isMobile()){
            return "c";
            return Redirect('musrenbang/'.$tahun.'/mobile/daftarUsulan/'.$request->usulan_tujuan);
        }*/

        if(Auth::user()->level==5){
            return "d";
             ///return Redirect('musrenbang/'.$tahun.'/usulan/show/'.$request->usulan_tujuan)->with('message_title','Success')->with('message','Sukses menambahkan usulan');
        }


        //return response()->json([]);
        return Redirect('musrenbang/'.$tahun1.'/usulan-'.$request->usulan_tujuan)->with('message_title','Success')->with('message','Sukses menambahkan usulan');
    }


    public function usulanKembaliAbaikan($tahun){
        //$usulan = Usulan::find(Input::get('id'));
        Usulan::where('USULAN_ID',Input::get('id'))->update(['USULAN_IDN' => '0']);

        return "Usulan Berhasil di Abaikan!";

    }


    public function setPrioritasUsulan($tahun){
        if (Auth::user()->level==1) {
            Usulan::where('USULAN_ID',Input::get('id'))->update(['USULAN_PRIORITAS' => '1']);
        }

        $n = new Notification_log;
        Log::debug("Usulan ".Input::get('id')." sudah di setting prioritas");

        $n->ID_USER_SUBJECT = Auth::user()->email;
        $n->ID_USER_TARGET  = $this->_parseUserEmail();
        $n->PREDIKAT_AKSI   = "setting prioritas";
        $n->OBJECT_AKSI     = "usulan";
        $n->AFFECTED_ROUTES = Route::getCurrentRoute()->getActionName();
        $n->IP_ADDRESS      = $_SERVER['REMOTE_ADDR'];
        $n->IS_READ         = false;
        $n->USULAN_ID       = Input::get('id');
        $n->save();

        return "Setting Prioritas Berhasil!";

    }


}
