<?php

namespace App\Http\Controllers\Musrenbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Validator;
use View;
use Response;
use Auth;
use App\Model\Tujuan;
use App\Model\Kamus;
use App\Model\Isu;
use App\Model\RW;
use App\Model\Kelurahan;
use App\Model\User;
use App\Model\Kecamatan;
use Illuminate\Support\Facades\Input;
use App\Model\RT;
use App\Model\SKPD;
use App\Model\Usulan;

class referensiController extends Controller
{
	public function tujuanIndex($tahun){
		return View('musrenbang.referensi.tujuan',['tahun'=>$tahun]);
	}
	public function kamusShow($tahun){
		$kamus    = Kamus::ALL();
		return View::make('musrenbang.referensi.kamus',['kamus'=>$kamus]);
	}
	public function rwShow($tahun, $id){
		return View::make('musrenbang.referensi.rw',compact('tahun', 'id'));
	}

	public function rtShow($tahun, $id){
		return View::make('musrenbang.referensi.rt',compact('tahun', 'id'));
	}

	public function skpdShow($tahun){
		return View::make('musrenbang.referensi.skpd',compact('tahun'));
	}

	public function adminShow($tahun){
		$user = User::where('email','01.01.01')->first();
		return View::make('musrenbang.referensi.admin',compact('tahun','user'));
	}

	public function getRW($tahun, $id){
		$data 	= user::where('KEL_ID', $id)->where('level','<',5)->orderBy('id')->get();
		$view 	= array();
		$no = 1;
		foreach($data as $d){
			if($d->active == '0') $stat = '<a title="non aktif, aktivasi account?" onclick="return active(\''.$d->id.'\')"><span class="text-danger"><i class="fa fa-close"></i></span></a>';
			else $stat = '<a title="aktif, non aktifkan account?" onclick="return nonActive(\''.$d->id.'\')"><span class="text-success"><i class="fa fa-check"></i></span></a>';
			//dd($d);
			if($d->level==1){
				$RW_NAMA     = $d->rw->RW_NAMA;
                $RW_KETUA    = $d->rw->RW_KETUA;
                $NIK       	 = $d->rw->RW_NIK;
                $TELP        = $d->rw->RW_TELP;
				$aksi = '<a title="lihat detail RT?" href="'.url('/').'/musrenbang/2017/pengaturan/rt/'.$d->RW_ID.'" class="btn btn-success"><i class="mi-eye"></i></a>
						<a title="edit profil RW?" class="btn btn-info" onclick="return getProfil(\''.$d->RW_ID.'\')"data-toggle="modal" data-target="#form-tinjau"><i class="fa fa-info"></i></a>';
			}else {
				$RW_NAMA     = '';
                $RW_KETUA    = '';
                $NIK       	 = '';
                $TELP        = '';
                $aksi = '';
            }

			$aksi .= '<a title="reset password RW?" class="btn btn-danger" onclick="return reset(\''.$d->id.'\')"><i class="fa fa-retweet"></i> Reset</a>';

			array_push($view, array(
									 'NO'        		=> $no,
									 'USER'        		=> $d->email,
                                     'RW_NAMA'        	=> $RW_NAMA,
                                     'RW_KETUA'        	=> $RW_KETUA,
                                     'NIK'        		=> $NIK,
                                     'TELP'        		=> $TELP,
                                     'STATUS'        	=> $stat,
                                     'AKSI'				=> $aksi));
		$no++;
		}
		$out = array("aaData"=>$view);
        return Response::JSON($out);
	}

	public function getRt($tahun, $id){
		$data 	= rt::where('RW_ID', $id)->orderBy('RT_ID')->get();
		$view 	= array();
		$no = 1;
		foreach($data as $d){
			array_push($view, array(
									 'NO'        		=> $no,
                                     'RT_NAMA'        	=> $d->RT_NAMA,
                                     'RT_KETUA'        	=> $d->RT_KETUA,
                                     'NIK'        		=> $d->RT_NIK,
                                     'TELP'        		=> $d->RT_TELP,
                                     'AKSI'				=> '<a class="btn btn-info" onclick="return getProfil(\''.$d->RT_ID.'\')"data-toggle="modal" data-target="#form-tinjau"><i class="fa fa-info"></i></a>'));
			$no++;
		}
		$out = array("aaData"=>$view);
        return Response::JSON($out);
	}

	public function getSKPD($tahun){
		$data 	= user::where('SKPD_ID','!=',0)->orderBy('id')->get();
		$view 	= array();
		$no = 1;
		foreach($data as $d){
			if($d->active == '0') $stat = '<a title="non aktif, aktivasi account?" onclick="return active(\''.$d->id.'\')"><span class="text-danger"><i class="fa fa-close"></i></span></a>';
			else $stat = '<a title="aktif, non aktifkan account?" onclick="return nonActive(\''.$d->id.'\')"><span class="text-success"><i class="fa fa-check"></i></span></a>';

			array_push($view, array(
			 'NO'        			=> $no,
             'SKPD_TAHUN'        	=> $d->skpd->SKPD_TAHUN,
             'SKPD_BIDANG'        	=> $d->skpd->SKPD_BIDANG,
             'SKPD_KODE'        	=> $d->skpd->SKPD_KODE,
             'SKPD_NAMA'        	=> $d->skpd->SKPD_NAMA,
             'SKPD_KEPALA'        	=> $d->skpd->SKPD_KEPALA,
             'SKPD_KEPALA_NIP'      => $d->skpd->SKPD_KEPALA_NIP,
             'SKPD_BENDAHARA'       => $d->skpd->SKPD_BENDAHARA,
             'SKPD_BENDAHARA_NIP'   => $d->skpd->SKPD_BENDAHARA_NIP,
             'STAT'   				=> $stat,
             'AKSI'					=>
             					'<a title="reset password skpd?" class="btn btn-danger " onclick="return reset(\''.$d->SKPD_ID.'\')"><i class="fa fa-retweet"></i> Reset</a>
             					<a title="edit profil skpd?" class="btn btn-info" onclick="return getProfil(\''.$d->SKPD_ID.'\')"data-toggle="modal" data-target="#form-tinjau"><i class="fa fa-info"></i></a>'));
			$no++;
		}
		$out = array("aaData"=>$view);
        return Response::JSON($out);
	}

	public function getAdmin($tahun){
		$lev = [8,9];
		$data 	= user::whereIn('level',$lev)->orderBy('id','Desc')->get();
		$view 	= array();
		$no = 1;
		foreach($data as $d){
			if($d->active == '0') $stat = '<a title="non aktif, aktivasi account?" onclick="return active(\''.$d->id.'\')"><span class="text-danger"><i class="fa fa-close"></i></span></a>';
			else $stat = '<a title="aktif, non aktifkan account?" onclick="return nonActive(\''.$d->id.'\')"><span class="text-success"><i class="fa fa-check"></i></span></a>';

			if($d->level == 8 ) $level= 'Bidang';
			else $level = 'Admin';

			array_push($view, array(
			 'NO'        	=> $no,
             'NAMA'        	=> $d->name,
             'USER'        	=> $d->email,
             'LEVEL'        => $level,
             'STAT'   		=> $stat,
             'AKSI'			=> '<a title="reset password skpd?" class="btn btn-danger " onclick="return reset(\''.$d->id.'\')"><i class="fa fa-retweet"></i> Reset</a>
             					<a title="edit profil skpd?" class="btn btn-info" onclick="return getProfil(\''.$d->id.'\')"data-toggle="modal" data-target="#form-tinjau"><i class="fa fa-info"></i></a>'));
			$no++;
		}
		$out = array("aaData"=>$view);
        return Response::JSON($out);
	}

	public function resetRW($tahun,$id){

		$user = user::find($id);

		if($user->level==1){
			User::where('id',$id)->update(['password'=>'$2y$10$6KRJ6LsEW8oys6DTlCcIeefviIFEQ3GGgYqnVO53vOgd3HFfG508a']);
			return 'Berhasil di reset. Password: "rwjuara"!';
		}
		else if($user->level==2){
			User::where('id',$id)->update(['password'=>'$2y$10$sjiz2G.RrrVIujvGBu37F.K3tnlcNYbWW89CpTKiTC09E1OOG8pB2']);
			return 'Berhasil di reset. Password: "lpmjuara"!';
		}
		else if($user->level==3){
			User::where('id',$id)->update(['password'=>'$2y$10$.v9XIeayx2.MwtNp.aI62.Sip3qrAMI35NiJt6uHSetqVUyt1tPQG']);
			return 'Berhasil di reset. Password: "pkkjuara"!';
		}
		else if($user->level==4){
			User::where('id',$id)->update(['password'=>'$2y$10$P1P123M.x4D/9oo5uCB.oOZUfe6/G351MQMIQjriQVDtG2Q2FBrUS']);
			return 'Berhasil di reset. Password: "kartajuara"!';
		}

	}

	public function resetAdmin($tahun,$id){

		$user = user::find($id);

		if($user->level==8){
			User::where('id',$id)->update(['password'=>'$2y$10$qtXXZ.uEfTkS0KpVRHXW0egYIYvysqD0jiDYYprrQCXt/IIRXPDRq']);
			return 'Berhasil di reset. Password: "bidangjuara"!';
		}
		else if($user->level==9){
			User::where('id',$id)->update(['password'=>'$2y$10$0aTxZEpkf2c3xvEwUsVKMO9jHN0SdyTtKsQLIQ6nWQpBOkkLaLURu']);
			return 'Berhasil di reset. Password: "adminjuara"!';
		}

	}

	public function kelurahanShow($tahun, $id){
		return View::make('musrenbang.referensi.kelurahan',compact('tahun','id'));
	}
	public function getKelurahan($tahun, $id){
		$data 	= User::where('KEC_ID',$id)->where('level',5)->orderBy('id')->get();
		$view 	= array();
		$no = 1;
		foreach($data as $d){
			if($d->active == '0') $stat = '<a title="non aktif, aktivasi account?" onclick="return active(\''.$d->id.'\')"><span class="text-danger"><i class="fa fa-close"></i></span></a>';
			else $stat = '<a title="aktif, non aktifkan account?" onclick="return nonActive(\''.$d->id.'\')"><span class="text-success"><i class="fa fa-check"></i></span></a>';

			array_push($view, array(
									 'NO'        		=> $no,
									 'KEL'        		=> $d->kelurahan->KEL_NAMA,
                                     'LURAH'        	=> $d->kelurahan->KEL_LURAH,
                                     'NIP'        		=> $d->kelurahan->KEL_NIP,
                                     'TELP'        		=> $d->kelurahan->KEL_TELP,
                                     'STAT'        		=> $stat,
                                     'AKSI'        		=> '<a title="lihat detail RW?" href="'.url('/').'/musrenbang/2017/pengaturan/rw/'.$d->KEL_ID.'" class="btn btn-success"><i class="mi-eye"></i></a>
                                     						<a title="reset password kelurahan?" class="btn btn-danger" onclick="return reset(\''.$d->id.'\')"><i class="fa fa-retweet"></i> Reset</a>
                                     						<a title="edit profil kelurahan?" class="btn btn-info" onclick="return getProfil(\''.$d->KEL_ID.'\')" data-toggle="modal" data-target="#form-tinjau"><i class="fa fa-info"></i></a>'));
		$no++;
		}
		$out = array("aaData"=>$view);
        return Response::JSON($out);
	}

	public function resetKelurahan($tahun,$id){
		User::where('id',$id)->where('level','5')->update(['password'=>'$2y$10$9KhzVO22hUmOaBQAprF0GuUUSSKoyAkqds3IuJbf/4QX9RaJg39S.']);
		return 'Berhasil di reset. Password: "lurahjuara"!';
	}

	public function resetSKPD($tahun,$id){
		User::where('id',$id)->where('level','7')->update(['password'=>'$2y$10$3O0cCV5qDFM5yavf9tSRjev30C1Kp.8PK2bB6BLkNv4UHrtW5YHPO']);
		return 'Berhasil di reset. Password: "skpdjuara"!';
	}

	public function getProfile($tahun, $id){
		$rw = rw::where('RW_ID',$id)->first();
		return Response::json($rw);
	}

	public function kecamatanShow($tahun){
		return View::make('musrenbang.referensi.kecamatan',compact('tahun'));
	}

	public function kecamatanAdd($tahun){
		return View::make('musrenbang.referensi.kecamatanform',compact('tahun'));
	}

	public function getKec($tahun){
		$data 	= User::where('level','6')->where('KEC_ID','<=',1000)->orderBy('id')->get();

		$view 	= array();
		$no=1;
		foreach($data as $d){
			if($d->active == '0') $stat = '<a title="non aktif, aktivasi account?" onclick="return active(\''.$d->id.'\')"><span class="text-danger"><i class="fa fa-close"></i></span></a>';
			else $stat = '<a title="aktif, non aktifkan account?" onclick="return nonActive(\''.$d->id.'\')"><span class="text-success"><i class="fa fa-check"></i></span></a>';
			array_push($view, array(
									 'NO'				=> $no,
									 'KEC'        		=> $d->kecamatan->KEC_NAMA,
                                     'NAMA'        		=> $d->kecamatan->KEC_CAMAT,
                                     'TELP'        		=> $d->kecamatan->KEC_TELP,
                                     'STAT'        		=> $stat,
                                     'AKSI'				=> '<a title="lihat detail kelurahan?" href="'.url('/').'/musrenbang/2017/pengaturan/kelurahan/'.$d->KEC_ID.'" class="btn btn-success"><i class="mi-eye"></i></a>
                                     						<a title="reset password kecamatan?" class="btn btn-danger " onclick="return reset(\''.$d->id.'\')"><i class="fa fa-retweet"></i> Reset</a>
                                     						<a title="edit profil kecamatan?" class="btn btn-info" onclick="return getProfil(\''.$d->KEC_ID.'\')" data-toggle="modal" data-target="#form-tinjau"><i class="fa fa-info"></i></a>'
                                     ));
			$no++;
		}
		$out = array("aaData"=>$view);
        return Response::JSON($out);
	}

	public function resetKecamatan($tahun,$id){
		User::where('id',$id)->where('level','6')->update(['password'=>'$2y$10$a2jSc1QTITxIoXj8QRjOkuFOtM1mn156JTJ7ERFviYMfNz6wtzVXS']);
		return 'Berhasil di reset. Password: "camatjuara"!';
	}

	public function getProfKec($tahun, $id){
		$kec = Kecamatan::where('KEC_ID',$id)->first();
		return Response::json($kec);

	}

	public function getProfKeL($tahun, $id){
		$kel = kelurahan::where('KEL_ID',$id)->first();
		return Response::json($kel);
	}

	public function getProfRt($tahun, $id){
		$rt = rt::where('RT_ID',$id)->first();
		return Response::json($rt);
	}

	public function getProfSkpd($tahun, $id){
		$skpd = skpd::where('SKPD_ID',$id)->first();
		return Response::json($skpd);
	}

	public function getProfAdmin($tahun, $id){
		$user = User::where('id',$id)->first();
		return Response::json($user);
	}

	public function simpanProfilKec($tahun){
		Kecamatan::where('KEC_ID',Input::get('KEC_ID'))
			->update([	'KEC_NAMA' 	=> Input::get('KEC_NAMA'),
						'KEC_CAMAT' => Input::get('KEC_CAMAT'),
						'KEC_NIP' => Input::get('KEC_NIP'),
						'KEC_TELP' => Input::get('KEC_TELP')
					]);
			return redirect()->back()->with('message_title','Success')->with('message','Sukses mengedit profil kecamatan');
	}

	public function simpanProfilKel($tahun){
		kelurahan::where('KEL_ID',Input::get('KEL_ID'))
			->update([	'KEL_NAMA' 	=> Input::get('KEL_NAMA'),
						'KEL_LURAH' => Input::get('KEL_LURAH'),
						'KEL_NIP' => Input::get('KEL_NIP'),
						'KEL_TELP' => Input::get('KEL_TELP')
					]);
			return redirect()->back()->with('message_title','Success')->with('message','Sukses mengedit profil kelurahan');
	}

	public function simpanProfilRw($tahun){
		rw::where('RW_ID',Input::get('RW_ID'))
			->update([	'RW_NAMA' 	=> Input::get('RW_NAMA'),
						'RW_NIK' => Input::get('RW_NIK'),
						'RW_KETUA' => Input::get('RW_KETUA'),
						'RW_TELP' => Input::get('RW_TELP')
					]);
			return redirect()->back()->with('message_title','Success')->with('message','Sukses mengedit profil RW');
	}

	public function simpanProfilRt($tahun){
		rt::where('RT_ID',Input::get('RT_ID'))
			->update([	'RT_NAMA' 	=> Input::get('RT_NAMA'),
						'RT_NIK' => Input::get('RT_NIK'),
						'RT_KETUA' => Input::get('RT_KETUA'),
						'RT_TELP' => Input::get('RT_TELP')
					]);
			return redirect()->back()->with('message_title','Success')->with('message','Sukses mengedit profil RT');
	}

	public function simpanProfilSkpd($tahun){
		skpd::where('SKPD_ID',Input::get('SKPD_ID'))
			->update([	'SKPD_KODE' 	=> Input::get('SKPD_KODE'),
						'SKPD_NAMA' => Input::get('SKPD_NAMA'),
						'SKPD_KEPALA' => Input::get('SKPD_KEPALA'),
						'SKPD_KEPALA_NIP' => Input::get('SKPD_KEPALA_NIP'),
						'SKPD_BENDAHARA' => Input::get('SKPD_BENDAHARA'),
						'SKPD_BENDAHARA_NIP' => Input::get('SKPD_BENDAHARA_NIP'),
					]);
			return redirect()->back()->with('message_title','Success')->with('message','Sukses mengedit profil SKPD');
	}

	public function simpanProfilAdmin($tahun){
		user::where('id',Input::get('id'))
			->update([	'name' 	=> Input::get('name'),
						'email' => Input::get('email')
					]);
			return redirect()->back()->with('message_title','Success')->with('message','Sukses mengedit profil bappelitbang');
	}

	public function aktivasi($tahun,$id){
		User::where('id',$id)->update(['active'=>1]);
		return 'Account Berhasil di Aktifkan!';
	}

	public function nonAktivasi($tahun,$id){
		User::where('id',$id)->update(['active'=>0]);
		return 'Account Telah di Non Aktifkan!';
	}

	public function aktivasiSemuaAccount($tahun,$id){
		if($id==0){
			User::whereIn('level', [1,2,3,4,5,6,7])->update(['active'=>0]);
			return redirect()->back()->with('message_type','error')->with('message','Semua Account RW s/d SKPD Telah di Non Aktifkan');
		}else{
			User::whereIn('level', [1,2,3,4,5,6,7])->update(['active'=>1]);
			return redirect()->back()->with('message_title','Success')->with('message','Semua Account RW s/d SKPD Sudah di Aktifkan');
		}
	}

	public function kamusAdd($tahun){
		$isu = Isu::all();
		return View::make('musrenbang.usulan.add-isu',compact('tahun','isu'));
	}

	public function kamusAdd2($tahun,$id){
		$kamus = Kamus::where('ISU_ID',$id);
		return View::make('musrenbang.usulan.add-kamus',compact('tahun','kamus'));
	}

	public function kamusAddSubmit($tahun, Request $request){

        $messages = [
            'ISU_TAHUN.required' => 'Mohon Mengisi Tahun Isu',
            'ISU_TUJUAN.required' => 'Mohon Pilih Tujuan Isu',
            'ISU_NAMA.required' => 'Mohon Masukkan Nama Isu Terbaru'
        ];

        $rules = [
            'ISU_TAHUN' => 'required',
            'ISU_TUJUAN' => 'required',
            'ISU_NAMA' => 'required',
        ];

        Validator::make($request->all(),$rules,$messages)->validate();

        $isu = new Isu;

        $isu->ISU_TAHUN   	= $request->ISU_TAHUN;
        $isu->ISU_TIPE      = $request->ISU_TUJUAN;
        $isu->ISU_NAMA	    = $request->ISU_NAMA;
        $isu->ISU_KUNCI	    = 0;

        $isu->save();

        return redirect()->back()->with('message_title','Success')->with('message','Sukses menambahkan isu, silahhkan input kamus usulan');
	}

	public function renjaanomaliAdmin($tahun){
        return View('musrenbang.referensi.renjaanomali',compact('tahun'));
    }

    public function renjaanomalijumlahviewAdmin($tahun){
        return View('musrenbang.referensi.renjaanomalijumlah',compact('tahun'));
    }

     public function pippklkkanomaliviewAdmin($tahun){
        return View('musrenbang.referensi.pippklkkanomaliview',compact('tahun'));
    }

    public function adminPerKecamatan($tahun){
        return View('musrenbang.referensi.perkecamatan',compact('tahun'));
    }

    public function adminPerKelurahan($tahun){
        return View('musrenbang.referensi.perkelurahan',compact('tahun'));
    }

    public function getIsuUsulan($tahun, $id){
		$isu = Isu::where('ISU_ID',$id)->first();
		return Response::json($isu);
	}

	public function maps($tahun){
		$usulan = Usulan::where('USULAN_ID', 2352)->first();
		//dd($usulan);
		return View('musrenbang.referensi.maps',compact('tahun','usulan'));
	}

	public function mapsPublic($tahun){
		$usulan = Usulan::where('USULAN_TAHUN', '2017')->where('USULAN_POSISI', 3)->where('USULAN_STATUS',2)->take(10)->get();

		//dd($usulan);
		//$usulan = Usulan::where('USULAN_ID', 2352)->first();
		//dd($usulan);
		return View('musrenbang.public.maps',compact('tahun','usulan'));
	}

}
