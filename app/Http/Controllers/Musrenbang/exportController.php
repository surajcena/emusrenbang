<?php

namespace App\Http\Controllers\Musrenbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Usulan;
use App\Model\SKPD;
use App\Model\User;
use App\Model\Kecamatan;
use Auth;
use Excel;
use DB;
use Carbon;

class exportController extends Controller
{
	public function __construct(){
        $this->middleware('auth');
    }

    private function doExportUsulan($response,$title = "Export")
    {
		Excel::create($title, function($excel) use ($response) {

            $excel->sheet('Sheet 1', function($sheet) use ($response) {
            	$header = [
            		"NO",
            		"Tipe",
            		"Prioritas",
            		"ID Pengusul",
            		"Nama Pengusul",
            		"Isu",
                    "Kamus",
            		"Urgensi",
            		"Kriteria",
            		"Volume",
            		"Satuan",
            		"Harga / Satuan",
            		"Anggaran",
            		"Alamat",
            		"Koordinat Peta",
            		"Penerima",
            		"Status Usulan",
            	];
            	$sheet->setColumnFormat(array(
                    'J' => "#,##0",
                    'L' => "#,##0",
                    'M' => "#,##0",
                ));
            	$sheet->row(1,$header);
            	$sheet->fromArray($response, null, 'A2', true,false);
            	$sheet->setBorder('A1:Q'.(count($response)+1),"thin");
            	$sheet->cells('A1:Q1',function($cells){
                	$cells->setAlignment('center');
                	$cells->setFontWeight("bold");
                	//$cells->setBorder('solid','solid','solid','solid');
                });
               	
                $sheet->setAutoFilter('A1:Q'.(count($response)+1));
            });
        })->export('xls');
    }

    private function usulanToExcelFormat($usulans)
    {
    	$response = array();
		foreach ($usulans as $key => $usulan) {
			$responseelement = array();


			$responseelement["no"] = $key+1;

			$responseelement["tipe"] = "RENJA";

			if($usulan->USULAN_TUJUAN == 2)
			{
				$responseelement["tipe"] = "PIPPK";
			}

			$responseelement["prioritas"] = $usulan->USULAN_PRIORITAS;

			$responseelement["idpengusul"] = $usulan->user->email;

            //$responseelement["namapengusul"] = $usulan->getStringPengusul();
			$responseelement["namapengusul"] = $usulan->user->name;
            	
            $responseelement["isu"] = $usulan->kamus->isu->ISU_NAMA;  

			$responseelement["kamus"] = $usulan->kamus->KAMUS_NAMA;		
			$responseelement["urgensi"] = $usulan->USULAN_URGENSI;		
			$responseelement["kriteria"] = $usulan->kamus->KAMUS_KRITERIA;		
			$responseelement["volume"] = $usulan->USULAN_VOLUME;		
			$responseelement["satuan"] = $usulan->kamus->KAMUS_SATUAN;		
			$responseelement["harga"] = $usulan->kamus->KAMUS_HARGA;		
			$responseelement["anggaran"] = $usulan->kamus->KAMUS_HARGA * $usulan->USULAN_VOLUME;		
			$responseelement["alamat"] = $usulan->ALAMAT;		
			$responseelement["peta"] = $usulan->USULAN_LAT." , ".$usulan->USULAN_LONG;			
			$responseelement["penerima"] = "-";
			if($usulan->user->level == 1)
			{		
				$responseelement["penerima"] = $usulan->getStringRTPenerima();		
			}
			$responseelement["statususulan"] = $usulan->getStringStatus();		
			

			array_push($response, $responseelement);
		}
		return $response;
    }

	public function exportusulan($tahun,$jenis){

		$usulans = Usulan::query();
        
		$level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();


        if(Auth::user()->level==5)
        {
            $ids = Auth::user()->kelurahan->user->whereIn('level',$level)->pluck('id')->all(); 

        }
        else if(Auth::user()->level==6)
        {
            $ids = Auth::user()->kecamatan->user->whereIn('level',$level)->pluck('id')->all();   
            $usulans = $usulans->UsulanKecamatan();            
        }
        else if(Auth::user()->level==7)
        {
            $usulans = $usulans->UsulanSKPD(Auth::user()->SKPD_ID)->NotDummies();
        }
        else if(Auth::user()->level==8)
        {
            $usulans = $usulans->UsulanBappeda()->NotDummies();
        }
        
       	//dd($usulans->toSQL());

		$usulans = $usulans->whereIn('USER_CREATED',$ids)
                    ->where('USULAN_TUJUAN',$jenis)
                    ->where('USULAN_TAHUN',$tahun)
					->where('USULAN_DELETED',0)
					->with([
						"user",
						"user.kecamatan",
						"user.rw",
						"user.kelurahan",
						"kamus",
						"kamus.isu",
					])
					->get();

                    
		
		$jenisusulan = "RENJA";

		if($jenis == 2)
		{
			$jenisusulan = "PIPPK";
		}
		$response = $this->usulanToExcelFormat($usulans);
            
		$this->doExportUsulan($response,"Usulan ".$jenisusulan);
	}

    public function exportUsulanKecamatan($tahun,$idkecamatan)
    {

        $usulans = Usulan::query();

        $level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();

        $usulans = $usulans->UsulanBappeda();

        $usulans = $usulans->whereIn("USER_CREATED",$ids)
                    ->whereHas('user',function($user) use ($idkecamatan){
                        return $user->where('KEC_ID',$idkecamatan);
                    })->with([
                        "user",
                        "kamus",
                        "kamus.isu",
                        "kamus.skpd",
                    ])
                    ->get();

        $response = $this->usulanToExcelFormat($usulans);
        $kecamatan = Kecamatan::find($idkecamatan)->KEC_NAMA;
        
        $this->doExportUsulan($response,"Usulan Kecamatan".$kecamatan);
    }
	public function exportUsulanSKPD($tahun,$idskpd)
	{

		$usulans = Usulan::query();
		$level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();
        

        if(Auth::user()->level==5)
        {
            $ids = Auth::user()->kelurahan->user->whereIn('level',$level)->pluck('id')->all();    
        }
        else if(Auth::user()->level==6)
        {
            $ids = Auth::user()->kecamatan->user->whereIn('level',$level)->pluck('id')->all(); 
            $usulans = $usulans->UsulanKecamatan();  
        }
        else if(Auth::user()->level==8)
        {

           $usulans = $usulans->UsulanBappeda(); 
        }
        
        $usulans = $usulans->whereIn("USER_CREATED",$ids)
        			->whereHas('kamus',function($kamus) use ($idskpd){
        				return $kamus->where('KAMUS_SKPD',$idskpd);
        			})
                   ->with([
                        "user",
                        "user.kecamatan",
                        "user.rw",
                        "user.kelurahan",
                        "kamus",
                        "kamus.isu",
                    ])
        			->get();

        $response = $this->usulanToExcelFormat($usulans);
        $namaskpd = SKPD::find($idskpd)->SKPD_NAMA;
           
        $this->doExportUsulan($response,"Usulan SKPD ".$namaskpd);
	}

    public function exportRekapKecamatan($tahun)
    {
        $kecamatans = Kecamatan::with(
            [
                "usulan"=>function($usulan){
                    $usulan->UsulanBappeda();
                },
                "usulan.kamus",
                'user',         
            ]
        )
        ->whereNotIn('KEC_ID',[31,32,9999])
        ->get();
        $response = array();

        foreach ($kecamatans as $key => $kecamatan) {
            $usulan = $kecamatan->usulan;

            $renja = $usulan->where('USULAN_TUJUAN',1);

            $jumlahrenja = $renja->count();

            $jumlahpengusulrenja = $renja->unique('USER_CREATED')->count();

            $pippk = $usulan->where('USULAN_TUJUAN',2);

            $jumlahpippk = $pippk->count();

            $jumlahpengusulpippk = $pippk->unique('USER_CREATED')->count();

            array_push($response, array( 
                                     'no'                       => $key+1,
                                     'kecamatan'                 => $kecamatan->KEC_NAMA,
                                     'jumlahrenja'              => $jumlahrenja,
                                     'jumlahpengusulrenja'              => $jumlahpengusulrenja,
                                     'totalnominalrenja'        => $kecamatan->getNominalUsulan(1),
                                     'jumlahpen'              => $jumlahpippk,
                                     'jumlahpengusulpippk'              => $jumlahpengusulpippk,
                                     'totalnominalpippk'        => $kecamatan->getNominalUsulan(2),
                                     ));    
        }
        
        Excel::create('Usulan Per Kecamatan', function($excel) use ($response) {

            $excel->sheet('Sheet 1', function($sheet) use ($response) {
                $header = [
                    "",
                    "",
                    "Jumlah",
                    "Jumlah Pengusul",
                    "Nominal Usulan",
                    "Jumlah",
                    "Jumlah Pengusul",
                    "Nominal Usulan",
                ];
                $sheet->row(1,["No","Kecamatan","Renja","","","PIPPK"]);
                $sheet->row(2,$header);
                $sheet->fromArray($response, null, 'A3', true,false);
                $sheet->setBorder('A1:H'.(count($response)+2),"thin");
                $sheet->mergeCells('C1:E1');
                $sheet->mergeCells('F1:H1');
                $sheet->mergeCells('A1:A2');
                $sheet->mergeCells('B1:B2');
                $sheet->cells('A1:H2',function($cells){
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setFontWeight("bold");
                    //$cells->setBorder('solid','solid','solid','solid');
                });
               
                $sheet->setAutoFilter('A2:H'.(count($response)+2));
            });

        })->export('xls');   
    }


    public function rekapKelurahan($tahun, $id){

       $level = [1,2,3,4,5];
        $ids = User::whereIn('level',$level)->pluck('id')->all();


        if(Auth::user()->level==5)
        {
            $ids = Auth::user()->kelurahan->user->whereIn('level',$level)->pluck('id')->all();  
        }    
        

            $data   = DB::select('select s."SKPD_NAMA" AS SKPD, keg."KEGIATAN_NAMA" AS KEGIATAN, k."KAMUS_NAMA" AS KAMUS, u."USULAN_VOLUME" AS VOLUME, k."KAMUS_HARGA" AS HARGA,
                u."USULAN_VOLUME"*k."KAMUS_HARGA" AS TOTAL, us."name" as PENGUSUL
                                    from "MUSRENBANG"."DAT_USULAN" u
                                    inner JOIN "REFERENSI"."REF_KAMUS" k
                                    on u."KAMUS_ID" = k."KAMUS_ID"
                                    inner JOIN "REFERENSI"."REF_KEGIATAN" keg
                                    on k."KEGIATAN_ID" = keg."KEGIATAN_ID"
                                    inner join "REFERENSI"."REF_SKPD" s
                                    on s."SKPD_ID" = k."KAMUS_SKPD"
                                    inner join "DATA"."users" us
                                    on us."id" = u."USER_CREATED"
                                    WHERE u."USULAN_TAHUN" = '.$tahun.' and 
                                    u."USULAN_DELETED" = 0 and u."USULAN_TUJUAN" = '.$id
                                    );
        
        
               //dd($data);
            $data = array_map(function ($value) {
                    return (array)$value;
                }, $data);
            Excel::create('REKAP USULAN DI KELURAHAN '.Carbon\Carbon::now()->format('d M Y - H'), function($excel) use($data){
                    $excel->sheet('REKAP USULAN DI KELURAHAN ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
            })->download('xls');
            
    }
}
