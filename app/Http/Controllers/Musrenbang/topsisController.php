<?php

namespace App\Http\Controllers\Musrenbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Usulan;
use App\Model\Usulan_kriteria;
use App\Model\Detail_kriteria;
use App\Model\Kriteria;
use App\Model\Kamus;
use App\Model\Skpd;
use App\Model\Isu;
use App\Model\Bobot_kriteria;
use Illuminate\Support\Facades\DB;
use Response;

class topsisController extends Controller
{
    public function index($tahun){
     // $tahun = $tahun;
        return View('musrenbang.usulan.show-usulan-skpd',compact('tahun'));    
    }

    public function topsis_daftar_usulan(){
      $kamusId = 106;
      $sitopsis = array();
      $pembagi = array();
      $kriteria = Kamus::find($kamusId)->kriteria;
      foreach ($kriteria as $key => $krit) {
        $pembagi[$krit->KRITERIA_ID] = 0;
      }

      $usulan = Usulan::
                    join('REFERENSI.REF_KAMUS', 'REF_KAMUS.KAMUS_ID', '=', 'DAT_USULAN.KAMUS_ID')
                  ->with([
                          "usulan_kriteria",
                          "usulan_kriteria.detail_kriteria",
                          "usulan_kriteria.detail_kriteria.bobot_kriteria",
                          "rw"
                    ])
                  ->join('MUSRENBANG.USULAN_KRITERIA', 'USULAN_KRITERIA.USULAN_ID', '=', 'DAT_USULAN.USULAN_ID')  
                  ->where('USULAN_TUJUAN',1)
                  ->where('USULAN_STATUS',2)
                  ->where('USULAN_POSISI',2)
                  ->where('DAT_USULAN.KAMUS_ID',$kamusId)
                  ->get();

        //echo '<pre>';
        //print_r($usulan);

        $standar_bobot_tmp = Kriteria::all();
        
        $standar_bobot = array();
        foreach ($standar_bobot_tmp as $key => $val) {
            $standar_bobot[$val->KRITERIA_ID] = $val->STANDAR_BOBOT;
        }

        $cur_usulan_id = 0;
        foreach ($usulan as $key => $us) {

            if ($cur_usulan_id != $us->USULAN_ID) {
                foreach ($us->usulan_kriteria as $key => $k) {
                  $k->detail_kriteria->bobot_kriteria->BOBOT . ' - ' . $k->detail_kriteria->kriteria->KRITERIA_ID;
                  //echo PHP_EOL;
                  $pembagi[$k->detail_kriteria->kriteria->KRITERIA_ID] += pow($k->detail_kriteria->bobot_kriteria->BOBOT, 2);
                }
                //echo PHP_EOL;

                $cur_usulan_id = $us->USULAN_ID;
            }
            
        }

        // Cari Pembagi
        foreach ($pembagi as $key => $p) {
            $pembagi[$key] = sqrt($p);
        }
        //print_r($pembagi);

        $cur_usulan_id = 0;
        foreach ($usulan as $key => $us) {

            if ($cur_usulan_id != $us->USULAN_ID) {
                $sit = array();
                $sit["usulan"] = $us;
                
                // Tabel Keputusan Ternormalisasi
                foreach ($us->usulan_kriteria as $key => $k) {
                  $pem = $pembagi[$k->detail_kriteria->kriteria->KRITERIA_ID];
                  $sit[$k->detail_kriteria->kriteria->KRITERIA_ID] = $k->detail_kriteria->bobot_kriteria->BOBOT/$pem;
                }

                // Tabel Keputusan Ternormalisasi dan Terbobot
                foreach ($us->usulan_kriteria as $key => $k) {
                  $std_bobot = $standar_bobot[$k->detail_kriteria->kriteria->KRITERIA_ID];
                  $sit[$k->detail_kriteria->kriteria->KRITERIA_ID] = $sit[$k->detail_kriteria->kriteria->KRITERIA_ID]/$std_bobot;
                }

                //print_r($sit);
                $cur_usulan_id = $us->USULAN_ID;
                $sitopsis[] = $sit;
            }
            
        }

        //mencari nilai maximal dan minimal
        $a_plus = array();
        $a_minus = array();
        foreach ($sitopsis as $row) {
            foreach ($row as $key => $val) {
              if($key != 'usulan') {
                $a_plus[$key] = 0;
                $a_minus[$key] = $val;
              }
            }
        }

        foreach ($sitopsis as $row) {
            foreach ($row as $key => $val) {
              if($key != 'usulan') {
                if($val < $a_minus[$key]) {
                  $a_minus[$key] = $val;
                }

                if($val > $a_plus[$key]) {
                  $a_plus[$key] = $val;
                }
              }
            }
        }

        //print_r($a_plus);
        //print_r($a_minus);

        $s_plus = array();
        $s_minus = array();

        foreach ($sitopsis as $row) {
            $USULAN_ID = '';
            foreach ($row as $key => $val) {
              if($key != 'usulan') {
                $s_plus[$USULAN_ID] += pow($a_plus[$key] - $val, 2);
                $s_minus[$USULAN_ID] += pow($val - $a_minus[$key], 2);
                //echo $val - $a_minus[$key];
                //echo PHP_EOL;
              } else {
                $USULAN_ID = (string)$val->USULAN_ID;
                $s_plus[$USULAN_ID] = 0;
                $s_minus[$USULAN_ID] = 0;
              }
            }
        }

        foreach ($s_plus as $key => $val) {
          $s_plus[$key] = sqrt($val);
        }

        foreach ($s_minus as $key => $val) {
          $s_minus[$key] = sqrt($val);
        }

        //print_r($s_plus);
        //print_r($s_minus);

        //relative closeness
        $rc = array();
        foreach ($s_plus as $key => $val) {
          $rc[$key] = $s_minus[$key] / (($s_minus[$key] + $s_plus[$key])+1);
        }

        //print_r($rc);
        $result = array();
        foreach ($sitopsis as $row) {
            foreach ($row as $key => $val) {
              if($key == 'usulan') {
                $val->RC = $rc[(string)$val->USULAN_ID];
                $result[] = $val;
              }
            }
        }

        //print_r($result);

        
        //sorting dari atas ke besar ke kecil 
        /*usort($result, function ($a, $b){
            return strcmp($b->RC, $a->RC);
        });*/

        $no             = 1;
        $aksi           = '';
        $view           = array();
        $rt             = '';
        $rts            = '';
        $jum            = 0;
         $krit = "";


        foreach ($result as $res) {
          array_push($view, array( 'NO'               => $no,
                                    'USULAN_ID'       => $res->USULAN_ID,
                                       'PENGUSUL'     => $res->user->email.'<br>'.$res->user->kelurahan->KEL_NAMA.'<br>'.$res->user->kecamatan->KEC_NAMA,
                                       'KAMUS_NAMA'   => $res->kamus->isu->ISU_NAMA.'<br>'.$res->KAMUS_NAMA,
                                       'VOL'          => $res->USULAN_VOLUME,
                                       'HARGA'        => $res->KAMUS_HARGA,
                                       'STATUS'       => $res->getHTMLIconStatus(),
                                       'KAMUS_NAMA'   => $res->KAMUS_NAMA,
                                       'SKPD'         => $res->kamus->skpd->SKPD_NAMA,
                                       'TOTAL'        => number_format($res->KAMUS_HARGA * $res->USULAN_VOLUME,0,'.',','),
                                       'RC'           => '<b>'.$res->RC.'<b>',
                                       'AKSI'         => '<a href="'.url('/musrenbang/'.$res->USULAN_TAHUN.'/usulan/editAcc/'.$res->USULAN_ID).'" class="action-preveiw"><i class="mi-eye"></i></a>'
                                ));
              $no++;
              $jum++;
        }
        
        
            $out = array("aaData"=>$view,"jumlah"=>number_format($jum,0,'.',','));
            return Response::JSON($out);
        //die();
            //return View('musrenbang.referensi.topsis',compact('result'));
    } 
}
