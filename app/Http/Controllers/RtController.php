<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\RT;

class RtController extends Controller
{
	public function ubah(Request $request)
	{
		$this->validate($request,[
	        'rtid' => 'required',
	        'namart'=> 'required',
	        'ketuart' => 'required',
	        'nik' => 'required',
	        'notelepon' => 'required'
	    ]);

		

		$rt = RT::find($request->rtid);
		$rt->RT_NAMA = $request->namart;
		$rt->RT_KETUA = $request->ketuart;
		$rt->RT_NIK = $request->nik;
		$rt->RT_TELP = $request->notelepon;
		$rt->RT_TAHUN = date('Y');
		$rt->RW_ID = $request->rwid;

		$rt->save();

		//$rw->rt()->save($rt);

		return back();
	}
}
