<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Jenssegers\Agent\Agent;

class UserAgentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('UserAgent', function ($app) {
            return new Agent;
        });
    }
}