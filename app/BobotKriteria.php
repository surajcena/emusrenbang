<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BobotKriteria extends Model
{
	protected $table	= 'REFERENSI.REF_BOBOT_KRITERIA';
	protected $primaryKey = 'BOBOT_KRITERIA_ID';
	public $timestamps = false;
    public $incrementing = false;

     public function detail_kriteria()
    {
        return $this->hasMany('App\Model\detail_kriteria','DETAIL_KRITERIA_ID');
    }
}
