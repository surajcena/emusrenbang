<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function RW(){
         return $this->hasOne('App\Model\RW', 'RW_ID','RW_ID');
    }

    public function kelurahan()
    {
        return $this->belongsTo('App\Model\Kelurahan','KEL_ID');
    }

     public function kecamatan()
    {
        return $this->belongsTo('App\Model\Kecamatan', 'KEC_ID');
    }
}
