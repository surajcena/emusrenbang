<?php

namespace App\DataTableTransformers;

use League\Fractal\TransformerAbstract;
use App\Model\Usulan;

class UsulanTransformer extends TransformerAbstract
{
    /**
     * @return  array
     */
    public function transform(Usulan $usulan)
    {

        $responseelement = array();

        $responseelement["usulanid"] = $usulan->USULAN_ID;
        $responseelement["no"] = $usulan->USULAN_ID;
        $responseelement["idpengusul"] = $usulan->user->email;
        $responseelement["namapengusul"] = $usulan->getStringPengusul();
        $responseelement["isukamus"] = $usulan->getHTMLIsuKamus();
        $responseelement["volume"] = number_format($usulan->USULAN_VOLUME,0,'.',',')." ".$usulan->kamus->KAMUS_SATUAN;
        $responseelement["harga"] = number_format($usulan->kamus->KAMUS_HARGA,0,'.',',');
        $responseelement["total"] = number_format($usulan->kamus->KAMUS_HARGA * $usulan->USULAN_VOLUME,0,'.',',');
        $responseelement["status"] = $usulan->getHTMLIconStatus();
        $responseelement["prioritas"] = $usulan->USULAN_PRIORITAS;
        $responseelement["keterangan"] = $usulan->KET_ACC;
        $responseelement["skpd"] = $usulan->kamus->getSKPDName();


        return $responseelement;
    }
}