<?php

namespace App\DataTableTransformers;

use League\Fractal\TransformerAbstract;
use App\Model\Kecamatan;

class RekapKecamatan extends TransformerAbstract
{
    /**
     * @return  array
     */
    public function transform(Kecamatan $kecamatan)
    {
        return [
            'KECAMATAN_ID'        => $keca->KEC_ID,
            'NO'                  => $no,
            'KECAMATAN_NAMA'      => $d->KEC_NAMA,
            'KEL'                 => $d->kelurahan->count('KEL_ID'),
            'RW'                  => $rw->count(),
            'RWA'                 => $rwa->count(),
            'PIPPK'               => $pippk,
            'RENJA'               => $renja,
            'totalusulan'         => $pippk + $renja,
            'rwsudahinput'        => $rwsudahinput,
            'KECAMATAN_USERNAME'  => $username,
            'TOTALNOMINALUSULAN'  => number_format($d->getNominalUsulan(),0,'.',','),
            ));
        ];
    }
}