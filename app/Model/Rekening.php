<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Rekening extends Model
{
	protected $table		= 'PROTECTED.REF_REKENING';
    protected $primaryKey 	= 'REKENING_ID';
    public $timestamps 		= false;
    public $incrementing 	= false;
}
