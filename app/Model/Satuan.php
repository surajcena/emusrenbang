<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Satuan extends Model
{
    protected $table	= 'REFERENSI.REF_SATUAN';
    protected $primaryKey = 'SATUAN_ID'; 
    public $timestamps = false;
    public $incrementing = false;

    public function usulan()
    {
        return $this->hasMany('App\Model\Usulan', 'SATUAN_ID');
    }
}
