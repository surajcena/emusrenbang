<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    private $dummiesKecamatan = [31,32];

   	protected $table 			= 'users';
   	protected $primaryKey 		= 'id';
   	public $timestamps 			= false;
    public $incrementing 		= false;

    public function usermusren(){
    	return $this->hasOne('App\Model\UserMusren','id');
    }

    public function scopeNotDummies($query)
    {
        $query->whereNotIn('KEC_ID',$this->dummiesKecamatan);
    }

    public function totalNominal($tujuan = null){
        $tot = 0;
        
        foreach ($this->usulan as $key => $usul) {
            if($tujuan == null)
            {
                $tot += $usul->USULAN_VOLUME * $usul->kamus->KAMUS_HARGA;
            }
            else
            {
                if($usul->USULAN_TUJUAN == $tujuan)
                {
                    $tot += $usul->USULAN_VOLUME * $usul->kamus->KAMUS_HARGA;
                }
            }
        }
        return $tot;
    }

    public function usulan(){
        return $this->hasMany('App\Model\Usulan','USER_CREATED');
    }

    public function skpd()
    {
        return $this->belongsTo('App\Model\SKPD', 'SKPD_ID');
    }

    public function kecamatan()
    {
        return $this->belongsTo('App\Model\Kecamatan', 'KEC_ID');
    }

    public function kelurahan()
    {
        return $this->belongsTo('App\Model\Kelurahan','KEL_ID');
    }

    public function rw()
    {
        return $this->belongsTo('App\Model\RW', 'RW_ID');
    }

    public function berita(){
        return $this->hasOne('App\Model\BeritaAcara','id');
    }

    public function beritaFoto(){
        return $this->hasOne('App\Model\BeritaAcaraFoto','id');
    }


    public function beritaacara(){
        return $this->hasMany('App\Model\BeritaAcara',"id");
    }
    public function beritaacarafoto(){
        return $this->hasMany('App\Model\BeritaAcaraFoto',"id");
    }


    public function hasBeritaAcara(){
        return (($this->beritaacara->count() > 0) || ($this->beritaacarafoto->count()) > 0);

    }
}
