<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Detail_kriteria extends Model
{
    protected $table	= 'REFERENSI.REF_DETAIL_KRITERIA';
    protected $primaryKey = 'DETAIL_KRITERIA_ID'; 
    public $timestamps = false;
    public $incrementing = false;

    public function usulan_kriteria()
	{
		return $this->hasMany('App\Model\Usulan_kriteria','USULAN_KRITERIA_ID');
	}
    public function bobot_kriteria()
    {
        return $this->belongsTo('App\BobotKriteria', 'BOBOT_KRITERIA_ID');
    }

    public function kriteria()
    {
        return $this->belongsTo('App\Model\Kriteria', 'KRITERIA_ID');
    }

}
