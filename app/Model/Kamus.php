<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kamus extends Model
{
    protected $table	= 'REFERENSI.REF_KAMUS';
    protected $primaryKey = 'KAMUS_ID'; 
    public $timestamps = false;
    public $incrementing = false;
    protected $casts = [
        'KAMUS_HARGA' => 'float',
    ];

    public function isu()
    {
        return $this->belongsTo('App\Model\Isu', 'ISU_ID');
    }

    public function usulan()
    {
        return $this->hasMany('App\Model\Usulan', 'KAMUS_ID');
    }

    public function skpd()
    {   
        return $this->belongsTo('App\Model\SKPD', 'KAMUS_SKPD');
    }

    public function getTotalVolume()
    {
        $vol = 0;
        foreach ($this->usulan as $key => $usul) {
            $vol += $usul->USULAN_VOLUME;
        }
        return $vol;
    }

    public function getSKPDName()
    {
        if ($this->skpd == null)
        {
            return "";
        }
        return $this->skpd->SKPD_NAMA;
    }

    public function kriteria()
    {
        return $this->hasMany('App\Model\Kriteria', 'KAMUS_ID');
    }

    public function kegiatan()
    {
        return $this->belongsTo('App\Model\Kegiatan', 'KAMUS_KEGIATAN');
    }
}
