<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kriteria extends Model
{
    protected $table	= 'REFERENSI.REF_KRITERIA';
    protected $primaryKey = 'KRITERIA_ID'; 
    public $timestamps = false;
    public $incrementing = false;

    public function kamus()
    {
        return $this->belongsTo('App\Model\Kamus', 'KAMUS_ID');
    }

    public function BobotKriteria()
	{
		return $this->belongsToMany('App\BobotKriteria','REFERENSI.REF_DETAIL_KRITERIA','KRITERIA_ID','BOBOT_KRITERIA_ID');
	}

    public function detail_kriteria()
    {
        return $this->hasMany('App\Model\detail_kriteria','DETAIL_KRITERIA_ID');
    }

}
