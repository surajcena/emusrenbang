<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JenisKegiatan extends Model
{
    protected $table		= 'PROTECTED.REF_JENIS_KEGIATAN';
    protected $primaryKey 	= 'JENIS_KEGIATAN_ID';
    public $timestamps 		= false;
    public $incrementing 	= false;
}
