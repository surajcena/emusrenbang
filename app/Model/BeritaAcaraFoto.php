<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BeritaAcaraFoto extends Model
{
    protected $table	= 'MUSRENBANG.DAT_BERITA_FOTO';
    protected $primaryKey = 'BERITA_FOTO_ID'; 
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable = ['tgl_update','image','id'];
    
    protected $casts = [
        'image' => 'array',
    ];


    public function user()
    {
        return $this->belongsTo('App\Model\User','id');
    }
}
