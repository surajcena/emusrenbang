<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sasaran extends Model
{
	protected $table		= 'PROTECTED.REF_SASARAN';
    protected $primaryKey 	= 'SASARAN_ID';
    public $timestamps 		= false;
    public $incrementing 	= false;
}
