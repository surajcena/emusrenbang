<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Stat extends Model
{
    protected $table		= 'MUSRENBANG.DAT_STAT';
    protected $primaryKey 	= 'id';
    public $timestamps 		= false;
    public $incrementing 	= false;
}
