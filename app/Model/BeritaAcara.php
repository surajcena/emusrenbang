<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BeritaAcara extends Model
{
    protected $table	= 'MUSRENBANG.DAT_BERITA';
    protected $primaryKey = 'BERITA_ID'; 
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable = ['TGL','lampiran','id'];
    protected $casts = [
        'lampiran' => 'array',
    ];


    public function user()
    {
        return $this->belongsTo('App\Model\User','id');
    }
    
}
