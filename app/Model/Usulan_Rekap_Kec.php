<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Usulan_Rekap_Kec extends Model
{
    protected $table	= 'MUSRENBANG.RKP_USULAN_KEC';
    protected $primaryKey = 'USULAN_ID'; 
    public $timestamps = false;
    public $incrementing = false;

    public function kamus()
    {
        return $this->belongsTo('App\Model\Kamus', 'KAMUS_ID');
    }

    public function usulan_rekap()
    {
        return $this->hasMany('App\Model\Usulan_Rekap', 'USULAN_ID');
    }

    public function rt()
    {
        return $this->belongsTo('App\Model\RT', 'RT_ID');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User','USER_CREATED');
    }
}
