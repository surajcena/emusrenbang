<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/* step how to use this */
/*
1. Misalkan ada fungsi yang mau di log, maka pada saat mau melempar data ke VIEW
maka serialize(data) terlebih dahulu. untuk dimsaukkan ke dalam field "object_sebelum"
2. Isi field "aksi" dalam bentuk string, dan juga ID_USER_SUBJECT
3. Lalu SAVE, dan AMBIL ID LOG HASIL SAVE. Selanjutnya masukkan ke dalam parameter VIEW tersebut
4. lakukan operasi apapun, lalu submit
5. pada model tujuan, lengkapi field yang ada. TERUTAMA field object_sesudah.
6. selanjutnya diikuti dengan field id_user_*, affected_model, affected_routes dan waktu
*/

class Notification_log extends Model
{
    protected $table = 'DATA.notification_log';
    protected $primaryKey   = 'id';
    public $incrementing    = true;
    protected $fillable = [
        //'id_user', 'aksi', 'entity_id', 'keterangan', 'waktu'
    	'ID',
    	'ID_USER_SUBJECT', //yang melakukan
    	'ID_USER_TARGET',  //yang menerima dampaknya
    	'PREDIKAT_AKSI',   //menambahkan, menghapus, mengubah
    	'OBJECT_AKSI', 	   //misalkan "usulan pippk" atau "usulan renja" dll
    	'AFFECTED_ROUTES', //controller/routes yang terkena dampak saat penggantian
    	'AFFECTED_MODEL',  //model yang terkena dampak saat penggantian
    	'OBJECT_SEBELUM',   //serialize dari data yang diambil
    	'OBJECT_SESUDAH',   //serialize dari data yang sudah diedit (submit)
        'IP_ADDRESS',       //Alamat IP dari pengedit
        'IS_READ',
        'USULAN_ID',
    ];

	public $timestamps = true; //jangan lupa tambahkan created_at & updated_at

	public function user_pelaku(){
		return $this->hasOne('App\Model\User','email','ID_USER_SUBJECT');
	}

	public function user_korban(){
		return $this->hasOne('App\Model\User','email','ID_USER_TARGET');
	}

    public function get_usulan(){
        return $this->hasOne('App\Model\Usulan','USULAN_ID','USULAN_ID');
    }

    public function getAllAttributes($obj) {
        $columns = $obj->getFillable();
        $attributes = $obj->getAttributes();

        foreach ($columns as $column) {
            if (!array_key_exists($column, $attributes)) {
                $attributes[$column] = null;
            }
        }
        return $attributes;
    }

    public function pengubahan(){
        $str = "<ol>";
        $field_berbeda = [];

        if (($this->OBJECT_SEBELUM != null) && ($this->OBJECT_SESUDAH != null)) {
            $object_sebelum =  unserialize(base64_decode($this->OBJECT_SEBELUM))->getAttributes();
            $object_sesudah =  unserialize(base64_decode($this->OBJECT_SESUDAH))->getAttributes();

            foreach ($object_sesudah as $key => $row){
                if ($key != "TIME_UPDATED"){
                    if (array_key_exists($key, $object_sebelum)){ //apakah keynya ada di $object_sebelum (validasi)
                        if ($object_sebelum[$key] != $row) { //apakah berbeda dengan object sesudah
                            $field_berbeda[$key] = [];

                            if ($object_sebelum[$key] == '') {
                                $field_berbeda[$key]['sebelum'] = '{kosong}';
                            } else { 
                                $field_berbeda[$key]['sebelum'] = $object_sebelum[$key];
                            }
                            
                            $field_berbeda[$key]['sesudah'] = $row;

                            $str = $str . "<li> Field <b>".$key."</b> dari <b>".$field_berbeda[$key]['sebelum']."</b> menjadi <b>".$field_berbeda[$key]['sesudah']. "</b></li>";
                        }
                    }
                }
            }
            $str = $str . "</ol>";
        } else $str = "-";

        //return $field_berbeda;
        return $str;
    }
}
