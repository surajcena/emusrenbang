<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SKPD extends Model
{
	protected $table		= 'REFERENSI.REF_SKPD';
    protected $primaryKey 	= 'SKPD_ID';
    public $timestamps 		= false;
    public $incrementing 	= false;

    public function program(){
        return $this->hasMany('App\Model\Program','SKPD_ID');
    } 

    public function user(){
    	return $this->hasMany('App\Model\User','SKPD_ID');
    }    

    public function usulan(){
    	return $this->hasManyThrough('App\Model\Usulan', 'App\Model\Kamus','KAMUS_SKPD','KAMUS_ID','SKPD_ID');
    }

    public function totalNominal($tujuan = null){
    	$tot = 0;
    	
    	foreach ($this->usulan as $key => $usul) {
    		if($tujuan == null)
    		{
    			$tot += $usul->USULAN_VOLUME * $usul->kamus->KAMUS_HARGA;
    		}
    		else
    		{
    			if($usul->USULAN_TUJUAN == $tujuan)
    			{
    				$tot += $usul->USULAN_VOLUME * $usul->kamus->KAMUS_HARGA;
    			}
    		}
    	}
    	return $tot;
    }
}
