<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Usulan_kriteria extends Model
{
    protected $table	= 'MUSRENBANG.USULAN_KRITERIA';
    protected $primaryKey = 'USULAN_KRITERIA_ID'; 
    public $timestamps = false;
    public $incrementing = false;

    public function usulan()
    {
        return $this->belongsTo('App\Model\Usulan', 'USULAN_ID');
    }

    public function detail_kriteria()
	{
		return $this->belongsTo('App\Model\Detail_kriteria','DETAIL_KRITERIA_ID');
	}
}
