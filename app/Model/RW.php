<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RW extends Model
{
    protected $table	= 'REFERENSI.REF_RW';
    protected $primaryKey = 'RW_ID'; 
    public $timestamps = false;
    public $incrementing = false;

    public function kelurahan()
    {
        return $this->belongsTo('App\Model\Kelurahan', 'KEL_ID');
    }

    public function rt()
    {
        return $this->hasMany('App\Model\RT', 'RW_ID')->orderBy('RT_NAMA');
    }

    public function usermusren(){
        return $this->hasMany('App\Model\UserMusren','RW_ID');
    }

    public function user()
    {
        return $this->hasMany('App\Model\User', 'RW_ID');
    }

    public function usulan()
    {
        return $this->hasMany('App\Model\Usulan','RW_ID');
    }

    public function nominalUsulan($tujuan=null)
    {
        $tot = 0;
        $this->usulan->each(function($item) use (&$tot,$tujuan){
            if ($tujuan == null)
            {
                $tot += $item->USULAN_VOLUME * ($item->kamus != null ? $item->kamus->KAMUS_HARGA  : 0);
            }
            else
            {
                $tot += $item->USULAN_VOLUME * ($item->USULAN_TUJUAN == $tujuan ? ($item->kamus != null ? $item->kamus->KAMUS_HARGA : 0)  : 0);   
            }
        });
        return $tot;
    }
}