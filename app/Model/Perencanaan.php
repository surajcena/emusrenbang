<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Perencanaan extends Model
{
    protected $table		= 'MUSRENBANG.DAT_PERENCANAAN';
    protected $primaryKey 	= 'PERENCANAAN_ID';
    public $timestamps 		= false;
    public $incrementing 	= false;
}
