<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pagu extends Model
{
	protected $table		= 'PROTECTED.REF_KATEGORI_PAGU';
    protected $primaryKey 	= 'PAGU_ID';
    public $timestamps 		= false;
    public $incrementing 	= false;
}
