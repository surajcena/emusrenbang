<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Komponen extends Model
{
	protected $table		= 'PROTECTED.REF_KOMPONEN';
    protected $primaryKey 	= 'KOMPONEN_ID';
    public $timestamps 		= false;
    public $incrementing 	= false;    
}
