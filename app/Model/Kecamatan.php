<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Kecamatan extends Model
{
    protected $table	= 'REFERENSI.REF_KECAMATAN';
    protected $primaryKey = 'KEC_ID'; 
    public $timestamps = false;
    public $incrementing = false;

    public function kelurahan()
    {
        return $this->hasMany('App\Model\Kelurahan', 'KEC_ID');
    }

    public function user()
    {
        return $this->hasMany('App\Model\User', 'KEC_ID');
    }

    public function usulan(){
        return $this->hasManyThrough('App\Model\Usulan', 'App\Model\User','KEC_ID','USER_CREATED','KEC_ID');
    }

    public function getUsulan($with = null){
        $id = $this->KEC_ID;
        $usul =  Usulan::wherehas("RW",function($RW) use ($id){
            $RW->wherehas('kelurahan',function($kelurahan) use ($id){
                $kelurahan->where('KEC_ID',$id);
            });
        });

        if($with != null)
        {
            $usul = $usul = $usul->with($with);
        }

        return $usul->get();
    }



    public function getNominalUsulan($jenis = null)
    {
        $nominal = 0;
        foreach ($this->usulan as $key => $usulan) {
            if($jenis == null || ($usulan->USULAN_TUJUAN == $jenis))
                $nominal += $usulan->kamus->KAMUS_HARGA * $usulan->USULAN_VOLUME;
        }
        return $nominal;
    }
}
