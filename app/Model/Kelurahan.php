<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $table	= 'REFERENSI.REF_KELURAHAN';
    protected $primaryKey = 'KEL_ID'; 
    public $timestamps = false;
    public $incrementing = false;

    public function usulan(){
        return $this->hasManyThrough('App\Model\Usulan', 'App\Model\User','KEL_ID','USER_CREATED','KEL_ID');
    }

    public function kecamatan()
    {
        return $this->belongsTo('App\Model\Kecamatan', 'KEC_ID');
    }

    public function rw(){
        return $this->hasMany('App\Model\RW','KEL_ID');
    }

    public function getUsulan($with = null){
        $id = $this->KEL_ID;
        $usul =  Usulan::wherehas("RW",function($RW) use ($id){
            $RW->where('KEL_ID',$id);
        });

        if($with != null)
        {
            $usul = $usul = $usul->with($with);
        }

        return $usul->get();
    }

    public function getNominalUsulan($tujuan = null,$status = null)
    {
        $nominal = 0;
        
        foreach ($this->usulan as $key => $usulan) {
            if( ($tujuan == null && $status == null) || ($status == null && $usulan->USULAN_TUJUAN == $tujuan ) || ($tujuan == null && $usulan->USULAN_STATUS == $status ) || ($usulan->USULAN_TUJUAN == $tujuan && $usulan->USULAN_STATUS == $status ))
            {
                $nominal += $usulan->kamus->KAMUS_HARGA * $usulan->USULAN_VOLUME;
            }
        }
        return $nominal;
    }

    public function user()
    {
        return $this->hasMany('App\Model\User', 'KEL_ID');
    }
}
