/* Your Custom JS for BDG Webkit = Script here */

/* Function Keyup */
function SetNumber(id){

	var numbers = $("#"+id).val();

	if (numbers != ""){
		var number_string = numeral().unformat(numbers);

		var number = numeral(number_string);

		var set_number = number.format('0,0');

		$("#"+id).val(set_number);
	}

}


$(window).load(function(){

	$('input.table-search').keyup( function () {
		$('.table').DataTable().search($('.table-search').val()).draw();
	});

	$("select.dtSelect").on('click',function() {
		$('.table').DataTable().page.len( $('.dtSelect').val() ).draw();
	});

	function show_inputbar(){
		
		$('.open-rincian').on('click',function(){
			$('.overlay').fadeIn('fast',function(){
				$('.input-rincian').animate({'right':'0'},"linear");	
				$("html, body").animate({ scrollTop: 0 }, "slow");
			});	
		});	
		$('.open-form-btl').on('click',function(){
			$('.overlay').fadeIn('fast',function(){
				$('.input-btl').animate({'right':'0'},"linear");	
				$("html, body").animate({ scrollTop: 0 }, "slow");
			});	
		});	
		$('.open-staff').on('click',function(){
					$('.overlay').fadeIn('fast',function(){
						$('.input-staff').animate({'right':'0'},"linear");	
						$("html, body").animate({ scrollTop: 0 }, "slow");
					});	
				});	

		// $('.open-rincian').on('click',function(){
		// 	$('.overlay').fadeIn('fast',function(){
		// 		$('.input-rincian').animate({'right':'0'},"linear");					
		// 	});			
		// });		
		// $('.open-staff').on('click',function(){
		// 	$('.overlay').fadeIn('fast',function(){
		// 		$('.input-staff').animate({'right':'0'},"linear");					
		// 	});			
		// });		

		$('.overlay').on('click',function(){			
			$('.input-sidebar').animate({'right':'-1050px'},"linear",function(){
				$('.overlay').fadeOut('fast');
			});	
		});	

		$('a.tutup-form').click(function(){
			$('.input-spp,.input-spp-langsung,.input-sidebar').animate({'right':'-1050px'},function(){
				$('.overlay').fadeOut('fast');
			});
			
		});	

	};
	show_inputbar();

	//Show kegiatan
	$('.btn-kegiatan,.aktifitas-total').on('click',function(){
			$('#kegiatan-popup').modal();
	});

	//show capaian kerja
	$('.btn-capaian-kerja').on('click',function(){
			$('#kegiatan-popup').modal();
	});
	

	//Tambah kegiatan
	$('.add_kegiatan').on('click',function(){
		$('#add_kegiatan').animate({'right':'0px'},'fast','linear');
	});

	//Back to program list
	$('.back_kegiatan_list').on('click',function(){
		$('#add_kegiatan').animate({'right':'-1010px'},'slow','linear');
	});

	//close modal
	$('.close_modal').on('click',function(){
		$('#kegiatan-popup,#aktivasi-popup').modal('hide');
		
	});

	//Aktivasi
	$('.aktivasi input').on('change',function(){
		if($(this).attr('checked') == 'checked'){
			$(this).attr('checked',false);

			$(this).parent().parent().parent().find('td').css({'color':'#b8bcce'});
		}else{
			$(this).attr('checked','checked');
			$(this).parent().parent().parent().find('td').css({'color':'#555b70'});
		}		
	});

	//Edit Kegiatan
	$('.edit_kegiatan').on('click',function(){		
			$('.th_search').attr('colspan','6');
			$('.aktivasi').fadeIn('fast');			
			$(this).fadeOut('fast',function(){
				$('.save_wrapper').fadeIn('fast');	
			});
	});

	//Save kegiatan
	$('.save_kegiatan').on('click',function(){
			$('.th_search').attr('colspan','5');
			$('.aktivasi').fadeOut('fast');			
			$('.save_wrapper').fadeOut('fast',function(){
				$('.edit_kegiatan').fadeIn('fast');	
			});
	});

	//Select All
	$('.select_all').on('click',function(){
			
			if($(this).attr('data-status') == 'active') {
				$('.aktivasi input').each(function(){
					$(this).trigger('click');							
				});	
				$(this).attr('data-status','inactive');
			}else{
				$('.aktivasi input').each(function(){
					$(this).attr('checked','checked');							
				});	
			}
			$('.table-aktivasi tbody tr td').css({'color':'#555b70'});

			
			
	});

	$('.clear_all').on('click',function(){
			
			$('.aktivasi input').each(function(){
				$(this).attr('checked',false);			
			});
			$('.table-aktivasi tbody tr td').css({'color':'#b8bcce'});
			$('.select_all').attr('data-status','active');
	});
	

	// Jurnal Detail
	$('.table-btl').on('click', ' > tbody > tr > td:nth-child(2)', function () {
		if($(this).parent().hasClass('shown')){			
				$('.jurnal_rincian').slideUp('fast').remove();	
				$(this).parent().removeClass('shown');	
				$('.mi-caret-up',this).addClass('mi-caret-down').css({'color':'#b5bbc2'}).removeClass('mi-caret-up');
			}else{
				$(this).parent().addClass('shown');		
				var data_detail = '<tr class="jurnal_rincian table-detail-1"><td style="background-color: #ffffff !important;padding: 0 0 0 !important;" colspan="7">' + $('#table-detail-btl').html() + '</td></tr>';
				
				$(data_detail).insertAfter('.table-btl tbody tr.shown');

				$('.mi-caret-down',this).addClass('mi-caret-up').css({'color':'#00b0ef'}).removeClass('mi-caret-down');
			}
			$('.jurnal_rincian').on('click', '.open-rincian', function () {
			
					$('.overlay').fadeIn('fast',function(){
						$('.input-rincian').animate({'right':'0'},"linear");	
						$('html, body').animate({ scrollTop: 0 }, "slow");
					});				
		    });
    });

	$('.table-program').on('click', '.table-program > tbody > tr ', function () {
		
		if($("tr").hasClass('program_rincian') == false){
			kode_urusan = $(this).children("td").eq(0).html();
			kode_skpd 	= $(this).children("td").eq(1).html();
		}	
		if(!$(this).hasClass('program_rincian')){
			if($(this).hasClass('shown')){			
				$('.program_rincian').slideUp('fast').remove();	
				$(this).removeClass('shown');	
				$('.icon-bdg_arrow6',this).addClass('icon-bdg_arrow5').css({'color':'#b5bbc2'}).removeClass('icon-bdg_arrow6');
			}else{
				$('.program_rincian').slideUp('fast').remove();	
				$(this).addClass('shown');		
				data_detail = '<tr class="program_rincian"><td style="padding:0!important;" colspan="4">' + $('#table-detail-program').html() + '</td></tr>';
				$(data_detail).insertAfter('.table-program .table tbody tr.shown');
				$('.icon-bdg_arrow5',this).addClass('icon-bdg_arrow6').css({'color':'#00b0ef'}).removeClass('icon-bdg_arrow5');
				$('.table-detail-program-isi').DataTable({
					sAjaxSource: "/main/2017/murni/pengaturan/program/getDataDetail/"+kode_urusan+"/"+kode_skpd,
					aoColumns: [
					{ mData: 'PROGRAM_KODE' },
					{ mData: 'PROGRAM_NAMA' },
					{ mData: 'AKSI' }
					]
				});
			}
		}
    });

});