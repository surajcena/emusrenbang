/* Your Custom JS for BDG Webkit = Script here */
$(document).ready(function(){

	/* Menu slide */
	$('.menu-mobile').click(function(){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			$('.bg-screen').animate({
				marginLeft: '75%'
			});
			$('.app-aside').animate({
				left: '0%'
			});
		}else{
			$(this).addClass('active');
			$('.bg-screen').animate({
				marginLeft: '0'
			});
			$('.app-aside').animate({
				left: '-75%'
			});
		}
	});

	

	/* List content */
	$('.row').each(function (){
		$(this).click(function(){
			if($(this).hasClass('active')){
				$(this).find(".child-kecamatan").slideUp('slow');
			    $(this).removeClass('active');
			}else{
				$(this).find(".child-kecamatan").slideDown('fast');
				$(this).addClass('active');
			}
			
		});
	});

	/* Submenu */
	$('nav > ul > li.parent a').click(function(){
		
		if($(this).parent().hasClass('on')){
			$(this).parent().removeClass('on')
			$(this).parent().find('ul').slideUp('fast');
			$(this).find('i.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down')
		}else{
			$(this).parent().addClass('on')
			$(this).parent().find('ul').slideDown('fast');
			$(this).find('i.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up')
		}
	});

	/* Carousel */
	$('.carousel').carousel({
    	interval: false
	}); 

	

});