@extends('budgeting.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

        <div class="bg-light lter">    
          <ul class="breadcrumb bg-white m-b-none">
            <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app" id="app">
              <i class="icon-bdg_expand1 text"></i>
              <i class="icon-bdg_expand2 text-active"></i>
            </a>   </li>
            <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
            <li><i class="fa fa-angle-right"></i>Belanja</li>                               
            <li><i class="fa fa-angle-right"></i>Belanja Langsung</li>                                
            <li class="active"><i class="fa fa-angle-right"></i>Nama Giat</li>                                
          </ul>
        </div>

        <div class="wrapper-lg bg-dark-grey">
          <div class="row">
            <div class="col-md-12">

              <div class="panel bg-white">

                <div class="panel-heading wrapper-lg">
                  <h5 class="inline font-semibold text-orange m-n ">Belanja Langsung : </h5>
                  <a href="" class="btn btn-info pull-right" style="margin-top: -10px;"><i class="fa fa-print"></i> Cetak RKA</a>
                </div>
                <div class="tab-content tab-content-alt-1 bg-white">
                  <div class="bg-white wrapper-lg">
                    <div class="input-wrapper">
                      <form class="form-horizontal">
                        <div class="form-group">
                          <label class="col-sm-2">Urusan</label>
                          <label class="col-sm-10">: 1.01 - Pendidikan</label>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Perangkat Daerah</label>
                          <label class="col-sm-9">: 1.01.01 - Dinas Pendidikan</label>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Program</label>
                          <label class="col-sm-9">: 1.01.1.01.01 - Program Adum</label>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Kegiatan</label>
                          <label class="col-sm-9">: 1.01.1.01.01.001 - Kegiatan Adum</label>
                        </div>
                        <hr class="m-t-xl">
                        <div class="form-group">
                          <h5 class="text-orange">Detail Kegiatan</h5>
                          <label class="col-sm-2">Jenis Kegiatan</label>
                          <label class="col-sm-4">: Fisik</label>
                          <label class="col-sm-2">Sumber Dana</label>
                          <label class="col-sm-4">: APBD Kota Bandung</label>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Kategori Pagu</label>
                          <label class="col-sm-4">: Janji Walikota</label>
                          <label class="col-sm-2">Waktu Kegiatan</label>
                          <label class="col-sm-4">: Januari s.d Desember</label>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2">Sasaran Kegiatan</label>
                          <label class="col-sm-4">: Masyarakat</label>
                          <label class="col-sm-2">Tagging Kegiatan</label>
                          <label class="col-sm-4">: #Musrenbang #PIPPK</label>
                        </div>                                  
                        <div class="form-group">
                          <label class="col-sm-2">Lokasi Kegiatan</label>
                          <label class="col-sm-10">: Kota Bandung</label>
                        </div>                                  
                        <hr class="m-t-xl">
                        <div class="form-group">
                          <h5 class="text-orange">Indikator Kegiatan</h5>
                          <table class="table">
                            <thead>
                              <tr>
                                <th width="20%">Indikator</th>
                                <th width="60%">Tolak Ukur</th>
                                <th width="20%">Target</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Capaian Program</td>
                                <td>Capaian Program</td>
                                <td>100%</td>
                              </tr>
                              <tr>
                                <td>Capaian Program</td>
                                <td>Capaian Program</td>
                                <td>100%</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="wrapper-lg bg-dark-grey" style="margin-top: -75px;">
          <div class="row">
            <div class="col-md-12">
              <div class="panel bg-white">
                <div class="wrapper-lg">
                  <h5 class="inline font-semibold text-orange m-n ">Rincian</h5>
                  <button class="open-rincian pull-right btn m-t-n-sm btn-success input-xl"><i class="m-r-xs fa fa-plus"></i> Tambah Komponen</button>
                </div>
                <div class="tab-content tab-content-alt-1 bg-white">
                  <div role="tabpanel" class="active tab-pane" id="tab-1">  
                    <div class="table-responsive dataTables_wrapper">
                     <table ui-jq="dataTable" ui-options="" class="table table-jurnal table-striped b-t b-b">
                      <thead>
                        <tr>
                          <th style="width: 1%">#</th>
                          <th>Rekening</th>
                          <th>Komponen</th>
                          <th>Subtitle</th>
                          <th style="width: 10%">Harga / Koefisien</th>
                          <th style="width: 5%">Pajak</th>
                          <th style="width: 5%">Total</th>
                        </tr>
                        <tr>
                          <th colspan="7" class="th_search">
                            <i class="icon-bdg_search"></i>
                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tr>                                    
                        <td>
                          <div class="btn-group dropdown">             
                            <button class="btn m-b-sm m-r-sm btn-default btn-sm" data-toggle="dropdown" aria-expanded="false">1 <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                              <li><a href=""><i class="fa fa-pencil-square"></i>Ubah</a></li>
                              <li><a href=""><i class="fa fa-close"></i>Hapus</a></li>
                              <li class="divider"></li>
                              <li><a href=""><i class="fa fa-info-circle"></i>Info</a></li>
                            </ul>
                          </div>
                        </td>
                        <td>5.2.1.1.01<br>
                          <p class="text-orange">Belanja Pegawai</p></td>
                          <td>Honorarium Ebudgeting<br>
                            <p class="text-orange">Pa Gagat</p>
                          </td>
                          <td>EBudgeting</td>
                          <td>100,000,000<br>
                            <p class="text-orange">1 Orang x 1 Tahun</p>
                          </td>
                          <td><i class="fa fa-check"></i></td>
                          <td>110,000,000,000</td>
                        </tr>
                      </table>
                    </div>
                    <hr class="m-t-xl">
                    <div class="form-group">
                      <div class="col-md-12">
                       <button class="btn input-xl m-t-md btn-danger pull-right" data-toggle="modal" type="button" data-target="#hapus"><i class="fa fa-close m-r-xs"></i>Hapus</button>
                       <button class="btn input-xl m-t-md btn-success pull-left" data-toggle="modal" type="button" data-target="#validasi"><i class="fa fa-check m-r-xs"></i>Validasi</button>
                       <button class="btn input-xl m-t-md btn-warning pull-left"><i class="fa fa-send m-r-xs"></i>Rekomendasi</button>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
<div class="overlay"></div>
<div class="bg-white wrapper-lg input-sidebar input-rincian">
<a href="#" class="close"><i class="icon-bdg_cross"></i></a>
    <form action="#" class="form-horizontal">
      <div class="input-wrapper">
        <h5>Tambah Komponen</h5>
           <div class="form-group">
            <label class="col-sm-3">Jenis</label>
            <div class="col-sm-9">
              <select ui-jq="chosen" class="w-full">
                  <option value="">Silahkan Pilih Jenis</option>
                  <option value="kegiatanA">Lelang</option>
                  <option value="kegiatanB">Swakelola</option>
              </select>
            </div>
          </div>
           <div class="form-group">
            <label class="col-sm-3">Kategori</label>
            <div class="col-sm-9">
              <select ui-jq="chosen" class="w-full">
                  <option value="">Silahkan Pilih Kategori</option>
                  <option value="kegiatanA">SSH</option>
                  <option value="kegiatanB">HSPK</option>
                  <option value="kegiatanC">ASB</option>
                  <option value="kegiatanD">BLUD</option>
              </select>
            </div>
          </div>
           <div class="form-group">
            <label class="col-sm-3">Rekening</label>
            <div class="col-sm-9">
              <select ui-jq="chosen" class="w-full">
                  <option value="">Silahkan Pilih Rekening</option>
                  <option value="kegiatanA">Rekening 1</option>
                  <option value="kegiatanA">Rekening 1</option>
                  <option value="kegiatanA">Rekening 1</option>
                  <option value="kegiatanA">Rekening 1</option>
              </select>
            </div>
          </div>


          <div class="form-group">
            <label for="no_spp" class="col-md-3">Komponen</label>          
            <div class="col-md-6">
              <input type="text" class="form-control" placeholder="Komponen" readonly="">          
            </div> 
            <button class="btn btn-warning col-md-1" data-toggle="modal" type="button" data-target="#kode-komponen">Pilih</button>
            <label for="no_spp" class="col-md-1" style="margin-left: 30px;">Pajak</label>          
                <div class="checkbox-remember" style="margin-top: -8px;">
                   <div class="checkbox m-b-lg">
                      <label class="checkbox-inline i-checks">
                        <input type="checkbox" checked="">
                        <i></i>  
                      </label>
                   </div>
                </div>
          </div>

          <div class="form-group">
            <label for="no_spp" class="col-md-3">Subtitle / Paket</label>          
            <div class="col-sm-9">
              <input type="text" class="form-control" placeholder="Masukan Subtitle" disabled="">          
            </div> 
          </div>

          <div class="form-group">
            <label for="no_spp" class="col-md-3">Keterangan</label>          
            <div class="col-sm-9">
              <input type="text" class="form-control" placeholder="Masukan Keterangan" >          
            </div> 
          </div>

          <div class="form-group">
            <label for="no_spp" class="col-md-3">Koefisien</label>          
            <div class="col-sm-4">
              <input type="text" class="form-control" placeholder="Masukan Jumlah" >      
            </div> 
            <div class="col-sm-4">
              <select ui-jq="chosen" class="w-full">
                  <option value="">Satuan</option>
                  <option value="kegiatanA">Rekening 1</option>
                  <option value="kegiatanA">Rekening 1</option>
                  <option value="kegiatanA">Rekening 1</option>
                  <option value="kegiatanA">Rekening 1</option>
              </select>    
            </div>
            <div class="col-sm-1">
              <button class="btn btn-success"><i class="fa fa-plus"></i></button>
            </div>
          </div>          
          <hr class="m-t-xl">
         <button class="btn input-xl m-t-md btn-success pull-right" type="submit"><i class="fa fa-plus m-r-xs "></i>Tambah Komponen</button>
      </div>
    </form>
  </div>
 </div>

 <div class="plih-komponen modal fade " id="kode-komponen" tabindex="-1" role="dialog">
  <div class="modal-dialog bg-white modal-lg">
      <div class="panel panel-default">
          <div class="wrapper-lg">
            <h5 class="inline font-semibold text-orange m-n text16 ">Pilih Komponen</h5>
          </div>
          <div class="table-responsive">
            <table ui-jq="dataTable" ui-options="" class="table table-popup table-striped b-t b-b">
                    <thead>
                      <tr>
                        <th>Kode</th>
                        <th>Komponen</th>
                        <th>Harga</th>                          
                      </tr>
                      <tr>
                        <th colspan="3" class="th_search">
                            <i class="icon-bdg_search"></i>
                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari Komponen" aria-controls="DataTables_Table_0">
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>1.1.1.1</td>
                      <td>Honor<br><p class="text-orange">Spesifikasi : S1 Akuntansu Pengalaman 8 Tahun</p></td>
                      <td>10,000,000</td>
                    </tr>
                    </tbody>
                  </table>
          </div>
      </div>
    </div>
</div>

<div class="validasi modal fade" id="validasi" tabindex="-1" role="dialog">
  <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h5 class="inline font-semibold text-orange m-n text16 ">Yakin Validasi?</h5>
            </div>
            <div class="modal-footer">
              <a type="button" class="btn btn-success">Validasi</a>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
          </div>
    </div>
</div>
<div class="hapus modal fade" id="hapus" tabindex="-1" role="dialog">
  <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h5 class="inline font-semibold text-orange m-n text16 ">Yakin Hapus?</h5>
            </div>
            <div class="modal-footer">
              <a type="button" class="btn btn-danger">Hapus</a>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
          </div>
    </div>
</div>
@endsection

@section('plugin')
<script type="text/javascript">
  $(document).ready(function(){
   $("#app").trigger('click');
 });
</script>
@endsection