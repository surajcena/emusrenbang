@extends('budgeting.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Belanja</li>                               
                <li class="active"><i class="fa fa-angle-right"></i>Belanja Langsung</li>                                
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">
                
                <div class="panel bg-white">

                  <div class="wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">Belanja Langsung</h5>
                    <a class="pull-right btn m-t-n-sm btn-success" href="{{ url('/') }}/main/{{$tahun}}/murni/belanja-langsung/tambah"><i class="m-r-xs fa fa-plus"></i> Tambah Belanja Langsung</a>
					<div class="col-sm-4 pull-right m-t-n-sm">
                    	<select ui-jq="chosen" class="form-control">
                        	<option value="">- Pilih SKPD -</option>
                            <option value="kegiatanA">Program A</option>
                            <option value="kegiatanA">Program B</option>
                            <option value="kegiatanA">Program C</option>
                            <option value="kegiatanA">Program D</option>
                        </select>
                    </div>                    
                  </div>

                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                             <table ui-jq="dataTable" ui-options="" class="table table-jurnal table-striped b-t b-b">
                                    <thead>
                                      <tr>
                                        <th rowspan="2" style="width: 1%">No</th>
                                        <th rowspan="2">Program/Kegiatan</th>
                                        <th colspan="2" style="text-align: center;">Anggaran</th>                                      
                                        <th colspan="3" style="text-align: center;">Status</th>                                      
                                      </tr>
                                      <tr>
                                        <th style="width: 15%">Pagu</th>                                      
                                        <th style="width: 15%">Rincian</th>                                      
                                        <th style="width: 1%">K</th>                                      
                                        <th style="width: 1%">R</th>                                      
                                        <th style="width: 1%">V</th>                                      
                                      </tr>
                                      <tr>
                                        <th colspan="7" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tr>                                    
                                    	<td>
                                    		<div class="btn-group dropdown">             
								              <button class="btn m-b-sm m-r-sm btn-default btn-sm" data-toggle="dropdown" aria-expanded="false">1 <span class="caret"></span></button>
								              <ul class="dropdown-menu">
								                <li><a href=""><i class="fa fa-search"></i> Detail</a></li>
								                <li><a href=""><i class="fa fa-pencil-square"></i> Ubah</a></li>
								                <li><a data-toggle="modal" type="button" data-target="#hapus"><i class="fa fa-close m-r-xs"></i> Hapus</button></li>
								                <li><a class="open-staff"><i class="fa fa-user"></i> Atur Staff</a></li>
								                <li><a href=""><i class="fa fa-print"></i> Cetak RKA</a></li>
								                <li class="divider"></li>
								                <li><a data-toggle="modal" type="button" data-target="#info"><i class="fa fa-info-circle"></i> Info</a></li>
								              </ul>
								            </div>
                                    	</td>
                                    	<td>1.01.1.01.01.01 - Program Administrasi Perkantoran<br>
                                    		<p class="text-orange">1.01.1.01.01.01.001 - Penyediaan Alat Tulis Kantor</p></td>
                                    	<td>100,000,000,000</td>
                                    	<td>100,000,000,000</td>
                                    	<td>
                                    		<i class="fa fa-lock text-danger"></i>
                                    		<label class="i-switch bg-danger m-t-xs m-r">
								              <input type="checkbox" readonly checked>
								              <i></i>
								            </label>
            							</td>
                                    	<td>
                                    		<i class="fa fa-lock text-danger"></i>
                                    		<label class="i-switch bg-danger m-t-xs m-r">
								              <input type="checkbox" readonly checked>
								              <i></i>
								            </label>                                   		
                                    	</td>
                                    	<td>
                                    		<i class="fa fa-close text-danger"></i>
                                    	</td>
                                    </tr>
                                    <tr>                                   
                                    	<td>
                                    		<div class="btn-group dropdown">             
								              <button class="btn m-b-sm m-r-sm btn-default btn-sm" data-toggle="dropdown" aria-expanded="false">2 <span class="caret"></span></button>
								              <ul class="dropdown-menu">
								                <li><a href=""><i class="fa fa-search"></i> Detail</a></li>
								                <li><a href=""><i class="fa fa-pencil-square"></i> Ubah</a></li>
								                <li><a href=""><i class="fa fa-user"></i> Atur Staff</a></li>
								                <li><a href=""><i class="fa fa-print"></i> Cetak</a></li>
								                <li class="divider"></li>
								                <li><a href=""><i class="fa fa-info-circle"></i> Info</a></li>
								              </ul>
								            </div>
                                    	</td>
                                    	<td>1.01.1.01.01.01 - Program Administrasi Perkantoran<br>
                                    		<p class="text-orange">1.01.1.01.01.01.002 - Penyediaan Alat Tulis Kantor</p></td>
                                    	<td>900,000,000,000</td>
                                    	<td>50,000,000,000</td>
                                    	<td>
                                    		<i class="fa fa-lock text-danger"></i>
                                    		<label class="i-switch bg-danger m-t-xs m-r">
								              <input type="checkbox" readonly checked>
								              <i></i>
								            </label>
                                    	</td>
                                    	<td>
                                    		<i class="fa fa-lock text-danger"></i>
                                    		<label class="i-switch bg-danger m-t-xs m-r">
								              <input type="checkbox" readonly checked>
								              <i></i>
								            </label>
                                    	</td>
                                    	<td><i class="fa fa-check text-danger"></i></td>
                                    </tr>
                                  </table>
                          		</div>
                        	</div>
                  </div>
                </div>

                
              </div>
            </div>

          </div>
          
      
        
        

      </div>
      <!-- App-content-body -->  

    </div>
    <!-- .col -->


    </div>
<div class="overlay"></div>
<div class="bg-white wrapper-lg input-sidebar input-staff">
<a href="#" class="close"><i class="icon-bdg_cross"></i></a>
    <form action="#" class="form-horizontal">
      <div class="input-wrapper">
        <h5>Tambah Staff</h5>
           <div class="form-group">
            <label class="col-sm-3">Staff 1</label>
            <div class="col-sm-9">
              <select ui-jq="chosen" class="w-full">
                  <option value="">Pilih Staff</option>
                  <option value="kegiatanA">1010101 - A</option>
                  <option value="kegiatanA">1010101 - A</option>
              </select>
            </div>
          </div>
           <div class="form-group">
            <label class="col-sm-3">Staff 2</label>
            <div class="col-sm-9">
              <select ui-jq="chosen" class="w-full">
                  <option value="">Pilih Staff</option>
                  <option value="kegiatanA">1010101 - A</option>
                  <option value="kegiatanA">1010101 - A</option>
              </select>
            </div>
          </div>
          <hr class="m-t-xl">
         <button class="btn input-xl m-t-md btn-success pull-right" type="submit"><i class="fa fa-plus m-r-xs "></i>Simpan Staff</button>
      </div>
    </form>
  </div>
</div>
<div class="hapus modal fade" id="hapus" tabindex="-1" role="dialog">
  <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h5 class="inline font-semibold text-orange m-n text16 ">Yakin Hapus?</h5>
            </div>
            <div class="modal-footer">
              <a type="button" class="btn btn-danger">Hapus</a>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
          </div>
    </div>
</div>
<div class="info modal fade" id="info" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h5 class="inline font-semibold text-orange m-n text16 ">Info Kegiatan</h5>
            </div>
            <div class="modal-body">
           		<div class="row">
           			<div class="col-sm-2">Dibuat Oleh</div>
           			<div class="col-sm-4">: 1010101010 - Roby Purnawan</div>
           			<div class="col-sm-2">Staff 1</div>
           			<div class="col-sm-4">: 1010101010 - Roby Purnawan</div>
           			<div class="col-sm-2">Waktu di buat</div>
           			<div class="col-sm-4">: 10/10/10 10:10:10</div>
           			<div class="col-sm-2">Staff 2</div>
           			<div class="col-sm-4">: 1010101010 - Roby Purnawan</div>
           			<div class="col-sm-2">Update terkahir</div>
           			<div class="col-sm-4">: 10/10/10 10:10:10</div>
           		</div>
           		<div class="wrapper-lg">
					<div class="streamline b-l b-grey m-l-lg m-b padder-v">       
				        <div>
				          <a class="pull-left thumb-sm avatar m-l-n-md">
				            <img src="{{ url('/') }}/assets/img/01.jpg" class="b-2x b-white img-circle" alt="...">
				          </a>
				          <div class="m-l-xxl">
				            <div class="m-b-sm">
				              <a href="" class="text-orange">1010101</a><a class="h4 font-semibold">Roby Purnawan</a>
				              <span class="text-muted m-l-sm pull-right">
				                10/10/10 10:10:10
				              </span>
				            </div>
				            <div class="m-b">
				              <div>Menambah Komponen Honor Rp. 100,000,000</div>
				            </div>
				          </div>
				        </div>
				        <hr>
						<div>
				          <a class="pull-left thumb-sm avatar m-l-n-md">
				            <img src="{{ url('/') }}/assets/img/01.jpg" class="b-2x b-white img-circle" alt="...">
				          </a>
				          <div class="m-l-xxl">
				            <div class="m-b-sm">
				              <a href="" class="text-orange">1010101</a><a class="h4 font-semibold">Roby Purnawan</a>
				              <span class="text-muted m-l-sm pull-right">
				                10/10/10 10:10:10
				              </span>
				            </div>
				            <div class="m-b">
				              <div>Menambah Komponen Honor Rp. 100,000,000</div>
				            </div>
				          </div>
				        </div>
				        <hr>
				        <div>
				          <a class="pull-left thumb-sm avatar m-l-n-md">
				            <img src="{{ url('/') }}/assets/img/01.jpg" class="b-2x b-white img-circle" alt="...">
				          </a>
				          <div class="m-l-xxl">
				            <div class="m-b-sm">
				              <a href="" class="text-orange">1010101</a><a class="h4 font-semibold">Roby Purnawan</a>
				              <span class="text-muted m-l-sm pull-right">
				                10/10/10 10:10:10
				              </span>
				            </div>
				            <div class="m-b">
				              <div>Merubah Indikator Kegiatan</div>
				            </div>
				          </div>
				        </div>
				        <hr>
      				</div>           			
           		</div>
            </div>
          </div>
    </div>
</div>
@endsection

@section('plugin')
@endsection