@extends('musrenbang.public.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">
        <div class="bg-light lter">    
          <ul class="breadcrumb bg-white m-b-none">
            <li>
              <a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                <i class="icon-bdg_expand1 text"></i>
                <i class="icon-bdg_expand2 text-active"></i>
              </a>   
            </li>
            <li>
              <a href= "{{ url('/') }}/musrenbang/{{$tahun}}/public">Beranda</a>
            </li>
          </ul>
        </div>
        <div class="wrapper-lg padder-bottom-none">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h3 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">Rp. {{ number_format($nominal['pippk'],0,',','.') }}</span></h3>
                      <p>Total Nominal Usulan PIPPK</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h3 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">Rp. {{ number_format($nominal['pippkproseskelurahan'],0,',','.') }}</span></h3>
                      <p>Total Nominal Usulan PIPPK Diproses Di Kelurahan</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h3 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">Rp. {{ number_format($nominal['pippktolakkelurahan'],0,',','.') }}</span></h3>
                      <p>Total Nominal Usulan PIPPK Ditolak Di Kelurahan</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h3 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">Rp. {{ number_format($nominal['pippkproseskecamatan'],0,',','.') }}</span></h3>
                      <p>Total Nominal Usulan PIPPK Diproses Di Kecamatan</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h3 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">Rp. {{ number_format($nominal['pippktolakkecamatan'],0,',','.') }}</span></h3>
                      <p>Total Nominal Usulan PIPPK Ditolak Di Kecamatan</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h3 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">Rp. {{ number_format($nominal['pippkfinal'],0,',','.') }}</span></h3>
                      <p>Total Nominal Usulan PIPPK Final</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection

@section('plugin')
<script type="text/javascript">
 
</script>
@endsection


