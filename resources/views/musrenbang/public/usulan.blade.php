@extends('musrenbang.public.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017/">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Usulan</li>                                
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              <div class="col-sm-12">
                <div class="panel bg-white wrapper-lg" style="height: 200px;">
                  <div class="form-group col-sm-12">
                      <label class="col-sm-2">Kecamatan</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="kecamatan" id="kecamatan">                                      
                          <option value="x" style="color:black">Kecamatan</option>
                          @foreach($kecamatan as $kec)
                          <option value="{{ $kec->KEC_ID }}" style="color:black">{{ $kec->KEC_NAMA }}</option>
                          @endforeach
                        </select>
                      </div>
                      <label class="col-sm-2">Tipe</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="tipe" id="tipe">                                      
                          <option value="x" style="color:black">Tipe</option>
                          <option value="renja" style="color:black">Renja</option>
                          <option value="pippk" style="color:black">PIPPK</option>
                        </select>
                      </div>
                  </div>                  
                  <div class="form-group col-sm-12">
                      <label class="col-sm-2">Kelurahan</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="kelurahan" id="kelurahan">                                      
                          <option value="x" style="color:black">Kelurahan</option>
                        </select>
                      </div>
                      <label class="col-sm-2">Isu</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="isu" id="isu">                                      
                          <option value="x" style="color:black">Isu</option>
                        </select>
                      </div>
                  </div>                  
                  <div class="form-group col-sm-12">
                      <label class="col-sm-2">RW</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="rw" id="rw">                                      
                          <option value="x" style="color:black">RW</option>
                        </select>
                      </div>
                      <label class="col-sm-2">Kamus</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="kamus" id="kamus">                                      
                          <option value="x" style="color:black">Kamus</option>
                        </select>
                      </div>
                  </div>                  
                </div>                
              </div>
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <div class="col-sm-2 pull-right m-t-n-sm">
                      <select class="form-control dtSelect" id="dtSelect">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                            <option value="500">500</option>
                            <option value="1000">1000</option>
                        </select>
                    </div>
                    <h5 class="font-semibold text-orange m-n ">Daftar Usulan</h5>
                  </div>           
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                             <table ui-jq="dataTable" ui-options="{
                                      sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/getUsulan/x/x/x/x/x/x',
                                      aoColumns: [
                                       { mData: 'USULAN_ID',class:'hide' },
                                       { mData: 'NO',class:'text-center' },
                                       { mData: 'PENGUSUL' },
                                       { mData: 'TIPE' },
                                       { mData: 'URGENSI' },
                                       { mData: 'LOKASI' },
                                       { mData: 'ISU' },
                                       { mData: 'VOLUME' },
                                       { mData: 'HARGASATUAN' },
                                       { mData: 'TOTAL' },
                                       { mData: 'SKPD' },
                                       { mData: 'ALAMAT' },
                                       { mData: 'MAP' },
                                       { mData: 'STATUS' }
                                      ],
                                    }" class="table table-striped b-t b-b" id="table-usulan">
                                    <thead>
                                      <tr>
                                        <th class="hide"></th>
                                        <th>No</th>
                                        <th>Pengusul</th>
                                        <th>Tipe</th>
                                        <th>Urgensi</th>
                                        <th>Lokasi</th>
                                        <th>Isu/Kamus</th>
                                        <th>Volume</th>
                                        <th>Harga / Satuan</th>
                                        <th>Nominal Usulan</th>
                                        <th>SKPD</th>
                                        <th>Alamat</th>
                                        <th>Map</th>
                                        <th>Status</th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        <th colspan="14" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari Usulan" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>

@endsection

@section('plugin')
<script type="text/javascript">
$(document).ready(function(){
  $("#kecamatan").change(function(e, params){
    var kec  = $('#kecamatan').val();
    $('#kelurahan').find('option').remove().end().append('<option value="x">Kelurahan</option>');    
    $('#rw').find('option').remove().end().append('<option value="x">RW</option>');    
    resetTable();
    if(kec != 'x'){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/public/usulan/s/kelurahan/"+kec,
        success : function (data) {
          $('#kelurahan').append(data).trigger('chosen:updated');
        }
      });
    }
  });
  $("#kelurahan").change(function(e, params){
    var kel  = $('#kelurahan').val();
    $('#rw').find('option').remove().end().append('<option value="x">RW</option>');    
    resetTable();
    if(kel!='x'){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/public/usulan/s/rw/"+kel,
        success : function (data) {
          $('#rw').append(data).trigger('chosen:updated');
        }
      });
    }
  });
  $("#rw").change(function(e, params){
    resetTable();
  });
  $("#tipe").change(function(e, params){
    var tipe  = $('#tipe').val();
    $('#isu').find('option').remove().end().append('<option value="x">Isu</option>');    
    $('#kamus').find('option').remove().end().append('<option value="x">Kamus</option>');    
    resetTable();
    if(tipe!='x'){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/public/usulan/s/isu/"+tipe,
        success : function (data) {
          $('#isu').append(data).trigger('chosen:updated');
        }
      });
    }
  });
  $("#isu").change(function(e, params){
    var isu  = $('#isu').val();
    $('#kamus').find('option').remove().end().append('<option value="x">Kamus</option>');
    resetTable();
    if(isu!='x'){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/public/usulan/s/kamus/"+isu,
        success : function (data) {
          $('#kamus').append(data).trigger('chosen:updated');
        }
      });
    }
  });
  $("#kamus").change(function(e, params){
    resetTable();
  });

  function resetTable(){
    var kec  = $('#kecamatan').val();
    var kel  = $('#kelurahan').val();
    var rw   = $('#rw').val();
    var tipe = $('#tipe').val();
    var isu  = $('#isu').val();
    var kamus= $('#kamus').val();
    $('#table-usulan').DataTable().destroy();
    $('#table-usulan').DataTable({
          sAjaxSource: "/musrenbang/{{$tahun}}/public/usulan/getUsulan/"+tipe+"/"+kec+"/"+kel+"/"+rw+"/"+isu+"/"+kamus,
          aoColumns: [
           { mData: 'USULAN_ID',class:'hide' },
           { mData: 'NO',class:'text-center' },
           { mData: 'PENGUSUL' },
           { mData: 'TIPE' },
           { mData: 'URGENSI' },
           { mData: 'LOKASI' },
           { mData: 'ISU' },
           { mData: 'VOLUME' },
           { mData: 'HARGASATUAN' },
           { mData: 'TOTAL' },
           { mData: 'SKPD' },
           { mData: 'ALAMAT' },
           { mData: 'MAP' },
           { mData: 'STATUS' }
          ]
        });
  }

  $('#table-usulan tbody').on('click', 'tr', function () {
        var dt = $('#table-usulan').DataTable();
        var id = dt.row( this ).data()["USULAN_ID"];
        window.location.replace("/musrenbang/2017/public/usulan/detail/"+id);
    } );
});
</script>
@endsection