@extends('musrenbang.public.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Rekap Approval</li>                                
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <div class="col-sm-2 pull-right m-t-n-sm">
                      <select class="form-control dtSelect" id="dtSelect">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                    <h5 class="font-semibold text-orange m-n ">Rekap Approval</h5>
                  </div>           
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                             <table ui-jq="dataTable" ui-options="{
                                      sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/public/kecamatan/getapproval',
                                      aoColumns: [
                                       { mData: 'KECID',class:'hide' },
                                       { mData: 'NO' },
                                       { mData: 'KECAMATAN' },
                                       { mData: 'USERNAME' },
                                       { mData: 'JUMLAHPIPPK' },
                                       { mData: 'JUMLAHPIPPKPROSES' },
                                       { mData: 'JUMLAHPIPPKTERIMA' },
                                       { mData: 'JUMLAHPIPPKTOLAK' },
                                       { mData: 'ACTIONPIPPK' },
                                       { mData: 'JUMLAHRENJA' },
                                       { mData: 'JUMLAHRENJAPROSES' },
                                       { mData: 'JUMLAHRENJATERIMA' },
                                       { mData: 'JUMLAHRENJATOLAK' },
                                       { mData: 'ACTIONRENJA' }
                                      ],
                                    }" class="table table-striped b-t b-b" id="table-skpd">
                                    <thead>
                                      <tr>
                                        <th class="hide" rowspan="2"></th>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Kecamatan</th>
                                        <th rowspan="2">Username</th>
                                        <th colspan="5">PIPPK</th>
                                        <th colspan="5">RENJA</th>
                                      </tr>
                                      <tr>
                                        <th>Jumlah</th>
                                        <th>Jumlah Proses</th>
                                        <th>Jumlah Terima</th>
                                        <th>Jumlah Tolak</th>
                                        <th>Action</th>
                                        <th>Jumlah</th>
                                        <th>Jumlah Proses</th>
                                        <th>Jumlah Terima</th>
                                        <th>Jumlah Tolak</th>
                                        <th>Action</th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        <th colspan="14" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari Usulan" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>

@endsection

@section('plugin')
<script type="text/javascript">
$(document).ready(function(){
  
  
});
</script>
@endsection


