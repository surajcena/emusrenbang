@extends('musrenbang.public.layout')

@section('content')
<style media="screen">
#pac-input {
background-color: #fff;
margin-top: 10px;
font-family: Roboto;
font-size: 15px;
font-weight: 300;
margin-left: 12px;
padding: 0 11px 0 13px;
text-overflow: ellipsis;
width: 300px;
}
</style>
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <!-- <i class="icon-bdg_expand1 text"></i> -->
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Usulan</li>
                <li class="active"><i class="fa fa-angle-right"></i>Berita Acara</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">

                <div class="panel bg-white" style="height:1300px;">
                
                <div class="wrapper-lg">
                  <h5 class="inline font-semibold text-orange m-n ">Berita Acara </h5>
                </div>  

                  <div class="tab-content tab-content-alt-1 bg-white">
                    <div class="bg-white wrapper-lg input-jurnal">
                        <div class="wrapper-xl padder-bottom-none" style="height:600px; margin-top:-50px;">
                    
                                      <h3> Foto Berita Acara </h3>
                                      <div class="wrapper-lg" style="height: : 300px;">
                                      @if($berita->count() > 0)
                                         @foreach($berita as $berita)
                                            @if($berita->lampiran != null)
                                              @foreach($berita->lampiran as $lampiran)
                                            <a href="{{ url('/') }}/uploads/{{ $lampiran }}" target="_blanḳ">
                                               <img src="{{ url('/') }}/uploads/{{ $lampiran }}" style="max-height: 100px;">
                                            </a>
                                             @endforeach
                                           @endif
                                          @endforeach
                                      @else
                                        <p>Foto berita acara belum di upload</p>
                                      @endif
                                        </div>
                                        
                                      <h3> Foto Rembuk Warga </h3>
                                       <div class="wrapper-lg" style="height: : 100px;">
                                       @if($beritaFoto->count() > 0)
                                          @foreach($beritaFoto as $foto)
                                            @if($foto-image != null)
                                              @foreach($foto->image as $img)
                                              <a href="{{ url('/') }}/uploads/{{ $img }}" target="_blanḳ">
                                               <img src="{{ url('/') }}/uploads/{{ $img }}" style="max-height: 100px;">
                                             </a>
                                              @endforeach
                                            @endif
                                          @endforeach
                                        @else
                                          <p>Foto rembuk warga belum di upload</p>
                                        @endif
                                          <p></p>
                                        </div>
                              </div>       

                    </div>    
                  </div>
                </div>

                 
              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
</div>




@endsection





@section('plugin')
<script>
    $(document).ready(function(){
        
    });
</script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>


<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

@endsection
