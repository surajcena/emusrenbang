@extends('musrenbang.public.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">
        <div class="bg-light lter">    
          <ul class="breadcrumb bg-white m-b-none">
            <li>
              <a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                <i class="icon-bdg_expand1 text"></i>
                <i class="icon-bdg_expand2 text-active"></i>
              </a>   
            </li>
            <li>
              <a href= "{{ url('/') }}/musrenbang/{{$tahun}}/public">Beranda</a>
            </li>
          </ul>
        </div>
        <div class="wrapper-lg padder-bottom-none">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h3 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">Rp. {{ number_format($nominal['renja'],0,',','.') }}</span></h3>
                      <p>Total Nominal Usulan RENJA</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h3 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">Rp. {{ number_format($nominal['renjaproseskelurahan'],0,',','.') }}</span></h3>
                      <p>Total Nominal Usulan RENJA Diproses Di Kelurahan</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h3 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">Rp. {{ number_format($nominal['renjatolakkelurahan'],0,',','.') }}</span></h3>
                      <p>Total Nominal Usulan RENJA Ditolak Di Kelurahan</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h3 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">Rp. {{ number_format($nominal['renjaproseskecamatan'],0,',','.') }}</span></h3>
                      <p>Total Nominal Usulan RENJA Diproses Di Kecamatan</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h3 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">Rp. {{ number_format($nominal['renjatolakkecamatan'],0,',','.') }}</span></h3>
                      <p>Total Nominal Usulan RENJA Ditolak Di Kecamatan</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h3 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">Rp. {{ number_format($nominal['renjaprosesskpd'],0,',','.') }}</span></h3>
                      <p>Total Nominal Usulan RENJA Diproses Di SKPD</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h3 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">Rp. {{ number_format($nominal['renjatolakskpd'],0,',','.') }}</span></h3>
                      <p>Total Nominal Usulan RENJA Ditolak Di SKPD</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h3 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">Rp. {{ number_format($nominal['renjafinal'],0,',','.') }}</span></h3>
                      <p>Total Nominal Usulan RENJA Final</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection

@section('plugin')
<script type="text/javascript">
 
</script>
@endsection


