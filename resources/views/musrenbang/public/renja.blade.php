@extends('musrenbang.public.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">
        <div class="bg-light lter">    
          <ul class="breadcrumb bg-white m-b-none">
            <li>
              <a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                <i class="icon-bdg_expand1 text"></i>
                <i class="icon-bdg_expand2 text-active"></i>
              </a>   
            </li>
            <li>
              <a href= "{{ url('/') }}/musrenbang/{{$tahun}}/public">Beranda</a>
            </li>
          </ul>
        </div>
        <div class="wrapper-lg padder-bottom-none">
          <div class="row">
              <div class="col-md-12">
              <div class="row">
                <div class="col-md-4">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h2 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">{{ number_format($jumlahrwmengusulkan,0,',','.') }}</span> / {{ number_format($jumlahrw,0,',','.') }}</h2>
                      <p>RW Sudah Mengajukan / RW Total</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h2 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">{{ number_format($jumlahkelurahanmengusulkan,0,',','.') }}</span> / {{ number_format($jumlahkelurahan,0,',','.') }}</h2>
                      <p>Kelurahan Sudah Mengajukan / Kelurahan Total</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h2 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">{{ number_format($jumlahkecamatanmengusulkan,0,',','.') }}</span> / {{ number_format($jumlahkecamatan,0,',','.') }}</h2>
                      <p>Kecamatan Sudah Mengajukan / Kecamatan Total</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h2 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">Rp. {{ number_format($totalusulanrt,0,',','.') }}</span></h2>
                      <p>Jumlah Total Usulan</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h2 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">Rp. {{ number_format($totalusulanrt/$jumlahrw,0,',','.') }}</span></h2>
                      <p>Usulan / RW</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection

@section('plugin')
<script type="text/javascript">
  $( document ).ready(function() {
    window.setInterval(function(){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/public/getOnline",
        success : function (data) {
          $('#user-online').text(data['o']);
          $('#pippk').text(data['pippk']);
          $('#renja').text(data['renja']);
        }
      });
    }, 5000);
  });
</script>
@endsection


