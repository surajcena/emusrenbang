@extends('musrenbang.public.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017">Halaman Utama</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Lacak Usulan</li>   
                <li class="active"><i class="fa fa-angle-right"></i>-</li>                                
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              <div class="col-sm-12">
                <div class="panel bg-white wrapper-lg" style="height: 200px;">
                  <div class="form-group col-sm-12">
                    <label class="col-sm-2">Kecamatan</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="kecamatan" id="kecamatan">                                      
                          <option value="x">Kecamatan</option>
                          @foreach($kecamatan as $k)
                            <option value="{{$k->KEC_ID}}">{{$k->KEC_NAMA}}</option>
                          @endforeach
                        </select>
                      </div>
                      
                      <label class="col-sm-2">RW</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="rw" id="rw">                        
                          <option value="x" style="color:black">Pilih RW</option>
                        </select>
                      </div>
                  </div>                  
                  <div class="form-group col-sm-12">
                      <label class="col-sm-2">Kelurahan</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="kelurahan" id="kelurahan">                                      
                          <option value="x" style="color:black">Pilih Kelurahan</option>
                        </select>
                      </div>
                      
                      <label class="col-sm-2">Tipe Usulan
                        </label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="tipe" id="tipe">                                      
                           <option value="1" style="color:black">RENJA</option>
                           <option value="2" style="color:black">PIPPK</option>
                        </select>
                      </div>
                     
                  </div>  

                   <div class="form-group col-sm-12">
                      <label class="col-sm-2">Tahun</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="tahun" id="tahun">  
                          <option value="x" style="color:black">Pilih Tahun</option>
                          @foreach($usulan_tahun as $ut)
                            <option style="color:black" value="{{$ut->USULAN_TAHUN}}">{{$ut->USULAN_TAHUN}}</option>
                          @endforeach
                        </select>
                      </div>

                  </div>                 

                </div>  


              </div>
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <h4>Rekapitulasi Usulan <text id="h_tipe"></text> RW.<text id="h_rw"></text> Kel.<text id="h_kel"></text> Kec.<text id="h_kec"></text> Pemerintah Kota Bandung Tahun <text id="h_tahun"></text></h4>
                    <div class="row">
                      <div class="col-sm-10">
                       <div class="nav-tabs-alt tabs-alt-1 b-t three-row" id="tab-jurnal" >
                          <ul class="nav nav-tabs" role="tablist">
                             <li class="active">
                              <a data-target="#tab-1" role="tab" data-toggle="tab" data-status="1" id="diproses" onclick="resetTables('1')">{{$diproses}} Di Proses <i class="fa fa-refresh text-info"></i></a>
                             </li>
                             <li>
                              <a data-target="#tab-2" role="tab" data-toggle="tab" data-status="2" id="diterima" onclick="resetTables('2')">{{$diterima}} Di Terima <i class="fa fa-check text-success"></i></a>
                             </li>
                             <li>
                              <a data-target="#tab-3" role="tab" data-toggle="tab" data-status="0" id="ditolak" onclick="resetTables('0')">{{$ditolak}} Di Tolak <i class="fa fa-close text-danger"></i></a>
                             </li>
                          </ul>
                        </div> 
                      </div>    
                      <div class="col-sm-2 pull-right m-t-n-sm">

<!--                         <a href="" id="export-to-excel" class="btn btn-success pull-right" style="margin-top:10px;"><i class="fa fa-back"></i> Export excel</a>
 -->                        <select class="form-control dtSelect" id="dtSelect" style="margin-top:57px">
                              <option value="10">10</option>
                              <option value="25">25</option>
                              <option value="50">50</option>
                              <option value="100">100</option>
                          </select>

                      </div>
                   </div> 

                  </div>  


                  <div class="tab-content tab-content-alt-0 bg-dark-grey" id="tab_usulan">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                              <table id="table-usulan" ui-jq="dataTable" ui-options="
                                  {
                                    sAjaxSource: '',
                                    aoColumns: [
                                    { mData: 'USULAN_ID',class:'hide' },
                                    { mData: 'NO',class:'text-center' },
                                    { mData: 'AKSI' },
                                    { mData: 'PENGUSUL' },
                                    { mData: 'ISUKAMUS' },
                                    { mData: 'VOLUME' },
                                    { mData: 'HARGA' },
                                    { mData: 'TOTAL' },
                                    { mData: 'STATUS' },
                                    { mData: 'KET' },
                                    { mData: 'SKPD' },
                                    ],
                                    order: [[10, 'asc']]
                                  }" class="table table-usulan table-striped b-t b-b">
                              
                                    <thead>
                                      <tr>
                                        <th class="hide">#</th>
                                        <th>No</th>
                                        <th>Action</th>
                                        <th>Nama Pengusul</th>
                                        <th>Isu / <font style='color:orange'>Usulan</font></th>
                                        <th>Volume</th>
                                        <th>Harga Satuan</th>
                                        <th>Total</th>
                                        <th>Status Approval</th>
                                        <th>Alasan / Catatan</th>
                                        <th>SKPD</th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        <th colspan="10" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                          <td colspan="3"> </td>
                                          <td colspan="3"><b>Total Nominal : Rp. <text id="totalnominal"></text> </b></td>
                                        </tr>  
                                      </tfoot>
                                  </table>
                              </div>
                          </div>
                        </div>  

                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>

@endsection

@section('plugin')
<script type="text/javascript">

function resetTables(status){
    var kecamatan = $('#kecamatan').val();
    var kelurahan = $('#kelurahan').val();
    var rw        = $('#rw').val();
    var tipe      = $('#tipe').val();
    var tahun     = $('#tahun').val();

    if(kecamatan=='x' || kelurahan=='x' || rw=='x' || tipe=='x' || tahun=='x' ){
       $.alert('Form harap diisi!');
    }

    $('#table-usulan').DataTable().destroy();
    
    $('#table-usulan').DataTable({
          sAjaxSource: "{{ url('/') }}/musrenbang/"+tahun+"/public/tracking/data/"+status+"/"+kecamatan+"/"+kelurahan+"/"+rw+"/"+tipe,
           aoColumns: [
            { mData: 'USULAN_ID',class:'hide' },
            { mData: 'NO',class:'text-center' },
            { mData: 'AKSI' },
            { mData: 'PENGUSUL' },
            { mData: 'ISUKAMUS' },
            { mData: 'VOLUME' },
            { mData: 'HARGA' },
            { mData: 'TOTAL' },
            { mData: 'STATUS' },
            { mData: 'KET' },
            { mData: 'SKPD' },
          ],
          "order": [[10, "asc"]],

        initComplete:function(setting,json){
            $("#totalnominal").html(json.jumlah);
            $("#diproses").html(json.diproses);
            $("#diterima").html(json.diterima);
            $("#ditolak").html(json.ditolak);
            $("#h_kec").html(json.kec);
            $("#h_kel").html(json.kel);
            $("#h_rw").html(json.rw);
            $("#h_tipe").html(json.tipe);
            $("#h_tahun").html(json.tahun);
        }
    });
  }



$(document).ready(function(){
 /* $(".buttonstatus").on("click",function(event) {

    status = $(this).data('status');
    
    resetTable();
  });

  $(".filter").on('change',function(e){
   // alert("momokurobo");
    resetTable();
  });*/

 $("#kecamatan").change(function(e, params){
          
    id  = $('#kecamatan').val();
     $('#kelurahan').find('option').remove().end().append('<option value="x">-Kelurahan-</option>');
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{$tahun}}/public/get/kelurahan/tracking/"+id,
        success : function (data) {
          $('#kelurahan').append(data['opt']).trigger('chosen:updated');
        }
      });
  });

 $("#kelurahan").change(function(e, params){
          
    id  = $('#kelurahan').val();
     $('#rw').find('option').remove().end().append('<option value="x">RW - Kelurahan</option>');
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{$tahun}}/public/get/rw/tracking/"+id,
        success : function (data) {
          $('#rw').append(data['opt']).trigger('chosen:updated');
        }
      });
  });
  
});

</script>
@endsection


