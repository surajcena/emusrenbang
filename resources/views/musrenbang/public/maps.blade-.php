

@extends('musrenbang.public.layout_map')
@section('content')
<br><br>
 <div id="mymap"></div>
         <div id="map"></div>

 <style type="text/css">
            /* Set a size for our map container, the Google Map will take up 100% of this container */
            #map {
                width: 100%;
                height: 100%;
            }
        </style>


@section('plugin')
 <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
 <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzixLw-QwjOpEiL8qmveEmafmgM27mgnk&libraries=places&callback=initMap">
</script>
        
        <script type="text/javascript">
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);
            
            function init() {

                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 13,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(-6.916091, 107.618427), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"all","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#d3fcbd"},{"saturation":"14"},{"lightness":"8"},{"gamma":"1.20"},{"weight":"1"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"hue":"#46ff00"}]},{"featureType":"landscape","elementType":"geometry.stroke","stylers":[{"hue":"#ff0000"}]},{"featureType":"landscape","elementType":"labels","stylers":[{"hue":"#3dff00"},{"visibility":"off"}]},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"lightness":"100"},{"saturation":"12"},{"visibility":"simplified"},{"color":"#eef5d4"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#f6ecc6"},{"lightness":"44"},{"saturation":"-14"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"hue":"#89ff00"},{"saturation":"-47"},{"lightness":"-12"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#d5f9ca"}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"color":"#a257f9"},{"visibility":"on"},{"lightness":"63"},{"saturation":"-67"}]},{"featureType":"poi.medical","elementType":"geometry.fill","stylers":[{"color":"#f6d7d7"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"poi.school","elementType":"geometry.fill","stylers":[{"color":"#dfdbea"},{"saturation":"-59"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#f2fc2f"},{"lightness":"25"},{"saturation":"2"},{"visibility":"on"}]},{"featureType":"transit.station.airport","elementType":"geometry","stylers":[{"color":"#e1b9f0"},{"saturation":"-12"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#b0dff8"}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                @foreach($usulan as $usulan)
                @if ($usulan->USULAN_LAT && $usulan->USULAN_LONG != '')
                
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng( {{$usulan->USULAN_LAT}}, {{$usulan->USULAN_LONG}}),
                    map: map,
                    title: 'Snazzy!'
                });

                 marker.addListener('click', function() {
                  infowindow.open(map, marker);
                });

                var contentString = '<div id="content">'+
                        '<div id="siteNotice">'+
                        '</div>'+
                        '<h1 id="firstHeading" class="firstHeading">Bdg</h1>'+
                        '<div id="bodyContent">'+
                        '<p><b>ID : </b>{{$usulan->USULAN_ID}}-.</p>'+
                        '<p><b>Usulan : </b>{{$usulan->kamus->KAMUS_NAMA}}-.</p>'+
                        '<p><b>Kriteria : </b>{{$usulan->USULAN_URGENSI}}-.</p>'+
                        '<p><b>Volume : </b>{{$usulan->USULAN_VOLUME}} {{$usulan->kamus->KAMUS_SATUAN}}-.</p>'+
                        '<p><b>Harga : </b>{{$usulan->kamus->KAMUS_HARGA}}-.</p>'+
                        '<p><b>Alamat : </b>{{$usulan->ALAMAT}}.</p>'+
                        '<p><b>Status : </b> @if($usulan->USULAN_POSISI == 3 && $usulan->USULAN_STATUS == 2) Kota @endif </p>'+
                        '</div>'+
                        '</div>';

                var infowindow = new google.maps.InfoWindow({
                  content: contentString
                });

                @endif
                @endforeach
                
            }
        </script>

@endsection

