@extends('musrenbang.public.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">
        <div class="bg-light lter">    
          <ul class="breadcrumb bg-white m-b-none">
            <li>
              <a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                <i class="icon-bdg_expand1 text"></i>
                <i class="icon-bdg_expand2 text-active"></i>
              </a>   
            </li>
            <li>
              <a href= "{{ url('/') }}/musrenbang/{{$tahun}}/public">Beranda</a>
            </li>
          </ul>
        </div>
        <div class="wrapper-lg padder-bottom-none">
          <div class="row">
            <!-- SEMUA RENJA PIPPK -->
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-body wrapper-sm">
                  <div class="table-responsive">
                    <table class="table table-bordered" style="background-color: white">
                      <tr>
                        <td colspan="10" align="center">
                          <b> Total usulan melalui e-musrenbang (Renja dan PIPPK) </b>
                        </td>
                      </tr>
                      <tr>
                          <td><b>Tahun</b></td>
                          <td><b>Total Semua Usulan</b></td>
                          <td><b>Usulan Tidak Lolos Validasi (tidak diterima)</b></td>
                          <td><b>Usulan Lolos Validasi </b></td>
                          <td><b>Usulan Masuk APBD</b></td>
                          <td><b>Jumlah / Total</b></td>
                      </tr>  
                      <tr>
                          <td>{{$tahun}}</td>
                          <td>
                            <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                            {{ number_format($nominal['countAll'],0,',','.') }} / 
                            <span class="text-success"> Rp.{{ number_format($nominal['all'],0,',','.') }}
                            </span></h5>
                          </td>
                          <td>
                              <i class="fa fa-close text-danger"></i>
                              <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format( 
                                ($nominal['countrenjatolakkelurahan'] + $nominal['countrenjatolakkecamatan'] +
                                 $nominal['countrenjaptolakskpd'] + $nominal['countrenjatolakapbd'] +
                                 $nominal['countpippktolakkelurahan'] + $nominal['countpippktolakkecamatan'] +
                                 $nominal['countpippktolakapbd'])
                                ,0,',','.') }} / <br>
                              <span class="text-success"> Rp.{{ number_format(
                                ($nominal['renjatolakkelurahan'] + $nominal['renjatolakkecamatan'] +
                                $nominal['renjatolakskpd'] + $nominal['renjatolakapbd'] +
                                $nominal['pippktolakkelurahan'] + $nominal['pippktolakkecamatan'] +
                                $nominal['pippktolakapbd'])
                                ,0,',','.') }}
                              </span></h5>
                              <br>
                              <i class="fa fa-close text-danger"></i>
                               Tolak di Kelurahan
                              <br>
                              {{ number_format(
                                $nominal['countpippktolakkelurahan'] + $nominal['countrenjatolakkelurahan']
                                ,0,',','.') }} / 
                              Rp.{{ number_format(
                              $nominal['pippktolakkelurahan'] + $nominal['renjatolakkelurahan'] 
                              ,0,',','.') }}
                              <br>
                              <i class="fa fa-close text-danger"></i>
                               Tolak di Kecamatan
                              <br>
                              {{ number_format(
                                $nominal['countpippktolakkecamatan'] + $nominal['countrenjatolakkecamatan']
                                ,0,',','.') }} / 
                              Rp.{{ number_format(
                              $nominal['pippktolakkecamatan'] + $nominal['renjatolakkecamatan'] 
                              ,0,',','.') }}
                              <br>
                              <i class="fa fa-close text-danger"></i>
                              Tolak di SKPD
                              <br>
                              {{ number_format( $nominal['countrenjaptolakskpd'] ,0,',','.') }} / 
                              Rp.{{ number_format( $nominal['renjatolakskpd'] ,0,',','.') }}
                              <br>
                              <i class="fa fa-close text-danger"></i>
                              Tolak APBD 
                              <br>
                              {{ number_format(
                                $nominal['countpippktolakapbd'] + $nominal['countrenjatolakapbd']
                                ,0,',','.') }} / 
                              Rp.{{ number_format(
                              $nominal['pippktolakapbd'] + $nominal['renjatolakapbd']
                              ,0,',','.') }}
                          </td>
                          <td>
                            <i class="fa fa-refresh text-info"></i>
                             <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                            {{ number_format(
                              ($nominal['countrenjaproseskelurahan'] + $nominal['countrenjaproseskecamatan'] +
                              $nominal['countrenjaprosesskpd'] + $nominal['countrenjaprosesapbd'] + 
                              $nominal['countpippkproseskelurahan'] + $nominal['countpippkproseskecamatan'] +
                              $nominal['countpippkprosesapbd'])
                              ,0,',','.') }} / <br>
                            <span class="text-success"> Rp.{{ number_format(
                             ($nominal['renjaproseskelurahan'] + $nominal['renjaproseskecamatan'] +
                              $nominal['renjaprosesskpd'] + $nominal['renjaprosesapbd'] +
                              $nominal['pippkproseskelurahan'] + $nominal['pippkproseskecamatan'] +
                              $nominal['pippkprosesapbd'])
                              ,0,',','.') }}
                            </span></h5>
                              <br>
                              <i class="fa fa-refresh text-info"></i>
                                Proses di Kelurahan 
                              <br>
                              {{ number_format(
                                $nominal['countpippkproseskelurahan'] + $nominal['countrenjaproseskelurahan']
                                ,0,',','.') }} /
                              Rp.{{ number_format(
                              $nominal['pippkproseskelurahan'] + $nominal['renjaproseskelurahan']
                              ,0,',','.') }}
                              <br>
                              <i class="fa fa-refresh text-info"></i>
                               Proses di Kecamatan 
                              <br>
                              {{ number_format(
                                $nominal['countpippkproseskecamatan'] + $nominal['countrenjaproseskecamatan']
                                ,0,',','.') }} / 
                              Rp.{{ number_format(
                              $nominal['pippkproseskecamatan'] + $nominal['renjaproseskecamatan']
                              ,0,',','.') }}
                              <br>
                              <i class="fa fa-refresh text-info"></i>
                               Proses di SKPD 
                              <br>
                              {{ number_format(
                                $nominal['countrenjaprosesskpd']
                                ,0,',','.') }} / 
                              Rp.{{ number_format(
                              $nominal['renjaprosesskpd']
                              ,0,',','.') }}
                              <br>
                              <i class="fa fa-refresh text-info"></i>
                               Proses APBD 
                              <br>
                              {{ number_format(
                                $nominal['countpippkprosesapbd'] + $nominal['countrenjaprosesapbd']
                                ,0,',','.') }} / 
                              Rp.{{ number_format(
                              $nominal['pippkprosesapbd'] + $nominal['renjaprosesapbd']
                              ,0,',','.') }}
                          </td>
                          <td>
                            <i class="fa fa-check text-success"></i>
                            <h5 class="m-t-xs text-orange font-semibold m-b-sm"> 
                            {{ number_format(
                              $nominal['countusulanfinal'] 
                              ,0,',','.') }} / 
                            <span class="text-success"> 
                            Rp.{{ number_format($nominal['usulanfinal'] ,0,',','.') }}
                            </span>
                            </h5>
                          </td>
                           <td>
                            <i class="fa fa-close text-danger"></i> +
                            <i class="fa fa-refresh text-info"></i> +
                            <i class="fa fa-check text-success"></i> = total
                            <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format( 
                                ($nominal['countrenjatolakkelurahan'] + $nominal['countrenjatolakkecamatan'] +
                                 $nominal['countrenjaptolakskpd'] + $nominal['countrenjatolakapbd'] +
                                 $nominal['countpippktolakkelurahan'] + $nominal['countpippktolakkecamatan'] +
                                 $nominal['countpippktolakapbd'])
                                ,0,',','.') }}
                                <span class="text-black"> + </span>
                                {{ number_format(
                              ($nominal['countrenjaproseskelurahan'] + $nominal['countrenjaproseskecamatan'] +
                              $nominal['countrenjaprosesskpd'] + $nominal['countrenjaprosesapbd'] + 
                              $nominal['countpippkproseskelurahan'] + $nominal['countpippkproseskecamatan'] +
                              $nominal['countpippkprosesapbd'])
                              ,0,',','.') }} 
                               <span class="text-black"> + </span>
                                {{ number_format( $nominal['countusulanfinal'] ,0,',','.') }}
                               <span class="text-black">
                                = {{ number_format($nominal['countAll'],0,',','.') }}
                               </span>
                              <br>  / <br>
                              <span class="text-success"> Rp.{{ number_format(
                                ($nominal['renjatolakkelurahan'] + $nominal['renjatolakkecamatan'] +
                                $nominal['renjatolakskpd'] + $nominal['renjatolakapbd'] +
                                $nominal['pippktolakkelurahan'] + $nominal['pippktolakkecamatan'] +
                                $nominal['pippktolakapbd'])
                                ,0,',','.') }}
                                <span class="text-black"> + </span>
                                Rp.{{ number_format(
                             ($nominal['renjaproseskelurahan'] + $nominal['renjaproseskecamatan'] +
                              $nominal['renjaprosesskpd'] + $nominal['renjaprosesapbd'] +
                              $nominal['pippkproseskelurahan'] + $nominal['pippkproseskecamatan'] +
                              $nominal['pippkprosesapbd'])
                              ,0,',','.') }}
                              <span class="text-black"> + </span>
                              Rp.{{ number_format($nominal['usulanfinal'] ,0,',','.') }}
                              <span class="text-black">
                              =  Rp.{{ number_format($nominal['all'],0,',','.') }}
                              </span>
                              
                              </span></h5>
                              
                          </td>
                      </tr>  
                      
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- PIPPK -->
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-body wrapper-sm">
                  <div class="table-responsive">
                    <table class="table table-bordered" style="background-color: white">
                      <tr>
                        <td colspan="10" align="center">
                          <b> Total usulan PIPPK melalui e-musrenbang </b>
                        </td>
                      </tr>
                      <tr>
                          <td><b>Tahun</b></td>
                          <td><b>Total Semua Usulan</b></td>
                          <td><b>Usulan Tidak Lolos Validasi (tidak diterima)</b></td>
                          <td><b>Usulan Lolos Validasi </b></td>
                          <td><b>Usulan Masuk APBD</b></td>
                          <td><b>Jumlah / Total</b></td>
                      </tr>  
                      <tr>
                          <td>{{$tahun}}</td>
                          <td>
                             <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format($nominal['countpippk'],0,',','.') }} / 
                              <span class="text-success"> 
                              Rp.{{ number_format($nominal['pippk'],0,',','.') }}
                              </span>
                              </h5>
                          </td>
                          <td>
                             <i class="fa fa-close text-danger"></i>
                             <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format( 
                                ($nominal['countpippktolakkelurahan'] + $nominal['countpippktolakkecamatan'] +
                                 $nominal['countpippktolakapbd'])
                                ,0,',','.') }} / <br>
                              <span class="text-success"> Rp.{{ number_format(
                                ($nominal['pippktolakkelurahan'] + $nominal['pippktolakkecamatan'] +
                                 $nominal['pippktolakapbd'])
                                ,0,',','.') }}
                              </span></h5>
                              <br>
                              <i class="fa fa-close text-danger"></i>
                              <b> Tolak di Kelurahan </b>
                              <br>
                              {{ number_format($nominal['countpippktolakkelurahan'],0,',','.') }} / 
                              Rp.{{ number_format($nominal['pippktolakkelurahan'],0,',','.') }}
                              <br>
                              <i class="fa fa-close text-danger"></i>
                              <b> Tolak di Kecamatan </b>
                              <br>
                              {{ number_format($nominal['countpippktolakkecamatan'],0,',','.') }} / 
                              Rp.{{ number_format($nominal['pippktolakkecamatan'],0,',','.') }}
                              <br>
                              <i class="fa fa-close text-danger"></i>
                              <b> Tolak APBD </b>
                              <br>
                                {{ number_format($nominal['countpippktolakapbd'],0,',','.') }} / 
                                Rp.{{ number_format($nominal['pippktolakapbd'],0,',','.') }} 
                        </td>
                          </td>
                           <td>
                             <i class="fa fa-refresh text-info"></i>
                              <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format(
                                ($nominal['countpippkproseskelurahan'] + $nominal['countpippkproseskecamatan'] +
                                 $nominal['countpippkprosesapbd'])
                                ,0,',','.') }} / <br>
                              <span class="text-success"> Rp.{{ number_format(
                               ($nominal['pippkproseskelurahan'] + $nominal['pippkproseskecamatan'] +
                                $nominal['pippkprosesapbd'])
                                ,0,',','.') }}
                              </span></h5>
                              <br>
                              <i class="fa fa-refresh text-info"></i>
                              <b> Proses di Kelurahan </b>
                              <br>
                              {{ number_format($nominal['countpippkproseskelurahan'],0,',','.') }} /
                              Rp.{{ number_format($nominal['pippkproseskelurahan'],0,',','.') }}
                            <br>
                              <i class="fa fa-refresh text-info"></i>
                              <b> Proses di Kecamatan </b>
                              <br>
                              {{ number_format($nominal['countpippkproseskecamatan'],0,',','.') }} / 
                              Rp.{{ number_format($nominal['pippkproseskecamatan'],0,',','.') }}
                            <br>
                              <i class="fa fa-refresh text-info"></i>
                             <b> Proses APBD </b>
                              <br>
                              {{ number_format($nominal['countpippkprosesapbd'],0,',','.') }} / 
                              Rp.{{ number_format($nominal['pippkprosesapbd'],0,',','.') }}
                          </td>
                          <td>
                            <i class="fa fa-check text-success"></i>
                            <h5 class="m-t-xs text-orange font-semibold m-b-sm"> 
                            {{ number_format($nominal['countpippkfinal'],0,',','.') }} / 
                            <span class="text-success"> 
                            Rp.{{ number_format($nominal['pippkfinal'],0,',','.') }}
                            </span>
                            </h5>
                          </td>
                          <td>
                            <i class="fa fa-close text-danger"></i> +
                            <i class="fa fa-refresh text-info"></i> +
                            <i class="fa fa-check text-success"></i> = total
                            <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format( 
                                ($nominal['countpippktolakkelurahan'] + $nominal['countpippktolakkecamatan'] +
                                 $nominal['countpippktolakapbd'])
                                ,0,',','.') }} 
                                <span class="text-black"> + </span>
                                {{ number_format(
                              ($nominal['countpippkproseskelurahan'] + $nominal['countpippkproseskecamatan'] +
                               $nominal['countpippkprosesapbd'])
                              ,0,',','.') }} 
                               <span class="text-black"> + </span>
                                {{ number_format($nominal['countpippkfinal'],0,',','.') }}
                               <span class="text-black">
                                = {{ number_format($nominal['countpippk'],0,',','.') }} 
                               </span>
                              <br>  / <br>
                              <span class="text-success"> Rp.{{ number_format(
                                ($nominal['pippktolakkelurahan'] + $nominal['pippktolakkecamatan'] +
                                 $nominal['pippktolakapbd'])
                                ,0,',','.') }}
                                <span class="text-black"> + </span>
                                Rp.{{ number_format(
                             ($nominal['pippkproseskelurahan'] + $nominal['pippkproseskecamatan'] +
                              $nominal['pippkprosesapbd'])
                              ,0,',','.') }}
                              <span class="text-black"> + </span>
                              Rp.{{ number_format($nominal['pippkfinal'],0,',','.') }}
                              <span class="text-black">
                              =  Rp.{{ number_format($nominal['pippk'],0,',','.') }}  
                              </span>
                              
                              </span></h5>
                              
                          </td>
                      </tr>  
                      
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- RENJA -->
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-body wrapper-sm">
                  <div class="table-responsive">
                    <table class="table table-bordered" style="background-color: white">
                      <tr>
                        <td colspan="10" align="center">
                          <b> Total usulan RENJA melalui e-musrenbang </b>
                        </td>
                      </tr>
                      <tr>
                          <td><b>Tahun</b></td>
                          <td><b>Total Semua Usulan</b></td>
                          <td><b>Usulan Tidak Lolos Validasi (tidak diterima)</b></td>
                          <td><b>Usulan Lolos Validasi </b></td>
                          <td><b>Usulan Masuk APBD</b></td>
                          <td><b>Jumlah / Total</b></td>
                      </tr>  
                      <tr>
                          <td>{{$tahun}}</td>
                          <td>
                             <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                            {{ number_format($nominal['countRenja'],0,',','.') }} / 
                            <span class="text-success"> 
                            Rp.{{ number_format($nominal['renja'],0,',','.') }}
                            </span>
                            </h5>
                          </td>
                          <td>
                             <i class="fa fa-close text-danger"></i>
                              <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format( 
                                ($nominal['countrenjatolakkelurahan'] + $nominal['countrenjatolakkecamatan'] +
                                 $nominal['countrenjaptolakskpd'] + $nominal['countrenjatolakapbd'])
                                ,0,',','.') }} / <br>
                              <span class="text-success"> Rp.{{ number_format(
                                ($nominal['renjatolakkelurahan'] + $nominal['renjatolakkecamatan'] +
                                $nominal['renjatolakskpd'] + $nominal['renjatolakapbd'])
                                ,0,',','.') }}
                              </span></h5>
                              <br>
                              <i class="fa fa-close text-danger"></i>
                               Tolak di Kelurahan
                              <br>
                              {{ number_format($nominal['countrenjatolakkelurahan'],0,',','.') }} / 
                              Rp.{{ number_format($nominal['renjatolakkelurahan'],0,',','.') }}
                              <br>
                              <i class="fa fa-close text-danger"></i>
                               Tolak di Kecamatan
                              <br>
                              {{ number_format($nominal['countrenjatolakkecamatan'],0,',','.') }} / 
                              Rp.{{ number_format($nominal['renjatolakkecamatan'],0,',','.') }}
                              <br>
                              <i class="fa fa-close text-danger"></i>
                              Tolak di SKPD
                              <br>
                              {{ number_format($nominal['countrenjaptolakskpd'],0,',','.') }} / 
                              Rp.{{ number_format($nominal['renjatolakskpd'],0,',','.') }}
                              <br>
                              <i class="fa fa-close text-danger"></i>
                              Tolak APBD 
                              <br>
                              {{ number_format($nominal['countrenjatolakapbd'],0,',','.') }} / 
                              Rp.{{ number_format($nominal['renjatolakapbd'],0,',','.') }}
                          </td>
                          <td>
                             <i class="fa fa-refresh text-info"></i>
                             <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                            {{ number_format(
                              ($nominal['countrenjaproseskelurahan'] + $nominal['countrenjaproseskecamatan'] +
                              $nominal['countrenjaprosesskpd'] + $nominal['countrenjaprosesapbd'])
                              ,0,',','.') }} / <br>
                            <span class="text-success"> Rp.{{ number_format(
                             ($nominal['renjaproseskelurahan'] + $nominal['renjaproseskecamatan'] +
                              $nominal['renjaprosesskpd'] + $nominal['renjaprosesapbd'])
                              ,0,',','.') }}
                            </span></h5>
                              <br>
                              <i class="fa fa-refresh text-info"></i>
                                Proses di Kelurahan 
                              <br>
                              {{ number_format($nominal['countrenjaproseskelurahan'],0,',','.') }} /
                              Rp.{{ number_format($nominal['renjaproseskelurahan'],0,',','.') }}
                              <br>
                              <i class="fa fa-refresh text-info"></i>
                               Proses di Kecamatan 
                              <br>
                              {{ number_format($nominal['countrenjaproseskecamatan'],0,',','.') }} / 
                              Rp.{{ number_format($nominal['renjaproseskecamatan'],0,',','.') }}
                              <br>
                              <i class="fa fa-refresh text-info"></i>
                               Proses di SKPD 
                              <br>
                              {{ number_format($nominal['countrenjaprosesskpd'],0,',','.') }} / 
                              Rp.{{ number_format($nominal['renjaprosesskpd'],0,',','.') }}
                              <br>
                              <i class="fa fa-refresh text-info"></i>
                               Proses APBD 
                              <br>
                              {{ number_format($nominal['countrenjaprosesapbd'],0,',','.') }} / 
                              Rp.{{ number_format($nominal['renjaprosesapbd'],0,',','.') }}
                          </td>
                          <td>
                            <i class="fa fa-check text-success"></i>
                            <h5 class="m-t-xs text-orange font-semibold m-b-sm"> 
                              {{ number_format($nominal['countrenjafinal'],0,',','.') }} / 
                            <span class="text-success"> 
                              Rp.{{ number_format($nominal['renjafinal'],0,',','.') }}
                            </span>
                            </h5>
                          </td>
                          <td>
                            <i class="fa fa-close text-danger"></i> +
                            <i class="fa fa-refresh text-info"></i> +
                            <i class="fa fa-check text-success"></i> = total
                            <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format( 
                                ($nominal['countrenjatolakkelurahan'] + $nominal['countrenjatolakkecamatan'] +
                                 $nominal['countrenjaptolakskpd'] + $nominal['countrenjatolakapbd'])
                                ,0,',','.') }} 
                                <span class="text-black"> + </span>
                                {{ number_format(
                              ($nominal['countrenjaproseskelurahan'] + $nominal['countrenjaproseskecamatan'] +
                              $nominal['countrenjaprosesskpd'] + $nominal['countrenjaprosesapbd'])
                              ,0,',','.') }} 
                               <span class="text-black"> + </span>
                                {{ number_format($nominal['countrenjafinal'],0,',','.') }}
                               <span class="text-black">
                                = {{ number_format($nominal['countRenja'],0,',','.') }} 
                               </span>
                              <br>  / <br>
                              <span class="text-success"> Rp.{{ number_format(
                                ($nominal['renjatolakkelurahan'] + $nominal['renjatolakkecamatan'] +
                                $nominal['renjatolakskpd'] + $nominal['renjatolakapbd'])
                                ,0,',','.') }}
                                <span class="text-black"> + </span>
                                Rp.{{ number_format(
                             ($nominal['renjaproseskelurahan'] + $nominal['renjaproseskecamatan'] +
                              $nominal['renjaprosesskpd'] + $nominal['renjaprosesapbd'])
                              ,0,',','.') }}
                              <span class="text-black"> + </span>
                              Rp.{{ number_format($nominal['renjafinal'],0,',','.') }}
                              <span class="text-black">
                              =  Rp.{{ number_format($nominal['renja'],0,',','.') }}  
                              </span>
                              
                              </span></h5>
                              
                          </td>
                      </tr>  
                      
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection

@section('plugin')
<script type="text/javascript">
  $( document ).ready(function() {
    window.setInterval(function(){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/public/getOnline",
        success : function (data) {
          $('#user-online').text(data['o']);
          $('#pippk').text(data['pippk']);
          $('#renja').text(data['renja']);
        }
      });
    }, 5000);
  });
</script>
@endsection


