@extends('musrenbang.public.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Detail SKPD</li>                                
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <div class="dropdown col-sm-2 pull-right m-t-n-sm">
                      <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown">Export
                      <span class="caret"></span></button>
                      <ul class="dropdown-menu">
                        <li class="dropdown-header">Export Per SKPD</li>
                        <li><a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpd/export/{{$id}}">Semua</a></li>
                        <li><a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpd/export/{{$id}}/2">Ditolak</a></li>
                        <li><a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpd/export/{{$id}}/1">Dalam Proses Dan Diterima</a></li>
                        <li class="dropdown-header">Export Per Kecamatan</li>
                        <li><a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpdperkecamatan/export/{{$id}}">Semua</a></li>
                        <li><a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpdperkecamatan/export/{{$id}}/2">Tolak</a></li>
                        <li><a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpdperkecamatan/export/{{$id}}/1">Dalam Proses Dan Diterima</a></li>
                      </ul>
                    </div>
                    <!-- <div class="col-sm-2 pull-right m-t-n-sm">
                      <a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpd/export/{{$id}}"><button class="btn btn-danger">Exports</button></a>
                    </div>
                     <div class="col-sm-2 pull-right m-t-n-sm">
                      <a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpdperkecamatan/export/{{$id}}"><button class="btn btn-danger">Export / Kecamatan</button></a>
                    </div> -->
                    <div class="col-sm-2 pull-right m-t-n-sm">
                      <select class="form-control dtSelect" id="dtSelect">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                    <h5 class="font-semibold text-orange m-n ">Detail SKPD</h5>
                  </div>           
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                             <table ui-jq="dataTable" ui-options="{
                                      sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/public/skpd/getdetail/{{$id}}',
                                      aoColumns: [
                                       { mData: 'NO' },
                                       { mData: 'PENGUSUL' },
                                       { mData: 'ISUKAMUS' },
                                       { mData: 'URGENSI' },
                                       { mData: 'LOKASI' },
                                       { mData: 'ALAMAT' },
                                       { mData: 'VOLUME' },
                                       { mData: 'HARGA' },
                                       { mData: 'NOMINAL' },
                                       { mData: 'STATUS' },
                                      ],
                                    }" class="table table-striped b-t b-b" id="table-usulan">
                                    <thead>
                                      <tr>
                                        <th>No</th>
                                        <th>Pengusul</th>
                                        <th>Isu/Kamus</th>
                                        <th>Urgensi</th>
                                        <th>Lokasi</th>
                                        <th>Alamat / Keterangan</th>
                                        <th>Volume</th>
                                        <th>Harga / Satuan</th>
                                        <th>Nominal Usulan</th>
                                        <th>Status</th>
                                      </tr>
                                      <tr>
                                        <th colspan="10" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari Usulan" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>

@endsection

@section('plugin')
<script type="text/javascript">
$(document).ready(function(){
  
  
});
</script>
@endsection


