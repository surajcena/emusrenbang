@extends('musrenbang.public.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017">Halaman Utama</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Album Musrenbang</li>                               
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              <div class="col-sm-12">
                <div class="panel bg-white wrapper-lg" style="height: 200px;">
                  <div class="form-group col-sm-12">
                    <label class="col-sm-2">Kecamatan</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="kecamatan" id="kecamatan">                                      
                          <option value="x">Kecamatan</option>
                         @foreach($kecamatan as $k)
                            <option value="{{$k->KEC_ID}}">{{$k->KEC_NAMA}}</option>
                          @endforeach
                        </select>
                      </div>
                      
                      <label class="col-sm-2">RW</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="rw" id="rw">                        
                          <option value="x" style="color:black">Pilih RW</option>
                        </select>
                      </div>
                  </div>                  
                  <div class="form-group col-sm-12">
                      <label class="col-sm-2">Kelurahan</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="kelurahan" id="kelurahan">                                      
                          <option value="x" style="color:black">Pilih Kelurahan</option>
                        </select>
                      </div>
                      
                      <label class="col-sm-2">Tahun</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="tahun" id="tahun">  
                          <option value="x" style="color:black">Pilih Tahun</option>
                          <option value="2017" style="color:black">2017</option>
                         
                        </select>
                      </div>
                     
                  </div>  

                   <div class="form-group col-sm-12">
                      <label class="col-sm-2"></label>
                      <div class="col-sm-4">
                        
                      </div>
                      <label class="col-sm-2"></label>
                      <div class="col-sm-4">
                        <button class="btn btn-success" onclick="resetTables()">Tampilkan Gambar</button>
                      </div>

                  </div>                 

                </div>  


              </div>
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class='list-group gallery' id="gallery-image">
                  @foreach($beritaFoto as $berita)
                    @foreach($berita->image as $lampiran)
                      <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                          <a class="thumbnail fancybox" rel="ligthbox" href="{{ url('/') }}/uploads/{{ $lampiran }}">
                              <img class="img-responsive" alt="" src="{{ url('/') }}/uploads/{{ $lampiran }}" />
                              <div class='text-right'>
                                  <small class='text-muted'>Rembuk Warga {{$berita->tgl_update}}</small>
                              </div> <!-- text-right / end -->
                          </a>
                      </div> 
                   @endforeach
                  @endforeach
                  

               </div> <!-- list-group / end -->

                  
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>



<style type="text/css">
  .gallery
{
    display: inline-block;
    margin-top: 20px;
}
</style>


@endsection

@section('plugin')
<script type="text/javascript">

function resetTables(){
    var kecamatan = $('#kecamatan').val();
    var kelurahan = $('#kelurahan').val();
    var rw        = $('#rw').val();
    var tahun     = $('#tahun').val();

    if(kecamatan=='x' || kelurahan=='x' || rw=='x' || tahun=='x' ){
       $.alert('Form harap diisi!');
    }

    //$('#gallery-image').destroy();
    document.getElementById("gallery-image").innerHTML = "Hello World!";
    /*$('#gallery').DataTable({
          sAjaxSource: "{{ url('/') }}/musrenbang/"+tahun+"/public/tracking/data/"+status+"/"+kecamatan+"/"+kelurahan+"/"+rw+"/"+tipe,
           aoColumns: [
            { mData: 'USULAN_ID',class:'hide' },
            { mData: 'NO',class:'text-center' },
            { mData: 'AKSI' },
            { mData: 'PENGUSUL' },
            { mData: 'ISUKAMUS' },
            { mData: 'VOLUME' },
            { mData: 'HARGA' },
            { mData: 'TOTAL' },
            { mData: 'STATUS' },
            { mData: 'KET' },
            { mData: 'SKPD' },
          ],
          "order": [[10, "asc"]],

        initComplete:function(setting,json){
            $("#totalnominal").html(json.jumlah);
            $("#diproses").html(json.diproses);
            $("#diterima").html(json.diterima);
            $("#ditolak").html(json.ditolak);
            $("#h_kec").html(json.kec);
            $("#h_kel").html(json.kel);
            $("#h_rw").html(json.rw);
            $("#h_tipe").html(json.tipe);
            $("#h_tahun").html(json.tahun);
        }
    });*/
  }



$(document).ready(function(){
 /* $(".buttonstatus").on("click",function(event) {

    status = $(this).data('status');
    
    resetTable();
  });

  $(".filter").on('change',function(e){
   // alert("momokurobo");
    resetTable();
  });*/

 $("#kecamatan").change(function(e, params){
          
    id  = $('#kecamatan').val();
     $('#kelurahan').find('option').remove().end().append('<option value="x">-Kelurahan-</option>');
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{$tahun}}/public/get/kelurahan/tracking/"+id,
        success : function (data) {
          $('#kelurahan').append(data['opt']).trigger('chosen:updated');
        }
      });
  });

 $("#kelurahan").change(function(e, params){
          
    id  = $('#kelurahan').val();
     $('#rw').find('option').remove().end().append('<option value="x">RW - Kelurahan</option>');
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{$tahun}}/public/get/rw/tracking/"+id,
        success : function (data) {
          $('#rw').append(data['opt']).trigger('chosen:updated');
        }
      });
  });
  
});

</script>


<script type="text/javascript">
  $(document).ready(function(){
    //FANCYBOX
    //https://github.com/fancyapps/fancyBox
    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });
});
</script>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

@endsection


