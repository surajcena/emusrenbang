@extends('musrenbang.public.layout_accordion')

@section('content')

<BR>
<h2 align="center">Daftar Usulan Renja & PIPPK pada Musrenbang {{$tahun}}</h2>
<h4 align="center">
<?php
if (!Session::get('fb_user_access_token')) {
?>

  Silahkan <a href="{{$login_url}}"><u>masuk</u></a> untuk menggunakan fitur komentar, share dan like.

<?php
} else {
?>
  Selamat datang, {{Session::get('facebook_user')['name']}}!
<?php
}
?>
</h4>

<br>
  <div class="container-fluid col-md-12">
    <div class="panel-group" id="accordionKecamatan">
    <?php
    foreach ($arr as $kec_name => $kel_row){
    ?>

      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordionKecamatan" href="#collapse{{str_replace(' ', '', $kec_name)}}"> {{$idkey[md5($kec_name)]}}-Kecamatan {{$kec_name}} (Usulan PIPPK: {{$usulancount['pippk'][md5($kec_name)]}} Renja: {{$usulancount['renja'][md5($kec_name)]}}) </a></h4>
        </div>

        <div id="collapse{{str_replace(' ', '', $kec_name)}}" class="panel-collapse collapse">
          <div class="panel-body"> 
            <!-- Here we insert another nested accordion -->
            <div class="panel-group" id="kelurahan{{str_replace(' ', '', $kec_name)}}">

              <?php
              foreach ($kel_row as $kel_name => $rw_row){
              ?>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"><a data-toggle="collapse" data-parent="#kelurahan{{str_replace(' ', '', $kec_name)}}" href="#collapse{{str_replace(' ', '', $kel_name)}}"> {{$idkey[md5($kec_name.$kel_name)]}}-Kelurahan {{$kel_name}} (Usulan PIPPK: {{$usulancount['pippk'][md5($kec_name.$kel_name)]}} Renja: {{$usulancount['renja'][md5($kec_name.$kel_name)]}})</a></h4>
                </div>

                <div id="collapse{{str_replace(' ', '', $kel_name)}}" class="panel-collapse collapse">
                  <div class="panel-body"> 
                    <div class="panel-group" id="rw{{str_replace(' ', '', $kel_name)}}">

                    <?php
                    foreach ($rw_row as $rw_name => $rw_id){
                    ?>
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title"><a rwid="{{$rw_id}}" data-toggle="collapse" data-parent="#rw{{str_replace(' ', '', $kel_name)}}" href="#rw{{str_replace(' ','',$rw_id)}}"> {{$idkey[md5($kec_name.$kel_name.$rw_name)]}}-RW {{$rw_name}} (Usulan PIPPK: {{$usulancount['pippk'][md5($kec_name.$kel_name.$rw_name)]}} Renja: {{$usulancount['renja'][md5($kec_name.$kel_name.$rw_name)]}}) </a></h4>
                        </div>

                        <div id="rw{{str_replace(' ','',$rw_id)}}" class="panel-collapse collapse">
                          <div class="panel-body" id="rwtabel{{$rw_id}}"> 
                            
                          </div>
                        </div>
                      </div>
                      <?php
                      }
                      ?>
                  </div>
                </div>
              </div>
            </div>
            <?php
            }
            ?>
            <!-- Inner accordion ends here --> 
          </div>
        </div>
      </div>
    </div>
    <?php
    }
    ?>  
  </div>
  
</div>

@endsection