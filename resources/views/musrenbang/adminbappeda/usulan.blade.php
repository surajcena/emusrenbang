@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Usulan</li>   
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <div class="row">    
                      <div class="col-sm-2 pull-right m-t-n-sm">
                        <a href="{{URL::to('/musrenbang/2017/export/usulan/')}}" id="export-to-excel" class="btn btn-success pull-right" style="margin-top:10px;"><i class="fa fa-back"></i> Export excel</a>
                        <select class="form-control dtSelect" id="dtSelect" style="margin-top:57px">
                              <option value="10">10</option>
                              <option value="25">25</option>
                              <option value="50">50</option>
                              <option value="100">100</option>
                          </select>

                      </div>
                   </div> 

                  </div>  


                  <div class="tab-content tab-content-alt-0 bg-dark-grey" id="tab_usulan">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                              <table id="table-usulan" ui-jq="dataTable" class="table table-usulan table-striped b-t b-b">
                              
                                    <thead>
                                      <tr>
                                        <th class="hide">#</th>
                                        <th>No</th>
                                        <th>Id Pengusul</th>
                                        <th>Nama Pengusul</th>
                                        <th>Isu / Kamus</th>
                                        <th>Volume</th>
                                        <th>Harga Satuan</th>
                                        <th>Total</th>
                                        <th>Status Approval</th>
                                        <th>Prioritas</th>
                                        <th>Alasan / Catatan</th>
                                        <th>SKPD</th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        <th colspan="11" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                        </div>  

                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>

@endsection

@section('plugin')
<script type="text/javascript">
$(window).load(function() {
  $('#table-usulan').DataTable().destroy();
  $('#table-usulan').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{{ url('/') }}/musrenbang/{{$tahun}}/adminbappeda/datatable/usulan',
      columns: [
          { data: 'usulanid',class:'hide' ,name :'USULAN_ID'},
          { data: 'no',class:'text-center',name :'USULAN_ID'},
          { data: 'idpengusul' ,name :'user.email'},
          { data: 'namapengusul', name:'user.id'},
          { data: 'isukamus' , name:'kamus.isu.ISU_NAMA'},
          { data: 'volume' ,name :'USULAN_VOLUME'},
          { data: 'harga' ,name :'kamus.KAMUS_HARGA'},
          { data: 'total' },
          { data: 'status',name :'USULAN_STATUS'},
          { data: 'prioritas',name :'USULAN_PRIORITAS' },
          { data: 'keterangan',name :'KET_ACC' },
          { data: 'skpd',name :'kamus.skpd.SKPD_NAMA'}
      ]
    });
});
var status = 1;
  
$(document).ready(function(){
  $(".buttonstatus").on("click",function(event) {

    status = $(this).data('status');
    
    resetTable();
  });

  $(".filter").on('change',function(e){
    resetTable();
  });

  function resetTable(){
    var pengusul   = $('#pengusul').val();
    var isu  = $('#isu').val();
    var kamus= $('#kamus').val();
    $('#table-usulan').DataTable().destroy();
    
    $('#table-usulan').DataTable({
          sAjaxSource: "{{ url('/') }}/musrenbang/{{$tahun}}/data/usulan/{"+status+"/"+pengusul+"/"+isu+"/"+kamus,
           aoColumns: [
            { mData: 'usulanid',class:'hide' },
            { mData: 'no',class:'text-center' },
            { mData: 'aksi' },
            { mData: 'idpengusul' },
            { mData: 'namapengusul' },
            { mData: 'isukamus' },
            { mData: 'volume' },
            { mData: 'harga' },
            { mData: 'total' },
            { mData: 'status' },
            { mData: 'prioritas' },
            { mData: 'keterangan' },
            { mData: 'skpd' },
          ],
          "order": [[10, "asc"]]
    });
  }
});
</script>
@endsection


