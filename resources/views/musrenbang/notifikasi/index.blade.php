@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

        <div class="bg-light lter">
          <ul class="breadcrumb bg-white m-b-none">
            <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
              <i class="icon-bdg_expand1 text"></i>
              <i class="icon-bdg_expand2 text-active"></i>
            </a>   </li>
            <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
            <li class="active"><i class="fa fa-angle-right"></i>Daftar Isu</li>
          </ul>
        </div>

        <div class="wrapper-lg bg-dark-grey">
          <div class="row">
            <div class="col-md-12">

              <div class="panel bg-white">

                <div class="wrapper-lg">
                  <h5 class="inline font-semibold text-orange m-n ">Daftar Isu</h5>
                  <div class="col-sm-1 pull-right m-t-n-sm">
                      <select class="form-control dtSelect" id="dtSelect">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>   
                </div>

                <div class="tab-content tab-content-alt-1 bg-white table-kecamatan" id="table-kecamatan">
                  <div role="tabpanel" class="active tab-pane table-kecamatan" id="tab-1">
                    <div class="table-responsive dataTables_wrapper table-kecamatan">
                      <table id="table-kamus" ui-jq="dataTable" ui-options="" class="table table-kecamatan table-striped b-t b-b">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>SUBJEK</th>
                          <th>PREDIKAT</th>
                          <th>OBJEK</th>
                          <th>PENGUBAHAN</th>
                          <th>WAKTU</th>
                          <th>ALAMAT IP</th>
                        </tr>
                        <tr>
                          <th colspan="10" class="th_search">
                            <i class="icon-bdg_search"></i>
                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


@endsection
@section('plugin')
<script type="text/javascript">
$(window).on('load', function (e) {
  $('#table-kamus').DataTable().destroy();
  $('#table-kamus').DataTable({
      'processing': true,
      'serverSide': true,
      'ajax'      : {
        'url' : '{{ url('/') }}/musrenbang/notifikasi/getNotificationAPI',
      },
      aoColumns: [
      { mData: 'no',class:'text-center' },
      { mData: 'SUBJEK' },
      { mData: 'PREDIKAT' },
      { mData: 'AKSI' },
      { mData: 'PENGUBAHAN' },
      { mData: 'WAKTU' },
      { mData: 'IP_ADDRESS'}
      ]
    });

  $('#table-kamus')
    .on('processing.dt', function ( e, settings, processing ) {
        if (processing) {
          if (! $('div.loading').length)
            $('<div class="loading">Loading&#8230;</div>').appendTo('body');
        }
        else $('div.loading').remove();
    });
});
</script>
@endsection