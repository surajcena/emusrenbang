@extends('musrenbang.layout')

@section('content')
<style media="screen">
#pac-input {
background-color: #fff;
margin-top: 10px;
font-family: Roboto;
font-size: 15px;
font-weight: 300;
margin-left: 12px;
padding: 0 11px 0 13px;
text-overflow: ellipsis;
width: 300px;
}
</style>
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                   <i class="icon-bdg_expand1 text"></i> 
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/{{$tahun}}">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Usulan</li>
                <li class="active"><i class="fa fa-angle-right"></i>Usulkan Kembali</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">

                  <div class="panel-heading wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">Tambah Usulan @if($usulan->USULAN_TUJUAN ==1) Renja  @else  PIPPK @endif</h5>
                  </div>

                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div class="bg-white wrapper-lg input-jurnal">
                          @if (count($errors) > 0)
                              <div class="alert alert-danger">
                                  <ul>
                                      @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                              </div>
                          @endif
                            <form action="{{url('/')}}/musrenbang/{{$tahun}}/usulan/tambah/kembali/submit" method="post" class="form-horizontal" enctype="multipart/form-data">
                              <div class="input-wrapper">

                                  <div class="form-group">
                                    <label class="col-sm-2">Isu Usulan</label>
                                    <div class="col-sm-10">
                                      <input type="text" class="form-control" name="isu" readonly="" value="{{ $usulan->kamus->isu->ISU_NAMA }}">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2">Kamus Usulan </label>
                                    <div class="col-sm-10">
                                      <input type="text" class="form-control" name="kamus" readonly="" value="{{ $usulan->kamus->KAMUS_NAMA }}">
                                      <input type="hidden" value="{{$usulan->KAMUS_ID}}" name="kamus">
                                    </div>
                                  </div>

                                  <div class="form-group" id="div-upload-gambar" style="display: none">
                                    <label class="col-sm-2">Upload Gambar</label>
                                    <div class="col-sm-7">
                                      <p style="color:red">
                                      
                                      </p>
                                      <input ui-jq="filestyle" name="image[]" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-0" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);" accept=".jpg,.png,.jpeg">
                                    </div>
                                    <div class="col-sm-1">
                                      <button class="btn btn-info" id="upload" type="button"><i class="fa fa-plus"> Tambah Gambar</i></button>
                                    </div>
                                  </div>

                                  <div class="form-group" style="display:none" id="div1">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-9">
                                      <input ui-jq="filestyle" name="image[]" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-1" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);" accept=".jpg,.png,.jpeg">
                                    </div>
                                    <div class="col-sm-1">
                                    </div>
                                  </div>

                                  <input type="hidden" class="form-control" name="kec" readonly="" value="{{ $lokasi->kelurahan->kecamatan->KEC_NAMA }}">
                                      <input type="hidden" class="form-control" name="kel" readonly="" placeholder="Kelurahan" value="{{ $lokasi->kelurahan->KEL_NAMA }}">
                                      <input type="hidden" class="form-control" name="rw" readonly="" placeholder="RW" value="RW {{ $lokasi->RW_NAMA }}">
                                 

                                   <div class="form-group">
                                    <label class="col-sm-2">Masukan @if(Auth::user()->level == 5) RW @elseif(Auth::user()->level == 1) RT @endif Penerima Manfaat</label>
                                    <div class="col-sm-8" style="margin-top:10px">
                                      @foreach ($rt as $rt)
                                        <input type="checkbox" class="" value="{{$rt->RT_ID}}" style="margin-left:10px;" name="rt[]"> {{$rt->RT_NAMA}}
                                      @endforeach
                                      </div>
                                  </div>

                                   <div class="form-group">
                                    <label class="col-sm-2">Urgensi Kegiatan / Keterangan</label>
                                    <div class="col-sm-10">
                                       <textarea width="20" name="urgensi" class="form-control" placeholder="Masukan Alasan kenapa kegiatan ini diperlukan dan keterangan lainya.">
                                         {{$usulan->USULAN_URGENSI}}
                                       </textarea>
                                    </div>
                                  </div>
                                 

                                  <input type="hidden" value="{{$usulan->USULAN_TUJUAN}}" name="usulan_tujuan">
                                  <input type="hidden" value="{{$usulan->USULAN_ID}}" name="usulan_id">

                                  <hr class="m-t-xl">
                                  <div class="form-group">
                                      
                                      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                                    <div class="col-md-12">
                                     <a href="{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/usulkankembali" class="btn input-xl btn-info pull-right"><i class="fa fa-reply m-r-xs"></i>Kembali</a>
                                      <a onclick="return abaikan('{{$usulan->USULAN_ID}}')" class="btn input-xl btn-danger pull-right"><i class="fa fa-close m-r-xs"></i>Abaikan</a>
                                      <button class="btn input-xl btn-success pull-right m-r-xs" type="submit"><i class="fa fa-check m-r-xs"></i>Usulkan Kembali</button>
                                    </div>
                                  </div>
                              </div>
                            </form>
                          </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
</div>
@endsection





@section('plugin')

<script>
    $(document).ready(function(){

        $("#upload").click(function(){
            $("#div1").fadeIn("fast");
            $("#upload").addClass("addGambar");
        });

        $("#isu").change(function(e, params){
            var id  = $('#isu').val();
           $('#kamus').find('option').remove().end().append('<option>Kamus Usulan</option>');
            $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/kamus/"+id,
              success : function (data) {
                $('#kamus').append(data['opt']).trigger('chosen:updated');
                if(data['tipe'] == 1 || data['tipe'] == 2) $('#div-upload-gambar').fadeIn('fast').removeClass('hide');
                else $('#div-upload-gambar').fadeOut('fast').addClass('hide');
              }
            });
        });

        $("#kamus").change(function(e, params){
          // alert('kamus');
            var id  = $('#kamus').val();
            $('#satuan').val();
            $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/satuan/"+id,
              success : function (data) {
                $('#satuan').val(data);
              }
            });
        });
        $("#isu").change();
    });

  function abaikan(id){
    var token        = $('#token').val();
    $.confirm({
        title: 'Abaikan Usulan!',
        content: 'Usulan tidak di usulkan kembali?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                      url: "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/usulkankembali/abaikan",
                      type: "POST",
                      data: {'_token'         : token,
                            'id'              : id},
                      success: function(msg){
                          $.alert(msg);
                          window.location.href = "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/usulkankembali";
                        }
                  });
                }
            },
            Tidak: function () {
            }
        }
    });
  }
</script>


<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

@endsection
