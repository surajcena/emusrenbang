@extends('musrenbang.layout')
@section('css')

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <style media="screen">
      .table-responsive{background-color: #fff}
    </style>
@stop
@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <!-- <i class="icon-bdg_expand1 text"></i> -->
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Forum Gabungan</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">

                <div class="panel bg-white">

                  <div class="wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">Daftar Forgab</h5>
                    @if(Auth::user()->level == 1 && Auth::user()->app == 1 )
                    <a class="pull-right btn m-t-n-sm btn-success input-xl" href="{{ url('/') }}/musrenbang/2017/usulan/tambah"><i class="m-r-xs fa fa-plus"></i> Tambah Usulan</a>
                    @endif
                  </div>
                  <div class="row">
                      <div class="col-lg-12">
                          <div class="panel panel-primary filterable">
                              <div class="panel-heading clearfix  ">
                                  <div class="panel-title pull-left">
                                         <div class="caption">
                                      <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                                      TableTools
                                  </div>
                                  </div>
                              </div>

                              <div class="panel-body table-responsive">
                                <table class="table table-striped table-bordered" id="table1 domains_table">
                                  
                                        <div class="col-lg-6">
                                          <select class="form-control input-sm" id="usulan" name="usulan">
                                                  <option value="">----== Pilih Isu ==----</option>
                                                   @foreach ($usulan as $usulan)
                                                      <option value="{{$usulan->kamus->KAMUS_ID}}">{{$usulan->kamus->KAMUS_ID}} - {{$usulan->kamus->KAMUS_NAMA}}</option>
                                                  @endforeach
                                              </select>
                                         </div>  
                                          
                                   
                              <div class="panel-body table-responsive">
                                  <table class="table table-striped table-bordered" id="table1">
                                      <thead>
                                          <tr>
                                              <th>NO</th>
                                              <th>URUSAN</th>
                                              <th>PROGRAM</th>
                                              <th>KEGIATAN</th>
                                              <th>ISU</th>
                                              <th>JENIS KEGIATAN</th>
                                              <th>KAMUS USULAN</th>
                                              <th>KODE KOMPONEN</th>
                                              <th>NAMA KOMPONEN</th>
                                              <th>PROGRAM</th>
                                              <th>HARGA</th>
                                              <th>SKPD TUJUAN</th>
                                              <th>DEFINISI OPERASIONAL</th>
                                              <th>KETERANGAN</th>
                                          </tr>
                                      </thead>
                                      <tbody id="kamus">
                                      <tbody>
                                        @foreach($kamus as $kamus)
                                          <tr>
                                              <td>{{$kamus->KAMUS_ID}}</td>
                                              <td>{{$kamus->KAMUS_URUSAN}}</td>
                                              <td>{{$kamus->KAMUS_PROGRAM}}</td>
                                              <td>{{$kamus->KAMUS_KEGIATAN}}</td>
                                              <td>{{$kamus->KAMUS_ISU}}</td>
                                              <td>--</td>
                                              <td>{{$kamus->KAMUS_NAMA}}</td>
                                              <td>{{$kamus->KAMUS_KODE_KOMPONEN}}</td>
                                              <td>{{$kamus->KAMUS_NAMA_KOMPONEN}}</td>
                                              <td>{{$kamus->KAMUS_PROGRAM}}</td>
                                              <td>{{$kamus->KAMUS_HARGA}}</td>
                                              <td>{{$kamus->KAMUS_SKPD}}</td>
                                              <td>{{$kamus->KAMUS_OPERASIONAL}}</td>
                                              <td>{{$kamus->KAMUS_KETERANGAN}}</td>
                                          </tr> 
                                          </tr>
                                          @endforeach
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>


                </div>


              </div>
            </div>

          </div>





      </div>
    </div>
    <!-- .col -->


    </div>
</div>
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
@endsection




@section('plugin')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>

<script>
    $(document).ready(function(){

        $("#usulan").change(function(e, params){
            var id  = $('#usulan').val();
            //$('#kamus').find('option').remove().end().append('<option>Kamus Usulan</option>');
            alert(id);
            console.log(id);
            $.ajax({
              type  : "get", 
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/forgab1/"+id,
              success : function (data) {
                $('#kamus').append(data['opt']);
                
              }
            });
        });

    });
</script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->


@endsection

