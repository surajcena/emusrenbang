@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">

  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Usulan</li>
                <li class="active"><i class="fa fa-angle-right"></i>Detail Usulan</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">

                <div class="panel bg-white">

                  <div class="wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">

                        Detail Usulan 
                     
                    </h5>
                    <a href="{{ url('/') }}/musrenbang/{{$tahun}}/usulan-{{ $usulan->USULAN_TUJUAN}}" class="pull-right"><li class="fa fa-reply"></li> Kembali</a>
                   
                  </div>

                  <div class="tab-content tab-content-alt-0 bg-dark-grey" id="tab_usulan">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">
                          <div class="wrapper-sm col-sm-8">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Deskripsi Usulan</h5>
                              </div>
                              <div class="panel-body wrapper-lg">
                                <table width="100%">
                                  <tr>
                                    <td> <b>Pengusul</b> </td>
                                    <td> : </td>
                                    <td>  Bpk/Ibu. {{ $usulan->user->rw->RW_KETUA }}, RW.{{ $usulan->user->rw->RW_NAMA }}, Kel.{{ $usulan->user->kelurahan->KEL_NAMA }}, KEC.{{ $usulan->user->kecamatan->KEC_NAMA }} </td>  
                                  </tr>
                                  <tr>
                                    <td> <b>Lokasi</b> </td>
                                    <td> : </td>
                                    <td>  @if($usulan->ALAMAT == '') -
                                          @else {{ $usulan->ALAMAT }} 
                                          @endif </td>  
                                  </tr>
                                  <tr>
                                    <td> <b>Isu</b> </td>
                                    <td> : </td>
                                    <td>  {{ $usulan->kamus->isu->ISU_NAMA }} </td>  
                                  </tr>
                                  <tr>
                                    <td> <b>Kamus Usulan</b> </td>
                                    <td> : </td>
                                    <td>  {{ $usulan->kamus->KAMUS_NAMA }} </td>  
                                  </tr>
                                  <tr>
                                    <td> <b>Kriteria </b> </td>
                                    <td> : </td>
                                    <td>  {{ $usulan->kamus->KAMUS_KRITERIA }} </td>  
                                  </tr>
                                  <tr>
                                    <td> <b>Volume</b> </td>
                                    <td> : </td>
                                    <td>  @if($usulan->USULAN_VOLUME == '') -
                                          @else 
                                            {{ $usulan->USULAN_VOLUME }} {{ $usulan->kamus->KAMUS_SATUAN }},
                                            {{ $usulan->USULAN_VOLUME1 }} {{ $usulan->kamus->KAMUS_SATUAN }}, 
                                            {{ $usulan->USULAN_VOLUME2 }} {{ $usulan->kamus->KAMUS_SATUAN }}
                                          @endif 
                                          </td>  
                                  </tr>
                                  <tr>
                                    <td> <b>Penerima RT</b> </td>
                                    <td> : </td>
                                    <td> 
                                      @if($daftar_rt != '-')
                                        {{ $daftar_rt->implode('RT_NAMA', ', ') }} 
                                      @else
                                        -
                                      @endif
                                    </td>  
                                  </tr>
                                  <tr>
                                    <td> <b>Keterangan</b> </td>
                                    <td> : </td>
                                    <td>  {{ $usulan->USULAN_URGENSI }} </td>  
                                  </tr>
                                  <tr><td><br></td></tr>
                                  <tr><td colspan="3"><b>Status Usulan di 
                                    @if($usulan->USULAN_STATUS == 0 )
                                      <i class="fa fa-close text-danger"></i> Tolak
                                    @else
                                      <i class="fa fa-check text-success"></i> Terima  
                                    @endif 

                                  </b></td></tr>
                                  <tr>
                                    <td> 
                                      @if($usulan->USULAN_POSISI > 3 )
                                      <b>SKPD</b> 
                                      @else
                                      <b>Tahap</b> 
                                      @endif
                                    </td>
                                    <td> : </td>
                                    <td>
                                      @if($usulan->USULAN_POSISI == 1 )
                                       Kelurahan
                                      @elseif($usulan->USULAN_POSISI == 2 )
                                      Kecamatan
                                      @elseif($usulan->USULAN_POSISI == 3 )
                                      Kota
                                      @else  
                                      {{$usulan->kamus->skpd->SKPD_NAMA}}
                                      @endif </td>  
                                  </tr>
                                  
                                  <tr>
                                    <td> <b>Alasan</b> </td>
                                    <td> : </td>
                                    <td>  <i>{{ $usulan->USULAN_ALASAN }} </i> </td>  
                                  </tr>
                                </table>
                                
                             
                              </div>
                            </div>
                          </div>
                          <div class="wrapper-sm col-sm-4">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Status</h5>
                              </div>
                              <div class="wrapper-lg">
                                <div class="streamline b-l m-b b-success">
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      <div class="text-muted">-</div>
                                      @if($usulan->USULAN_POSISI == 1 && $usulan->USULAN_STATUS == 2)
                                        <p class="text-grey">
                                          <i class="fa fa-check text-success"></i> Kelurahan <br>
                                          <i class="fa fa-refresh text-info"></i> Kecamatan
                                        </p>
                                      @elseif($usulan->USULAN_POSISI == 1 && $usulan->USULAN_STATUS == 0)
                                        <p class="text-grey"><i class="fa fa-close text-danger"></i> Kelurahan </p>
                                      @elseif($usulan->USULAN_POSISI == 1 && $usulan->USULAN_STATUS == 1)
                                        <p class="text-grey"><i class="fa fa-refresh text-info"></i> Kelurahan </p>
                                      @else
                                        <p class="text-grey"><i class="fa fa-check text-success"></i> Kelurahan </p>
                                      @endif
                                    </div>
                                  </div>
                                  @if($usulan->USULAN_POSISI < 2)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($usulan->USULAN_POSISI == 2 && $usulan->USULAN_STATUS == 2)
                                      <p class="text-grey">
                                        <i class="fa fa-check text-success"></i> Kecamatan <br>
                                        <i class="fa fa-refresh text-info"></i> Kota
                                      </p>
                                      @elseif($usulan->USULAN_POSISI <= 2 && $usulan->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kecamatan </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kecamatan 
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                  @if($usulan->USULAN_POSISI < 3)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($usulan->USULAN_POSISI == 3 && $usulan->USULAN_STATUS == 2)
                                      <p class="text-grey">
                                        <i class="fa fa-check text-success"></i> Kota  <br>
                                        <i class="fa fa-refresh text-info"></i> APBD
                                      </p>
                                      @elseif($usulan->USULAN_POSISI <= 3 && $usulan->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kota </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kota </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                  @if($usulan->USULAN_POSISI < 4)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($usulan->USULAN_POSISI == 4 && $usulan->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> APBD </p>
                                      @elseif($usulan->USULAN_POSISI <= 4 && $usulan->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> APBD </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> APBD </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Lokasi : {{ $usulan->ALAMAT }}</h5>
                              </div>
                              <div class="wrapper-lg" id="map" style="height: 300px;">
                              </div>
                            </div>
                          </div>
                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n">Foto</h5>
                              </div>
                              <div class="wrapper-lg">
                                 <div class='list-group gallery' id="gallery-image" >
                                  @foreach($usulan->USULAN_GAMBAR as $img)
                                    <a class="thumbnail fancybox" rel="ligthbox" href="{{ url('/') }}/uploads/{{ $img }}">
                                        <img class="img-responsive" alt="" src="{{ url('/') }}/uploads/{{ $img }}"  style="height: : 240px;" />
                                        <div class='text-right'>
                                            <small class='text-muted'>{{ $usulan->kamus->isu->ISU_NAMA }}</small>
                                        </div> <!-- text-right / end -->
                                    </a>
                                  @endforeach  
                               </div> <!-- list-group / end -->
                                
                                
                              </div>
                            </div>
                          </div>
                   
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->
    </div>
    <!-- .col -->
    </div>
</div>

<div id="komentar" class="modal fade" role="dialog">
<form method="post" action="">
{{ csrf_field() }}
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">KETERANGAN TERIMA / TOLAK</h4>
      </div>
      <div class="modal-body">
      <div class="form-group">
          <label>Alasan : </label>
          <textarea class="form-control" rows="6" placeholder="Isi Alasan" name="alasan" required>
              @if($usulan->USULAN_POSISI == 2 && $usulan->USULAN_STATUS == 1 )
                 {{$usulan->USULAN_CATATANKEL}} 
              @elseif ($usulan->USULAN_POSISI == 2 && $usulan->USULAN_STATUS == 2)
                {{$usulan->USULAN_CATATANKEC}}
              @elseif ($usulan->USULAN_POSISI == 3 && $usulan->USULAN_STATUS == 2)
                {{$usulan->USULAN_CATATANSKPD}}
              @elseif ($usulan->USULAN_POSISI > 3)
                {{$usulan->USULAN_CATATANBAPPEDA}}
              @endif
          </textarea>
      </div>
      </div>
    </div>
  </div>
</form>
</div>


<style type="text/css">
  .gallery
{
    display: inline-block;
    margin-top: 20px;
    width: 300px;
}
</style>

@endsection


@section('plugin')
    <script>
      function initMap() {

        
        var uluru = {lat: {{ $usulan->USULAN_LAT }}, lng: {{ $usulan->USULAN_LONG }}};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
        

        var contentString = '{{ $usulan->kamus->KAMUS_NAMA }}<br>Lokasi: {{$usulan->ALAMAT}}';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        marker.addListener('click', function() {
          infowindow.open(map, marker);
          geocodeLatLng(geocoder, map, infowindow);
        });

      }

    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgP7Og3t7CZhdJdxMQjv3d5HKHuqWj0fc&callback=initMap">
    </script>


<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->


<script type="text/javascript">
  $(document).ready(function(){
    //FANCYBOX
    //https://github.com/fancyapps/fancyBox
    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });
});
</script>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

@endsection
