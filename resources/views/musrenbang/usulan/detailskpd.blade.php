@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Detail SKPD</li>                                
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <div class="col-sm-2 pull-right m-t-n-sm">
                      <a href="{{ url('/') }}/musrenbang/{{$tahun}}/usulan/skpd/export/{{$id}}"><button class="btn btn-danger">Export</button></a>
                    </div>
                    <div class="col-sm-2 pull-right m-t-n-sm">
                      <select class="form-control dtSelect" id="dtSelect">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                    <h5 class="font-semibold text-orange m-n ">Detail SKPD {{$skpd->SKPD_NAMA}}</h5>
                  </div>           
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                             <table id="table-usulan" ui-jq="dataTable" ui-options="{
                                    sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/skpd/data/usulan/{{$id}}',
                                    aoColumns: [
                                    { mData: 'usulanid',class:'hide' },
                                    { mData: 'no',class:'text-center' },
                                    { mData: 'idpengusul' },
                                    { mData: 'namapengusul' },
                                    { mData: 'isukamus' },
                                    { mData: 'volume' },
                                    { mData: 'harga' },
                                    { mData: 'total' },
                                    { mData: 'status' },
                                    { mData: 'prioritas' },
                                    { mData: 'keterangan' },
                                    { mData: 'skpd' },
                                    ]}" class="table table-usulan table-striped b-t b-b">
                              
                                    <thead>
                                      <tr>
                                        <th class="hide">#</th>
                                        <th>No</th>
                                        <th>Id Pengusul</th>
                                        <th>Nama Pengusul</th>
                                        <th>Isu / Kamus</th>
                                        <th>Volume</th>
                                        <th>Harga Satuan</th>
                                        <th>Total</th>
                                        <th>Status Approval</th>
                                        <th>Prioritas</th>
                                        <th>Alasan / Catatan</th>
                                        <th>SKPD</th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        <th colspan="11" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                              </table>
                              </div>
                          </div>
                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>

@endsection

@section('plugin')
<script type="text/javascript">
$(document).ready(function(){
  
  
});
</script>
@endsection


