@extends('musrenbang.layout')

@section('content')

<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                 <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Kamus Usulan</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">

                <div class="panel bg-white">

                  <div class="panel-heading wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n "></h5>
                  </div>

                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div class="bg-white wrapper-lg input-jurnal">
                            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                              <input type="hidden" name="usulan_id" value="{{$kamus->KAMUS_ID}}">
                              <div class="input-wrapper"> 
                                  <div class="form-group">
                                    <label class="col-sm-2">Isu Usulan</label>
                                    <div class="col-sm-10">
                                      <input type="text" class="form-control" name="isu" value="{{ $isu->ISU_NAMA }}">
                                      <input type="hidden" class="form-control" id="isu" name="isu" readonly="" value="{{ $isu->ISU_ID }}">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2">Kamus Usulan</label>
                                    <div class="col-sm-10">
                                      <input type="text" class="form-control" name="kamus" value="{{ $kamus->KAMUS_NAMA }}">
                                    </div>
                                  </div>
                                 
                                  <div class="form-group">
                                    <label class="col-sm-2">Harga</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" name="vol" value="{{$kamus->KAMUS_HARGA}}" placeholder="">
                                    </div>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control" name="satuan" value="{{ $kamus->KAMUS_SATUAN }}" placeholder="Satuan">
                                    </div>
                                  </div>

                                  <!--  <div class="form-group">
                                    <label class="col-sm-3">
                                        <button class="btn btn-success" data-toggle="modal" data-target="#form-tambah-kriteria">Tambah Kriteria Kamus</button>
                                    </label>
                                  </div> -->

                                   <div class="tab-content tab-content-alt-1 bg-white" id="example">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">
                            <div class="table-responsive dataTables_wrapper">
                              <table ui-jq="dataTable" ui-options="{
                                     sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/kamus/kriteria/{{$kamus->KAMUS_ID}}',
                                     aoColumns: [
                                     { mData: 'KAMUS_ID',class:'hide' },
                                     { mData: 'NO',class:'text-center' },
                                     { mData: 'KRITERIA' },
                                     { mData: 'BOBOT' },
                                     { mData: 'PERTANYAAN' },
                                     { mData: 'AKSI' },
                                     ]}" class="table table-usulan table-striped b-t b-b" id="table-index">
                                    <thead>
                                      <tr>
                                        <th class="hide">#</th>
                                        <th>#</th>
                                        <th>Kriteria</th>
                                        <th>Standar Bobot</th>
                                        <th>Pertanyaan</th>
                                        <th>Opsi</th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        <th colspan="6" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                  </div>

                                  <div class="form-group">
                                    <div class="col-md-12">
                                      <div class="col-sm-12" style="margin-top: -25px;">
                                          <button type="submit" class="btn btn-info pull-right" data-dismiss="modal" value="edit" >Simpan</button>  
                                       
                                      </div>
                                    </div>
                                  </div>
                              </div>
                              <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            </form>
                          </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
</div>

<div class="modal fade" id="modal-kamus" tabindex="-1" role="dialog">
  <div class="modal-dialog bg-white">
    <div class="panel panel-default">
      <div class="wrapper-lg">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">KRITERIA KAMUS USULAN </h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label>Nama Kriteria : </label>
            <input type="text" name="kriteria_nama" id="kriteria_nama" value="" class="form-control">
            <label>Standar Bobot : </label>
            <input type="number" name="standar_bobot" id="standar_bobot" value="" class="form-control">
            <label>Pertanyaan Kriteria : </label>
            <input type="text" name="pertanyaan" id="pertanyaan" value="" class="form-control">
            <input type="hidden" id="kriteria_id" class="form-control">
        </div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-info" onclick="return simpankamus()">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="form-tambah-kriteria" tabindex="-1" role="dialog">
  <div class="modal-dialog bg-white">
    <div class="panel panel-default">
      <div class="wrapper-lg">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">TAMBAH KRITERIA KAMUS USULAN </h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label>Nama Kriteria : </label>
            <input type="text" name="kriteria_nama" value="" class="form-control">
            <label>Standar Bobot : </label>
            <input type="number" name="standar_bobot" value="" class="form-control">
            <label>Pertanyaan Kriteria : </label>
            <input type="text" name="pertanyaan" value="" class="form-control">
            <input type="hidden" id="kriteria_id" class="form-control">
        </div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-success" onclick="return simpanKriteriaBaru()">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
      </div>
    </div>
  </div>
</div>
@endsection


@section('plugin')

<script type="text/javascript">
function setkriteria(id){
   // alert(id);
    $('#kriteria_id').val(id);
    $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{$tahun}}/usulan/kamusUsulanSingle/get/kriteria/"+id,
        success : function (data) {
          $('#kriteria_nama').val(data['KRITERIA']);
          $('#standar_bobot').val(data['STANDAR_BOBOT']);
          $('#pertanyaan').val(data['PERTANYAAN']);
        }
    });
    $('#modal-kamus').modal('show');    
  }
  function simpankamus(){
    var token            = $('#token').val();    
    var id               = $('#kriteria_id').val();    
    var kriteria         = $('#kriteria_nama').val();    
    var standar_bobot    = $('#standar_bobot').val();    
    var pertanyaan       = $('#pertanyaan').val();    
    $.ajax({
      url: "{{ url('/') }}/musrenbang/{{$tahun}}/usulan/kamusUsulanSingle/SET/kriteria",
      type: "POST",
      data: {'_token'         : token,
             'KRITERIA'       : kriteria,
             'STANDAR_BOBOT'  : standar_bobot,
             'PERTANYAAN'     : pertanyaan,
             'KRITERIA_ID'    : id},
      success: function(msg){
        $('#table-index').DataTable().ajax.reload();
        $('#modal-kamus').modal('hide');                          
        $.alert(msg);
      }
    });  
  }

  function simpanKriteriaBaru(){
    var token            = $('#token').val();    
    var id               = {{$kamus->KAMUS_ID}};    
    var kriteria         = $('#kriteria_nama').val();    
    var standar_bobot    = $('#standar_bobot').val();    
    var pertanyaan       = $('#pertanyaan').val();    
    $.ajax({
      url: "{{ url('/') }}/musrenbang/{{$tahun}}/usulan/kamusUsulanSingle/simpan/kriteria",
      type: "POST",
      data: {'_token'         : token,
             'KRITERIA'       : kriteria,
             'STANDAR_BOBOT'  : standar_bobot,
             'PERTANYAAN'     : pertanyaan,
             'KAMUS_ID'       : id},
      success: function(msg){
        $('#table-index').DataTable().ajax.reload();
        $('#form-tambah-kriteria').modal('hide');                          
        $.alert(msg);
      }
    });  
  }

  function simpanEditKamus(){
    var token            = $('#token').val();    
    var id               = {{$kamus->KAMUS_ID}};    
    var kriteria         = $('#kriteria_nama').val();    
    var standar_bobot    = $('#standar_bobot').val();    
    var pertanyaan       = $('#pertanyaan').val();    
    $.ajax({
      url: "{{url('/')}}/musrenbang/{{ $tahun }}/usulan/edit/submit",
      type: "POST",
      data: {'_token'         : token,
             'KRITERIA'       : kriteria,
             'STANDAR_
             BOBOT'  : standar_bobot,
             'PERTANYAAN'     : pertanyaan,
             'KAMUS_ID'       : id},
      success: function(msg){
        $('#table-index').DataTable().ajax.reload();
        $('#form-tambah-kriteria').modal('hide');                          
        $.alert(msg);
      }
    });  
  }
</script>


    <script>
    $(document).ready(function(){
        $("#upload").click(function(){
            $("#div1").fadeIn("fast");
        });

        $("#upload1").click(function(){
            $("#div2").fadeIn("fast");
        });

        var id  = $('#isu').val();
        $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/kamus/"+id,
              success : function (data) {
                $('#kamus').append(data['opt']).trigger('chosen:updated');
                if(data['tipe'] == 1) $('#div-upload-gambar').fadeIn('fast').removeClass('hide');
                else $('#div-upload-gambar').fadeOut('fast').addClass('hide');
              }
        });

    });
    </script>


    <!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

@endsection
