@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">
        <div class="bg-light lter">    
          <ul class="breadcrumb bg-white m-b-none">
            <li>
              <a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                <i class="icon-bdg_expand1 text"></i>
                <i class="icon-bdg_expand2 text-active"></i>
              </a>   
            </li>
            <li>
              <a href= "{{ url('/') }}/musrenbang/{{$tahun}}/public">Beranda</a>
            </li>
          </ul>
        </div>
        <div class="wrapper-lg padder-bottom-none" style="height:550px;">
          <div class="row">
              <div class="col-md-12">

            <!-- grafik admin bappeda -->

            @if(Auth::user()->level == 9)
             <div class="panel bg-white hvr-underline-from-left col-md-12">
              <h4>Monitoring PIPPK Pengusul RW</h4>
              <div id="chartdiv"></div>
            </div> 

            <div class="panel bg-white hvr-underline-from-left col-md-12">
              <h4>Monitoring PIPPK Pengusul LPM</h4>
              <div id="chartdiv1"></div>
            </div> 

             <div class="panel bg-white hvr-underline-from-left col-md-12">
              <h4>Monitoring PIPPK Pengusul PKK</h4>
              <div id="chartdiv2"></div>
            </div>

             <div class="panel bg-white hvr-underline-from-left col-md-12">
              <h4>Monitoring PIPPK Pengusul KARTA</h4>
              <div id="chartdiv3"></div>
            </div> 
            @endif

            </div>
             
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection

@section('plugin')
<script type="text/javascript">
  $( document ).ready(function() {
    window.setInterval(function(){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/getOnline",
        success : function (data) {
          $('#user-online').text(data['o']);
          $('#pippk').text(data['pippk']);
          $('#renja').text(data['renja']);
        }
      });
    }, 5000);
  });
</script>



<!-- Styles -->
<style>
#chartdiv {
  width: 100%;
  height: 500px;
} 

#chartdiv1 {
  width: 100%;
  height: 500px;
}  

#chartdiv2 {
  width: 100%;
  height: 500px;
} 

#chartdiv3 {
  width: 100%;
  height: 500px;
}                   
</style>

<!-- Resources -->
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

<!-- Chart code -->
<script>
var chart = AmCharts.makeChart("chartdiv", {
    "theme": "light",
    "type": "serial",
  "startDuration": 2,
    "dataProvider": [{
        "country": "Proses",
        "visits": {{$prosesRw}},
        "harga": 1500,
        "color": "#0D8ECF"
    }, {
        "country": "Terima",
        "visits": {{$terimaRw}},
        "harga": 1500,
        "color": "#04D215"
    }, {
        "country": "Tolak",
        "visits": {{$tolakRw}},
        "harga": 1500,
        "color": "#FF0F00"
    }],
    "valueAxes": [{
        "position": "left",
        "title": "Jumlah usulan"
    }],
    "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b>",
        "fillColorsField": "color",
        "fillAlphas": 1,
        "lineAlpha": 0.1,
        "type": "column",
        "valueField": "visits"
    }],
    "depth3D": 20,
  "angle": 30,
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "country",
    "categoryAxis": {
        "gridPosition": "start",
        "labelRotation": 0
    },
    "export": {
      "enabled": true
     }

});
</script>


<!-- Chart code -->
<script>
var chart = AmCharts.makeChart("chartdiv1", {
    "theme": "light",
    "type": "serial",
  "startDuration": 2,
    "dataProvider": [{
        "country": "Proses",
        "visits": {{$prosesLpm}},
        "harga": 1500,
        "color": "#0D8ECF"
    }, {
        "country": "Terima",
        "visits": {{$terimaLpm}},
        "harga": 1500,
        "color": "#04D215"
    }, {
        "country": "Tolak",
        "visits": {{$tolakLpm}},
        "harga": 1500,
        "color": "#FF0F00"
    }],
    "valueAxes": [{
        "position": "left",
        "title": "Jumlah usulan"
    }],
    "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b>",
        "fillColorsField": "color",
        "fillAlphas": 1,
        "lineAlpha": 0.1,
        "type": "column",
        "valueField": "visits"
    }],
    "depth3D": 20,
  "angle": 30,
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "country",
    "categoryAxis": {
        "gridPosition": "start",
        "labelRotation": 0
    },
    "export": {
      "enabled": true
     }

});
</script>


<!-- Chart code -->
<script>
var chart = AmCharts.makeChart("chartdiv2", {
    "theme": "light",
    "type": "serial",
  "startDuration": 2,
    "dataProvider": [{
        "country": "Proses",
        "visits": {{$prosesPkk}},
        "harga": 1500,
        "color": "#0D8ECF"
    }, {
        "country": "Terima",
        "visits": {{$terimaPkk}},
        "harga": 1500,
        "color": "#04D215"
    }, {
        "country": "Tolak",
        "visits": {{$tolakPkk}},
        "harga": 1500,
        "color": "#FF0F00"
    }],
    "valueAxes": [{
        "position": "left",
        "title": "Jumlah usulan"
    }],
    "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b>",
        "fillColorsField": "color",
        "fillAlphas": 1,
        "lineAlpha": 0.1,
        "type": "column",
        "valueField": "visits"
    }],
    "depth3D": 20,
  "angle": 30,
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "country",
    "categoryAxis": {
        "gridPosition": "start",
        "labelRotation": 0
    },
    "export": {
      "enabled": true
     }

});
</script>


<!-- Chart code -->
<script>
var chart = AmCharts.makeChart("chartdiv3", {
    "theme": "light",
    "type": "serial",
  "startDuration": 2,
    "dataProvider": [{
        "country": "Proses",
        "visits": {{$prosesKarta}},
        "harga": 1500,
        "color": "#0D8ECF"
    }, {
        "country": "Terima",
        "visits": {{$terimaKarta}},
        "harga": 1500,
        "color": "#04D215"
    }, {
        "country": "Tolak",
        "visits": {{$tolakKarta}},
        "harga": 1500,
        "color": "#FF0F00"
    }],
    "valueAxes": [{
        "position": "left",
        "title": "Jumlah usulan"
    }],
    "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b>",
        "fillColorsField": "color",
        "fillAlphas": 1,
        "lineAlpha": 0.1,
        "type": "column",
        "valueField": "visits"
    }],
    "depth3D": 20,
  "angle": 30,
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "country",
    "categoryAxis": {
        "gridPosition": "start",
        "labelRotation": 0
    },
    "export": {
      "enabled": true
     }

});
</script>
@endsection


