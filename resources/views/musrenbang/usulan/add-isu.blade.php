@extends('musrenbang.layout')

@section('content')
<style media="screen">
#pac-input {
background-color: #fff;
margin-top: 10px;
font-family: Roboto;
font-size: 15px;
font-weight: 300;
margin-left: 12px;
padding: 0 11px 0 13px;
text-overflow: ellipsis;
width: 300px;
}
</style>
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i> 
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Usulan</li>
                <li class="active"><i class="fa fa-angle-right"></i>Tambah Isu Usulan</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">
                <div class="nav-tabs-alt tabs-alt-1 b-t three-row" id="tab-jurnal" >

                  <div class="panel-heading wrapper-lg">
                         <h5> Input Kamus Usulan </h5>
                  </div>       
                
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div class="bg-white wrapper-lg input-jurnal">
                          @if (count($errors) > 0)
                              <div class="alert alert-danger">
                                  <ul>
                                      @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                              </div>
                          @endif

                              <div class="input-wrapper">
                                <form action="{{url('/')}}/musrenbang/{{$tahun}}/pengaturan/kamus/tambahKamusSubmit" method="post" class="form-horizontal" enctype="multipart/form-data">
                                  <div class="form-group">
                                    <label class="col-sm-2">Isu Usulan</label>
                                    <div class="col-sm-10">
                                      <select ui-jq="chosen" class="w-full isu form-control" name="isu" id="isu">
                                            <option value="" style="color:black">Pilih Isu</option>
                                        @foreach ($isu as $isu)
                                            <option value="{{$isu->ISU_ID}}" style="color:black">{{$isu->ISU_NAMA}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                  </div><br>
                                  <div class="form-group">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10"><br>
                                      <a href="" class="btn btn-info" onclick="return getProfil('{{$isu->ISU_ID}}')" data-toggle="modal" data-target="#form-tinjau">
                                        <i class="fa fa-check m-r-xs"></i>Input Kamus Usulan
                                      </a>
                                    </div>
                                  </div>
                                  <br>
                                  <hr class="m-t-xl">
                                </form>

                                  <form action="{{url('/')}}/musrenbang/{{$tahun}}/pengaturan/kamus/tambahKamusSubmit" method="post" class="form-horizontal" enctype="multipart/form-data">

                                  <div class="panel-heading wrapper-lg">
                                       <h5> Input Isu Usulan Baru</h5>
                                  </div>   
                                  <br>    

                                  <div class="form-group">
                                    <label class="col-sm-2">Tahun Usulan</label>
                                    <div class="col-sm-2">
                                      <input class="form-control" placeholder="" name="ISU_TAHUN" value="{{$tahun}}">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2">Tujuan Pengguna Isu </label>
                                    <div class="col-sm-6">
                                      <select ui-jq="chosen" class="w-full form-control" name="ISU_TUJUAN" required>
                                            <option value="" style="color:black">--Pilih Pengguna--</option>
                                            <option value="0" style="color:black">Renja RW Fisik</option>
                                            <option value="1" style="color:black">Renja RW Non Fisik </option>
                                            <option value="2" style="color:black">PIPPK RW</option>
                                            <option value="3" style="color:black">PIPPK LPM</option>
                                            <option value="5" style="color:black">PIPPK PKK</option>
                                            <option value="4" style="color:black">PIPPK KARTA</option>
                                      </select>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2">Isu Usulan</label>
                                    <div class="col-sm-10">
                                      <input class="form-control" type="text" placeholder="masukan isu usulan" name="ISU_NAMA" required>
                                    </div>
                                  </div>
                                
                                  <hr class="m-t-xl">
                                  <div class="form-group">
                                      
                                      <div class="col-md-4"></div>
                                      
                                      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                                    <div class="col-md-8">
                                      <a href="{{ url('/') }}/musrenbang/{{ $tahun }}/kamusUsulan" class="btn input-xl btn-danger pull-right"><i class="fa fa-close m-r-xs"></i>Batal</a>
                                      <button class="btn input-xl btn-success pull-right m-r-xs" type="submit"><i class="fa fa-check m-r-xs"></i>Simpan</button>
                                    </div>
                                  </div>

                              </div>
                            </form>
                          </div>
                        </div>
                </div>
              </div>
            </div>
          </div>

      </div>
    </div>
    </div>
</div>


<div id="form-tinjau" class="modal fade" role="dialog">
<form method="post" action="{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/simpan/profile/kec" id="formtinjau">
{{ csrf_field() }}
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Kamus Usulan</h4>
      </div>
      <div class="modal-body">
         <div class="form-group">
            <label class="col-sm-4">Tahun</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" name="KAMUS_TAHUN" required="" value="{{$tahun}}">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">ISU</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="ISU_NAMA" readonly>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">Nama Kamus</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="KAMUS_NAMA" required="" value="" placeholder="nama kamus">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">Harga Kamus</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" name="KAMUS_HARGA" required="" value="" placeholder="0">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">Satuan Kamus</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="KAMUS_SATUAN" required="" placeholder="ex. m2" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">Kriteria Kamus</label>
            <div class="col-sm-8">
              <textarea name="KAMUS_KRITERIA" class="form-control" placeholder="Kriteria Kamus Usulan"></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-4">Jenis Kamus</label>
            <div class="col-sm-8">
              <select class="form-control" name="KAMUS_JENIS"> 
                  <option value="FISIK">Renja Fisik RW</option>   
                  <option value="NON FISIK">Renja Non Fisik RW</option>   
                  <option value="PIPPK">PIPPK RW</option>   
                  <option value="LPM">LPM</option>   
                  <option value="PKK">PKK</option>   
                  <option value="KARTA">KARTA</option>   
              </select>  
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">Kunci</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" name="KAMUS_KUNCI" required="" value="0" length="5">
            </div>
          </div>
           <div class="form-group">
            <label class="col-sm-4">SKPD</label>
            <div class="col-sm-8">
              <select class="form-control" name="KAMUS_SKPD"> 
                  <option value="">SKPD</option>   
                  <option value=""></option>  
              </select> 
            </div>
          </div>
           <div class="form-group">
            <label class="col-sm-4">Kode Komponen</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="KAMUS_KODE_KOMPONEN" value="" length="8" placeholder="2.13.31.04.07.001.003">
            </div>
          </div>
           <div class="form-group">
            <label class="col-sm-4">Nama Komponen</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="KAMUS_NAMA_KOMPONEN" value="" placeholder="Nama Komponen">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">Urusan Kamus</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="KAMUS_URUSAN" placeholder="Urusan">
            </div>
          </div>
           <div class="form-group">
            <label class="col-sm-4">Program Kamus</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="KAMUS_PROGRAM" placeholder="Program">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">Kegiatan Kamus</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="KAMUS_KEGIATAN" placeholder="Kegiatan">
            </div>
          </div>
          <input type="hidden" class="form-control" name="ISU_ID" value="" >
      </div>
      <div class="modal-footer">
        
       
          <button type="submit" class="btn btn-info">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
       
      </div>
    </div>
  </div>
</form>
@endsection



@section('plugin')

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzixLw-QwjOpEiL8qmveEmafmgM27mgnk&libraries=places&callback=initMap">
</script>

<script type="text/javascript">
    function getProfil(id){   
      $.ajax({
          method: 'GET', // Type of response and matches what we said in the route
          url: "{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/referensi/getIsuUsulan/"+id,
           // a JSON object to send back
          success: function(response){ // What to do if we succeed
              console.log(response); 
              $("#formtinjau input[name='ISU_NAMA']").val(response.ISU_NAMA);

          },
          error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
              console.log(JSON.stringify(jqXHR));
              console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
          }
    }); 
    
  }
</script>



<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

@endsection
