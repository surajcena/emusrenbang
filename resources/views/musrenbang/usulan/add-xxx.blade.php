@extends('musrenbang.layout')

@section('content')
<style media="screen">
#pac-input {
background-color: #fff;
margin-top: 10px;
font-family: Roboto;
font-size: 15px;
font-weight: 300;
margin-left: 12px;
padding: 0 11px 0 13px;
text-overflow: ellipsis;
width: 300px;
}
</style>
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                   <i class="icon-bdg_expand1 text"></i> 
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Usulan</li>
                <li class="active"><i class="fa fa-angle-right"></i>Tambah Usulan @if($usulan_tujuan ==1) Renja  @elseif($usulan_tujuan ==2) PIPPK @endif</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">

                  <div class="panel-heading wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">Tambah Usulan @if($usulan_tujuan ==1) Renja  Kelurahan @elseif($usulan_tujuan ==2) PIPPK @endif</h5>
                  </div>

                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div class="bg-white wrapper-lg input-jurnal">
                          @if (count($errors) > 0)
                              <div class="alert alert-danger">
                                  <ul>
                                      @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                              </div>
                          @endif
                            <form action="{{url('/')}}/musrenbang/{{$tahun}}/usulan/tambah/submit" method="post" class="form-horizontal" enctype="multipart/form-data">
                              <div class="input-wrapper">

                                  <div class="form-group">
                                    <label class="col-sm-2">Isu Usulan</label>
                                    <div class="col-sm-10">
                                      <select ui-jq="chosen" class="w-full isu form-control" name="isu" id="isu" required>
                                            <option value="" style="color:black">Pilih Isu</option>
                                        @foreach ($isu as $isu)
                                            <option value="{{$isu->ISU_ID}}" style="color:black">{{$isu->ISU_NAMA}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2">Kamus Usulan </label>
                                    <div class="col-sm-10">
                                      <select ui-jq="chosen" class="w-full form-control" name="kamus" id="kamus" required>
                                            <option value="" style="color:black">Pilih Kamus</option>
                                      </select>
                                    </div>
                                  </div>
                                  
                                  <div class="form-group" id="div-upload-gambar" style="display: none">
                                    <label class="col-sm-2">Upload Gambar</label>
                                    <div class="col-sm-7">
                                      <p style="color:red">
                                      
                                      </p>
                                      <input ui-jq="filestyle" name="image[]" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-0" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);" accept=".jpg,.png,.jpeg">
                                    </div>
                                    <div class="col-sm-1">
                                      <button class="btn btn-info" id="upload" type="button"><i class="fa fa-plus"> Tambah Gambar</i></button>
                                    </div>
                                  </div>
                                  <div class="form-group" style="display:none" id="div1">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-9">
                                      <input ui-jq="filestyle" name="image[]" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-1" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);" accept=".jpg,.png,.jpeg">
                                    </div>
                                    <div class="col-sm-1">
                                    </div>
                                  </div>

                                  <input type="hidden" class="form-control" name="kec" readonly="" value="{{ $lokasi->kelurahan->kecamatan->KEC_NAMA }}">
                                      <input type="hidden" class="form-control" name="kel" readonly="" placeholder="Kelurahan" value="{{ $lokasi->kelurahan->KEL_NAMA }}">
                                      <input type="hidden" class="form-control" name="rw" readonly="" placeholder="RW" value="RW {{ $lokasi->RW_NAMA }}">
                                 

                                   <div class="form-group">
                                    <label class="col-sm-2">Masukan @if(Auth::user()->level == 5) RW @elseif(Auth::user()->level == 1) RT @endif Penerima Manfaat</label>
                                    <div class="col-sm-8" style="margin-top:10px">
                                      @foreach ($rt as $rt)
                                        <input type="checkbox" class="" value="{{$rt->RT_ID}}" style="margin-left:10px;" name="rt[]"> {{$rt->RT_NAMA}}
                                      @endforeach
                                      </div>
                                  </div>

                                   <div class="form-group">
                                    <label class="col-sm-2">Urgensi Kegiatan / Keterangan</label>
                                    <div class="col-sm-10">
                                       <textarea width="20" name="urgensi" class="form-control" placeholder="Masukan Alasan kenapa kegiatan ini diperlukan dan keterangan lainya."></textarea>
                                    </div>
                                  </div>

                                  <input type="hidden" value="{{$usulan_tujuan}}" name="usulan_tujuan">

                                  <hr class="m-t-xl">

                                  <div class="form-group">

                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                                    <div class="col-md-12">
                                      <a href="{{ url('/') }}/musrenbang/{{ $tahun }}/usulan-{{$usulan_tujuan}}" class="btn input-xl btn-info"><i class="fa fa-reply m-r-xs"></i>Batal</a>
                                      <button class="btn input-xl btn-success pull-right m-r-xs" type="submit"><i class="fa fa-check m-r-xs"></i>Simpan</button>
                                    </div>
                                  </div>
                              </div>
                            </form>
                          </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
</div>
@endsection





@section('plugin')

<script>
    $(document).ready(function(){

        $("#upload").click(function(){
            $("#div1").fadeIn("fast");
            $("#upload").addClass("addGambar");
        });

        $("#isu").change(function(e, params){
            var id  = $('#isu').val();
           $('#kamus').find('option').remove().end().append('<option>Kamus Usulan</option>');
            $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/kamus/"+id,
              success : function (data) {
                $('#kamus').append(data['opt']).trigger('chosen:updated');
                if(data['tipe'] == 1 || data['tipe'] == 2) $('#div-upload-gambar').fadeIn('fast').removeClass('hide');
                else $('#div-upload-gambar').fadeOut('fast').addClass('hide');
              }
            });
        });

        $("#kamus").change(function(e, params){
          // alert('kamus');
            var id  = $('#kamus').val();
            $('#satuan').val();
            $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/satuan/"+id,
              success : function (data) {
                $('#satuan').val(data);
              }
            });
        });
        $("#isu").change();
    });
</script>

@endsection
