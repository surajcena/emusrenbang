@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017/">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Detail Usulan Renja Per SKPD</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <div class="col-sm-2 pull-right m-t-n-sm">
                      <a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/rekapskpd/export/grafik"><button class="btn btn-danger">Export</button></a>
                    </div>
                    <div class="col-sm-2 pull-right m-t-n-sm">
                      <select class="form-control dtSelect" id="dtSelect">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                    <h5 class="font-semibold text-orange m-n ">Detail Usulan Renja Per SKPD</h5>
                  </div>           
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                             <table ui-jq="dataTable" ui-options="{
                                      sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/grafikRenja/detail/getRenja',
                                      aoColumns: [
                                       { mData: 'SKPDID',class:'hide' },
                                       { mData: 'NO' },
                                       { mData: 'KODESKPD' },
                                       { mData: 'NAMASKPD' },
                                       { mData: 'JUMLAHRENJA' },
                                       { mData: 'JUMLAHPENGUSUL' },
                                       { mData: 'TOTALNOMINAL' },
                                       { mData: 'PROSES' },
                                       { mData: 'TERIMA' },
                                       { mData: 'TOLAK' },
                                       { mData: 'AKSI' }
                                      ],
                                    }" class="table table-striped b-t b-b" id="table-skpd">
                                    <thead>
                                      <tr>
                                        <th class="hide"></th>
                                        <th>No</th>
                                        <th>Kode SKPD</th>
                                        <th>Nama Perangkat Daerah</th>
                                        <th>Total Usulan Renja</th>
                                        <th>Jumlah Pengusul</th>
                                        <th>Total Nominal</th>
                                        <th>Proses</th>
                                        <th>Terima</th>
                                        <th>Tolak</th>
                                        <th>Opsi</th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        <th colspan="11" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari Usulan" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                  </div>
                </div>                
              </div>
            </div>

          </div>





      </div>
      <!-- App-content-body -->

    </div>
    <!-- .col -->


    </div>
</div>

@endsection



@section('plugin')
<script type="text/javascript">
$(document).ready(function() {
    $(".buttonstatus").on("click",function(event) {

      status = $(this).data('status');
      
      resetTable();
    });
    var table = $('#example').DataTable();

    $("#example tfoot th").each( function ( i ) {
        var select = $('<select><option value=""></option></select>')
            .appendTo( $(this).empty() )
            .on( 'change', function () {
                table.column( i )
                    .search( $(this).val() )
                    .draw();
            } );

        table.column( i ).data().unique().sort().each( function ( d, j ) {
            select.append( '<option value="'+d+'">'+d+'</option>' )
        } );
    } );
} );
</script>

<script type="text/javascript">
  function resetTable(){
   // var kec  = $('#kecamatan').val();
   // var kel  = $('#kelurahan').val();
   
    $('#table-usulan').DataTable().destroy();
   
    $('#table-usulan').DataTable({
     sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/usulan/kamusUsulan/'+status,
     aoColumns: [
     { mData: 'KAMUS_ID',class:'hide' },
     { mData: 'NO',class:'text-center' },
     { mData: 'ISU_NAMA' },
     { mData: 'KAMUS_NAMA' },
     { mData: 'HARGA' },
     { mData: 'SATUAN' },
     { mData: 'KRITERIA'},
     { mData: 'SKPD' },
     { mData: 'AKSI' },
   ]});
  }
  function hapus(id){
    var token        = $('#token').val();
    $.confirm({
        title: 'Hapus Data!',
        content: 'Yakin hapus data?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                      url: "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/delete",
                      type: "POST",
                      data: {'_token'         : token,
                            'id'              : id},
                      success: function(msg){
                          $.alert(msg);
                          $('.table').DataTable().ajax.reload();
                        }
                  });
                }
            },
            Tidak: function () {
            }
        }
    });
  }
</script>


<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

@endsection
