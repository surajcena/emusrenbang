@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017/">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Daftar Usulan Tahun 2017</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">

                <div class="panel bg-white">

                  <div class="wrapper-lg">  
                  <h4>Daftar Usulan Renja Tahun 2017 yang sudah lolos di validasi oleh kecamatan </h4>                 

                   <div class="row">
                         
                      <div class="col-sm-2 pull-right m-t-n-sm">
                        <select class="form-control dtSelect" id="dtSelect">
                              <option value="10">10</option>
                              <option value="25">25</option>
                              <option value="50">50</option>
                              <option value="100">100</option>
                          </select>
                      </div>  
                      
                   </div> 
                   <br>

                  <div class="tab-content tab-content-alt-1 bg-white" id="example">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">
                            <div class="table-responsive dataTables_wrapper">
                              <table id="table-usulan" ui-jq="dataTable" ui-options="{
                                     sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/usulan/usulkankembali/getdata',
                                     aoColumns: [
                                     { mData: 'USULAN_ID',class:'hide' },
                                     { mData: 'NO',class:'text-center' },
                                     { mData: 'OPSI' },
                                     { mData: 'PENGUSUL' },
                                     { mData: 'ISUKAMUS' },
                                     { mData: 'VOLHAR' },
                                     { mData: 'TOTAL',class:'text-right' },
                                     { mData: 'STATUS' },
                                     { mData: 'ALAMAT' },
                                     ]}" class="table table-usulan table-striped b-t b-b">
                                    <thead>
                                      <tr>
                                        <th class="hide">#</th>
                                        <th>#</th>
                                        <th>OPSI</th>
                                        <th>Pengusul</th>
                                        <th>Isu / Kamus</th>
                                        <th>Volume / Harga</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>ALAMAT</th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        <th colspan="8" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                      <tfoot>
                                        <tr>
                                          <td colspan="3"> </td>
                                          <td colspan="3"><b>Total Nominal : Rp. <text id="totalnominal"></text></b></td>
                                        </tr>  
                                      </tfoot>
                                  </table>
                          		</div>
                        	</div>
                  </div>
                </div>


              </div>
            </div>

          </div>





      </div>
      <!-- App-content-body -->

    </div>
    <!-- .col -->


    </div>
</div>
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
@endsection



@section('plugin')
<script type="text/javascript">
$(document).ready(function() {
    $(".buttonstatus").on("click",function(event) {

      status = $(this).data('status');
      
      resetTable();
    });
    var table = $('#example').DataTable();

    $("#example tfoot th").each( function ( i ) {
        var select = $('<select><option value=""></option></select>')
            .appendTo( $(this).empty() )
            .on( 'change', function () {
                table.column( i )
                    .search( $(this).val() )
                    .draw();
            } );

        table.column( i ).data().unique().sort().each( function ( d, j ) {
            select.append( '<option value="'+d+'">'+d+'</option>' )
        } );
    } );
} );
</script>
<script type="text/javascript">
  function resetTable(){
   // var kec  = $('#kecamatan').val();
   // var kel  = $('#kelurahan').val();
   
    $('#table-usulan').DataTable().destroy();
    
    $('#table-usulan').DataTable({
     sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/usulan/data/'+status,
     aoColumns: [
     { mData: 'USULAN_ID',class:'hide' },
     { mData: 'NO',class:'text-center' },
     { mData: 'LOKASI' },
     { mData: 'ISU' },
     { mData: 'VOLUME' },
     { mData: 'HARGA' },
     { mData: 'ANGGARAN',class:'text-right' },
     { mData: 'STATUS' },
     { mData: 'AKSI' },
      ],
      initComplete:function(setting,json){

        $("#totalnominal").html(json.jumlah);


      }
    });
  }
  function hapus(id){
    var token        = $('#token').val();
    $.confirm({
        title: 'Hapus Data!',
        content: 'Yakin hapus data?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                      url: "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/delete",
                      type: "POST",
                      data: {'_token'         : token,
                            'id'              : id},
                      success: function(msg){
                          $.alert(msg);
                          $('.table').DataTable().ajax.reload();
                        }
                  });
                }
            },
            Tidak: function () {
            }
        }
    });
  }
</script>


<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

@endsection
