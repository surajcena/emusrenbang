@extends('musrenbang.layout')
@section('css')

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <style media="screen">
      .table-responsive{background-color: #fff}
    </style>
@stop
@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Daftar Kamus Usulan</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <!-- baris satu -->
              <div class="col-sm-12">
                <div class="panel bg-white wrapper-lg" style="height: 165px;">
                  <div class="form-group col-sm-12">
                      <label class="col-sm-2">
                              SKPD
                      </label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="filter w-full form-control" name="skpd" id="skpd">                                      
                          <option value="x"> SKPD </option>
                          @foreach($skpd as $skpd)
                          <option value="{{$skpd->SKPD_ID}}" style="color:black"> {{$skpd->SKPD_NAMA}} </option>
                          @endforeach
                        </select>
                      </div>
                      <label class="col-sm-2">Kamus</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="filter w-full form-control" name="kamus" id="kamus">                        
                          <option value="x" style="color:black">Kamus</option>
                          @foreach($kamus as $kamus)
                          <option value="{{$kamus->KAMUS_ID}}" style="color:black"> {{$kamus->KAMUS_NAMA}} </option>
                          @endforeach
                        </select>
                      </div>
                  </div>                  
                  <div class="form-group col-sm-12">
                      <label class="col-sm-2">Isu</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="filter w-full form-control" name="isu" id="isu">                                      
                          <option value="x" style="color:black">Isu</option>                          @foreach($isu as $isu)
                          <option value="{{$isu->ISU_ID}}" style="color:black"> {{$isu->ISU_NAMA}} </option>
                          @endforeach
                        </select>
                      </div>
                      <label class="col-sm-2">Tipe</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="filter w-full form-control" name="tipe" id="tipe">                                      
                          <option value="1" style="color:black">Renja</option>
                          <option value="2" style="color:black">PIPPK</option>
                        </select>
                      </div>
                  </div>                 
                </div>  
              </div>
              <!-- baris dua  -->
              <!-- <div class="col-md-12">
                <div class="panel bg-white wrapper-lg" style="height: 65px;">
                  <div class="form-group col-sm-12">
                    <button class="btn btn-info" data-toggle="modal" data-target="#form-usulkan-kamus" title= "Usulankan Kamus Ke SKPD" style="margin-top: -15px">Usulkan Kamus</button>
                    <a href="{{ url('/') }}/musrenbang/{{$tahun}}/usulan/kamus/download">
                    <button class="btn btn-success" title= "Usulankan Kamus Ke SKPD" style="margin-top: -15px">Print Kamus Usulan</button></a>
                  </div>
                </div>    
              </div>  -->     
              <!-- baris tiga -->
              <div class="col-md-12">
                <div class="panel bg-white">
                   <div class="wrapper-lg">
                    <div class="row">
                      <div class="col-sm-10">
                       <div class="nav-tabs-alt tabs-alt-1 b-t three-row" id="tab-jurnal" >
                          <ul class="nav nav-tabs" role="tablist">
                             <li class="active">
                              <a data-target="#tab-1" role="tab" data-toggle="tab" data-status="1" class="buttonstatus"> Tersedia <i class="fa fa-check text-success"></i></a>
                             </li>
                             <li>
                              <a data-target="#tab-2" role="tab" data-toggle="tab" data-status="2" class="buttonstatus"> Di Usulkan <i class="fa fa-refresh text-info"></i></a>
                             </li>
                          </ul>
                        </div> 
                      </div>   
                      <div class="col-sm-2 pull-right m-t-n-sm">
                        <select class="form-control dtSelect" id="dtSelect" style="margin-top:57px">
                              <option value="10">10</option>
                              <option value="25">25</option>
                              <option value="50">50</option>
                              <option value="100">100</option>
                          </select>
                      </div>  
                      
                   </div> 

                  </div>  

                  <div class="tab-content tab-content-alt-1 bg-white" id="example">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">
                            <div class="table-responsive dataTables_wrapper">
                              <table id="table-usulan" ui-jq="dataTable" ui-options="{
                                     sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/usulan/kamusUsulan/1/x/x/x/x',
                                     aoColumns: [
                                     { mData: 'KAMUS_ID',class:'hide' },
                                     { mData: 'NO',class:'text-center' },
                                     { mData: 'SKPD' },
                                     { mData: 'ISU_NAMA' },
                                     { mData: 'KAMUS_NAMA' },
                                     { mData: 'HARGA' },
                                     { mData: 'SATUAN' },
                                     { mData: 'KRITERIA'},
                                     { mData: 'AKSI' },
                                     ]}" class="table table-usulan table-striped b-t b-b" id="table-kamus">
                                    <thead>
                                      <tr>
                                        <th class="hide">#</th>
                                        <th>#</th>
                                        <th>SKPD</th>
                                        <th>ISU</th>
                                        <th>Kamus</th>
                                        <th>Harga</th>
                                        <th>Satuan</th>
                                        <th>Kriteria</th>
                                        <th>Opsi</th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        <th colspan="8" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                  </div>


                </div>
              </div>


            </div>
          </div>





      </div>
      <!-- App-content-body -->

    </div>
    <!-- .col -->


    </div>


<div class="overlay"></div>
  <div class="bg-white wrapper-lg input-sidebar input-kamus">
  <a href="#" class="tutup-form"><i class="icon-bdg_cross"></i></a>
      <form id="form-urusan" class="form-horizontal">
        <div class="input-wrapper">
          <h5 id="judul-form">Ubah Kamus Usulan</h5>
            <div class="form-group">
              <label for="kode_urusan" class="col-md-3">Kamus Usulan</label>          
              <div class="col-sm-9">
                <input type="text" class="form-control" id="id_kamus"  readonly="">
                <input type="hidden" class="form-control" value="{{ csrf_token() }}" name="_token" id="token">          
                <input type="hidden" class="form-control" name="id_kamus" id="id_kamus">          
              </div> 
            </div>

            <hr class="m-t-xl">
            <a class="btn input-xl m-t-md btn-success pull-right" onclick="return simpanUsulan()"><i class="fa fa-plus m-r-xs "></i>Simpan</a>
        </div>
      </form>
    </div>


</div>

<div class="modal fade" id="form-usulkan-kamus" tabindex="-1" role="dialog">
  <div class="modal-dialog bg-white">
    <div class="panel panel-default">
      
      <div class="wrapper-lg">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">FORM USULAN KAMUS  </h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label>Pilih Isu : </label>
            <select name="input_isu" id="input_isu" class="form-control">
              <option value=""></option>
             
            </select>
            <label>Nama Kamus : </label>
            <input type="text" name="input_kamus" id="input_kamus" value="" class="form-control">
            <label>Alasan Mengusulkan Kamus : </label>
            <textarea class="form-control m-t-sm" placeholder="Deskripsikan alasan mengusulan kamus" id="input_keterangan" name="input_keterangan"></textarea>
            <input type="hidden" id="account_id" class="form-control">
            <input name="_method" type="hidden" value="PATCH" id="method">
        </div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-success" onclick="return simpanKamusBaru()">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
      </div>
    
    </div>
  </div>
</div>
@endsection


@section('plugin')
<script type="text/javascript">
var status = 1;

$(document).ready(function(){
  $(".buttonstatus").on("click",function(event) {
    status = $(this).data('status');
    resetTable();
  });

  $(".filter").on('change',function(e){
    resetTable();
  });

 $("#skpd").change(function(e, params){
    id  = $('#skpd').val();
     $('#isu').find('option').remove().end().append('<option value="x">-isu-</option>');
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{$tahun}}/usulan/kamus/get/isu/"+id,
        success : function (data) {
          $('#isu').append(data['opt']).trigger('chosen:updated');
        }
      });
  });

 $("#isu").change(function(e, params){
    id  = $('#isu').val();
     $('#kamus').find('option').remove().end().append('<option value="x">-kamus-</option>');
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{$tahun}}/usulan/kamus/get/kamus/kamus/"+id,
        success : function (data) {
          $('#kamus').append(data['opt']).trigger('chosen:updated');
        }
      });
  });
  

  function resetTable(){
    var skpd = $('#skpd').val();
    var isu = $('#isu').val();
    var kamus        = $('#kamus').val();
    var tipe      = $('#tipe').val();
    $('#table-kamus').DataTable().destroy();
    
    $('#table-kamus').DataTable({
          sAjaxSource: "{{ url('/') }}/musrenbang/{{$tahun}}/usulan/kamusUsulan/"+status+"/"+skpd+"/"+isu+"/"+kamus+"/"+tipe,
           aoColumns: [
            { mData: 'KAMUS_ID',class:'hide' },
             { mData: 'NO',class:'text-center' },
             { mData: 'SKPD' },
             { mData: 'ISU_NAMA' },
             { mData: 'KAMUS_NAMA' },
             { mData: 'HARGA' },
             { mData: 'SATUAN' },
             { mData: 'KRITERIA'},
             { mData: 'AKSI' },
          ],
          "order": [[10, "asc"]],

        initComplete:function(setting,json){
            $("#totalnominal").html(json.jumlah);
            $("#diproses").html(json.diproses);
            $("#diterima").html(json.diterima);
            $("#ditolak").html(json.ditolak);
        }
    });
  }
});

function simpanKamusBaru(){
    var token               = $('#token').val();    
    var method              = $('#method').val();    
    var input_isu           = $('#input_isu').val();   
    var input_kamus         = $('#input_kamus').val();
    var input_keterangan    = $('#input_keterangan').val(); 
    $.ajax({
      url: "{{ url('/') }}/musrenbang/{{$tahun}}/kamus/simpan/kamusBaru",
      type: "POST",
      data: {'_token'           : token,
             'method'           : method,
             'ISU_ID'           : input_isu,
             'KAMUS_NAMA'       : input_kamus,
             'KAMUS_KETERANGAN' : input_keterangan},
      success: function(msg){
        $('#form-usulkan-kamus').modal('hide');                          
        $.alert(msg);
      }
    });  
  }

  
</script>

<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
@endsection
