@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">

  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

        <div class="bg-light lter">    
          <ul class="breadcrumb bg-white m-b-none">
            <li>
              <a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
               <!--  <i class="icon-bdg_expand2 text"></i> -->
                <i class="icon-bdg_expand1 text-active"></i>
              </a>   
            </li>
            <li class="active"><i class="fa fa-angle-right"></i>Dashboard</li>                
          </ul>
        </div>

 

        <div class="wrapper-xl padder-bottom-none" style="height:600px; ">
@if(Auth::user()->app == 1 && (Auth::user()->level == 1 ))
              <div class="row">
                <div class="col-md-6">
                  <div class="panel panel-default">
                    <a href="{{ url('/') }}/musrenbang/2017/usulan/1">
                      <div class="panel-body wrapper-sm">
                        <h2 class="m-t-xs text-orange font-semibold m-b-sm">Musrenbang</h2>
                        <p>Daftar Usulan Musrenbang</p>
                      </div>
                    </a>
                  </div>
                </div>

                 <div class="col-md-6">
                  <div class="panel panel-default">
                    <a href="{{ url('/') }}/musrenbang/2017/usulan/2">
                      <div class="panel-body wrapper-sm">
                        <h2 class="m-t-xs text-orange font-semibold m-b-sm">PIPPK</h2>
                        <p>Daftar Usulan PIPPK</p>
                      </div>
                    </a>  
                  </div>
                </div>
        </div> 
        @endif   
  

        
      </div>
      </div>
    </div>
  </div>
</div>
@endsection


@section('plugin')

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

@endsection

