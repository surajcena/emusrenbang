@extends('musrenbang.layout')
@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                   <i class="icon-bdg_expand1 text"></i> 
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Rekap Usulan</li>
                <li class="active"><i class="fa fa-angle-right"></i>Verifikasi Usulan @if($usulan->USULAN_TUJUAN ==1) Renja @else PIPPK @endif</li>
                <li class="active"><i class="fa fa-angle-right"></i>{{$tahun}}</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">

                <div class="panel bg-white">

                  <div class="panel-heading wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">Verifikasi Usulan @if($usulan->USULAN_TUJUAN ==1) Renja @else PIPPK @endif Tahun {{$tahun}}</h5>
                    <a href="{{ url('/') }}/musrenbang/{{$tahun}}/usulan/show/{{$usulan->USULAN_TUJUAN}}" class="pull-right"><li class="fa fa-reply"></li> Kembali</a>

                  </div>

                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div class="bg-white wrapper-lg input-jurnal">
                            <form id="formsubmit" action="{{url('/')}}/musrenbang/{{ $tahun }}/usulan/edit/submit" method="post" class="form-horizontal" enctype="multipart/form-data">
                              <input type="hidden" name="prioritas" >
                              <input type="hidden" name="keterangan">
                              <input type="hidden" name="val" value="acc">
                              <input type="hidden" name="backurl" value ="{{redirect()->back()->getTargetUrl()}}">
                              <?php
                              if ($rw!=null){
                              ?>
                                <input type="hidden" name="tujuan" value="{{$rw->USULAN_TUJUAN}}">
                              <?php
                              } else {
                              ?>
                                <input type="hidden" name="tujuan" value="1">
                              <?php 
                              }
                              ?>
                              <input type="hidden" name="usulan_id" value="{{$usulan->USULAN_ID}}">
                              <div class="input-wrapper">
                                  <div class="form-group">
                                    <label class="col-sm-2">Isu Usulan</label>
                                    <div class="col-sm-10">
                                      <input type="text" class="form-control" name="isu" readonly="" value="{{ $usulan->kamus->isu->ISU_NAMA }}">
                                      <input type="hidden" class="form-control" id="isu" name="isu" readonly="" value="{{ $usulan->kamus->ISU_ID }}">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2">Kamus Usulan</label>
                                    <div class="col-sm-10">
                                      <input type="text" class="form-control" name="kamus" readonly="" value="{{ $usulan->kamus->KAMUS_NAMA }}">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2">Volume</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" name="vol" value="{{$usulan->USULAN_VOLUME}}" placeholder="">
                                    </div>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control" name="satuan" value="{{ $usulan->kamus->KAMUS_SATUAN }}" readonly="" placeholder="Satuan">
                                    </div>
                                  </div>

                                  <div class="form-group" id="div-upload-gambar" style="display: none">
                                    <label class="col-sm-2">Upload Gambar</label>
                                    <div class="col-sm-8">
                                      <input ui-jq="filestyle" name="image1" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-0" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);">
                                    </div>
                                    <div class="col-sm-1">
                                      <button class="btn btn-info" id="upload" type="button"><i class="fa fa-plus"></i></button>
                                    </div>
                                  </div>
                                  <div class="form-group" style="display:none" id="div1">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-9">
                                      <input ui-jq="filestyle" name="image2" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-1" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);">
                                    </div>
                                    <div class="col-sm-1">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2">Masukan @if(Auth::user()->level == 5) RW @elseif(Auth::user()->level == 1) RT @endif Penerima Manfaat</label>
                                    <div class="col-sm-8" style="margin-top:10px">
                                      @foreach ($rt as $rt)
                                        <input type="checkbox" class="" value="{{$rt->RT_ID}}" style="margin-left:10px;" name="rt[]"> {{$rt->RT_NAMA}}
                                      @endforeach
                                      </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2"> </label>
                                    <div class="col-sm-10">
                                      <input type="hidden" class="form-control" name="kec" readonly="" 
                                      value="{{ $w->kelurahan->kecamatan->KEC_NAMA }},  Kelurahan {{ $w->kelurahan->KEL_NAMA }}, RW {{ $w->RW_NAMA }}">
                                      
                                      <input type="hidden" class="form-control" name="kec" readonly="" value="{{ $w->kelurahan->kecamatan->KEC_NAMA }}">
                                      <input type="hidden" class="form-control" name="kel" readonly="" placeholder="Kelurahan" value="{{ $w->kelurahan->KEL_NAMA }}">
                                      <input type="hidden" class="form-control" name="rw" readonly="" placeholder="RW" value="RW {{ $w->RW_NAMA }}">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2">Urgensi Kegiatan</label>
                                    <div class="col-sm-10">
                                      <input type="text" class="form-control" name="urgensi" placeholder="Isi alasan atau urgensi kegiatan" required="" value="{{$usulan->USULAN_URGENSI}}" readonly="">

                                    </div>
                                  </div>

                                  <div class="form-group" id="div-upload-gambar">
                                    <label class="col-sm-2">Upload Foto Rembuk</label>
                                    <div class="col-sm-8">
                                      <input ui-jq="filestyle" name="image1" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-0" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);">
                                    </div>
                                    <div class="col-sm-1">
                                      <button class="btn btn-info" id="upload" type="button"><i class="fa fa-plus"></i></button>
                                    </div>
                                  </div>

                                   <div class="form-group">
                                    <label class="col-sm-2">Alamat Lokasi </label>
                                    <div class="col-sm-10">
                                       <textarea width="20" name="alamat" class="form-control" placeholder="Masukan Detail Alamat (untuk memudahkan survei lokasi kegiatan)">{{$usulan->ALAMAT}}</textarea>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2">Peta</label>
                                    
                                      <input type="hidden" class="form-control" name="latitude" placeholder="Latitude" id="latitude" value="{{$usulan->USULAN_LAT}}">
                                    
                                      <input type="hidden" class="form-control" name="longitude" placeholder="Longitude" id="longitude" value="{{$usulan->USULAN_LONG}}">
                                   
                                  </div>
                                  <div class="form-group">
                                    <div class="col-sm-12" id="maps" style="height: 500px;">
                                    </div>
                                  </div>
                                 <br>

                                 <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                                  <div class="panel bg-white">
                                    <div class="panel-heading wrapper-lg">
                                      <h5 class="inline font-semibold text-orange m-n">Foto</h5>
                                    </div>
                                    <div class="wrapper-lg" style="height: : 300px;">
                                      @foreach($usulan->USULAN_GAMBAR as $img)
                                        <a href="{{ url('/') }}/uploads/{{ $img }}" target="_blank">
                                        <img src="{{ url('/') }}/uploads/{{ $img }}" style="max-height: 240px;">
                                        </a>
                                      @endforeach
                                    </div>
                                  </div>
                                </div>

                                  <div class="form-group">
                                    <div class="col-md-12">
                                      <div class="col-sm-12" style="margin-top: -25px;">
                                        
                                        @if(( Auth::user()->level==5 || Auth::user()->level==6 || Auth::user()->level== 7 )  && Auth::user()->active == 1 && $perencanaan->PERENCANAAN_AKTIF == 1)
                                          <button type="submit" class="btn btn-info pull-right" style="margin-right: 10px;" data-dismiss="modal" value="edit" name="val">Simpan Update</button>
                                        @endif


                                      @if($usulan->USULAN_STATUS !== 0  || $total_usulan <= 4)  
                                        @if(( Auth::user()->level==7 || Auth::user()->level==5 || Auth::user()->level== 6)  && Auth::user()->active == 1 && $perencanaan->PERENCANAAN_AKTIF == 1) 
                                         
                                            <a class="btn btn-success pull-right" style="margin-right: 10px;" data-toggle="modal" data-target="#form-terima"><i class="fa fa-check"></i> Terima</a>
                                            <a class="btn btn-danger pull-right" style="margin-right: 10px;" data-toggle="modal" data-target="#form-tolak"><i class="fa fa-close"></i> Tolak</a>
                                        @endif 
                                      @endif
                                      
                                          <a href="{{ url('/') }}/musrenbang/{{$tahun}}/usulan/show/1" class="btn btn-default pull-right" style="margin-right: 10px;"><i class="fa fa-back"></i> Batal</a>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                                      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            </form>
                          </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
</div>

<div id="form-terima" class="modal fade" role="dialog">
<form id="formterima" method="post" action="{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/edit/submit">
{{ csrf_field() }}
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Terima Usulan</h4>
      </div>
      <div class="modal-body">
      <div class="form-group">
        @if(Auth::user()->level == 5)
        <label>Prioritas Usulan: </label><br>
        <select name="prioritas" class="form-control">
            <option value=""></option>
            @for($i=1; $i<=$total_usulan; $i++ )
              <option value="{{$i}}">{{$i}}</option>
            @endfor
            
        </select>
        @endif
         <label>Keterangan : </label>
          <input type="hidden" name="usulan_id" value="{{$usulan->USULAN_ID}}">
          <textarea class="form-control" rows="6" placeholder="Keterangan" name="keterangan" required></textarea>
      </div>
      </div>
      <div class="modal-footer">
        @if(Auth::user()->level == 5 || Auth::user()->level == 6 ||Auth::user()->level == 7)
          <button type="submit" class="btn btn-success" name="val" value="acc">Terima</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        @endif
      </div>
    </div>
  </div>
</form>
</div>
<div id="form-tolak" class="modal fade" role="dialog">
<form method="post" action="{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/tolak">
{{ csrf_field() }}
  <input type="hidden" name="backurl" value ="{{redirect()->back()->getTargetUrl()}}">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tolak Usulan</h4>
      </div>
      <div class="modal-body">
      <div class="form-group">
          <label>Alasan : </label>
          <input type="hidden" name="usulan_id" value="{{$usulan->USULAN_ID}}">
          <textarea class="form-control" rows="6" placeholder="Isi Alasan" name="alasan" required></textarea>
      </div>
      </div>
      <div class="modal-footer">
        
        @if(Auth::user()->level == 5 || Auth::user()->level == 6 || Auth::user()->level == 7)
          <button type="submit" class="btn btn-danger">Tolak</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        @endif
      </div>
    </div>
  </div>
</form>
</div>
@endsection


@section('plugin')


    <script type="text/javascript">
      var oldMarker;
      function initMap() {
        var bandung = {lat: {{ $usulan->USULAN_LAT }}, lng: {{ $usulan->USULAN_LONG }}};
        var map = new google.maps.Map(document.getElementById('maps'), {
          zoom: 15,
          style: 'mapbox://styles/mapbox/satellite-v9',
          center: bandung
        });

        var marker = new google.maps.Marker({
          position: bandung,
          icon: "{{ url('/') }}/marker-green.png",
          map: map
        });

        var contentString = '{{ $usulan->kamus->KAMUS_NAMA }}<br>Lokasi : {{$usulan->ALAMAT}}';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });

        google.maps.event.addListener(map, 'click', function( event ){
          $('#latitude').val(event.latLng.lat());
          $('#longitude').val(event.latLng.lng());


          placeMarker(event.latLng,map);
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // [START region_getplaces]
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
          return;
        }

        // Clear out the old markers.
        markers.forEach(function(marker) {
          marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          };

          // Create a marker for each place.
          markers.push(new google.maps.Marker({
            map: map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
          }));

          if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        map.fitBounds(bounds);
        });
        // [END region_getplaces]
      }

      function placeMarker(location,map) {
            marker = new google.maps.Marker({
                position: location,
                map: map

            });

            if (oldMarker != undefined){
                oldMarker.setMap(null);
            }
            oldMarker = marker;
        }
</script>


<script>
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

function initAutocomplete() {
var map = new google.maps.Map(document.getElementById('maps'), {
center: {lat: {{ $usulan->USULAN_LAT }}, lng: {{ $usulan->USULAN_LONG }}},
zoom: 13,
mapTypeId: google.maps.MapTypeId.ROADMAP
});

// Create the search box and link it to the UI element.
var input = document.getElementById('pac-input');
var searchBox = new google.maps.places.SearchBox(input);
map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

// Bias the SearchBox results towards current map's viewport.
map.addListener('bounds_changed', function() {
searchBox.setBounds(map.getBounds());
});

var markers = [];
// [START region_getplaces]
// Listen for the event fired when the user selects a prediction and retrieve
// more details for that place.
searchBox.addListener('places_changed', function() {
var places = searchBox.getPlaces();

if (places.length == 0) {
  return;
}

// Clear out the old markers.
markers.forEach(function(marker) {
  marker.setMap(null);
});
markers = [];

// For each place, get the icon, name and location.
var bounds = new google.maps.LatLngBounds();
places.forEach(function(place) {
  var icon = {
    url: place.icon,
    size: new google.maps.Size(71, 71),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(17, 34),
    scaledSize: new google.maps.Size(25, 25)
  };

  // Create a marker for each place.
  markers.push(new google.maps.Marker({
    map: map,
    icon: icon,
    title: place.name,
    position: place.geometry.location
  }));

  if (place.geometry.viewport) {
    // Only geocodes have viewport.
    bounds.union(place.geometry.viewport);
  } else {
    bounds.extend(place.geometry.location);
  }
});
map.fitBounds(bounds);
});
// [END region_getplaces]
}


</script>

    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzixLw-QwjOpEiL8qmveEmafmgM27mgnk&callback=initMap">
    </script>


    <script>
    $(document).ready(function(){
        $("#upload").click(function(){
            $("#div1").fadeIn("fast");
        });

        $("#upload1").click(function(){
            $("#div2").fadeIn("fast");
        });

        var id  = $('#isu').val();
        $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/kamus/"+id,
              success : function (data) {
                $('#kamus').append(data['opt']).trigger('chosen:updated');
                if(data['tipe'] == 1) $('#div-upload-gambar').fadeIn('fast').removeClass('hide');
                else $('#div-upload-gambar').fadeOut('fast').addClass('hide');
              }
        });

        $("#formterima").on("submit",function(e){
          e.preventDefault();
          $("#formsubmit input[name='prioritas']").val($("#formterima select[name='prioritas']").val());
          $("#formsubmit input[name='keterangan']").val($("#formterima textarea[name='keterangan']").val());
          $("#formsubmit").submit()
        });

    });
    </script>


    <!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
@endsection
