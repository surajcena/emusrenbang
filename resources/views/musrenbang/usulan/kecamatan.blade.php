@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Usulan Per Kecamatan</li>                                
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <div class="col-sm-2 pull-right m-t-n-sm">
                      <a href="{{ url('/') }}/musrenbang/{{$tahun}}/usulan/perkecamatan/export"><button class="btn btn-danger">Export</button></a>
                    </div>
                    <div class="col-sm-2 pull-right m-t-n-sm">
                      <select class="form-control dtSelect" id="dtSelect">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                    <h5 class="font-semibold text-orange m-n ">Usulan Per Kecamatan</h5>
                  </div>           
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                             <table ui-jq="dataTable" ui-options="{
                                      sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/kecamatan/data/usulan/',
                                      aoColumns: [
                                       { mData: 'kecamatanid',class:'hide' },
                                       { mData: 'no' },
                                       { mData: 'kecamatan' },
                                       { mData: 'jumlahrenja' },
                                       { mData: 'jumlahpengusulrenja' },
                                       { mData: 'totalnominalrenja' },
                                       { mData: 'jumlahpippk' },
                                       { mData: 'jumlahpengusulpippk' },
                                       { mData: 'totalnominalpippk' },
                                      ],
                                    }" class="table table-striped b-t b-b" id="table-kecamatan">
                                    <thead>
                                      <tr>
                                        <th class="hide" rowspan="2"></th>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Pengusul</th>
                                        <th colspan="3">RENJA</th>
                                        <th colspan="3">PIPPK</th>
                                      </tr>
                                      <tr>
                                        <th>Jumlah</th>
                                        <th>Jumlah Pengusul</th>
                                        <th>Nominal</th>
                                        <th>Jumlah</th>
                                        <th>Jumlah Pengusul</th>
                                        <th>Nominal</th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        <th colspan="9" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari Usulan" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>

@endsection

@section('plugin')
<script type="text/javascript">
$(document).ready(function(){
  $('#table-kecamatan tbody').on('click', 'tr', function () {
      var dt = $('#table-kecamatan').DataTable();
      var id = dt.row( this ).data()["kecamatanid"];
      //window.location.replace("/musrenbang/2017/public/skpd/detail/"+id);
      window.open("{{url('/')}}/musrenbang/2017/usulan/kecamatan/"+id,'_blank');
  } );
});
</script>
@endsection


