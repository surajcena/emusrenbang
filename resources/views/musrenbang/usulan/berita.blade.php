@extends('musrenbang.layout')

@section('content')
<style media="screen">
#pac-input {
background-color: #fff;
margin-top: 10px;
font-family: Roboto;
font-size: 15px;
font-weight: 300;
margin-left: 12px;
padding: 0 11px 0 13px;
text-overflow: ellipsis;
width: 300px;
}
</style>
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Prioritas Usulan</li>
                <li class="active"><i class="fa fa-angle-right"></i>Berita Acara</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">

                <div class="panel bg-white" style="height:1300px;">

                  <div class="panel-heading wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">Form Berita Acara</h5>

                    <!-- <a href="" class="pull-right"><li class="fa fa-reply"></li> Kembali</a> -->

                  </div>

                  <div class="tab-content tab-content-alt-1 bg-white">
                    <div class="bg-white wrapper-lg input-jurnal">
                        <div class="wrapper-xl padder-bottom-none" style="height:600px; margin-top:-50px;">
                          <p>Langkah untuk mengisi Berita acara<br>
1. Silakan download formulir Berita Acara pada link di bawah ini<br>
2. Isi formulir Berita Acara sesuai dengan data isian yang dibutuhkan<br></p>
                              @if(Auth::user()->level == 1 || Auth::user()->level == 2 || Auth::user()->level == 3 || Auth::user()->level == 4  || Auth::user()->level == 7)
                                            <div class="row">
                                              <a href="https://drive.google.com/open?id=0BxIx84aCMiVnLV9BMjFXWm94Ujg" target="_blank">
                                              <div class="col-md-12 ">
                                                <div class="panel panel-default hvr-back-pulse">
                                                    <div class="panel-body wrapper-sm">
                                                      <h2 class="m-t-xs text-orange font-semibold m-b-sm">Download</h2>
                                                      <p>Download Format Berita Acara</p>
                                                    </div>
                                                </div>
                                              </div>
                                              </a>
                                            </div>

                                            
<p>
3. Upload foto dari Berita Acara yang telah diisi dan ditandatangani pada kotak yang telah disediakan di bawah ini<br>
4. Upload foto-foto Kegiatan 
@if(Auth::user()->level == 1) 
Rembuk Warga 
@elseif(Auth::user()->level == 2 || Auth::user()->level == 3 || Auth::user()->level == 4)
Forum LKK
@elseif(Auth::user()->level == 7)
Forum SKPD
@endif
pada kotak yang telah disediakan</p><br>

                                            
                                            <div class="row">  
                                              <a href="#" data-toggle="modal" data-target="#uploadBerita">
                                               <div class="col-md-6">
                                                <div class="panel panel-default hvr-push hvr-shadow-radial">
                                                    <div class="panel-body wrapper-sm">
                                                      <h4 class="m-t-xs text-orange font-semibold m-b-sm">Upload Foto Berita Acara</h4>
                                                      <p>Upload Foto Berita Acara</p>
                                                    </div>
                                                </div>
                                              </a>    
                                              </div>

                                              <a href="#" data-toggle="modal" data-target="#uploadFoto">
                                              <div class="col-md-6">
                                                <div class="panel panel-default hvr-push hvr-shadow-radial">
                                                    <div class="panel-body wrapper-sm">
                                                      <h4 class="m-t-xs text-orange font-semibold m-b-sm">Upload Foto Rembuk Warga</h4>
                                                      <p>Upload Foto Forum Rembuk Warga</p>
                                                    </div>
                                                </div>
                                              </div>
                                              </a>  
                                      </div> 
                                      @endif  

                                      <hr> 

                                      <h3> Foto Berita acara </h3>
                                      <div class="wrapper-lg" style="height: : 300px;">
                                         @foreach($berita as $berita)
                                            @foreach($berita->lampiran as $lampiran)
                                          <a href="{{ url('/') }}/uploads/{{ $lampiran }}" target="_blanḳ">
                                             <img src="{{ url('/') }}/uploads/{{ $lampiran }}" style="max-height: 100px;">
                                          </a>
                                           @endforeach
                                          @endforeach
                                        </div>
                                        
                                      <h3> Foto Rembuk Warga </h3>
                                       <div class="wrapper-lg" style="height: : 100px;">
                                          @foreach($beritaFoto as $foto)
                                          
                                            @foreach($foto->image as $img)
                                            <a href="{{ url('/') }}/uploads/{{ $img }}" target="_blanḳ">
                                             <img src="{{ url('/') }}/uploads/{{ $img }}" style="max-height: 100px;">
                                           </a>
                                            @endforeach
                                          
                                          @endforeach
                                          <p></p>
                                        </div>
                              </div>       

                    </div>    
                  </div>
                </div>

                 
              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
</div>



<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">


<div id="uploadBerita" class="modal fade" role="dialog">
  <form action="{{url('/')}}/musrenbang/berita/tambah" method="POST" enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="hidden" value="{{Auth::user()->RW_ID}}" name="rwid">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Foto Berita Acara (.png/.jpg)</h4>
        </div>
       <div class="modal-body">
          <div class="form-group" id="div-upload-gambar">
            <label>Upload Foto Berita Acara</label>
            <div>
              <p style="color:red">
                
              </p>
              <input ui-jq="filestyle" name="image[]" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-2" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);">
            </div>
          </div>
          <div class="form-group" up id="div1" style="display: none">
            <input ui-jq="filestyle" name="image[]" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-1" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);">
          </div>
          <br>
          <div>
            <button class="btn btn-info" id="upload" type="button"><i class="fa fa-plus"> Tambah Gambar</i></button>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default">Simpan</button>
        </div>
      </div>
    </div>
  </form>
</div>



<div id="uploadFoto" class="modal fade" role="dialog">
  <form action="{{url('/')}}/musrenbang/berita/tambahGambar" method="POST" enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="hidden" value="{{Auth::user()->RW_ID}}" name="rwid">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Foto Berita Acara</h4>
        </div>
        <div class="modal-body">
          <div class="form-group" id="div-upload-gambar">
            <label>Upload Foto Rembuk Warga</label>
            <div>
              <p style="color:red">
               
              </p>
              <input ui-jq="filestyle" name="image1[]" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-1-1" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);">
            </div>
          </div>
          <div class="form-group" up id="div11" style="display: none">
            <input ui-jq="filestyle" name="image1[]" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-1-2" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);">
          </div>
          <br>
          <div>
            <button class="btn btn-info" id="upload1" type="button"><i class="fa fa-plus"> Tambah Gambar</i></button>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default">Simpan</button>
        </div>
      </div>
    </div>
  </form>
</div>


@endsection





@section('plugin')
<script>
    $(document).ready(function(){
        $("#upload").click(function(){
            $("#div1").fadeIn("fast");
            $("#upload").addClass("addGambar");
        });

        $("#upload1").click(function(){
            $("#div11").fadeIn("fast");
            $("#upload1").addClass("addGambar");
        });
    });
</script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>


<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

@endsection
