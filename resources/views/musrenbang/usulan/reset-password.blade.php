@extends('musrenbang.layout')

@section('content')
<style media="screen">
#pac-input {
background-color: #fff;
margin-top: 10px;
font-family: Roboto;
font-size: 15px;
font-weight: 300;
margin-left: 12px;
padding: 0 11px 0 13px;
text-overflow: ellipsis;
width: 300px;
}
</style>
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <!-- <i class="icon-bdg_expand1 text"></i> -->
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Usulan</li>
                <li class="active"><i class="fa fa-angle-right"></i>Reset Password</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">

                  <div class="panel-heading wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">Reset Password</h5>
                  </div>

                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div class="bg-white wrapper-lg input-jurnal">
                          
                              <div class="input-wrapper">
                                
                                @foreach($user as $user)
                                  <div class="form-group">
                                    <label class="col-sm-4">{{ $user->name }}</label>
                                     <label class="col-sm-4">
                                      @if($user->validasi == 1)
                                      Aktivasi
                                      @else 
                                        Belum Aktivasi 
                                      @endif  
                                    </label>
                                    <div class="col-sm-4">

                            <form action="{{url('/')}}/musrenbang/2017/reset/ubah" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <input type="hidden" value="{{ $user->email }}" name="email">
                                <input type="hidden" value="{{ $user->level }}" name="level">
                                      <input type="submit" class="btn btn-danger" value="Reset Password" readonly="">
                                                                </form>

                                    </div>
                                  </div>
                                  <br>
                                  <hr>
                                 @endforeach  


                                  </div>
                              </div>
                          </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
</div>
@endsection





@section('plugin')




<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzixLw-QwjOpEiL8qmveEmafmgM27mgnk&libraries=places&callback=initMap">
</script>

<script>
    $(document).ready(function(){

        $("#upload").click(function(){
            $("#div1").fadeIn("fast");
            $("#upload").addClass("addGambar");
        });

        $("#isu").change(function(e, params){
            var id  = $('#isu').val();
           // $('#kamus').find('option').remove().end().append('<option>Kamus Usulan</option>');
            $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/kamus/"+id,
              success : function (data) {
                $('#kamus').append(data['opt']).trigger('chosen:updated');
                if(data['tipe'] == 1 || data['tipe'] == 2) $('#div-upload-gambar').fadeIn('fast').removeClass('hide');
                else $('#div-upload-gambar').fadeOut('fast').addClass('hide');
              }
            });
        });

        $("#kamus").change(function(e, params){
            var id  = $('#kamus').val();
            $('#satuan').val();
            $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/satuan/"+id,
              success : function (data) {
                $('#satuan').val(data);
              }
            });
        });

    });
</script>


<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

@endsection
