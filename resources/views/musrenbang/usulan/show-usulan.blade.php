@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Rekap Usulan</li>
                <li class="active"><i class="fa fa-angle-right"></i>@if($id==1) Renja @else PIPPK @endif</li>
                <li class="active"><i class="fa fa-angle-right"></i>{{$tahun}}</li>
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              <div class="col-sm-12">
                <div class="panel bg-white wrapper-lg" style="height: 165px;">
                  <div class="form-group col-sm-12">
                      <label class="col-sm-2">
                        @if(Auth::user()->level==8)
                              SKPD
                            @else
                              Pengusul
                            @endif </label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="filter w-full isu form-control" name="pengusul" id="pengusul">
                          <option value="x" style="color:black">
                            @if(Auth::user()->level==8)
                              SKPD
                            @else
                              Pengusul
                            @endif
                          </option>
                          @foreach($users as $u)
                           <option value="{{$u->id}}" style="color:black">{{$u->name}}</option>
                          @endforeach
                        </select>
                      </div>
                      <label class="col-sm-2">Kamus</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="filter w-full isu form-control" name="kamus" id="kamus">
                          <option value="x" style="color:black">Kamus</option>
                          @foreach($kamus as $k)
                            <option value="{{$k->KAMUS_ID}}" style="color:black">{{$k->KAMUS_NAMA}}</option>
                          @endforeach
                        </select>
                      </div>
                  </div>
                  <div class="form-group col-sm-12">
                      <label class="col-sm-2">Isu</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="filter w-full isu form-control" name="isu" id="isu">
                          <option value="x" style="color:black">Isu</option>
                          @foreach($isu as $i)
                            <option value="{{$i->ISU_ID}}" style="color:black">{{$i->ISU_NAMA}}</option>
                          @endforeach
                        </select>
                      </div>
                      @if(Auth::user()->level == 7)
                      <label class="col-sm-2">Kecamatan</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="isu" id="kecamatan">
                          <option value="x" style="color:black">Kecamatan</option>
                          @foreach($kecamatan as $k)
                            <option @if($k->KEC_ID == $kecid) selected @endif value="{{$k->KEC_ID}}" style="color:black">{{$k->KEC_NAMA}}</option>
                          @endforeach
                        </select>
                      </div>
                      @endif
                  </div>
                </div>


              </div>
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <h4>Rekap Usulan @if($id==1) Renja @else PIPPK @endif Tahun {{$tahun}}</h4>
                    <a href="{{ url('/') }}/musrenbang/{{$tahun}}/usulan-{{$id}}" class="pull-right"><li class="fa fa-reply"></li> Kembali</a>

                    <div class="row">
                      <div class="col-sm-10">
                       <div class="nav-tabs-alt tabs-alt-1 b-t three-row" id="tab-jurnal" >
                          <ul class="nav nav-tabs" role="tablist">
                             <li class="active">
                              <a data-target="#tab-1" role="tab" data-toggle="tab" data-status="1" class="buttonstatus">[{{$diproses}}] Di Proses <i class="fa fa-refresh text-info"></i></a>
                             </li>
                             <li>
                              <a data-target="#tab-2" role="tab" data-toggle="tab" data-status="2" class="buttonstatus">[{{$diterima}}] Di Terima <i class="fa fa-check text-success"></i></a>
                             </li>
                             <li>
                              <a data-target="#tab-3" role="tab" data-toggle="tab" data-status="0" class="buttonstatus">[{{$ditolak}}] DI Tolak <i class="fa fa-close text-danger"></i></a>
                             </li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-sm-2 pull-right m-t-n-sm">

                       @if(Auth::user()->level==7)
                        <a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpdperkecamatan/export/{{Auth::user()->SKPD_ID}}" target="_blank" id="export-to-excel" class="btn btn-success pull-right" style="margin-top:10px;"><i class="fa fa-back"></i> Export excel</a>
                        @else
                        <a href="{{URL::to('/musrenbang/'.$tahun.'/export/usulan/'.$id)}}" target="_blank" id="export-to-excel" class="btn btn-success pull-right" style="margin-top:10px;"><i class="fa fa-back"></i> Export excel</a>
                        @endif
  <select class="form-control dtSelect" id="dtSelect" style="margin-top:57px">
                              <option value="10">10</option>
                              <option value="25">25</option>
                              <option value="50">50</option>
                              <option value="100">100</option>
                          </select>

                      </div>
                   </div>

                  </div>


                  <div class="tab-content tab-content-alt-0 bg-dark-grey" id="tab_usulan">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">
                            <div class="table-responsive dataTables_wrapper">
                              <table id="table-usulan" ui-jq="dataTable" ui-options="
                                  {
                                    sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/data/usulan/{{$id}}/1/x/x/x/{{$kecid}}',
                                    aoColumns: [
                                    { mData: 'usulanid',class:'hide' },
                                    { mData: 'no',class:'text-center' },
                                    { mData: 'aksi' },
                                    { mData: 'skpd' },
                                    { mData: 'kegiatan' },
                                    { mData: 'isukamus' },
                                    { mData: 'volume' },
                                    { mData: 'total' },
                                    { mData: 'namapengusul' },
                                    { mData: 'status' },
                                    ],
                                    order: [[9, 'asc']]
                                  }" class="table table-usulan table-striped b-t b-b">

                                    <thead>
                                      <tr>
                                        <th class="hide">#</th>
                                        <th>No</th>
                                        <th>Action</th>
                                        <th>SKPD</th>
                                        <th>Program / <br><font style='color:orange'>Kegiatan</font></th>
                                        <th>Isu / <br><font style='color:orange'>Usulan</font></th>
                                        <th>Volume / <br><font style='color:orange'>Harga</font></th>
                                        <th>Total</th>
                                        <th>Pengusul</th>
                                        <th>Status Approval</th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        @if(Auth::user()->level >= 5)

                                        <th colspan="9" class="th_search">

                                        @else
                                        <th colspan="9" class="th_search">
                                        @endif
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                          <td colspan="3"> </td>
                                          <td colspan="3"> </td>
                                          <td colspan="3" style="text-align:right;"><b>Total Nominal : Rp. <text id="totalnominal"></text> </b></td>
                                        </tr>
                                      </tfoot>
                                  </table>
                              </div>
                          </div>
                        </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->
    </div>
    <!-- .col -->
</div>

@endsection

@section('plugin')
<script type="text/javascript">
var status = 1;
  function changePriority(usulanid, kel_id){


    var priority = prompt("Silahkan masukkan nilai prioritas", "1");
    console.log(usulanid+'-'+kel_id+'-'+priority);
    $.ajax({
      'url' : '<?php echo URL::to('/'); ?>/musrenbang/usulan/set_prioritas/'+usulanid+'/'+kel_id+'/'+priority,
      'type' : 'GET',
      'success' : function(data) {
        console.log(data);
        $("#usulan-"+usulanid).html(priority);
      }
    });

  }

$(document).ready(function(){
  $(".buttonstatus").on("click",function(event) {

    status = $(this).data('status');

    resetTable();
  });

  $(".filter").on('change',function(e){
    resetTable();
  });

  $("#kecamatan").on('change',function(e){
    //resetTable();
    var kecamatan= $('#kecamatan').val();
    window.location = "{{ url('/') }}/musrenbang/2017/usulan/show/1/"+kecamatan;
  });

  function resetTable(){
    var pengusul   = $('#pengusul').val();
    var isu  = $('#isu').val();
    var kamus= $('#kamus').val();
    var kecamatan= $('#kecamatan').val();
    $('#table-usulan').DataTable().destroy();
    if(kecamatan == undefined || kecamatan == null)
    {
      kecamatan = "x"
    }
    $('#table-usulan').DataTable({
          sAjaxSource: "{{ url('/') }}/musrenbang/{{$tahun}}/data/usulan/{{$id}}/"+status+"/"+pengusul+"/"+isu+"/"+kamus+"/"+kecamatan,
           aoColumns: [
            { mData: 'usulanid',class:'hide' },
            { mData: 'no',class:'text-center' },
            { mData: 'aksi' },
            { mData: 'skpd' },
            { mData: 'kegiatan' },
            { mData: 'isukamus' },
            { mData: 'volume' },
            { mData: 'total' },
            { mData: 'namapengusul' },
            { mData: 'status' },
          ],
          "order": [[9, "asc"]],
          initComplete:function(setting,json){
            $("#totalnominal").html(json.jumlah);

        }
    });
  }
});

function hapus(id){
    var token        = $('#token').val();
    $.confirm({
        title: 'Hapus Data!',
        content: 'Yakin hapus data?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                      url: "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/delete",
                      type: "POST",
                      data: {'_token'         : token,
                            'id'              : id},
                      success: function(msg){
                          $.alert(msg);
                          $('.table').DataTable().ajax.reload();
                        }
                  });
                }
            },
            Tidak: function () {
            }
        }
    });
  }

  function cekbox(){
    var closestTr = $(':checkbox:checked').closest('tr').attr('id');
    alert(closestTr);
}
</script>
@endsection


