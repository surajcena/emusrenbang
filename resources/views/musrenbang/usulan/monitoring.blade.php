@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">
        <div class="bg-light lter">    
          <ul class="breadcrumb bg-white m-b-none">
            <li>
              <a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                <i class="icon-bdg_expand1 text"></i>
                <i class="icon-bdg_expand2 text-active"></i>
              </a>   
            </li>
            <li>
              <a href= "{{ url('/') }}/musrenbang/{{$tahun}}/public">Beranda</a>
            </li>
          </ul>
        </div>
        <div class="wrapper-lg padder-bottom-none" style="height:550px;">
          <div class="row">
              <div class="col-md-12">
                @if ((Auth::user()->level >=5) && (Auth::user()->level <= 7))
              <div class="row">
                <div class="col-md-3">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h2 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">{{ number_format($rwv,0,',','.') }}</span> / {{ number_format($rw,0,',','.') }}</h2>
                      <p>RW Aktif / RW Total</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h2 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">{{ number_format($pkkpernahusulan,0,',','.') }}</span> / {{ number_format($pkk,0,',','.') }}</h2>
                      <p>PKK Pernah Mengusulkan / PKK Total</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h2 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">{{ number_format($lpmpernahusulan,0,',','.') }}</span> / {{ number_format($lpm,0,',','.') }}</h2>
                      <p>LPM Pernah Mengusulkan / LPM Total</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h2 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">{{ number_format($kartapernahusulan,0,',','.') }}</span> / {{ number_format($karta,0,',','.') }}</h2>
                      <p>Tarka Pernah Mengusulkan / Tarka Total</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h2 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">{{ number_format($rwpernahusulan,0,',','.') }}</span> / {{ number_format($rw,0,',','.') }}</h2>
                      <p>RW Pernah Mengusulkan / RW Total</p>
                    </div>
                  </div>
                </div>
                @endif


              @if ((Auth::user()->level == 7) && ($dashboard_skpd != null))
                <div class="col-md-4">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h2 class="m-t-xs text-orange font-semibold m-b-sm" id="renja">{{ number_format($dashboard_skpd['JUMLAHRENJA'],0,',','.') }}</h2>
                      <p>Total Usulan RENJA</p>
                    </div>
                  </div>
                </div> 

                <div class="col-md-4">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h2 class="m-t-xs text-orange font-semibold m-b-sm" id="renja">{{ number_format($dashboard_skpd['JUMLAHPENGUSUL'],0,',','.') }}</h2>
                      <p>Total Pengusul</p>
                    </div>
                  </div>
                </div> 

                <div class="col-md-4">
                  <div class="panel panel-default">
                    <div class="panel-body wrapper-sm">
                      <h2 class="m-t-xs text-orange font-semibold m-b-sm" id="renja">{{ $dashboard_skpd['TOTALNOMINAL'] }}</h2>
                      <p>Total Nominal</p>
                    </div>
                  </div>
                </div> 
                @endif
               
              </div>
           @if(Auth::user()->level == 5 || Auth::user()->level == 6 || Auth::user()->level == 8)
            <div class="panel bg-white hvr-underline-from-left col-md-12">
              <a href="{{ url('/') }}/musrenbang/2017/usulan/show/1">
                <div class="wrapper-lg">
                  <h3 class="inline font-semibold text-orange m-n ">Rekap Usulan Renja
                  (
                  <?php
                  if (count($renjadesc)>0) {
                      $renjatotal = $renjadesc['diproses'] + $renjadesc['diterima'] + $renjadesc['ditolak'];
                      echo $renjatotal;
                  }
                  ?>
                  )
                  </h3>                    
                </div>
              </a>
            </div>

            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body wrapper-sm">
                  <h2 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">
                  <?php
                  $renjatotal = 0;
                  if (count($renjadesc)>0) {
                    echo $renjadesc['diproses'];
                  }
                  else echo "-";
                  ?>
                  </h2>
                  <p>Renja diproses</p>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body wrapper-sm">
                  <h2 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">
                  <?php
                   if (count($renjadesc)>0) echo $renjadesc['diterima'];
                  else echo "-";
                  ?>
                  </h2>
                  <p>Renja diterima</p>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body wrapper-sm">
                  <h2 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">
                  <?php
                   if (count($renjadesc)>0) echo $renjadesc['ditolak'];
                  else echo "-";
                  ?>
                  </h2>
                  <p>Renja ditolak</p>
                </div>
              </div>
            </div>


            <div class="panel bg-white hvr-underline-from-left col-md-12">
              <a href="{{ url('/') }}/musrenbang/2017/usulan/show/2">
                <div class="wrapper-lg">
                  <h3 class="inline font-semibold text-orange m-n ">Rekap Usulan PIPPK
                  (
                  <?php
                  if (count($pippkdesc)>0) {
                      $pippktotal = $pippkdesc['diproses'] + $pippkdesc['diterima'] + $pippkdesc['ditolak'];
                      echo $pippktotal;
                  }
                  ?>
                  )
                  </h3>                    
                </div>
              </a>
            </div> 

            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body wrapper-sm">
                  <h2 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">
                  <?php
                     if (count($pippkdesc)>0) echo $pippkdesc['diproses'];
                    else echo "-";
                  ?>
                  </h2>
                  <p>PIPPK diproses</p>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body wrapper-sm">
                  <h2 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">
                  <?php
                    if (count($pippkdesc)>0) echo $pippkdesc['diterima'];
                    else echo "-";
                  ?>
                  </h2>
                  <p>PIPPK diterima</p>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-body wrapper-sm">
                  <h2 class="m-t-xs text-orange font-semibold m-b-sm"><span class="text-success">
                  <?php
                    if (count($pippkdesc)>0) echo $pippkdesc['ditolak'];
                    else echo "-";
                  ?>
                  </h2>
                  <p>PIPPK ditolak</p>
                </div>
              </div>
            </div>

            @elseif(Auth::user()->level == 7 || Auth::user()->level == 8)
            <div class="panel bg-white hvr-underline-from-left col-md-12">
              <a href="{{ url('/') }}/musrenbang/2017/usulan/show/1">
                <div class="wrapper-lg">
                  <h3 class="inline font-semibold text-orange m-n ">Rekap Usulan Renja</h3>                    
                </div>
              </a>
            </div>
            @else
             <div class="panel bg-white hvr-underline-from-left col-md-12">
              <a href="{{ url('/') }}/musrenbang/2017/usulan-2">
                <div class="wrapper-lg">
                  <h3 class="inline font-semibold text-orange m-n ">Rekap Usulan PIPPK</h3>                    
                </div>
              </a>
            </div> 
            @endif

            

            </div>
             
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection

@section('plugin')
<script type="text/javascript">
  $( document ).ready(function() {
    window.setInterval(function(){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/getOnline",
        success : function (data) {
          $('#user-online').text(data['o']);
          $('#pippk').text(data['pippk']);
          $('#renja').text(data['renja']);
        }
      });
    }, 5000);
  });
</script>
@endsection


