@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Usulan</li>   
                <li class="active"><i class="fa fa-angle-right"></i> Layak </li>                                
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              
              <div class="col-md-12">
                <div class="panel bg-white">
                 

                  <div class="tab-content tab-content-alt-0 bg-dark-grey" id="tab_usulan">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                              <table id="table-usulan" ui-jq="dataTable" ui-options="{
                                    sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/topsis/daftarUsulan',
                                    aoColumns: [
                                    { mData: 'USULAN_ID',class:'hide' },
                                    { mData: 'NO',class:'text-center' },
                                    { mData: 'PENGUSUL' },
                                    { mData: 'KAMUS_NAMA' },
                                    { mData: 'RC' },
                                    { mData: 'VOL' },
                                    { mData: 'HARGA' },
                                    { mData: 'TOTAL' },
                                    { mData: 'STATUS' },
                                    { mData: 'SKPD' },
                                    { mData: 'AKSI' },
                                    ]}" class="table table-usulan table-striped b-t b-b">
                              
                                    <thead>
                                      <tr>
                                        <th class="hide">#</th>
                                        <th>#</th>
                                        <th>Pengusul</th>
                                        <th>Isu/Usulan</th>
                                        <th>Prioritas (RC)</th>
                                        <th>Volume</th>
                                        <th>Harga</th>
                                        <th>TOTAL</th>
                                        <th>Status</th>
                                        <th>SKPD</th>
                                        <th>AKSI</th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        @if(Auth::user()->level >= 5)

                                        <th colspan="10" class="th_search">

                                        @else
                                        <th colspan="10" class="th_search">
                                        @endif
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                        </div>  

                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>

@endsection

@section('plugin')
<script type="text/javascript">
var status = 1;
$(document).ready(function(){
  /*$("#kecamatan").change(function(e, params){
    var kec  = $('#kecamatan').val();
    $('#kelurahan').find('option').remove().end().append('<option value="x">Kelurahan</option>');    
    $('#rw').find('option').remove().end().append('<option value="x">RW</option>');    
    resetTable();
    if(kec != 'x'){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/s/kelurahan/"+kec,
        success : function (data) {
          $('#kelurahan').append(data).trigger('chosen:updated');
        }
      });
    }
  });
  $("#kelurahan").change(function(e, params){
    var kel  = $('#kelurahan').val();
    $('#rw').find('option').remove().end().append('<option value="x">RW</option>');    
    resetTable();
    if(kel!='x'){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/s/rw/"+kel,
        success : function (data) {
          $('#rw').append(data).trigger('chosen:updated');
        }
      });
    }
  });*/
  /*$("#rw").change(function(e, params){
    resetTable();
  });
  $("#tipe").change(function(e, params){
    var tipe  = $('#tipe').val();
    $('#isu').find('option').remove().end().append('<option value="x">Isu</option>');    
    $('#kamus').find('option').remove().end().append('<option value="x">Kamus</option>');    
    resetTable();
    if(tipe!='x'){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/s/isu/"+tipe,
        success : function (data) {
          $('#isu').append(data).trigger('chosen:updated');
        }
      });
    }
  });
  $("#isu").change(function(e, params){
    var isu  = $('#isu').val();
    $('#kamus').find('option').remove().end().append('<option value="x">Kamus</option>');
    resetTable();
    if(isu!='x'){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/s/kamus/"+isu,
        success : function (data) {
          $('#kamus').append(data).trigger('chosen:updated');
        }
      });
    }
  });
  $("#kamus").change(function(e, params){
    resetTable();
  });*/

  $(".buttonstatus").on("click",function(event) {

    status = $(this).data('status');
    
    resetTable();
  });

  $(".filter").on('change',function(e){
    resetTable();
  });

  function resetTable(){
   // var kec  = $('#kecamatan').val();
    var kel  = $('#kel').val();
    var rw   = $('#rw').val();
    var tipe = $('#tipe').val();
    var isu  = $('#isu').val();
    var kamus= $('#kamus').val();
    
    $('#table-usulan').DataTable().destroy();

    $('#table-usulan').DataTable({
          sAjaxSource: "{{ url('/') }}/musrenbang/{{$tahun}}/usulan/showUsulanDataKel/"+status+"/"+kel+"/"+rw+"/"+isu+"/"+tipe+"/"+kamus,
          aoColumns: [
          { mData: 'USULAN_ID',class:'hide' },
          { mData: 'NO',class:'text-center' },
          { mData: 'LOKASI' },
          { mData: 'ISU' },
          { mData: 'VOLUME' },
          { mData: 'HARGA' },
          { mData: 'ANGGARAN' },
          { mData: 'STATUS' },
          { mData: 'PRIORITAS' },
          { mData: 'KETERANGAN' },
          { mData: 'AKSI' },
          ]
    });
  }
});


$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
@endsection


