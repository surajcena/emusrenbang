@extends('musrenbang.mobile.layout.layout2')


@section('content')

<div class="content container bg-grey">
        
        <form action="" class="form-horizontal">
          <!-- Search -->
          <div class="input-group m-b-md">                    
                  <input type="text" class="input-xxl no-borders form-control" placeholder="Cari Daftar Kelurahan">
                  <span class="input-group-addon bg-white "><i class="fa fa-search"></i></span>
                </div>
            </form>

              <div class="list-card box-shadow">
                <div class="heading">
                  <i class="fa fa-user-circle"></i>
                  <p>RW. 01, Kel. Kebon Jeruk, Kec. Andir</p>
                </div>
                <div class="list-content">
                  <p>Kategori Isu : <span>25</span></p>
                  <p>Banyaknya usulan : <span>210  dari 8 RW</span></p>
                  <p>Besaran Dana : <span>Rp. 10.000.000,00</span></p>
                  <button type="submit" class="w-full m-b-md btn btn-soft-orange input-xxl">Lihat Usulan per RW</button>
                </div>
              </div>

               <div class="list-card box-shadow">
                <div class="heading">
                  <i class="fa fa-user-circle"></i>
                  <p>RW. 02, Kel. Kebon Jeruk, Kec. Andir</p>
                </div>
                <div class="list-content">
                  <p>Kategori Isu : <span>12</span></p>
                  <p>Banyaknya usulan : <span>210  dari 8 RW</span></p>
                  <p>Besaran Dana : <span>Rp. 10.000.000,00</span></p>
                  <button type="submit" class="w-full m-b-md btn btn-soft-orange input-xxl">Lihat Usulan per RW</button>
                </div>
              </div>

               <div class="list-card box-shadow">
                <div class="heading">
                  <i class="fa fa-user-circle"></i>
                  <p>RW. 03, Kel. Kebon Jeruk, Kec. Andir</p>
                </div>
                <div class="list-content">
                  <p>Kategori Isu : <span>12</span></p>
                  <p>Banyaknya usulan : <span>210  dari 8 RW</span></p>
                  <p>Besaran Dana : <span>Rp. 10.000.000,00</span></p>
                  <button type="submit" class="w-full m-b-md btn btn-soft-orange input-xxl">Lihat Usulan per RW</button>
                </div>
              </div>
          
      </div>

@endsection


@section('plugin')

@endsection