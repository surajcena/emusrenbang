@extends('musrenbang.mobile.layout.layout1')



@section('content')
<body class="login">
  <div class="bg-screen">

<!-- .container -->
 <div class="container">
    
      <div class="logo">
        <img src="{{ url('/') }}/mobile_assets/img/logo.png" alt="">
      </div>
      
      <div class="container">
        <div class="login-form">
          <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
           {{ csrf_field() }}
                  <div class="input-group m-b-md">
                    <span class="input-group-addon bg-white "><i class="fa fa-group"></i></span>
                    <input type="text" name="email" class="input-xxl no-borders form-control" placeholder="Username">
                  </div>

                  <div class="input-group m-b-md">
                    <span class="input-group-addon bg-white "><i class="fa fa-lock"></i></span>
                    <input type="password" name="password" class="input-xxl no-borders form-control" placeholder="Password">
                  </div>

              <button type="submit" class="w-full m-b-md btn btn-info input-xxl">LOGIN</button>

            <div class="row">
              <div class="col-xs-6">
                
                <div class="checkbox">
                        <label>
                          <input type="checkbox" value="">
                          Tetapkan Login
                        </label>
                      </div>

              </div>

              <div class="col-xs-6">
                <div class="checkbox text-right">
                        <label>
                          <a href="#">Lupa Password?</a>
                        </label>
                      </div>
                
              </div>
            </div>
          </form>
        </div>
      </div>

    </div>
    <!-- .container -->

    </div>
  <!-- .bg-screen-->

@endsection
