<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>E-Musrenbang | Input Usulan</title>
	<meta name="description" content="Bandung Web Kit" />
  	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  	<link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/animate.css/animate.css" type="text/css" />
  	<link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/font-awesome/css/font-awesome.min.css" type="text/css" />
  	<link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/simple-line-icons/css/simple-line-icons.css" type="text/css" />
  	<link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/jquery/bootstrap/dist/css/bootstrap.css" type="text/css" />

  
	<link rel="stylesheet" href="{{ url('/') }}/mobile_assets/css/font.css" type="text/css" />
	<link rel="stylesheet" href="{{ url('/') }}/mobile_assets/fonts/simdaicon/simdaicon.css" type="text/css" />
	<link rel="stylesheet" href="{{ url('/') }}/mobile_assets/fonts/simdaicon/mobileicon.css" type="text/css" />
	<link rel="stylesheet" href="{{ url('/') }}/mobile_assets/css/style.css" type="text/css" />
	<link rel="stylesheet" href="{{ url('/') }}/mobile_assets/css/mobile.css" type="text/css" />

</head>
<body class="pages">
	<div class="bg-screen bg-white">
			
			<div class="top-header bg-blue">
				<div class="container">
					<a href="#" class="menu-mobile pull-left active"><i class="fa fa-bars"></i></a>
					<p class="pull-left">INPUT USULAN</p>
					
					<a href="#" class="pull-right">
						<span class="badge badge-sm up bg-danger pull-right-xs">2</span>
						<i class="fa  fa-bell-o"></i>
					</a>
				</div>
			</div>
			

			<div class="content container bg-grey">
				<div class="list-detail">					

					<div class="list-content input-usulan">
						<form action="" class="form-horizontal">
							<span class="label">Isu</span>
			            		<div class="input-group m-b-md">			              
					              <select ui-jq="chosen" class="w-full">
					              	<option value="">Silahkan pilih berdasarkan kategori isu</option>
					              	<option value="">issue1</option>
					              	<option value="">issue2</option>
					              </select>
					              <span class="input-group-addon bg-white "><i class="fa fa-caret-down"></i></span>
					            </div>

		            		<span class="label">Urgensi</span>
			            		<div class="input-group m-b-md">					              
					              <input type="text" class="w-full input-xxl no-borders form-control" placeholder="Beri Keterangan Urgensi">
					            </div>

		            		<span class="label">Volume</span>
		            			<div class="input-group m-b-md">					              
					              <input type="text" class="w-full input-xxl no-borders form-control" placeholder="Input Besaran Volume">
					            </div>

							<span class="label">Lokasi RT</span>
		            			<div class="input-group m-b-md">			              
					              <select ui-jq="chosen" class="w-full">
					              	<option value="">Silahkan pilih berdasarkan kategori isu</option>
					              	<option value="">issue1</option>
					              	<option value="">issue2</option>
					              </select>
					              <span class="input-group-addon bg-white "><i class="fa fa-caret-down"></i></span>
					            </div>

		            		<span class="label">Prioritas Usulan</span>
		            			<div class="input-group m-b-md">					              
					              <input type="text" class="w-full input-xxl no-borders form-control" placeholder="Input Besaran Volume">
					            </div>

		            		<span class="label">Peta Lokasi</span>
		            		<div class="image">
	                			<div id="maps"></div>
	                		</div>

	                		<div class="image-upload">
	                			<img src="img/bg-input-image.png " alt="">
	                			<img src="img/bg-input-image.png " alt="">
	                			<img src="img/bg-input-image.png " alt="">
	                		</div>

	                		<div class="upload-button">
	                			<input type="file" >
	                			<button class="w-full no-borders btn btn-grey"><i class="fa fa-upload"></i> Upload Image</button>
	                		</div>


		            		
		            		<button type="button" class="w-full btn btn-green input-xxl"><i class="fa fa-plus"></i>TAMBAHKAN USULAN</button>
						</form>
	            	</div>
				</div>

			</div>
			<!-- .container -->

			
	
	</div>
	<aside class="app-aside hidden-xs active">
		<div class="profile">
			<span class="thumb-md avatar pull-left m-t-n-sm m-b-n-sm m-r-sm">
                <img src="img/01.jpg" alt="...">                
            </span>
            <p class="pull-left">Ridwan Kamil</p>
            <p class="pull-left"><span>Kepala SKPD</span></p>
		</div>

		<nav>
			<ul>
				<li class="parent">
					<a href="#"><i class="fa fa-pencil m-r-sm"></i> Input Usulan <i class="pull-right fa fa-chevron-down m-t-xxs"></i></a>
					<ul class="submenu">
						<li><a href="#">Renja</a></li>
						<li><a href="#">PIPPK</a></li>
					</ul>
				</li>
				<li><a href="#"><i class="fa fa-book m-r-sm"></i> Daftar Usulan</a></li>
				<li class="active"><a href="#"><i class="fa fa-bookmark-o m-r-sm"></i> Kamus Usulan</a></li>
				<li><a href="#"><i class="fa fa-list-alt m-r-sm"></i> Program 2017</a></li>
				<li><a href="#"><i class="fa fa-sign-out m-r-sm"></i> Logout</a></li>
			</ul>
		</nav>
	</aside>
	<!-- .bg-screen -->

	<script src="{{ url('/') }}/libs_dashboard/jquery/jquery/dist/jquery.js"></script>
	<script src="{{ url('/') }}/libs_dashboard/jquery/bootstrap/dist/js/bootstrap.js"></script>
	<script src="{{ url('/') }}/mobile_assets/js/ui-load.js"></script>
	<script src="{{ url('/') }}/mobile_assets/js/ui-jp.config.js"></script>
	<script src="{{ url('/') }}/mobile_assets/js/ui-jp.js"></script>
	<script src="{{ url('/') }}/mobile_assets/js/ui-nav.js"></script>
	<script src="{{ url('/') }}/mobile_assets/js/ui-toggle.js"></script>
	<script src="{{ url('/') }}/mobile_assets/js/ui-client.js"></script>
	<script src="{{ url('/') }}/mobile_assets/js/custom.js"></script>
	
	<script>

	 var oldMarker;
      function initMap() {
        var bandung = {lat: -6.914744, lng: 107.609810};
        var map = new google.maps.Map(document.getElementById('maps'), {
          zoom: 15,
          center: bandung,
          scrollwheel: false
        });
        


        google.maps.event.addListener(map, 'click', function( event ){        
          $('#latitude').val(event.latLng.lat());
          $('#longitude').val(event.latLng.lng());


          placeMarker(event.latLng,map);
        });
      }

      function placeMarker(location,map) {

            marker = new google.maps.Marker({
                position: location,
                map: map              

            });

            if (oldMarker != undefined){
                oldMarker.setMap(null);
            }
            oldMarker = marker;
           

        }

   
                
   
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzixLw-QwjOpEiL8qmveEmafmgM27mgnk&callback=initMap">
    </script>
</body>
</html>