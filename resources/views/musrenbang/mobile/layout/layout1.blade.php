 <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>.bdg Musrenbang</title>
  <meta name="description" content="Bandung Web Kit" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/animate.css/animate.css" type="text/css" />
    <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/font-awesome/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/simple-line-icons/css/simple-line-icons.css" type="text/css" />
    <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/jquery/bootstrap/dist/css/bootstrap.css" type="text/css" />

  
  <link rel="stylesheet" href="{{ url('/') }}/mobile_assets/css/font.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/mobile_assets/fonts/simdaicon/simdaicon.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/mobile_assets/fonts/simdaicon/mobileicon.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/mobile_assets/css/style.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/mobile_assets/css/mobile.css" type="text/css" />


  @yield('css')

</head>

    <!-- content -->
    @yield('content')

    @yield('plugin')

</body>

</html>

