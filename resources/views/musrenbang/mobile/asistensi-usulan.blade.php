@extends('musrenbang.mobile.layout.layout3')


@section('content')
<div class="top-header bg-blue">
				<div class="container">
					<a href="#" class="menu-mobile pull-left active"><i class="fa fa-bars"></i></a>
					<p class="pull-left">DAFTAR USULAN</p>
					<a href="#" class="pull-right">
						<span class="badge badge-sm up bg-danger pull-right-xs">2</span>
						<i class="fa  fa-bell-o"></i>
					</a>
				</div>
			</div>

			<div class="tabs-alt-mobile-blue three" >
                <ul class="nav nav-tabs" role="tablist" id="tab-mobile">
                	<li  class="active">
                          <a data-target="#tab-1" role="tab" data-toggle="tab">
                            <span class="badge badge-sm">4</span> Baru
                          </a>
                    </li>
                    <li>
                          <a data-target="#tab-2" role="tab" data-toggle="tab">
                            <span class="badge badge-sm">5</span> Disetujui
                          </a>
                    </li>                    
                    <li>
                          <a data-target="#tab-3" role="tab" data-toggle="tab">
                            <span class="badge badge-sm">10</span> Ditolak
                          </a>
                    </li>                    
                </ul>
            </div>
            <div class="top-label">
				<p>RW. 01,  Kel. Kebon Jeruk, Kec Andir</p>
			</div>
			

			<div class="content container bg-grey">
				<div class="tab-content tab-content-alt-mobile">
					<div role="tabpanel" class="tab-pane active" id="tab-1">
							<form action="" class="form-horizontal">
								<!-- Search -->
								<div class="input-group m-b-md">			              
					              <select ui-jq="chosen" class="w-full">
					              	<option value="">Filter berdasarkan kategori issue</option>
					              	<option value="">issue1</option>
					              	<option value="">issue2</option>
					              </select>
					              <span class="input-group-addon bg-white "><i class="fa fa-caret-down"></i></span>
					            </div>
					        </form>

				            <div class="list-card box-shadow">                    		
				            	<div class="heading">
				            		<span class="badge badge-sm bg-green">RT 02</span>
				            		<p>Pembelian Bola Basket</p>
				            	</div>
				            	<div class="image">
	                    			<div id="maps"></div>
	                    		</div>
				            	<div class="list-content">
				            		<p>Olahraga Basket katang taruna menjadi trend di masyarakat balubur</p>
									
									<button type="submit" class="w-full m-b-md btn btn-green input-xl">Verifikasi Usulan</button>
				            	</div>
			            	</div>

			            	 <div class="list-card box-shadow">
                    		
				            	<div class="heading">
				            		<span class="badge badge-sm bg-green">RT 02</span>
				            		<p>Sarana dan Prasarana Kebersihan</p>
				            	</div>
				            	<div class="image">
	                    			<img src="{{ url('/') }}/mobile_assets/img/sarana.jpg" alt="">
	                    		</div>
				            	<div class="list-content">
				            		<p>Tempat sampah sudah rusak dan perlu diperbaharui, Demi kenyamanan masyarakat</p>
				            		
				            		<button type="submit" class="w-full m-b-md btn btn-green input-xl">Verifikasi Usulan</button>
				            	</div>
				            </div>
				    </div>

				    <div role="tabpanel" class="tab-pane " id="tab-2">
							<form action="" class="form-horizontal">
								<!-- Search -->
								<div class="input-group m-b-md">			              
					              <select ui-jq="chosen" class="w-full">
					              	<option value="">Filter berdasarkan kategori issue</option>
					              	<option value="">issue1</option>
					              	<option value="">issue2</option>
					              </select>
					              <span class="input-group-addon bg-white "><i class="fa fa-caret-down"></i></span>
					            </div>
					        </form>

				            <div class="list-card box-shadow">                    		
				            	<div class="heading">
				            		<span class="badge badge-sm bg-green">RT 02</span>
				            		<p>Pembelian Bola Basket</p>
				            	</div>
				            	<div class="image">
	                    			<div id="maps"></div>
	                    		</div>
				            	<div class="list-content">
				            		<p>Olahraga Basket katang taruna menjadi trend di masyarakat balubur</p>
									
									<button type="submit" class="w-full m-b-md btn btn-green input-xl">Verifikasi Usulan</button>
				            	</div>
			            	</div>

			            	 <div class="list-card box-shadow">
                    		
				            	<div class="heading">
				            		<span class="badge badge-sm bg-green">RT 02</span>
				            		<p>Sarana dan Prasarana Kebersihan</p>
				            	</div>
				            	<div class="image">
	                    			<img src="{{ url('/') }}/mobile_assets/img/sarana.jpg" alt="">
	                    		</div>
				            	<div class="list-content">
				            		<p>Tempat sampah sudah rusak dan perlu diperbaharui, Demi kenyamanan masyarakat</p>
				            		
				            		<button type="submit" class="w-full m-b-md btn btn-green input-xl">Verifikasi Usulan</button>
				            	</div>
				            </div>
				    </div>

				    <div role="tabpanel" class="tab-pane" id="tab-3">
							<form action="" class="form-horizontal">
								<!-- Search -->
								<div class="input-group m-b-md">			              
					              <select ui-jq="chosen" class="w-full">
					              	<option value="">Filter berdasarkan kategori issue</option>
					              	<option value="">issue1</option>
					              	<option value="">issue2</option>
					              </select>
					              <span class="input-group-addon bg-white "><i class="fa fa-caret-down"></i></span>
					            </div>
					        </form>

				            <div class="list-card box-shadow">                    		
				            	<div class="heading">
				            		<span class="badge badge-sm bg-green">RT 02</span>
				            		<p>Pembelian Bola Basket</p>
				            	</div>
				            	<div class="image">
	                    			<div id="maps"></div>
	                    		</div>
				            	<div class="list-content">
				            		<p>Olahraga Basket katang taruna menjadi trend di masyarakat balubur</p>
									
									<button type="submit" class="w-full m-b-md btn btn-green input-xl">Verifikasi Usulan</button>
				            	</div>
			            	</div>

			            	 <div class="list-card box-shadow">
                    		
				            	<div class="heading">
				            		<span class="badge badge-sm bg-green">RT 02</span>
				            		<p>Sarana dan Prasarana Kebersihan</p>
				            	</div>
				            	<div class="image">
	                    			<img src="{{ url('/') }}/mobile_assets/img/sarana.jpg" alt="">
	                    		</div>
				            	<div class="list-content">
				            		<p>Tempat sampah sudah rusak dan perlu diperbaharui, Demi kenyamanan masyarakat</p>
				            		
				            		<button type="submit" class="w-full m-b-md btn btn-green input-xl">Verifikasi Usulan</button>
				            	</div>
				            </div>
				    </div>

				   
				</div>
	             
			    
			</div>
			<!-- .container -->
@endsection


@section('plugin')
<script>
      var oldMarker;
      function initMap() {
        var bandung = {lat: -6.914744, lng: 107.609810};
        var map = new google.maps.Map(document.getElementById('maps'), {
          zoom: 17,
          center: bandung,
          navigationControl: false,
	      mapTypeControl: false,
	      scaleControl: false,
	      draggable: false,
	      scrollwheel: false,
        });
                
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzixLw-QwjOpEiL8qmveEmafmgM27mgnk&callback=initMap">
    </script>

@endsection      