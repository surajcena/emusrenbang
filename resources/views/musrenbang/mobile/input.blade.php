@extends('musrenbang.mobile.layout.layout3')


@section('content')

      <div class="top-header bg-blue">
        <div class="container">
          <a href="#" class="menu-mobile pull-left active"><i class="fa fa-bars"></i></a>
          <p class="pull-left">INPUT USULAN {{$tahun}}</p>
          <a href="#" class="pull-right">
            <span class="badge badge-sm up bg-danger pull-right-xs">2</span>
            <i class="fa  fa-bell-o"></i>
          </a>
        </div>
      </div>


      <div class="content container bg-grey">
        <div class="list-detail">         

          <div class="list-content input-usulan">

              @if (count($errors) > 0)
                              <div class="alert alert-danger">
                                  <ul>
                                      @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                              </div>
                          @endif
            <form action="" class="form-horizontal">
              <span class="label">Isu</span>
                      <div class="input-group m-b-md" name="isu" id="isu">                    
                        <select ui-jq="chosen" class="w-full">
                          <option>Pilih Isu</option>
                          @foreach ($isu as $isu)
                                            <option value="{{$isu->ISU_ID}}" style="color:black">{{$isu->ISU_NAMA}}</option>
                                     @endforeach
                        </select>
                        <span class="input-group-addon bg-white "><i class="fa fa-caret-down"></i></span>
                      </div>


                       <span class="label">Kamus</span>
                      <div class="input-group m-b-md" name="kamus" id="kamus">                    
                        <select ui-jq="chosen" class="w-full">
                          
                        </select>
                        <span class="input-group-addon bg-white "><i class="fa fa-caret-down"></i></span>
                      </div>

                    

                    <span class="label">Volume</span>
                      <div class="input-group m-b-md">                        
                        <input type="text" class="w-full input-xxl no-borders form-control" placeholder="Input Besaran Volume" name="vol">
                      </div>

                      <span class="label">Satuan</span>
                      <div class="input-group m-b-md">                        
                        <input type="text" class="w-full input-xxl no-borders form-control" id="satuan" readonly>
                      </div>

                      <span class="label" id="div-upload-gambar">Upload</span>

                      <div class="upload-button" style="display:none" id="div-upload-gambar">
                        <input type="file" name="image[]">
                        <button class="w-full no-borders btn btn-grey"><i class="fa fa-upload"></i> Upload Image</button>
                        <button class="w-full no-borders btn btn-info" id="div-upload-gambar upload1" type="button"><i class="fa fa-plus"> Tambah Gambar</i></button>
                      </div>
                       <div class="upload-button" style="display:none" id="div-upload-gambar div1">
                        <input type="file" name="image[]">
                        <button class="w-full no-borders btn btn-grey"><i class="fa fa-upload"></i> Upload Image</button>
                      </div>

                      


              <span class="label">Lokasi RT</span>
                      <div class="input-group m-b-md"> 
                           @foreach ($rt as $rt)                
                          <label class="checkbox-inline">
                          <input type="checkbox" class="" value="{{$rt->RT_ID}}" name="rt[]"> {{$rt->RT_NAMA}}
                        </label>
                        @endforeach  
                      </div>

                      <span class="label">Urgensi</span>
                      <div class="input-group m-b-md">                        
                        <input type="text" class="w-full input-xxl no-borders form-control" placeholder="Beri Keterangan Urgensi" name="urgensi">
                      </div>

                    <span class="label">Peta Lokasi</span>
                    <div class="image">
                        <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                        <div id="maps"></div>                       
                      </div>

                      


                      <input type="text" value="{{$usulan_tujuan}}" name="usulan_tujuan">

                    
                    <button type="button" class="w-full btn btn-green input-xxl"><i class="fa fa-plus"></i>TAMBAHKAN USULAN</button>
            </form>
                </div>
        </div>

      </div>
      <!-- .container -->
						

@endsection


@section('plugin')
<script type="text/javascript">
      var oldMarker;
      function initMap() {
        var bandung = {lat: -6.914744, lng: 107.609810};
        var map = new google.maps.Map(document.getElementById('maps'), {
          zoom: 16,
          center: bandung,
          scrollwheel: false,
          
        });

        google.maps.event.addListener(map, 'click', function( event ){
          $('#latitude').val(event.latLng.lat());
          $('#longitude').val(event.latLng.lng());


          placeMarker(event.latLng,map);
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // [START region_getplaces]
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
          return;
        }

        // Clear out the old markers.
        markers.forEach(function(marker) {
          marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          };

          // Create a marker for each place.
          markers.push(new google.maps.Marker({
            map: map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
          }));

          if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        map.fitBounds(bounds);
        });
        // [END region_getplaces]
      }

      function placeMarker(location,map) {
            marker = new google.maps.Marker({
                position: location,
                map: map

            });

            if (oldMarker != undefined){
                oldMarker.setMap(null);
            }
            oldMarker = marker;
        }
</script>
<script>
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

function initAutocomplete() {
var map = new google.maps.Map(document.getElementById('maps'), {
center: {lat: -33.8688, lng: 151.2195},
zoom: 13,
mapTypeId: google.maps.MapTypeId.ROADMAP
});

// Create the search box and link it to the UI element.
var input = document.getElementById('pac-input');
var searchBox = new google.maps.places.SearchBox(input);
map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

// Bias the SearchBox results towards current map's viewport.
map.addListener('bounds_changed', function() {
searchBox.setBounds(map.getBounds());
});

var markers = [];
// [START region_getplaces]
// Listen for the event fired when the user selects a prediction and retrieve
// more details for that place.
searchBox.addListener('places_changed', function() {
var places = searchBox.getPlaces();

if (places.length == 0) {
  return;
}

// Clear out the old markers.
markers.forEach(function(marker) {
  marker.setMap(null);
});
markers = [];

// For each place, get the icon, name and location.
var bounds = new google.maps.LatLngBounds();
places.forEach(function(place) {
  var icon = {
    url: place.icon,
    size: new google.maps.Size(71, 71),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(17, 34),
    scaledSize: new google.maps.Size(25, 25)
  };

  // Create a marker for each place.
  markers.push(new google.maps.Marker({
    map: map,
    icon: icon,
    title: place.name,
    position: place.geometry.location
  }));

  if (place.geometry.viewport) {
    // Only geocodes have viewport.
    bounds.union(place.geometry.viewport);
  } else {
    bounds.extend(place.geometry.location);
  }
});
map.fitBounds(bounds);
});
// [END region_getplaces]
}


</script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzixLw-QwjOpEiL8qmveEmafmgM27mgnk&callback=initMap&libraries=places">
    </script>

<script>
    $(document).ready(function(){

        $("#upload1").click(function(){
            $("#div1").fadeIn("fast");
            $("#upload").addClass("addGambar");
        });

        $("#isu").change(function(e, params){
            var id  = $('#isu').val();
            $('#kamus').find('option').remove().end().append('<option>Kamus Usulan</option>');
            $.ajax({
              type  : "get",
              url   : "{{ url('/musrenbang/2017/mobile/data/kamus/') }}"+id,
              success : function (data) {
                $('#kamus').append(data['opt']).trigger('chosen:updated');
                if(data['tipe'] == 1 || data['tipe'] == 2) $('#div-upload-gambar').fadeIn('fast').removeClass('hide');
                else $('#div-upload-gambar').fadeOut('fast').addClass('hide');
              }
            });
        });

        $("#kamus").change(function(e, params){
            var id  = $('#kamus').val();
            $('#satuan').val();
            $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/mobile/data/satuan/"+id,
              success : function (data) {
                $('#satuan').val(data);
              }
            });
        });

    });
</script>

@endsection      