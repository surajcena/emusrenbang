@extends('musrenbang.mobile.layout.layout2')


@section('content')
			

			<div class="content container bg-grey">

	            <div class="list-card box-shadow">
	            	<div class="heading">
	            		<i class="fa fa-book"></i>
	            		<h4><a href="">RENJA</a></h4>
	            	</div>
	            	<div class="list-content">
	            		<p>{{$renjadiproses}} Proses, {{$renjaditerima}} Terima , {{$renjaditolak}} Tolak  </p>
	            		<a href="{{ url('/') }}/musrenbang/2017/usulan/tambah/1">
	            			<button type="submit" class="w-full m-b-md btn btn-green input-xxl"><i class="fa fa-pencil"></i>INPUT USULAN RENJA</button>
	            		</a>	
	            	</div>
	            </div>

	               <div class="list-card box-shadow">
	            	<div class="heading">
	            		<i class="fa fa-book"></i>
	            		<h4><a href="">PIPPK</a></h4>
	            	</div>
	            	<div class="list-content">
	            		<p>{{$pippkdiproses}} Proses, {{$pippkditerima}} Terima , {{$pippkditolak}} Tolak  </p>
	            		<a href="{{ url('/') }}/musrenbang/2017/usulan/tambah/2">
	            			<button type="submit" class="w-full m-b-md btn btn-green input-xxl"><i class="fa fa-pencil"></i>INPUT USULAN PIPPK</button>
	            		</a>	
	            	</div>
	            </div>
			    
			</div>
@endsection


@section('plugin')
<script>
      var oldMarker;
      function initMap() {
        var bandung = {lat: -6.914744, lng: 107.609810};
        var map = new google.maps.Map(document.getElementById('maps'), {
          zoom: 17,
          center: bandung,
          navigationControl: false,
	      mapTypeControl: false,
	      scaleControl: false,
	      draggable: false,
	      scrollwheel: false,
        });
                
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzixLw-QwjOpEiL8qmveEmafmgM27mgnk&callback=initMap">
    </script>
@endsection      