@extends('musrenbang.mobile.layout.layout2')


@section('content')

			<div class="tabs-alt-mobile" >
                <ul class="nav nav-tabs" role="tablist" id="tab-mobile">
                	<li  class="active">
                          <a data-target="#tab-1" role="tab" data-toggle="tab">
                           <!--  <span class="badge badge-sm">2</span> --> Diproses
                          </a>
                    </li>
                    <li>
                          <a data-target="#tab-2" role="tab" data-toggle="tab">
                            <!-- <span class="badge badge-sm">4</span> --> Disetujui
                          </a>
                    </li>
                    <li>
                          <a data-target="#tab-3" role="tab" data-toggle="tab">
                           <!--  <span class="badge badge-sm">6</span>  -->Ditolak
                          </a>
                    </li>
                </ul>
            </div>
			

			<div class="content container bg-grey">
				<!--  tab 1  -->
				<div class="tab-content tab-content-alt-mobile">
					<div role="tabpanel" class="tab-pane active" id="tab-1">

			            <div class="list-card box-shadow">
                    		
			            	<div class="heading">
			            		<span class="badge badge-sm bg-green">RT 02</span>
			            		<p>ISU : {{ $rw->kamus->isu->ISU_NAMA }}, Kamus {{ $rw->kamus->KAMUS_NAMA }}</p>
			            	</div>
			            	<div class="image">
                    			@foreach($rw->USULAN_GAMBAR as $img)
                                  <img src="{{ url('/') }}/uploads/{{ $img }}">
                                @endforeach
                    		</div>
			            	<div class="list-content">
			            		<p>Tempat sampah sudah rusak dan perlu diperbaharui, Demi kenyamanan masyarakat</p>
			            		
			            		<ul class="progress-tracker">
								  <li class="complete">
								    <div class="step"><i class="fa fa-check"></i></div>
								    <div class="label">RW</div>
								  </li>
								  @if($timeline->USULAN_POSISI == 2 && $timeline->USULAN_STATUS == 2)
								  <li class="complete">
								    <div class="step"><i class="fa fa-check"></i></div>
								    <div class="label">KEL</label>
								  </li>
								  @if($timeline->USULAN_POSISI == 3 && $timeline->USULAN_STATUS == 2)
								  <li>
								    <div class="step"><i class="fa fa-check"></i></div>
								    <div class="label">KEC</div>
								  </li>
								  @if($timeline->USULAN_POSISI == 4 && $timeline->USULAN_STATUS == 2)
								  <li>
								    <div class="step"><i class="fa fa-check"></i></div>
								    <div class="label">KOTA</div>
								  </li>
								  @if($timeline->USULAN_POSISI == 6 && $timeline->USULAN_STATUS == 2)
								  <li>
								    <div class="step"><i class="fa fa-check"></i></div>
								    <div class="label">APBD</div>
								  </li>
								  @endif
								</ul>

								<div class="button-action">
									<a href="#"><i class="fa fa-trash-o"></i> Delete</a>
									<a href="#"><i class="fa fa-pencil-square-o"></i> Edit</a>
									<a href="#" class="active"><i class="fa fa-eye"></i> Rincian</a>
								</div>
			            	</div>
			            </div>
						
						<div class="fixed-button">
							<a href="{{ url('musrenbang/2017/mobile/dashboard') }}">
			            	<button type="button" class="w-full btn btn-green input-xxl"><i class="fa fa-plus"></i>TAMBAHKAN USULAN
			          		</a>
			            </div>
                    	
                    </div>

                   <!--  tab 2  -->
					<div role="tabpanel" class="tab-pane" id="tab-2">

                    	<div class="list-card box-shadow">                    		
			            	<div class="heading">
			            		<span class="badge badge-sm bg-green">RT 02</span>
			            		<p>Pembelian Bola Basket</p>
			            	</div>
			            	<div class="image">
                    			<div id="maps"></div>
                    		</div>
			            	<div class="list-content">
			            		<p>Olahraga Basket katang taruna menjadi trend di masyarakat balubur</p>
			            		
			            		<button type="button" class="w-full m-b-md btn btn-soft-green-notif input-xxl"><i class="fa fa-check-circle-o"></i>Usulan Disetujui</button>
			            	</div>
			            </div>

			            <div class="list-card box-shadow">
                    		
			            	<div class="heading">
			            		<span class="badge badge-sm bg-green">RT 02</span>
			            		<p>Sarana dan Prasarana Kebersihan</p>
			            	</div>
			            	<div class="image">
                    			<img src="img/sarana.jpg" alt="">
                    		</div>
			            	<div class="list-content">
			            		<p>Tempat sampah sudah rusak dan perlu diperbaharui, Demi kenyamanan masyarakat</p>
			            		
			            		<button type="button" class="w-full m-b-md btn btn-soft-green-notif input-xxl"><i class="fa fa-check-circle-o"></i>Usulan Disetujui</button>
			            	</div>
			            </div>
						
						<div class="fixed-button">
			            	<button type="button" class="w-full btn btn-green input-xxl"><i class="fa fa-plus"></i>TAMBAHKAN USULAN
			            </div>
                    	
                    </div>


                    <!--  tab 3  -->
                    <div role="tabpanel" class="tab-pane " id="tab-3">

                    	<div class="list-card box-shadow">                    		
			            	<div class="heading">
			            		<span class="badge badge-sm bg-green">RT 02</span>
			            		<p>Pembelian Bola Basket</p>
			            	</div>
			            	<div class="image">
                    			<div id="maps"></div>
                    		</div>
			            	<div class="list-content">
			            		<p>Olahraga Basket katang taruna menjadi trend di masyarakat balubur</p>
			            		
			            		<button type="button" class="w-full m-b-md btn btn-soft-red input-xxl"><i class="fa fa-close"></i>Dana Tidak Mencukupi Usulan</button>
			            	</div>
			            </div>

			            <div class="list-card box-shadow">
                    		
			            	<div class="heading">
			            		<span class="badge badge-sm bg-green">RT 02</span>
			            		<p>Sarana dan Prasarana Kebersihan</p>
			            	</div>
			            	<div class="image">
                    			<img src="img/sarana.jpg" alt="">
                    		</div>
			            	<div class="list-content">
			            		<p>Tempat sampah sudah rusak dan perlu diperbaharui, Demi kenyamanan masyarakat</p>
			            		
			            		<button type="button" class="w-full m-b-md btn btn-soft-red input-xxl"><i class="fa fa-close"></i>Coba lagi dlm beberapa bulan kedepan</button>
			            	</div>
			            </div>
						
						<div class="fixed-button">
			            	<button type="button" class="w-full btn btn-green input-xxl"><i class="fa fa-plus"></i>TAMBAHKAN USULAN
			            </div>
                    	
                    </div>
                </div>

	            

	             
			    
			</div>
			<!-- .container -->
@endsection


@section('plugin')

@endsection      