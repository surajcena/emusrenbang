@extends('musrenbang.mobile.layout.layout2')


@section('content')
<div class="content container bg-grey">
				
				<form action="" class="form-horizontal">
					<!-- Search -->
					<div class="input-group m-b-md">			              
		              <input type="text" class="input-xxl no-borders form-control" placeholder="Cari Nama Program">
		              <span class="input-group-addon bg-white "><i class="fa fa-search"></i></span>
		            </div>
		        </form>

	            <div class="list-card box-shadow">
	            		<div class="heading full">	            		
	            			<p>Dukungan Kelancaran Pemilihan Umum</p>

		            	</div>
		            	
		            	<div class="list-content">
		            		<p>Total Perangkat Daerah : <span>12</span></p>
		            		<p>Total Kegiatan : <span>210  dari 8 RW</span></p>
		            		<p>Total Anggaran : <span>Rp. 10.000.000,00</span></p>
		            		
		            	</div>
	            </div>
	            
				<div class="list-card box-shadow">
	            		<div class="heading full">	            		
	            			<p>Orientasi Peningkatan Kapasitas Informasi</p>

		            	</div>
		            	
		            	<div class="list-content">
		            		<p>Total Perangkat Daerah : <span>12</span></p>
		            		<p>Total Kegiatan : <span>210  dari 8 RW</span></p>
		            		<p>Total Anggaran : <span>Rp. 10.000.000,00</span></p>
		            		
		            	</div>
	            </div>

	            <div class="list-card box-shadow">
	            		<div class="heading full">	            		
	            			<p>Pemantapan wawasan Kebangsaan</p>

		            	</div>
		            	
		            	<div class="list-content">
		            		<p>Total Perangkat Daerah : <span>12</span></p>
		            		<p>Total Kegiatan : <span>210  dari 8 RW</span></p>
		            		<p>Total Anggaran : <span>Rp. 10.000.000,00</span></p>
		            		
		            	</div>
	            </div>

	             
			    
			</div>
			<!-- .container -->
@endsection


@section('plugin')

@endsection      