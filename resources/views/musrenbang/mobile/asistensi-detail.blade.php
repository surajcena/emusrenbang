@extends('musrenbang.mobile.layout.layout2')


@section('content')
			

			<div class="content container bg-grey">
				<div class="list-detail">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						  <!-- Indicators -->
						  <ol class="carousel-indicators">
						    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
						    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
						    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
						  </ol>

						  
						  <div class="carousel-inner" role="listbox">
						    <div class="item active">
						      	  <img src="{{ url('/') }}/mobile_assets/img/sarana.jpg" alt="">
						    </div>

						    <div class="item">
						      	  <img src="{{ url('/') }}/mobile_assets/img/sarana.jpg" alt="">
						    </div>

						    <div class="item">
						      	  <img src="{{ url('/') }}/mobile_assets/img/sarana.jpg" alt="">
						    </div>
						    
						  </div>

						  
					</div>

					<div class="list-content">
						<span class="label">Isu</span>
	            		<p>Sarana dan Prasarana Kebersihan</p>

	            		<span class="label">Urgensi</span>
	            		<p>Tempat sampah sudah rusak dan perlu diperbaharui demi, kenyamanan masyarakat</p>

	            		<span class="label">Volume</span>
	            		<p>55 set tempat sampah</p>

						<span class="label">Lokasi RT</span>
	            		<p>Jl. Cisitu, Kec. Coblong, RW 08, RT 02, Bandung </p>	

	            		<span class="label">Prioritas Usulan</span>
	            		<p>Sampah-sampah berceceran di jalan raya</p>	            		

	            		<span class="label">Peta Lokasi</span>
	            		<div class="image">
                			<div id="maps"></div>
                		</div>


	            		
	            		<button type="submit" class="w-half m-r-sm m-b-md  btn btn-danger input-xl">Ditolak</button>
	            		<button type="submit" class="w-half m-b-md btn btn-green input-xl">Disetujui</button>
	            	</div>
				</div>

			</div>
			<!-- .container -->
@endsection


@section('plugin')
<script>
      var oldMarker;
      function initMap() {
        var bandung = {lat: -6.914744, lng: 107.609810};
        var map = new google.maps.Map(document.getElementById('maps'), {
          zoom: 17,
          center: bandung,
          navigationControl: false,
	      mapTypeControl: false,
	      scaleControl: false,
	      draggable: false,
	      scrollwheel: false,
        });
                
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzixLw-QwjOpEiL8qmveEmafmgM27mgnk&callback=initMap">
    </script>

@endsection      