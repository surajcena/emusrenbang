@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Kelola RW </li>                                
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">

                    <h5 class="inline font-semibold text-orange m-n ">Kelola  Rw </h5>

                  </div>           
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                             <table ui-jq="dataTable" ui-options="{
                                    sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/pengaturan/rw/getrw/{{$id}}',
                                    aoColumns: [
                                    { mData: 'NO' },
                                    { mData: 'USER' },
                                    { mData: 'RW_NAMA' },
                                    { mData: 'RW_KETUA' },
                                    { mData: 'NIK' },
                                    { mData: 'TELP' },
                                    { mData: 'STATUS' },
                                    { mData: 'AKSI' }
                                    ]}" class="table table-striped b-t b-b" id="table-tahapan">
                                    <thead>
                                      <tr>
                                        <th>No</th>
                                        <th>Username</th>
                                        <th>Nomor RW</th>
                                        <th>Ketua</th>
                                        <th>N.I.K</th>
                                        <th>No Telepon</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                      </tr>
                                      <tr>
                                        <th colspan="8" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari Tahapan" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>

<div id="form-tinjau" class="modal fade" role="dialog">
<form method="post" action="{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/simpan/profile/rw" id="formtinjau">
{{ csrf_field() }}
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Profile RW</h4>
      </div>
      <div class="modal-body">
         <div class="form-group">
            <label class="col-sm-4">Nomor RW</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" name="RW_NAMA" required="" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">Nama Ketua RW</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="RW_KETUA" required="" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">NIK Ketua RW</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" name="RW_NIK" required="" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">No Tlp/Hp</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" name="RW_TELP" required="" value="" length="5">
            </div>
          </div>
          <input type="hidden" class="form-control" name="RW_ID" required="" value="" >
      </div>
      <div class="modal-footer">
        
       
          <button type="submit" class="btn btn-info">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        
      </div>
    </div>
  </div>
</form>
</div>
@endsection

@section('plugin')
<script type="text/javascript">
  function reset(id){
    $.confirm({
        title: 'Reset Password!',
        content: 'Yakin reset password?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                    url: "{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/rw/reset/"+id,
                    type: "GET",
                    success: function(msg){
                      $.alert(msg);
                    }
                  });    
                }
            },
            Tidak: function () {
            }
        }
    });
  }

  function getProfil(id){
        
      $.ajax({
          method: 'GET', // Type of response and matches what we said in the route
          url: "{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/rw/getProfile/"+id,
           // a JSON object to send back
          success: function(response){ // What to do if we succeed
              console.log(response); 
              $("#formtinjau input[name='RW_NAMA']").val(response.RW_NAMA);
              $("#formtinjau input[name='RW_KETUA']").val(response.RW_KETUA);
              $("#formtinjau input[name='RW_NIK']").val(response.RW_NIK);
              $("#formtinjau input[name='RW_TELP']").val(response.RW_TELP);
              $("#formtinjau input[name='RW_ID']").val(response.RW_ID);

          },
          error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
              console.log(JSON.stringify(jqXHR));
              console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
          }
    }); 
    
  }

  function active(id){
    $.confirm({
        title: 'Aktivasi account!',
        content: 'Yakin Account di Aktifkan?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                    url: "{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/aktivasi/"+id,
                    type: "GET",
                    success: function(msg){
                      $.alert(msg);
                    }
                  });    
                }
            },
            Tidak: function () {
            }
        }
    });
  }

  function nonActive(id){
    $.confirm({
        title: 'Matikan account!',
        content: 'Yakin Account di Matikan?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                    url: "{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/nonAktivasi/"+id,
                    type: "GET",
                    success: function(msg){
                      $.alert(msg);
                    }
                  });    
                }
            },
            Tidak: function () {
            }
        }
    });
  }
</script>
@endsection


