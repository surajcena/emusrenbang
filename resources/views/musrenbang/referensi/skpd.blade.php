@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

      	<div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Kelola SKPD</li>                                
              </ul>
          </div>

           <div class="wrapper-lg">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">Kelola SKPD</h5>
                  </div>           
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                             <table ui-jq="dataTable" ui-options="{
                                    sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/pengaturan/skpd/getSkpd',
                                    aoColumns: [
                                    { mData: 'NO' },
                                    { mData: 'SKPD_TAHUN' },
                                    { mData: 'SKPD_BIDANG' },
                                    { mData: 'SKPD_KODE' },
                                    { mData: 'SKPD_NAMA' },
                                    { mData: 'SKPD_KEPALA' },
                                    { mData: 'SKPD_KEPALA_NIP' },
                                    { mData: 'SKPD_BENDAHARA' },
                                    { mData: 'SKPD_BENDAHARA_NIP' },
                                    { mData: 'STAT' },
                                    { mData: 'AKSI' }
                                    ]}" class="table table-striped b-t b-b" id="table-tahapan">
                                    <thead>
                                      <tr>
                                      	<th>NO</th>
                                        <th>TAHUN</th>
                                        <th>Bidang</th>
                                        <th>Kode SKPD</th>
                                        <th>Nama SKPD</th>
                                        <th>Kepala SKPD</th>
                                        <th>NIP Kepala</th>
                                        <th>Bendahara</th>
                                        <th>NIP Bendahara</th>
                                        <th>Status</th>
                                        <th>AKSI</th>
                                      </tr>
                                      <tr>
                                        <th colspan="11" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari Kelurahan" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                  </div>
                </div>                
              </div>
            </div>
          </div>

        </div>  

     </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div> 	


<div id="form-tinjau" class="modal fade" role="dialog">
<form method="post" action="{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/simpan/profile/skpd" id="formtinjau">
{{ csrf_field() }}
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Profile SKPD</h4>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label class="col-sm-4">KODE SKPD</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="SKPD_KODE" required="" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">NAMA SKPD</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="SKPD_NAMA" required="" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">KEPALA SKPD</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="SKPD_KEPALA" required="" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">NIP KEPALA SKPD</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="SKPD_KEPALA_NIP" required="" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">BENDAHARA SKPD</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="SKPD_BENDAHARA" required="" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">NIP BENDAHARA SKPD</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="SKPD_BENDAHARA_NIP" required="" value="">
            </div>
          </div>
          <input type="hidden" class="form-control" name="SKPD_ID" required="" value="" >
      </div>
      <div class="modal-footer">
        
       
          <button type="submit" class="btn btn-info">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
       
      </div>
    </div>
  </div>
</form>
</div>

@endsection

@section('plugin')
<script type="text/javascript">
  function reset(id){
    $.confirm({
        title: 'Reset Password!',
        content: 'Yakin reset password?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                    url: "{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/skpd/reset/"+id,
                    type: "GET",
                    success: function(msg){
                      $.alert(msg);
                    }
                  });    
                }
            },
            Tidak: function () {
            }
        }
    });
  }

  function getProfil(id){
        
      $.ajax({
          method: 'GET', // Type of response and matches what we said in the route
          url: "{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/skpd/getProfile/"+id,
           // a JSON object to send back
          success: function(response){ // What to do if we succeed
              console.log(response); 
              $("#formtinjau input[name='SKPD_KODE']").val(response.SKPD_KODE);
              $("#formtinjau input[name='SKPD_NAMA']").val(response.SKPD_NAMA);
              $("#formtinjau input[name='SKPD_KEPALA']").val(response.SKPD_KEPALA);
              $("#formtinjau input[name='SKPD_KEPALA_NIP']").val(response.SKPD_KEPALA_NIP);
              $("#formtinjau input[name='SKPD_BENDAHARA']").val(response.SKPD_BENDAHARA);
              $("#formtinjau input[name='SKPD_BENDAHARA_NIP']").val(response.SKPD_BENDAHARA_NIP);
              $("#formtinjau input[name='SKPD_ID']").val(response.SKPD_ID);

          },
          error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
              console.log(JSON.stringify(jqXHR));
              console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
          }
    }); 
    
  }

  function active(id){
    $.confirm({
        title: 'Aktivasi account!',
        content: 'Yakin Account di Aktifkan?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                    url: "{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/aktivasi/"+id,
                    type: "GET",
                    success: function(msg){
                      $.alert(msg);
                    }
                  });    
                }
            },
            Tidak: function () {
            }
        }
    });
  }

  function nonActive(id){
    $.confirm({
        title: 'Matikan account!',
        content: 'Yakin Account di Matikan?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                    url: "{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/nonAktivasi/"+id,
                    type: "GET",
                    success: function(msg){
                      $.alert(msg);
                    }
                  });    
                }
            },
            Tidak: function () {
            }
        }
    });
  }
</script>
@endsection