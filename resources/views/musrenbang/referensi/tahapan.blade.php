@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Tahapan</li>                                
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">Tahapan Tahun {{ $tahun }}</h5>
                  </div>           
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                             <table ui-jq="dataTable" ui-options="{
                                    sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/tahapan/getData',
                                    aoColumns: [
                                    { mData: 'ID_TAHAPAN',class:'hide' },
                                    { mData: 'MENU',class:'text-center' },
                                    { mData: 'KET' },
                                    { mData: 'TGL_AWAL' },
                                    { mData: 'TGL_AKHIR' }
                                    ]}" class="table table-striped b-t b-b" id="table-tahapan">
                                    <thead>
                                      <tr>
                                        <th class="hide"></th>
                                        <th>No</th>
                                        <th>Tahapan</th>
                                        <th>Awal</th>
                                        <th>Akhir</th>
                                      </tr>
                                      <tr>
                                        <th class="hide"></th>
                                        <th colspan="4" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari Tahapan" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>

<div class="overlay"></div>
<div class="bg-white wrapper-lg input-sidebar input-tahapan">
<a href="#" class="tutup-form"><i class="icon-bdg_cross"></i></a>
    <form id="form-urusan" class="form-horizontal">
      <div class="input-wrapper">
        <h5 id="judul-form">Ubah Tahapan</h5>
          <div class="form-group">
            <label for="kode_urusan" class="col-md-3">Tahapan</label>          
            <div class="col-sm-9">
              <input type="text" class="form-control" id="tahapan"  readonly="">
              <input type="hidden" class="form-control" value="{{ csrf_token() }}" name="_token" id="token">          
              <input type="hidden" class="form-control" name="id_tahapan" id="id_tahapan">          
            </div> 
          </div>

          <div class="form-group">
            <label for="awal" class="col-md-3">Tanggal Awal</label>          
            <div class="col-sm-9">
            <input type="text" ui-jq="daterangepicker" ui-options="{singleDatePicker:true,format:'YYYY-MM-DD',timePicker: true}" placeholder="Tanggal Awal" class="form-control" id="awal">
            </div> 
          </div>

          <div class="form-group">
            <label for="awal" class="col-md-3">Tanggal Akhir</label>          
            <div class="col-sm-9">
            <input type="text" ui-jq="daterangepicker" ui-options="{singleDatePicker:true,format:'YYYY-MM-DD',timePicker: true}" placeholder="Tanggal Akhir" class="form-control" id="akhir">
            </div> 
          </div>

          <hr class="m-t-xl">
          <a class="btn input-xl m-t-md btn-success pull-right" onclick="return simpanTahapan()"><i class="fa fa-plus m-r-xs "></i>Simpan</a>
      </div>
    </form>
  </div>
 </div>
@endsection

@section('plugin')
<script type="text/javascript">
  function simpanTahapan(){
    var id_tahapan   = $('#id_tahapan').val();
    var awal         = $('#awal').val();
    var akhir        = $('#akhir').val();
    var token        = $('#token').val();
    console.log(awal);
    console.log(akhir);
    if(awal == "" || akhir == "" || tahapan == ""){
      $.alert('Form harap diisi!');
    }else{
      uri = "{{ url('/') }}/musrenbang/{{ $tahun }}/tahapan/submit";
      $.ajax({
        url: uri,
        type: "POST",
        data: {'_token'         : token,
              'id_tahapan'      : id_tahapan,
              'awal'            : awal, 
              'akhir'           : akhir},
        success: function(msg){
            if(msg == 1){
              $('#tahapan').val('');
              $('#awal').val('');
              $('#akhir').val('');
              $('#table-tahapan').DataTable().ajax.reload();              
              $.alert({
                title:'Info',
                content: 'Data berhasil disimpan',
                autoClose: 'ok|1000',
                buttons: {
                    ok: function () {
                      $('.input-sidebar,.input-tahapan').animate({'right':'-1050px'},function(){
                        $('.overlay').fadeOut('fast');
                      });
                      $('#table-tahapan').DataTable().ajax.reload();                      
                    }
                }
              });
            }else{
              $.alert('Data telah tersedia!');
            }
          }
        });
    }
  }


  $('#table-tahapan').on('click','tbody > tr', function(){
        kode = $(this).children('td').eq(0).html();
        $.ajax({
          url: "{{ url('/') }}/musrenbang/{{ $tahun }}/tahapan/getData/"+kode,
          type: "GET",
          success: function(msg){
            $('#tahapan').val(msg['KET']);
            $('#id_tahapan').val(msg['ID_TAHAPAN']);
            $('#awal').val(msg['TGL_AWAL']);
            $('#akhir').val(msg['TGL_AKHIR']);
            $('.overlay').fadeIn('fast',function(){
              $('.input-tahapan').animate({'right':'0'},"linear");  
              $("html, body").animate({ scrollTop: 0 }, "slow");
            });
          }
        });
  })

  $('.overlay').on('click',function(){      
      $('.input-sidebar').animate({'right':'-1050px'},"linear",function(){
        $('.overlay').fadeOut('fast');
      }); 
        $('#awal').val('');
        $('#akhir').val('');
        $('#id_tahapan').val('');
        $('#btn-tutup').attr('disabled',true);  
  });
</script>
@endsection


