<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

//Route::get
Route::get('/',function() {
	//return redirect('login');
	if(isMobile()){
		return redirect('musrenbang/2017/mobile/dashboard');
		//return redirect('login');
	}else{
		return redirect('musrenbang/2017');
	}

});
// Route::get('/login',function() {
// 	//return redirect('login');
// 	if(isMobile()){
// 		return redirect('musrenbang/2017/mobile/login');
// 		//return redirect('login');
// 	}else{
// 		return redirect('login');
// 	}
// });

//topsis
Route::get('/musrenbang/{tahun}/topsis/daftarUsulan', 'Musrenbang\topsisController@topsis_daftar_usulan');
Route::get('/musrenbang/{tahun}/topsis/usulanLayak', 'Musrenbang\topsisController@index');

//usulan dari ebudgeting
Route::get('/musrenbang/{tahun}/api/ebudgeting/usulan', 'Musrenbang\publicController@getApiEbudgeting');

//dari EMUSRENBANG
Route::get('/musrenbang/{tahun}/api/emusrenbang/usulan', 'Musrenbang\publicController@apiMusrenbang');
Route::get('/public/{tahun}/api/emusrenbang/usulan', 'apiController@apiMusrenbang');
Route::get('/public/{tahun}/api/musrenbang/usulan', 'apiController@apiMus');
Route::get('/public/{tahun}/api/pippk/usulan', 'apiController@apiPIPPK');
Route::get('/public/{tahun}/api/musrenbang/tolak', 'apiController@apiMusTolak');
Route::get('/public/{tahun}/api/pippk/tolak', 'apiController@apiPIPPKTolak');

//tracking
Route::get('/musrenbang/{tahun}/public/tracking', 'Musrenbang\publicController@trackingUsulan');
Route::get('/musrenbang/{tahun}/public/get/kelurahan/tracking/{id}', 'Musrenbang\publicController@getKelurahanTracking');
Route::get('/musrenbang/{tahun}/public/get/rw/tracking/{id}', 'Musrenbang\publicController@getRwTracking');
Route::get('/musrenbang/{tahun?}/public/tracking/data/{status}/{kecamatan?}/{kelurahan?}/{rw?}/{tipe?}', 'Musrenbang\publicController@trackingGetdata');
Route::get('/musrenbang/{tahun}/public/tracking/detail/{id}', 'Musrenbang\publicController@trackingUsulanDetail');
Route::get('/musrenbang/{tahun}/public/album', 'Musrenbang\publicController@album');


//kamus usulan
Route::get('/musrenbang/{tahun}/usulan/kamus/get/isu/{id}', 'Musrenbang\usulanController@getKamusIsu');
Route::get('/musrenbang/{tahun}/usulan/kamus/get/kamus/kamus/{id}', 'Musrenbang\usulanController@getKamusKamus');


//belajar redis
Route::get('/musrenbang/user/redis/latihan1/{id}/{name}/{title}', 'Musrenbang\UserController@setRedis');
Route::get('/musrenbang/user/redis/latihan2', 'Musrenbang\UserController@getRedis');


Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/register', 'Auth\LoginController@logout');

//MUSRENBANG-GET
Route::get('/keluar', 'Musrenbang\mainController@logout');
//Route::get('/pesanerror', 'Musrenbang\publicController@pesanerror');
Route::get('/keepalive', 'keepaliveController@keepalive');
Route::get('/musrenbang/{tahun}', 'Musrenbang\mainController@index');

Route::get('/musrenbang/{tahun}/usulan', 'Musrenbang\usulanController@index');
Route::get('/musrenbang/usulan/history/{usulan_id}', 'Musrenbang\usulanController@showHistory');

Route::get('/musrenbang/{tahun}/usulan-{tujuan}', 'Musrenbang\usulanController@indextujuan');
Route::get('/musrenbang/{tahun}/usulan/tambah/{id}', 'Musrenbang\usulanController@tambah');
Route::get('/musrenbang/{tahun}/usulan/detail/{id}', 'Musrenbang\usulanController@detail');
Route::get('/musrenbang/{tahun}/usulan/edit/{id}', 'Musrenbang\usulanController@edit');
Route::get('/musrenbang/{tahun}/usulan/edit/prioritas/{id}', 'Musrenbang\usulanController@editPrioritas');
Route::get('/musrenbang/{tahun}/usulan/tinjau/{id}', 'Musrenbang\usulanController@acc');
Route::get('/musrenbang/{tahun}/kamus', 'Musrenbang\usulanController@kamus');
Route::get('/musrenbang/{tahun}/forgab', 'Musrenbang\usulanController@forgab');
Route::get('/musrenbang/{tahun}/daftarUsulan', 'Musrenbang\usulanController@daftarUsulan');
Route::get('/musrenbang/{tahun}/daftarUsulan1', 'Musrenbang\usulanController@daftarUsulan1');
Route::get('/musrenbang/{tahun}/berita', 'Musrenbang\usulanController@berita');
Route::get('/musrenbang/usulan/set_prioritas/{usulan_id}/{kel_id}/{prioritas}/','Musrenbang\usulanController@setPriority');


//usulkan kembali
Route::get('/musrenbang/{tahun}/usulan/usulkankembali', 'Musrenbang\usulanController@usulkanKembali');
Route::get('/musrenbang/{tahun}/usulan/usulkankembali/getdata', 'Musrenbang\usulanController@usulkanKembaliGetData');
Route::get('/musrenbang/{tahun}/usulan/usulkankembali/add/{id}', 'Musrenbang\usulanController@usulankanKembaliAdd');

//post usulkan kembali
Route::post('/musrenbang/{tahun}/usulan/tambah/kembali/submit', 'Musrenbang\usulanController@submitAddKembali');
Route::post('/musrenbang/{tahun}/usulan/usulkankembali/abaikan', 'Musrenbang\usulanController@usulanKembaliAbaikan');


Route::get('/musrenbang/{tahun}/usulan/s/{tipe}/{id}', 'Musrenbang\usulanController@getS');
//Route::get('/musrenbang/{tahun}/usulan/getUsulan', 'Musrenbang\usulanController@getUsulan');
Route::get('/musrenbang/{tahun}/usulan/dashboard', 'Musrenbang\usulanController@dashboard');
Route::get('/musrenbang/{tahun}/usulan/show/{id}/{kecid?}', 'Musrenbang\usulanController@showUsulan');
Route::get('/musrenbang/{tahun}/usulan/editAcc/{id}', 'Musrenbang\usulanController@editAcc');
Route::get('/musrenbang/{tahun}/usulan/daftarBerita', 'Musrenbang\usulanController@daftarBerita');
Route::get('/musrenbang/{tahun}/usulan/add-kel/{id}', 'Musrenbang\usulanController@addKelurahan');
//Route::get('/musrenbang/{tahun}/usulan/showUsulanData/{nama}', 'Musrenbang\usulanController@showUsulanData');
Route::get('/musrenbang/{tahun}/usulan/showUsulanData/{nama}/{status}/{rw?}/{isu?}/{tipe?}/{kamus?}', 'Musrenbang\usulanController@showUsulanData');
Route::get('/musrenbang/{tahun}/usulan/showUsulanDataKel/{nama}/{status}/{kel?}/{rw?}/{isu?}/{tipe?}/{kamus?}', 'Musrenbang\usulanController@showUsulanDataKel');
Route::get('/musrenbang/{tahun}/usulan/getUsulan/{rw}/{tipe}/{isu}/{kamus}', 'Musrenbang\usulanController@getUsulan');
Route::get('/musrenbang/{tahun}/pengaturan/rw/getProfile/{id}', 'Musrenbang\referensiController@getProfile');

Route::get('/musrenbang/{tahun}/pengaturan/referensi/getIsuUsulan/{id}', 'Musrenbang\referensiController@getIsuUsulan');

Route::get('/musrenbang/{tahun}/pengaturan/tujuan', 'Musrenbang\referensiController@tujuanShow');

//pengaturan kamus
Route::get('/musrenbang/{tahun}/pengaturan/isu', 'Musrenbang\referensiController@isuShow');
Route::get('/musrenbang/{tahun}/pengaturan/kamus', 'Musrenbang\referensiController@kamusShow');
Route::get('/musrenbang/{tahun}/pengaturan/kamus/tambahKamus', 'Musrenbang\referensiController@kamusAdd');
Route::get('/musrenbang/{tahun}/pengaturan/kamus/tambahKamus2/{id}', 'Musrenbang\referensiController@kamusAdd2');
Route::post('/musrenbang/{tahun}/pengaturan/kamus/tambahKamusSubmit', 'Musrenbang\referensiController@kamusAddSubmit');


//pengaturan account kelurahan
Route::get('/musrenbang/{tahun}/pengaturan/kelurahan/{id}', 'Musrenbang\referensiController@kelurahanShow');
Route::get('/musrenbang/{tahun}/pengaturan/kelurahan/getKelurahan/{id}', 'Musrenbang\referensiController@getKelurahan');
Route::get('/musrenbang/{tahun}/pengaturan/kelurahan/reset/{id}', 'Musrenbang\referensiController@resetKelurahan');
Route::get('/musrenbang/{tahun}/pengaturan/kelurahan/getProfile/{id}', 'Musrenbang\referensiController@getProfKeL');
//pengaturan account rw
Route::get('/musrenbang/{tahun}/pengaturan/rw/{id}', 'Musrenbang\referensiController@rwShow');
Route::get('/musrenbang/{tahun}/pengaturan/rw/getrw/{id}', 'Musrenbang\referensiController@getRW');
Route::get('/musrenbang/{tahun}/pengaturan/rw/reset/{id}', 'Musrenbang\referensiController@resetRW');
Route::get('/musrenbang/{tahun}/pengaturan/rt', 'Musrenbang\referensiController@rtShow');
//pengaturan account kecamatan
Route::get('/musrenbang/{tahun}/pengaturan/kecamatan', 'Musrenbang\referensiController@kecamatanShow');
Route::get('/musrenbang/{tahun}/pengaturan/kecamatan/add', 'Musrenbang\referensiController@kecamatanAdd');
Route::get('/musrenbang/{tahun}/pengaturan/kecamatan/getKec', 'Musrenbang\referensiController@getKec');
Route::get('/musrenbang/{tahun}/pengaturan/kecamatan/reset/{id}', 'Musrenbang\referensiController@resetKecamatan');
Route::get('/musrenbang/{tahun}/pengaturan/kecamatan/getProfile/{id}', 'Musrenbang\referensiController@getProfKec');

//pengaturan account rt
Route::get('/musrenbang/{tahun}/pengaturan/rt/{id}', 'Musrenbang\referensiController@rtShow');
Route::get('/musrenbang/{tahun}/pengaturan/rt/getrt/{id}', 'Musrenbang\referensiController@getRt');
Route::get('/musrenbang/{tahun}/pengaturan/rt/getProfile/{id}', 'Musrenbang\referensiController@getProfRt');
//aktivasi non aktivasi account
Route::get('/musrenbang/{tahun}/pengaturan/aktivasi/{id}', 'Musrenbang\referensiController@aktivasi');
Route::get('/musrenbang/{tahun}/pengaturan/nonAktivasi/{id}', 'Musrenbang\referensiController@nonAktivasi');
//pengaturan account skpd
Route::get('/musrenbang/{tahun}/pengaturan/skpd', 'Musrenbang\referensiController@skpdShow');
Route::get('/musrenbang/{tahun}/pengaturan/skpd/getSkpd', 'Musrenbang\referensiController@getSkpd');
Route::get('/musrenbang/{tahun}/pengaturan/skpd/reset/{id}', 'Musrenbang\referensiController@resetSKPD');
Route::get('/musrenbang/{tahun}/pengaturan/skpd/getProfile/{id}', 'Musrenbang\referensiController@getProfSkpd');
//pengaturan account admin
Route::get('/musrenbang/{tahun}/pengaturan/admin', 'Musrenbang\referensiController@adminShow');
Route::get('/musrenbang/{tahun}/pengaturan/admin/getAdmin', 'Musrenbang\referensiController@getAdmin');
Route::get('/musrenbang/{tahun}/pengaturan/admin/reset/{id}', 'Musrenbang\referensiController@resetAdmin');
Route::get('/musrenbang/{tahun}/pengaturan/admin/getProfile/{id}', 'Musrenbang\referensiController@getProfAdmin');
Route::get('/musrenbang/{tahun}/pengaturan/admin/allAccount/{id}', 'Musrenbang\referensiController@aktivasiSemuaAccount');


Route::get('/musrenbang/{tahun}/maps', 'Musrenbang\referensiController@maps');
Route::get('/musrenbang/{tahun}/mapsPublic', 'Musrenbang\referensiController@mapsPublic');


//MUSRENBANG-POST
Route::post('/musrenbang/{tahun}/usulan/tambah/submit', 'Musrenbang\usulanController@submitAdd');
Route::post('/musrenbang/{tahun}/usulan/edit/submit', 'Musrenbang\usulanController@submitEdit');
Route::post('/musrenbang/{tahun}/usulan/edit/submitKel', 'Musrenbang\usulanController@submitEditKel');
Route::post('/musrenbang/{tahun}/usulan/delete', 'Musrenbang\usulanController@delete');
Route::post('/musrenbang/{tahun}/usulan/tolak', 'Musrenbang\usulanController@deny');
//simpan edit profil di pengaturan
Route::post('/musrenbang/{tahun}/usulan/simpan/profile', 'Musrenbang\usulanController@simpanProfile');
Route::post('/musrenbang/{tahun}/usulan/simpan/profile/kec', 'Musrenbang\referensiController@simpanProfilKec');
Route::post('/musrenbang/{tahun}/usulan/simpan/profile/kel', 'Musrenbang\referensiController@simpanProfilKel');
Route::post('/musrenbang/{tahun}/usulan/simpan/profile/rw', 'Musrenbang\referensiController@simpanProfilRw');
Route::post('/musrenbang/{tahun}/usulan/simpan/profile/rt', 'Musrenbang\referensiController@simpanProfilRt');
Route::post('/musrenbang/{tahun}/usulan/simpan/profile/skpd', 'Musrenbang\referensiController@simpanProfilSkpd');
Route::post('/musrenbang/{tahun}/usulan/simpan/profile/admin', 'Musrenbang\referensiController@simpanProfilAdmin');

//MUSRENBANG-API
Route::get('/musrenbang/{tahun}/usulan/data', 'Musrenbang\usulanController@getData');
Route::get('/musrenbang/{tahun}/usulan-{nama}/data/{status?}', 'Musrenbang\usulanController@getData1');
Route::get('/musrenbang/{tahun}/usulan/prioritas/{tujuan}', 'Musrenbang\usulanController@indexTujuanPrioritas');
Route::get('/musrenbang/{tahun}/usulan-{nama}/prioritas/{status?}', 'Musrenbang\usulanController@getDataPrioritas');

//post fitur baru
Route::post('/musrenbang/{tahun}/usulan/set/prioritas', 'Musrenbang\usulanController@setPrioritasUsulan');

//kamus usulan
Route::get('/musrenbang/{tahun}/usulan/data/kamus/filter/{id}', 'Musrenbang\usulanController@getKamus');
Route::get('/musrenbang/{tahun}/kamusUsulan', 'Musrenbang\usulanController@showKamusUsulan');
Route::get('/musrenbang/{tahun}/usulan/data/satuan/{id}', 'Musrenbang\usulanController@getSatuan');

Route::get('/musrenbang/{tahun}/usulan/kamusUsulan/{status}/{skpd?}/{isu?}/{kamus?}/{tipe?}', 'Musrenbang\usulanController@getKamusUsulan');
Route::get('/musrenbang/{tahun}/kamus/kriteria/{id}', 'Musrenbang\usulanController@getKamusUsulanKriteria');
//Route::get('/musrenbang/{tahun}/usulan/kamusUsulanSingle/{id}', 'Musrenbang\usulanController@getKamusUsulan');
Route::get('/musrenbang/{tahun}/usulan/kamusUsulanSingle/edit/{id}', 'Musrenbang\usulanController@editKamusUsulan');
Route::get('/musrenbang/{tahun}/usulan/kamusUsulanSingle/get/kriteria/{id}', 'Musrenbang\usulanController@getKamusUsulanSingleKriteria');

Route::post('/musrenbang/{tahun}/usulan/kamusUsulanSingle/SET/kriteria', 'Musrenbang\usulanController@setKamusUsulanSingleKriteria');
Route::post('/musrenbang/{tahun}/usulan/kamusUsulanSingle/simpan/kriteria', 'Musrenbang\usulanController@simpanKamusUsulanSingleKriteria');
Route::post('/musrenbang/{tahun}/kamus/simpan/kamusBaru', 'Musrenbang\usulanController@simpanKamusUsulanBaru');


Route::get('/musrenbang/{tahun}/reset', 'Musrenbang\usulanController@resetPassword');
Route::post('/musrenbang/{tahun}/reset/ubah', 'Musrenbang\usulanController@resetPassUbah');


Route::get('/musrenbang/{tahun}/profile', 'Musrenbang\usulanController@profile');
Route::get('/musrenbang/{tahun}/profile/getData', 'Musrenbang\usulanController@profileGetData');
Route::get('/musrenbang/{tahun}/validasi', 'Musrenbang\usulanController@validasi');

//admin route
Route::get('/musrenbang/{tahun}/admin/renjaSkpd', 'Musrenbang\publicController@renjaSkpd');
Route::get('/musrenbang/{tahun}/admin/pippk', 'Musrenbang\publicController@adminLkk');

//Route Baru
Route::post('/musrenbang/users/ubahpassword','Auth\UserController@changePassword');
Route::post('/musrenbang/rw/ubah','RwController@update');
Route::post('/musrenbang/lurah/ubah','RwController@updateLurah');
Route::post('/musrenbang/camat/ubah','RwController@updateCamat');
Route::post('/musrenbang/rw/tambahrt','RwController@tambahrt');
Route::post('/musrenbang/rt/editrt','RtController@ubah');


//mobile musrenbang
Route::get('/musrenbang/{tahun}/mobile', 'Musrenbang\mobileController@index');
Route::get('/musrenbang/{tahun}/mobile/login', 'Musrenbang\mobileController@login');
Route::get('/musrenbang/{tahun}/mobile/dashboard', 'Musrenbang\mobileController@dashboard');
Route::get('/musrenbang/{tahun}/mobile/dashboard-usulan', 'Musrenbang\mobileController@dashboard-usulan');
Route::get('/musrenbang/{tahun}/mobile/input/{id}', 'Musrenbang\mobileController@tambah');
Route::get('/musrenbang/{tahun}/mobile/kamus', 'Musrenbang\mobileController@kamus');
Route::get('/musrenbang/{tahun}/mobile/daftarUsulan/{id}', 'Musrenbang\mobileController@daftarUsulan');
Route::get('/musrenbang/{tahun}/mobile/data/kamus/{id}', 'Musrenbang\mobileController@getKamus');
Route::get('/musrenbang/{tahun}/mobile/data/satuan/{id}', 'Musrenbang\mobileController@getSatuan');

//root html
Route::get('/musrenbang/{tahun}/mobile/kecamatan', 'Musrenbang\mobileController@kecamatan');
Route::get('/musrenbang/{tahun}/mobile/kelurahan', 'Musrenbang\mobileController@kelurahan');
Route::get('/musrenbang/{tahun}/mobile/daftar-usulan-kecamatan', 'Musrenbang\mobileController@daftarUsulanKecamatan');
Route::get('/musrenbang/{tahun}/mobile/daftar-usulan-kelurahan', 'Musrenbang\mobileController@daftarUsulanKelurahan');
Route::get('/musrenbang/{tahun}/mobile/daftar-program', 'Musrenbang\mobileController@daftarProgram');
Route::get('/musrenbang/{tahun}/mobile/profile', 'Musrenbang\mobileController@profile');

Route::get('/musrenbang/{tahun}/mobile/notifikasi', 'Musrenbang\mobileController@notifikasi');
Route::get('/musrenbang/{tahun}/mobile/asistensi-usulan', 'Musrenbang\mobileController@asistensiUsulan');
Route::get('/musrenbang/{tahun}/mobile/asistensi-detail', 'Musrenbang\mobileController@asistensiDetail');
Route::get('/musrenbang/{tahun}/mobile/forgot-password', 'Musrenbang\mobileController@forgotPassword');
Route::get('/musrenbang/{tahun}/mobile/navigation', 'Musrenbang\mobileController@navigation');
Route::get('/musrenbang/{tahun}/mobile/input', 'Musrenbang\mobileController@input');


Route::get('/musrenbang/{tahun}/tahapan', 'Musrenbang\tahapanController@index');
Route::get('/musrenbang/{tahun}/tahapan/getData', 'Musrenbang\tahapanController@getData');
Route::get('/musrenbang/{tahun}/tahapan/getData/{id}', 'Musrenbang\tahapanController@getDetail');
Route::post('/musrenbang/{tahun}/tahapan/submit', 'Musrenbang\tahapanController@submit');

Route::get('/musrenbang/{tahun}/public', 'Musrenbang\publicController@index');


## Kelurahan Approval
Route::get('/musrenbang/{tahun}/public/kelurahan/getapproval', 'Musrenbang\publicController@getapproval');
Route::get('/musrenbang/{tahun}/public/kelurahan/approval', 'Musrenbang\publicController@approval');

Route::get('/musrenbang/{tahun}/public/kelurahan/getapprovalkelajax', 'Musrenbang\publicController@getapprovalkelajax');
Route::get('/musrenbang/{tahun}/public/kelurahan/approvalkelajax', 'Musrenbang\publicController@approvalkelajax');
## End of Kelurahan Approval

## Kecamatan Approval
Route::get('/musrenbang/{tahun}/public/kecamatan/getapproval', 'Musrenbang\publicController@getapprovalkecamatan');
Route::get('/musrenbang/{tahun}/public/kecamatan/approval', 'Musrenbang\publicController@approvalkec');
## End of Kecamatan Approval

## SKPD Approval
Route::get('/musrenbang/{tahun}/public/skpd/getapproval', 'Musrenbang\publicController@getapprovalskpd');
Route::get('/musrenbang/{tahun}/public/skpd/approval', 'Musrenbang\publicController@approvalskpd');
## End of SKPD Approval

Route::get('/musrenbang/{tahun}/public/kecamatan', 'Musrenbang\publicController@kecamatan');
Route::get('/musrenbang/{tahun}/public/kecamatan/getKecamatan', 'Musrenbang\publicController@getKecamatan');

Route::get('/musrenbang/{tahun}/public/getskpd', 'Musrenbang\publicController@getskpd');
Route::get('/musrenbang/{tahun}/public/skpd', 'Musrenbang\publicController@skpd');
Route::get('/musrenbang/{tahun}/public/skpd/getdetail/{id}', 'Musrenbang\publicController@getdetailskpd');
Route::get('/musrenbang/{tahun}/public/skpd/detail/{id}', 'Musrenbang\publicController@detailskpd');
Route::get('/musrenbang/{tahun}/public/exportusulan', 'Musrenbang\publicController@exportUsulan');

Route::get('/musrenbang/{tahun}/public/rw', 'Musrenbang\publicController@rw');
Route::get('/musrenbang/{tahun}/public/rwajax', 'Musrenbang\publicController@rwajax');
Route::get('/musrenbang/{tahun}/public/kelurahan/getrwajax', 'Musrenbang\publicController@getrwAJAX');

Route::get('/musrenbang/{tahun}/public/getOnline', 'Musrenbang\publicController@getOnline');
Route::get('/musrenbang/{tahun}/public/kecamatan/getKelurahan/{id}', 'Musrenbang\publicController@getKelurahan');
Route::get('/musrenbang/{tahun}/public/kelurahan', 'Musrenbang\publicController@kelurahan');
Route::get('/musrenbang/{tahun}/public/kelurahan/getKelurahan/{id}', 'Musrenbang\publicController@getKelurahan');
Route::get('/musrenbang/{tahun}/public/kelurahan/getrw/{id}', 'Musrenbang\publicController@getrw');
//Route::get('/musrenbang/{tahun}/public/usulan', 'Musrenbang\publicController@usulan');
Route::get('/musrenbang/{tahun}/public/usulan/getUsulan', 'Musrenbang\publicController@getUsulan');
Route::get('/musrenbang/{tahun}/public/isu', 'Musrenbang\publicController@isu');
Route::get('/musrenbang/{tahun}/public/isu/getIsu', 'Musrenbang\publicController@getIsu');

# Public Kamus
Route::get('/musrenbang/{tahun}/public/kamus', 'Musrenbang\publicController@kamus');
Route::get('/musrenbang/{tahun}/public/kamus/getKamus', 'Musrenbang\publicController@getKamus');

Route::get('/musrenbang/{tahun}/public/kamusajax', 'Musrenbang\publicController@kamusajax');
Route::get('/musrenbang/{tahun}/public/kamus/getKamusajax', 'Musrenbang\publicController@getKamusajax');
# End of public kamus

Route::get('/musrenbang/{tahun}/public/usulan', 'Musrenbang\publicController@showUsulan');

Route::get('/musrenbang/{tahun}/public/usulanajax', 'Musrenbang\publicController@showUsulanAJAX');
Route::get('/musrenbang/{tahun}/public/usulan/getUsulanajax', 'Musrenbang\publicController@getUsulanAJAX');


Route::get('/musrenbang/{tahun}/public/usulan/getUsulan/{tipe}/{kecamatan}/{kelurahan}/{rw}/{isu}/{kamus}', 'Musrenbang\publicController@getUsulan');
Route::get('/musrenbang/{tahun}/public/usulan/s/{tipe}/{id}', 'Musrenbang\publicController@getS');
Route::get('/musrenbang/{tahun}/public/usulan/detail/{id}', 'Musrenbang\publicController@detailusulan');
Route::get('/musrenbang/{tahun}/public/berita/{id}', 'Musrenbang\publicController@berita');
Route::get('/musrenbang/{tahun}/public/lkk/data/', 'Musrenbang\publicController@dataRekapLKK');



Route::get('/musrenbang/{tahun}/usulan/getExport/{rw?}/{isu?}/{tipe?}/{kamus?}','Musrenbang\usulanController@getExport');
Route::get('/musrenbang/{tahun}/usulan/editAccKel/{id}', 'Musrenbang\usulanController@editAcc');




//mobile controller copyan dari usulan controller
/*
Route::get('/musrenbang/{tahun}/mobile', 'Musrenbang\mobileController@index');
Route::get('/musrenbang/{tahun}/mobile/{nama}', 'Musrenbang\usulanController@indextujuan');
Route::get('/musrenbang/{tahun}/mobile/tambah/{id}', 'Musrenbang\mobileController@tambah');
Route::get('/musrenbang/{tahun}/mobile/detail/{id}', 'Musrenbang\mobileController@detail');
Route::get('/musrenbang/{tahun}/mobile/edit/{id}', 'Musrenbang\mobileController@edit');
Route::get('/musrenbang/{tahun}/mobile/tinjau/{id}', 'Musrenbang\mobileController@acc');
Route::get('/musrenbang/{tahun}/mobile/kamus', 'Musrenbang\mobileController@kamus');
Route::get('/musrenbang/{tahun}/mobile/forgab', 'Musrenbang\mobileController@forgab');
Route::get('/musrenbang/{tahun}/mobile/daftarUsulan', 'Musrenbang\mobileController@daftarUsulan');
Route::get('/musrenbang/{tahun}/mobile/berita', 'Musrenbang\mobileController@berita');

//MUSRENBANG-POST
Route::post('/musrenbang/{tahun}/mobile/tambah/submit', 'Musrenbang\mobileController@submitAdd');
Route::post('/musrenbang/{tahun}/mobile/edit/submit', 'Musrenbang\mobileController@submitEdit');
Route::post('/musrenbang/{tahun}/mobile/delete', 'Musrenbang\mobileController@delete');
Route::post('/musrenbang/{tahun}/mobile/tolak', 'Musrenbang\mobileController@deny');
//MUSRENBANG-API
Route::get('/musrenbang/{tahun}/mobile/data', 'Musrenbang\usulanController@getData');
Route::get('/musrenbang/{tahun}/mobile-{nama}/data', 'Musrenbang\usulanController@getData1');
Route::get('/musrenbang/{tahun}/mobile/data/kamus/{id}', 'Musrenbang\usulanController@getKamus');
Route::get('/musrenbang/{tahun}/mobile/data/satuan/{id}', 'Musrenbang\usulanController@getSatuan');


Route::get('/musrenbang/{tahun}/mobile/profile', 'Musrenbang\mobileController@profile');*/






//RESES


//BUDGETING
/*Route::get('/main', 'Budgeting\dashboardController@index');														//SHOW DASHBOARD BUDGETIN
//BL
Route::get('/main/{tahun}/{status}/belanja-langsung', 'Budgeting\blController@index');										//SHOW BL
Route::get('/main/{tahun}/{status}/belanja-langsung/detail/{id}', 'Budgeting\blController@showDetail');						//SHOW DETAIL BL
Route::get('/main/{tahun}/{status}/belanja-langsung/tambah', 'Budgeting\blController@add');									//SHOW ADD FORM
Route::get('/main/{tahun}/{status}/belanja-langsung/ubah/{id}', 'Budgeting\blController@edit');								//SHOW EDIT FORM
Route::get('/main/{tahun}/{status}/belanja-langsung/hapus/{id}', 'Budgeting\blController@delete');							//DELETE BL
Route::get('/main/{tahun}/{status}/belanja-langsung/validasi/{id}', 'Budgeting\blController@validasi');						//VALID BL
Route::get('/main/{tahun}/{status}/belanja-langsung/rincian/{id}', 'Budgeting\blController@rincian');						//SHOW RINCIAN
Route::get('/main/{tahun}/{status}/belanja-langsung/rincian/hapus/{id}', 'Budgeting\blController@rincian');					//DELETE RINCIAN
Route::post('/main/{tahun}/{status}/belanja-langsung/detail/simpan', 'Budgeting\blController@submitDetail');				//STORE BL
Route::post('/main/{tahun}/{status}/belanja-langsung/rincian/simpan', 'Budgeting\blController@submitRincian');				//STORE BL RINCIAN
Route::post('/main/{tahun}/{status}/belanja-langsung/detail/ubah/simpan', 'Budgeting\blController@submitDetailEdit');		//STORE EDIT BL
Route::post('/main/{tahun}/{status}/belanja-langsung/rincian/ubah/simpan', 'Budgeting\blController@submitRincianEdit');		//STORE EDIT RINCIAN
//BTL
Route::get('/main/{tahun}/{status}/belanja-tidak-langsung/', 'Budgeting\btlController@index');					//DELETE RINCIAN
Route::get('/main/{tahun}/{status}/belanja-tidak-langsung/hapus/{id}', 'Budgeting\btlController@delete');					//DELETE RINCIAN/
//PENDAPATAN
Route::get('/main/{tahun}/{status}/pendapatan/', 'Budgeting\pendapatanController@index');					//DELETE RINCIAN
Route::get('/main/{tahun}/{status}/pendapatan/hapus/{id}', 'Budgeting\pendapatanController@delete');					//DELETE RINCIAN
//PEMBIAYAAN
Route::get('/main/{tahun}/{status}/pembiayaan/', 'Budgeting\pembiayaanController@index');					//DELETE RINCIAN
Route::get('/main/{tahun}/{status}/pembiayaan/hapus/{id}', 'Budgeting\pembiayaanController@delete');

*/
					//DELETE RINCIAN

/*//PENGATURAN
//URUSAN
Route::get('/main/{tahun}/{status}/pengaturan/urusan', 'Budgeting\Referensi\urusanController@index');					//DELETE RINCIAN
Route::get('/main/{tahun}/{status}/pengaturan/urusan/getData', 'Budgeting\Referensi\urusanController@getData');					//DELETE RINCIAN
Route::get('/main/{tahun}/{status}/pengaturan/urusan/getData/{id}', 'Budgeting\Referensi\urusanController@getDetail');					//DELETE RINCIAN
Route::post('/main/{tahun}/{status}/pengaturan/urusan/delete', 'Budgeting\Referensi\urusanController@delete');					//DELETE RINCIAN
Route::post('/main/{tahun}/{status}/pengaturan/urusan/add/submit', 'Budgeting\Referensi\urusanController@submitAdd');					//DELETE RINCIAN
Route::post('/main/{tahun}/{status}/pengaturan/urusan/edit/submit', 'Budgeting\Referensi\urusanController@submitEdit');					//DELETE RINCIAN
//SKPD
Route::get('/main/{tahun}/{status}/pengaturan/skpd', 'Budgeting\Referensi\referensiController@skpdShow');					//DELETE RINCIAN
Route::get('/main/{tahun}/{status}/pengaturan/skpd/getData', 'Budgeting\Referensi\skpdController@getData');					//DELETE RINCIAN
Route::get('/main/{tahun}/{status}/pengaturan/skpd/getData/{id}', 'Budgeting\Referensi\skpdController@getDetail');					//DELETE RINCIAN
Route::post('/main/{tahun}/{status}/pengaturan/skpd/delete', 'Budgeting\Referensi\skpdController@delete');					//DELETE RINCIAN
Route::post('/main/{tahun}/{status}/pengaturan/skpd/add/submit', 'Budgeting\Referensi\skpdController@submitAdd');					//DELETE RINCIAN
Route::post('/main/{tahun}/{status}/pengaturan/skpd/edit/submit', 'Budgeting\Referensi\skpdController@submitEdit');					//DELETE RINCIAN
//PROGRAM
Route::get('/main/{tahun}/{status}/pengaturan/program', 'Budgeting\Referensi\programController@index');					//DELETE RINCIAN
Route::get('/main/{tahun}/{status}/pengaturan/program/getData', 'Budgeting\Referensi\programController@getData');					//DELETE RINCIAN
Route::get('/main/{tahun}/{status}/pengaturan/program/getData/{id}', 'Budgeting\Referensi\programController@getDetail');					//DELETE RINCIAN
Route::get('/main/{tahun}/{status}/pengaturan/program/getDataDetail/{urusan}/{skpd}', 'Budgeting\Referensi\programController@getDataDetail');
Route::post('/main/{tahun}/{status}/pengaturan/program/delete', 'Budgeting\Referensi\programController@delete');					//DELETE RINCIAN
Route::post('/main/{tahun}/{status}/pengaturan/program/add/submit', 'Budgeting\Referensi\programController@submitAdd');					//DELETE RINCIAN
Route::post('/main/{tahun}/{status}/pengaturan/program/edit/submit', 'Budgeting\Referensi\programController@submitEdit');					//DELETE RINCIAN
*/

Route::post('/musrenbang/berita/tambah','Musrenbang\usulanController@tambahBerita');
Route::post('/musrenbang/berita/tambahGambar','Musrenbang\usulanController@tambahBeritaGambar');


//route public baru
Route::get('/musrenbang/{tahun}/public/renja', 'Musrenbang\publicController@renja');
Route::get('/musrenbang/{tahun}/public/pippk', 'Musrenbang\publicController@pippk');
Route::get('/musrenbang/{tahun}/public/lkk', 'Musrenbang\publicController@lkk');


//layout: get per pengusul
Route::get('/musrenbang/{tahun}/usulan/getRw/{tab}', 'Musrenbang\usulanController@getUsulanRw');
Route::get('/musrenbang/{tahun}/usulan/getLpm/{tab}', 'Musrenbang\usulanController@getUsulanLpm');
Route::get('/musrenbang/{tahun}/usulan/getPkk/{tab}', 'Musrenbang\usulanController@getUsulanPkk');
Route::get('/musrenbang/{tahun}/usulan/getKarta/{tab}', 'Musrenbang\usulanController@getUsulanKarta');
Route::get('/musrenbang/{tahun}/usulan/getKelurahan/{tab}', 'Musrenbang\usulanController@getUsulanKelurahan');

//show per pengusul
Route::get('/musrenbang/{tahun}/usulan/showRw', 'Musrenbang\usulanController@showUsulanRw');
Route::get('/musrenbang/{tahun}/usulan/showLpm', 'Musrenbang\usulanController@showUsulanLpm');
Route::get('/musrenbang/{tahun}/usulan/showPkk', 'Musrenbang\usulanController@showUsulanPkk');
Route::get('/musrenbang/{tahun}/usulan/showKarta', 'Musrenbang\usulanController@showUsulanKarta');
Route::get('/musrenbang/{tahun}/usulan/showKelurahan', 'Musrenbang\usulanController@showUsulanKelurahan');

//filter per pengusul
Route::get('/musrenbang/{tahun}/usulan/filterRw', 'Musrenbang\usulanController@filterUsulanRw');
Route::get('/musrenbang/{tahun}/usulan/filterLpm', 'Musrenbang\usulanController@filterUsulanLpm');
Route::get('/musrenbang/{tahun}/usulan/filterPkk', 'Musrenbang\usulanController@filterUsulanPkk');
Route::get('/musrenbang/{tahun}/usulan/filterKarta', 'Musrenbang\usulanController@filterUsulanKarta');
Route::get('/musrenbang/{tahun}/usulan/filterKelurahan', 'Musrenbang\usulanController@filterUsulanKelurahan');

Route::get('/musrenbang/{tahun}/usulan/getperskpd','Musrenbang\usulanController@getusulanperskpd');
Route::get('/musrenbang/{tahun}/usulan/skpd/get/{id}','Musrenbang\usulanController@getdetailusulanperskpd');
Route::get('/musrenbang/{tahun}/usulan/skpd/bappeda/get/{id}','Musrenbang\usulanController@getdetailusulanperskpdbappeda');
Route::get('/musrenbang/{tahun}/usulan/skpd/{id}','Musrenbang\usulanController@detailusulanperskpd');
Route::get('/musrenbang/{tahun}/usulan/kecamatan/{id}','Musrenbang\usulanController@detailusulankecamatan');
Route::get('/musrenbang/{tahun}/usulan/getperpengusul','Musrenbang\usulanController@getusulanperpengusul');
Route::get('/musrenbang/{tahun}/usulan/perskpd','Musrenbang\usulanController@usulanperskpd');
Route::get('/musrenbang/{tahun}/usulan/perkecamatan','Musrenbang\usulanController@usulanperkecamatan');
Route::get('/musrenbang/{tahun}/usulan/perpengusul','Musrenbang\usulanController@usulanperpengusul');
Route::get('/musrenbang/{tahun}/usulan/perskpd/export','Musrenbang\usulanController@exportusulanperskpd');
Route::get('/musrenbang/{tahun}/usulan/perpengusul/export','Musrenbang\usulanController@exportusulanperpengusul');
Route::get('/musrenbang/{tahun}/usulan/perkecamatan/export','Musrenbang\exportController@exportRekapKecamatan');

//admin route
Route::get('/musrenbang/{tahun}/grafikRenja','Musrenbang\usulanController@grafikRenja');
Route::get('/musrenbang/{tahun}/grafikPippk','Musrenbang\usulanController@grafikPippk');
Route::get('/musrenbang/{tahun}/grafikRenja/detail','Musrenbang\usulanController@grafikRenjaDetail');
Route::get('/musrenbang/{tahun}/grafikRenja/detail/getRenja','Musrenbang\usulanController@getRenja');
Route::get('/musrenbang/{tahun}/grafikRenja/detail/skpd/{id}','Musrenbang\usulanController@grafikRenjaSkpdDetail');
Route::get('/musrenbang/{tahun}/grafikRenja/detail/getRenjaSkpd/{id}/{status}','Musrenbang\usulanController@getRenjaSkpd');

//Route Export Baru
Route::get('/musrenbang/{tahun}/export/usulan/{jenis}','Musrenbang\exportController@exportusulan');
//Route::get('/musrenbang/{tahun}/export/usulan/{id}','Musrenbang\exportController@rekapKelurahan');

//Route Data Baru
Route::get('/musrenbang/{tahun}/data/usulan/{jenis}/{status}/{pengusul?}/{isu?}/{kamus?}/{kecamatan?}', 'Musrenbang\dataController@dataUsulan');
Route::get('/musrenbang/{tahun}/skpd/data/usulan/{idskpd}', 'Musrenbang\dataController@dataUsulanSKPD');
Route::get('/musrenbang/{tahun}/kecamatan/data/usulan/{idskpd}', 'Musrenbang\dataController@dataUsulanKecamatan');
Route::get('/musrenbang/{tahun}/kecamatan/data/usulan/', 'Musrenbang\dataController@dataRekapKecamatan');
Route::get('/musrenbang/{tahun}/usulan/skpd/export/{id}','Musrenbang\exportController@exportUsulanSKPD');
Route::get('/musrenbang/{tahun}/public/usulan/skpd/export/{id}/{tipe?}','Musrenbang\publicExportController@exportUsulanSKPD');
Route::get('/musrenbang/{tahun}/public/usulan/skpdperkecamatan/export/{id}/{tipe?}','Musrenbang\publicExportController@exportUsulanSKPDPerKecamatan');
Route::get('/musrenbang/{tahun}/public/usulan/rekapskpd/export/','Musrenbang\publicExportController@exportRekapSKPD');
Route::get('/musrenbang/{tahun}/usulan/kecamatan/export/{id}','Musrenbang\exportController@exportUsulanKecamatan');

Route::get('/musrenbang/{tahun}/public/usulan/rekapskpd/export/grafik','Musrenbang\publicExportController@exportRekapSKPDGrafik');


//Route Amin Bapeda
Route::get('/musrenbang/{tahun}/adminbapeda/kecamatan','Musrenbang\adminBappedaController@kecamatan');
Route::get('/musrenbang/{tahun}/adminbappeda/usulan','Musrenbang\adminBappedaController@usulan');
Route::get('/musrenbang/{tahun}/adminbappeda/datatable/usulan','Musrenbang\dataTableController@getUsulan');

//rekap admin per kecamatan dan per kelurahan
Route::get('/musrenbang/{tahun}/admin/kecamatan', 'Musrenbang\referensiController@adminPerKecamatan');
Route::get('/musrenbang/{tahun}/admin/kelurahan', 'Musrenbang\referensiController@adminPerKelurahan');

Route::get('/test/{tahun}',"Musrenbang\publicController@getUsulanAJAX");

//Route notification
Route::get('/musrenbang/notifikasi/test','Musrenbang\notificationController@test');
Route::get('/musrenbang/notifikasi/index','Musrenbang\notificationController@index');
Route::get('/musrenbang/notifikasi/getNotificationAPI','Musrenbang\notificationController@getNotificationAPI');

Route::get('/musrenbang/{tahun}/balikin/usulan/{id}','Musrenbang\usulanController@balikinUsulan');
Route::get('/musrenbang/{tahun}/ubah/kamus/skpd/{id_kamus}/{kode_skpd}','Musrenbang\publicController@ubahkamusskpd');


Route::get('/musrenbang/{tahun}/public/anomali/pippklkk/{threshold}/api','Musrenbang\publicController@pippklkkanomali');
Route::get('/musrenbang/{tahun}/public/anomali/pippklkkanomaliview/','Musrenbang\publicController@pippklkkanomaliview');

Route::get('/musrenbang/{tahun}/public/anomali/renja/{threshold}/api','Musrenbang\publicController@renjaanomalinominal');
Route::get('/musrenbang/{tahun}/public/anomali/renjaanomaliview','Musrenbang\publicController@renjaanomali');

Route::get('/musrenbang/{tahun}/public/anomali/renjajumlah/{threshold}/api','Musrenbang\publicController@renjaanomalijumlah');
Route::get('/musrenbang/{tahun}/public/anomali/renjajumlahanomaliview','Musrenbang\publicController@renjaanomalijumlahview');

Route::get('/musrenbang/{tahun}/public/usulan/skpd/pdf/','Musrenbang\publicExportController@doExportUsulanPDF');

Route::get('/musrenbang/{tahun}/public/api/rw/usulantabel/{rwid}','Musrenbang\publicController@getUsulanRWTabel');
Route::get('/musrenbang/{tahun}/public/accordion','Musrenbang\publicController@showAccordion');

Route::get('/musrenbang/public/api/usulan/{usulanid}/{react}','Musrenbang\publicController@likeUsulan');

Route::get('/musrenbang/public/{usulanid}/comment','Musrenbang\publicController@commentUsulanView');
Route::post('/musrenbang/public/api/usulan/{usulanid}/comment','Musrenbang\publicController@commentUsulanProcess');


//anomali admin
Route::get('/musrenbang/{tahun}/admin/anomali/renjaanomaliview','Musrenbang\referensiController@renjaanomaliAdmin');
Route::get('/musrenbang/{tahun}/admin/anomali/renjajumlahanomaliview','Musrenbang\referensiController@renjaanomalijumlahviewAdmin');
Route::get('/musrenbang/{tahun}/admin/anomali/pippklkkanomaliview/','Musrenbang\referensiController@pippklkkanomaliviewAdmin');

/* Facebook Login */
// Generate a login URL
Route::get('/public/profile',function(){
	dd(Session::get('facebook_user'),Session::get('fb_user_access_token'));
});

Route::get('/facebook/login', function(SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb)
{
    // Send an array of permissions to request
    $login_url = $fb
    			->getRedirectLoginHelper()
    			->getLoginUrl('http://bappeda.bandung.go.id/facebook/callback',['email']);

    // Obviously you'd do this in blade :)
    echo '<a href="' . $login_url . '">Login with Facebook</a>';
});

// Endpoint that is redirected to after an authentication attempt
Route::get('/facebook/callback', function(SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb)
{
    // Obtain an access token.
    try {
        $token = $fb->getAccessTokenFromRedirect("http://bappeda.bandung.go.id/facebook/callback");
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
        dd($e->getMessage(), "When getting token");
    }

    // Access token will be null if the user denied the request
    // or if someone just hit this URL outside of the OAuth flow.
    if (!$token) {
        // Get the redirect helper
        $helper = $fb->getRedirectLoginHelper();

        if (! $helper->getError()) {
            abort(403, 'Unauthorized action.');
        }

        // User denied the request
        dd(
            $helper->getError(),
            $helper->getErrorCode(),
            $helper->getErrorReason(),
            $helper->getErrorDescription(),
            "Get Token"
        );
    }

    if (! $token->isLongLived()) {
        // OAuth 2.0 client handler
        $oauth_client = $fb->getOAuth2Client();

        // Extend the access token.
        try {
            $token = $oauth_client->getLongLivedAccessToken($token);
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage(),"Long Lived");
        }
    }

    $fb->setDefaultAccessToken($token);

    // Get basic info on the user from Facebook.
    try {
        $response = $fb->get('/me?fields=id,name,email');
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
        dd($e->getMessage());
    }

    // Convert the response to a `Facebook/GraphNodes/GraphUser` collection
    $facebook_user = $response->getGraphUser();

    // Save for later
    Session::put('fb_user_access_token', (string) $token);
    Session::put('facebook_user',$facebook_user);

    return redirect('/musrenbang/2017/public/accordion');
});



//rekap musrenbang kelurahan
Route::get('/musrenbang/{tahun}/data/rekap/kelurahan', 'Musrenbang\dataController@rekapKelurahan');
Route::get('/musrenbang/{tahun}/data/get/usulan/{id}', 'Musrenbang\dataController@getUsulan');
Route::get('/musrenbang/{tahun}/data/show/detail/usulan/{id}/{kecid?}', 'Musrenbang\dataController@showDetailUsulan');


//rekap pivot
Route::get('/musrenbang/{tahun}/pivot/rw/{id}','Musrenbang\publicExportController@rekapTahapRw');