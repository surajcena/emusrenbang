import beanstalkc
import psycopg2
import psycopg2.extras
import sys

def updateQuery(usulan_id, usulan_prioritas):
        # untuk mengupdate prioritas
        global conn
        global cursor

        cursor.execute("""UPDATE "MUSRENBANG"."DAT_USULAN" SET "USULAN_PRIORITAS"="""+usulan_prioritas+"""
WHERE "MUSRENBANG"."DAT_USULAN"."USULAN_ID"="""+usulan_id)
        conn.commit()


def changeTree(listallp, listaddids, prior):
        #algoritma Depth First Search, untuk mengubah direktori yg conflict
        prior = int(prior)
        idx = searchPriorityIndex(listallp, prior)
        if (idx != -1): #kalau ada yg bentrok
                prior = prior + 1

                print "Usulan ID "+str(listaddids[idx])+": update prioritas sebelumnya: "+str(listallp[idx])+" ke "+str(prior)
                updateQuery(str(listaddids[idx]),str(prior))

                del listallp[idx]
                del listaddids[idx]
                changeTree(listallp, listaddids, prior)

def searchPriorityIndex(listallp, prior):
        #cari index dari priority
        for i in range(0,len(listallp)):
                if (int(prior) == int(listallp[i])):
                        return i
        return -1

def setPriority(kel_id, usulan_id, usulan_prioritas):
        ## List all prioritas di suatu kelurahan
        global conn
        global cursor

        cursor.execute("""SELECT * FROM
"MUSRENBANG"."DAT_USULAN" JOIN "DATA"."users" ON "MUSRENBANG"."DAT_USULAN"."USER_CREATED"="DATA"."users"."id"
WHERE "DATA"."users"."KEL_ID"="""+kel_id)
        records = cursor.fetchall()

        listallprio = []
        listallusulanids = []
        for x in records:
                listallprio.append(x['USULAN_PRIORITAS'])
                listallusulanids.append(x['USULAN_ID'])

        #print "List all prioritas dan usulan id di kelurahan: "+kel_id
        #print listallprio
        #print listallusulanids

        #algoritma update prioritas
        changeTree(listallprio,listallusulanids,usulan_prioritas)
        updateQuery(usulan_id,usulan_prioritas)

# connect to database server
conn_string = "host='192.168.56.107' dbname='dev_musrenbang' user='postgres' password='untuk_Sid14!'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)

# connect to beanstalkd server
bean = beanstalkc.Connection(host='127.0.0.1', port=40116)
bean.use('musrenbang-priority')
bean.watch('musrenbang-priority')

#put jobs in queue, test only
bean.put("updateprioritykelurahan-208-28-2")

# get jobs from queue
print "Priority Changer background service started..."

while True:
        job = bean.reserve()
        data = job.body.split("-")
        print "Getting Job...."
        try:
                print "Usulan ID: "+data[1]
                print "Kelurahan ID: "+data[2]
                print "Usulan Prioritas: "+data[3]

                setPriority(data[2], data[1], data[3])
                job.delete()
        except:
                print "Error ketika menerima request: "+job.body
                job.delete()
